/** @constructor */
function net_northfuse_cf2js_dom_raw_RawConsole() {}
var net_northfuse_cf2js_dom_raw_RawConsole_$__$$$init$$$__$_$$V = function (arg0) {
/*implement constructor!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("net_northfuse_cf2js_dom_raw_RawConsole_$__$$$init$$$__$_$$V"));
throw ex;
};
var net_northfuse_cf2js_dom_raw_RawConsole_$_debug_$_$Ljava_lang_String$$$_$V = (function () {
	function (arg0) {
		/*implement method!*/
		/*


		 */
//must have a return of type void
		if (arg0 === null) {
			console.debug(null);
		} else {
			console.debug(arg0.str);
		}
	}
})();
var net_northfuse_cf2js_dom_raw_RawConsole_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var net_northfuse_cf2js_dom_raw_RawConsole_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var net_northfuse_cf2js_dom_raw_RawConsole_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var net_northfuse_cf2js_dom_raw_RawConsole_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var net_northfuse_cf2js_dom_raw_RawConsole_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var net_northfuse_cf2js_dom_raw_RawConsole_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var net_northfuse_cf2js_dom_raw_RawConsole_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var net_northfuse_cf2js_dom_raw_RawConsole_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var net_northfuse_cf2js_dom_raw_RawConsole_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
net_northfuse_cf2js_dom_raw_RawConsole.prototype._$$$init$$$__$_$$V = net_northfuse_cf2js_dom_raw_RawConsole_$__$$$init$$$__$_$$V;
net_northfuse_cf2js_dom_raw_RawConsole.debug_$_$Ljava_lang_String$$$_$V = net_northfuse_cf2js_dom_raw_RawConsole_$_debug_$_$Ljava_lang_String$$$_$V;
net_northfuse_cf2js_dom_raw_RawConsole.prototype.wait_$_$J$V = net_northfuse_cf2js_dom_raw_RawConsole_$_wait_$_$J$V;
net_northfuse_cf2js_dom_raw_RawConsole.prototype.wait_$_$JI$V = net_northfuse_cf2js_dom_raw_RawConsole_$_wait_$_$JI$V;
net_northfuse_cf2js_dom_raw_RawConsole.prototype.wait_$_$$V = net_northfuse_cf2js_dom_raw_RawConsole_$_wait_$_$$V;
net_northfuse_cf2js_dom_raw_RawConsole.prototype.equals_$_$Ljava_lang_Object$$$_$Z = net_northfuse_cf2js_dom_raw_RawConsole_$_equals_$_$Ljava_lang_Object$$$_$Z;
net_northfuse_cf2js_dom_raw_RawConsole.prototype.toString_$_$$Ljava_lang_String$$$_ = net_northfuse_cf2js_dom_raw_RawConsole_$_toString_$_$$Ljava_lang_String$$$_;
net_northfuse_cf2js_dom_raw_RawConsole.prototype.hashCode_$_$$I = net_northfuse_cf2js_dom_raw_RawConsole_$_hashCode_$_$$I;
net_northfuse_cf2js_dom_raw_RawConsole.prototype.getClass_$_$$Ljava_lang_Class$$$_ = net_northfuse_cf2js_dom_raw_RawConsole_$_getClass_$_$$Ljava_lang_Class$$$_;
net_northfuse_cf2js_dom_raw_RawConsole.prototype.notify_$_$$V = net_northfuse_cf2js_dom_raw_RawConsole_$_notify_$_$$V;
net_northfuse_cf2js_dom_raw_RawConsole.prototype.notifyAll_$_$$V = net_northfuse_cf2js_dom_raw_RawConsole_$_notifyAll_$_$$V;
