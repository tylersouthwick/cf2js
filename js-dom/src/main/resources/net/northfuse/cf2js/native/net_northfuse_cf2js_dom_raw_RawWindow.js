/** @constructor */
function net_northfuse_cf2js_dom_raw_RawWindow() {}
function net_northfuse_cf2js_dom_raw_RawWindow_stringUtil(s) {
	if (s == null) {
		return null;
	} else {
		return s.str;
	}
}
var net_northfuse_cf2js_dom_raw_RawWindow_$__$$$init$$$__$_$$V = function (arg0) {
/*implement constructor!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("net_northfuse_cf2js_dom_raw_RawWindow_$__$$$init$$$__$_$$V"));
throw ex;
};
var net_northfuse_cf2js_dom_raw_RawWindow_$_alert_$_$Ljava_lang_String$$$_$V = function (arg0) {
/*implement method!*/
/*


*/
//must have a return of type void
	alert(net_northfuse_cf2js_dom_raw_RawWindow_stringUtil(arg0));
};
var net_northfuse_cf2js_dom_raw_RawWindow_$_confirm_$_$Ljava_lang_String$$$_$Z = function (arg0) {
/*implement method!*/
/*


*/
//must have a return of type boolean
	return confirm(net_northfuse_cf2js_dom_raw_RawWindow_stringUtil(arg0));
};
var net_northfuse_cf2js_dom_raw_RawWindow_$_prompt_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*


*/
	var message = net_northfuse_cf2js_dom_raw_RawWindow_stringUtil(arg0);
	var defaultValue = net_northfuse_cf2js_dom_raw_RawWindow_stringUtil(arg1);
//must have a return of type String
	return prompt(message, defaultValue);
};
var net_northfuse_cf2js_dom_raw_RawWindow_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var net_northfuse_cf2js_dom_raw_RawWindow_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var net_northfuse_cf2js_dom_raw_RawWindow_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var net_northfuse_cf2js_dom_raw_RawWindow_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var net_northfuse_cf2js_dom_raw_RawWindow_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var net_northfuse_cf2js_dom_raw_RawWindow_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var net_northfuse_cf2js_dom_raw_RawWindow_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var net_northfuse_cf2js_dom_raw_RawWindow_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var net_northfuse_cf2js_dom_raw_RawWindow_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
net_northfuse_cf2js_dom_raw_RawWindow.prototype._$$$init$$$__$_$$V = net_northfuse_cf2js_dom_raw_RawWindow_$__$$$init$$$__$_$$V;
net_northfuse_cf2js_dom_raw_RawWindow.alert_$_$Ljava_lang_String$$$_$V = net_northfuse_cf2js_dom_raw_RawWindow_$_alert_$_$Ljava_lang_String$$$_$V;
net_northfuse_cf2js_dom_raw_RawWindow.confirm_$_$Ljava_lang_String$$$_$Z = net_northfuse_cf2js_dom_raw_RawWindow_$_confirm_$_$Ljava_lang_String$$$_$Z;
net_northfuse_cf2js_dom_raw_RawWindow.prompt_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = net_northfuse_cf2js_dom_raw_RawWindow_$_prompt_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_;
net_northfuse_cf2js_dom_raw_RawWindow.prototype.wait_$_$J$V = net_northfuse_cf2js_dom_raw_RawWindow_$_wait_$_$J$V;
net_northfuse_cf2js_dom_raw_RawWindow.prototype.wait_$_$JI$V = net_northfuse_cf2js_dom_raw_RawWindow_$_wait_$_$JI$V;
net_northfuse_cf2js_dom_raw_RawWindow.prototype.wait_$_$$V = net_northfuse_cf2js_dom_raw_RawWindow_$_wait_$_$$V;
net_northfuse_cf2js_dom_raw_RawWindow.prototype.equals_$_$Ljava_lang_Object$$$_$Z = net_northfuse_cf2js_dom_raw_RawWindow_$_equals_$_$Ljava_lang_Object$$$_$Z;
net_northfuse_cf2js_dom_raw_RawWindow.prototype.toString_$_$$Ljava_lang_String$$$_ = net_northfuse_cf2js_dom_raw_RawWindow_$_toString_$_$$Ljava_lang_String$$$_;
net_northfuse_cf2js_dom_raw_RawWindow.prototype.hashCode_$_$$I = net_northfuse_cf2js_dom_raw_RawWindow_$_hashCode_$_$$I;
net_northfuse_cf2js_dom_raw_RawWindow.prototype.getClass_$_$$Ljava_lang_Class$$$_ = net_northfuse_cf2js_dom_raw_RawWindow_$_getClass_$_$$Ljava_lang_Class$$$_;
net_northfuse_cf2js_dom_raw_RawWindow.prototype.notify_$_$$V = net_northfuse_cf2js_dom_raw_RawWindow_$_notify_$_$$V;
net_northfuse_cf2js_dom_raw_RawWindow.prototype.notifyAll_$_$$V = net_northfuse_cf2js_dom_raw_RawWindow_$_notifyAll_$_$$V;
