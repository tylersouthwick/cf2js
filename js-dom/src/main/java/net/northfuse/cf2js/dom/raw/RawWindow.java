package net.northfuse.cf2js.dom.raw;

/**
 * @author tylers2
 */
public class RawWindow {
	
	public static void alert(String message) {
		throw new UnsupportedOperationException("only implemented in js");
	}
	
	public static boolean confirm(String message) {
		throw new UnsupportedOperationException("only implemented in js");
	}

	public static String prompt(String message, String defaultText) {
		throw new UnsupportedOperationException("only implemented in js");
	}
}
