package net.northfuse.cf2js.dom.raw;

/**
 * @author tylers2
 */
public class RawConsole {

	public static void debug(String message) {
		throw new UnsupportedOperationException("implemented only in js");
	}
}
