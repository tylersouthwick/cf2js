package net.northfuse.cf2js.dom

/**
 * @author tylers2
 */

object Console {

	def debug(message : String) {
		raw.RawConsole.debug(message)
	}
}
