package net.northfuse.cf2js.dom

/**
 * @author Tyler Southwick
 */
object Window {

	def alert(message : String) {
		raw.RawWindow.alert(message)
	}

	def confirm(message : String) : Boolean = {
		raw.RawWindow.confirm(message)
	}

	def prompt(message : String, defaultMessage : String = "") : Option[String] = {
		val result = raw.RawWindow.prompt(message, defaultMessage)
		if (result == null) {
			None
		} else {
			Some(result)
		}
	}
}
