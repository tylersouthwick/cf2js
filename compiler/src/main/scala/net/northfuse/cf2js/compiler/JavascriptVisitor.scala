package net.northfuse.cf2js.compiler

import org.objectweb.asm._
import java.lang.String

import collection.mutable.ListBuffer
import java.io.PrintWriter
import org.slf4j.LoggerFactory

class JavascriptVisitor() extends ClassVisitor {
	private var className: String = null
	private var superName: String = null
	private var interfaces: Array[String] = null
	private var methods = new ListBuffer[JavascriptMethodVisitor]

	import collection.JavaConversions._
	def referencedClasses = methods.foldLeft(Seq[String]()) { (set, method) =>
		set ++ method.referencedClasses
	}

	import JavascriptVisitor.{LOG, EMPTY_VISITOR}

	def visitEnd() {}

	def visitMethod(access: Int, name: String, desc: String, signature: String, exceptions: Array[String]) = {
		LOG.debug("visiting method: " + name + " -> " + access)
		val visitor = new JavascriptMethodVisitor(new MethodDetails(className, name, desc, signature, access))
		methods += visitor
		visitor
	}

	def visitField(access: Int, name: String, desc: String, signature: String, defaultValue: AnyRef) = {
		//println("visiting field: " + name)
		EMPTY_VISITOR
	}

	def visitInnerClass(p1: String, p2: String, p3: String, p4: Int) {}

	def visitAttribute(p1: Attribute) {}

	def visitAnnotation(p1: String, p2: Boolean) = EMPTY_VISITOR

	def visitOuterClass(p1: String, p2: String, p3: String) {}

	def visitSource(p1: String, p2: String) {}

	def visit(version: Int, access: Int, name: String, signature: String, superName: String, interfaces: Array[String]) {
		this.className = name
		this.superName = superName
		this.interfaces = interfaces
	}

	def getClassWriter = new JavascriptClassWriter(className, superName, interfaces, methods.map(_.getWriter))
}

import org.objectweb.asm.commons._

object JavascriptVisitor {
	val EMPTY_VISITOR = new EmptyVisitor
	val LOG = LoggerFactory.getLogger(classOf[JavascriptVisitor])
}
