package net.northfuse.cf2js.compiler

import org.objectweb.asm.Label

/**
 * @author Tyler Southwick
 */
case class ExceptionHandler(start : Label, end : Label, target: Label, exceptionType2 : String) {

	/**
	 * Decides if the given label is within the start (inclusive) and end (exclusive) of the exception block.
	 *
	 * @param methodLabels All the labels of the method, in order
	 * @param label the Label to see if it is between the start/end
	 *
	 * @return true if the label is handled by this handler
	 */
	def contains(methodLabels : Seq[Label], label : Label) = {
		//could use a foldLeft here, but this seems (right now) to be more obvious
		var valid = false
		val labels = methodLabels.filter{methodLabel => {
			if (methodLabel == start) {
				//inclusive
				valid = true
			}
			if (methodLabel == end) {
				//exclusive
				valid = false
			}
			valid
		}}
		labels.contains(label)
	}

	def handler = target.toString
	def exceptionType = {
		if (exceptionType2 == null)
			null
		else
			NameMangler.mangleClassName(exceptionType2)
	}
}