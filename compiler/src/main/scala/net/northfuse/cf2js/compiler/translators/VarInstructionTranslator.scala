package net.northfuse.cf2js.compiler.translators

import org.objectweb.asm.Opcodes._
import java.util.HashSet
import net.northfuse.cf2js.compiler.translators.TranslatorOperandStack._

/**
 * @author Tyler Southwick
 */
trait VarInstructionTranslator extends OpCodeTranslator with VariableLoader {

	def apply(opcode : Int, index : Int) = opcode match {
		case ILOAD => load(index)
		case LLOAD => load(index)
		case FLOAD => load(index)
		case DLOAD => load(index)
		case ALOAD => load(index)
		case ISTORE => store(index)
		case LSTORE => store(index)
		case FSTORE => store(index)
		case DSTORE => store(index)
		case ASTORE => store(index)
			
		case RET => "" //not tested... don't know what to do
		case _ => throw new OpCodeNotImplementedException(opcode)
	}
}

trait VariableLoader {

	def popValue : String

	def load(index : Int) : String

	def getValue(index : Int) : String

	def storeValue(index : Int, value : String) : String
	final def store(index : Int) = storeValue(index, popValue)
	final def storeArgument(localIndex : Int, argIndex : Int) = storeValue(localIndex, "arg" + argIndex)

	def clearVariables() : String
}

trait ArrayVariableLoader extends VariableLoader with TranslatorOperandStack {
	import VariableLoader.LOCAL_VARIABLE_ARRAY_NAME

	def buildVariableName(index : Int) = LOCAL_VARIABLE_ARRAY_NAME + "[" + index + "]"
	def load(index : Int) = pushValue(buildVariableName(index))


	def getValue(index: Int) = buildVariableName(index)

	def storeValue(index : Int,  value : String) = buildVariableName(index) + " = " + value

	def clearVariables() = "/*cleared variables*/"
}

trait VarVariableLoader extends VariableLoader with TranslatorOperandStack {
	import VariableLoader.LOCAL_VARIABLE_ARRAY_NAME

	private val variables = new java.util.HashSet[Int]()
	def buildVariableName(index : Int) = LOCAL_VARIABLE_ARRAY_NAME + "_" + index
	def load(index : Int) = pushValue(buildVariableName(index))

	def storeValue(index : Int, value : String) = {
		val contains = !variables.add(index)
		val prefix = if (contains) "" else "var "
		prefix + buildVariableName(index) + " = " + value
	}

	def getValue(index : Int) = buildVariableName((index))

	def clearVariables() = {
		variables.clear()
		"/*cleared variables*/"
	}
}

object VariableLoader {
	val LOCAL_VARIABLE_ARRAY_NAME = "localVariables"
}
