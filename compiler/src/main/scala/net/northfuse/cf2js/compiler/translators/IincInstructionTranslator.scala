package net.northfuse.cf2js.compiler.translators

/**
 * @author tylers2
 */
trait IincInstructionTranslator extends VariableLoader {

	def apply(index : Int, increment : Int) = getValue(index) + " += " + increment
}
