package net.northfuse.cf2js.compiler

import java.lang.reflect.{Constructor, Method, Modifier}
import io.{Codec, Source}
import java.io._
import translators.LdcInstructionTranslator

/**
 * @author tylers2
 */
class NativeImplementation(name : String) {

	import NativeImplementation.LOG
	val klass = java.lang.Class.forName(name)

	def exists = {
		LOG.debug("checking for native implementation: " + name)
		val nativeImplementation = "/net/northfuse/cf2js/native/" + name.replaceAllLiterally(".", "_") + ".js"
		LOG.trace(nativeImplementation)
		val is = classOf[NativeImplementation].getResourceAsStream(nativeImplementation)
		if (is == null) {
			LOG.debug("No native implementation found")
			None
		} else {
			LOG.debug("Found a native implementation: " + name)
			//found it
			//load it
			val codec = Codec.UTF8
			val script = Source.fromInputStream(is)(codec).mkString
			validateNativeImplementation(script)
			LOG.trace(script)
			Some(script)
		}
	}

	val methods = klass.getDeclaredMethods.filter(method => !Modifier.isPrivate(method.getModifiers))

	def validateNativeImplementation(script : String) {
		/*
		for (method <- methods) {
			println(method)
		}
		*/
		//NativeImplementation.generateStub(name)
		//TODO validate native implementation against class definition
	}

	implicit object OrderNativeMethods extends Ordering[NativeMethodWriter] {
		def compare(x: NativeImplementation.this.type#NativeMethodWriter, y: NativeImplementation.this.type#NativeMethodWriter) = x.fullMethodName.compareTo(y.fullMethodName)
	}
	
	implicit object OrderConstructors extends Ordering[NativeConstructorWriter] {
		def compare(x: NativeImplementation.this.type#NativeConstructorWriter, y: NativeImplementation.this.type#NativeConstructorWriter) = x.fullMethodName.compareTo(y.fullMethodName)
	}

	private def generateStub(os : OutputStream) {
		val writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(os, "UTF-8")))
		val superClass = if (klass.getSuperclass == null) null else klass.getSuperclass.getName
		LOG.info("getting javadoc")
		val javadoc = JavadocDownloader(klass)
		LOG.info("got javadoc")
		val methodsToImplement = methods.map(new NativeMethodWriter(javadoc, _)).sorted
		val constructorsToImplement = klass.getConstructors.map(new NativeConstructorWriter(javadoc, _)).sorted
		val allMethods = constructorsToImplement ++ methodsToImplement
		val classWriter = new JavascriptClassWriter(name, superClass, klass.getInterfaces.map(_.getName), allMethods)
		classWriter.write(writer)
		writer.close()
	}

	trait NativeImplementationWriter extends MethodWriter with JavascriptWriter {
		def doc : MethodDescription
		def methodType : String
		def doWrite(out : PrintWriter) {
			out.println("/*implement " + methodType + "!*/")
			out.println("/*")
			out.println(doc.description)
			out.println("")
			if (!doc.parameters.isEmpty) {
				out.println("Parameters:")
				out.println("")
				for (parameter <- doc.parameters) {
					out.println(parameter._1 + " -> " + parameter._2)
					out.println("")
				}
			}
			out.println("*/")
			val string = LdcInstructionTranslator.createString(fullMethodName)
			out.println("var ex = new " + NameMangler.mangleClassName(classOf[UnsupportedOperationException]) + "();")
			out.println("ex." + NameMangler.mangleMethodName (name = "<init>", desc = "(Ljava/lang/String;)V") + "(ex, " + string + ");")
			out.println("throw ex;")
		}
	}
	
	class NativeConstructorWriter(javadoc : JavadocDownloader, constructor : Constructor[_]) extends NativeImplementationWriter {
		val methodDetails = new MethodDetails(name, constructor)

		val doc = javadoc.findConstructor(constructor)

		val methodType = "constructor"

	}

	class NativeMethodWriter(javadoc : JavadocDownloader, method : Method) extends NativeImplementationWriter {
		val methodDetails = new MethodDetails(name, method)

		val doc = javadoc.findMethod(method)

		val methodType = "method"

		override def doWrite(out: PrintWriter) = {
			super.doWrite(out)
			if (method.getReturnType != classOf[Void]) {
				out.println("//must have a return of type " + method.getReturnType.getSimpleName)
			}
		}
	}
}

object NativeImplementation{
	val LOG = org.slf4j.LoggerFactory.getLogger(classOf[NativeImplementation])

	def apply(name : String) = (new NativeImplementation(name)).exists
	
	private def doGenerateStub(name : String, os : OutputStream) {
		(new NativeImplementation(name)).generateStub(os)
	}
	
	def generateStub(name : String, basePath : String = "src/main/resources/net/northfuse/cf2js/native/") {
		val directory = new File(basePath)
		directory.mkdirs()
		val jsName = name.replaceAllLiterally(".", "_") + ".js"
		println("Generating " + jsName)
		val out = new FileOutputStream(basePath + jsName)
		doGenerateStub(name, out)
		out.close()
	}

	def generateStubInteraction() {
		println("Enter your classes to generate:")
		def doGenerateStub(name : String) {
			if (name.isEmpty) {
				System.exit(0)
			}
			try {
				generateStub(name)
			} catch {
				case cnfe : ClassNotFoundException => {
					println("Invalid Class: " + name)
				}
				case t : Throwable => throw t
			}
		}
		io.Source.stdin.getLines().foreach(doGenerateStub)
	}

	def main(args : Array[String]) {
		generateStubInteraction()
	}
}
