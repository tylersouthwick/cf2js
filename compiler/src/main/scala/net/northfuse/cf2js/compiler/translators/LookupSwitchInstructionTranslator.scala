package net.northfuse.cf2js.compiler.translators

import org.objectweb.asm.Label

/**
 * @author Tyler Southwick
 */
trait LookupSwitchInstructionTranslator extends TranslatorOperandStack {

	def apply(default: Label, keys: Array[Int], labels: Array[Label]) = {
		val statementStart = "switch(" + popValue + ") {"
		val items = keys.zip(labels)
		val statementBody = items.map{case (index, label) => "case " + index + ": " + LabelFunctionCreator.goto(label)}.mkString(";\n")
		val invokeDefaultTarget = "default: " + LabelFunctionCreator.goto(default)
		
		statementStart + statementBody + invokeDefaultTarget + "\n}"
	}
}
