package net.northfuse.cf2js.compiler.translators

import collection.mutable.ListBuffer

/**
 * @author Tyler Southwick
 */
trait ArrayCreatorTranslator extends OpCodeTranslator with TranslatorOperandStack {

	def createArray(arrayType : String) : String = pushValue(createArray(arrayType, popValue, "null"))

	def createArray(arrayType : String, size : String, initialValue : String) : String = "(function (count) {\n" +
			"if (count < 0) {\n" +
			"   " + throwException(classOf[NegativeArraySizeException]) + ";\n" +
			"} else {\n" +
			"   var array = " + initializeArray(size, initialValue) + ";\n" +
			"   array.length = count;\n" +
			"   return array;\n" +
			"}})(" + size + ")\n"
	
	def initializeArray(size : String, body : String) = {
		val list = ListBuffer[String]()
		list += "(function () {"
		list += "var array = [];"
		list += "var counter = 0;"
		list += "for (counter = 0; counter < " + size + "; counter += 1) {"
		list += "array[counter] = " + body + ";"
		list += "}"
		list += "return array;"
		list += "})()"
		list.mkString("\n")
	}
}
