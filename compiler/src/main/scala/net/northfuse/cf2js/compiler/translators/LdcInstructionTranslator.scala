package net.northfuse.cf2js.compiler.translators

import net.northfuse.cf2js.compiler.NameMangler

/**
 * @author tylers2
 */
trait LdcInstructionTranslator extends ConstTranslator {

	import LdcInstructionTranslator._
	def apply(cst : Any) = {
		const (cst match {
			case s : String => createString(s)
			case num : Number => num
			case _ => {
				LdcInstructionTranslator.LOG.warn("unknown ldc const: " + cst)
				cst
				""
			}
		})
	}
}

object LdcInstructionTranslator {
	val LOG = org.slf4j.LoggerFactory.getLogger(classOf[LdcInstructionTranslator])

	def createString(s : String) = "new " + NameMangler.mangleClassName(classOf[String]) + "(" + processString(s) + ")"

	def processString(s : String) = {
		val processedString = "\"" + s
			.replaceAllLiterally("""\""", """\\""")
			.replaceAllLiterally("\n", "\\\n")
			.replaceAllLiterally ("\"", "\\\"") +
			"\""
		//println(s + " => " + processedString)
		/*
		val error = "\"" + """\\""" + "\"\""
		if (processedString == error) {
			throw new IllegalStateException()
		}
		*/
		processedString
	}
}
