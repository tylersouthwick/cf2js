package net.northfuse.cf2js.compiler.translators

import org.objectweb.asm.Opcodes._
import net.northfuse.cf2js.compiler.{NameMangler, MethodDetails}
import org.objectweb.asm.Type

/**
 * @author Tyler Southwick
 */
trait MethodInstructionTranslator extends TranslatorOperandStack with OpCodeTranslator {

	def apply(opcode: Int, owner: String, name: String, desc: String) = pushIfNotUndefined(opcode match {
		case INVOKESTATIC => invokeStatic(owner, name, desc)
		case INVOKEINTERFACE => invokeVirtual(owner, name, desc)
		case INVOKESPECIAL => invokeSpecial(owner, name, desc)
		case INVOKEVIRTUAL => invokeVirtual(owner, name, desc)
		case _ => throw new OpCodeNotImplementedException(opcode)
	})
	
	def pushIfNotUndefined(command : String) = "(function () {\nvar result = (function () {\nreturn " + command + "\n})(); if (result != undefined)\n{" + pushValue("result") + "}})()"

	def invokeStatic(owner: String, name: String, desc: String) = {
		val method = NameMangler.mangleClassName(owner) + "." + NameMangler.mangleMethodName(name = name, desc = desc)
		val types = Type.getArgumentTypes(desc).toSeq
		val poppedArgs = types.map(x => popValue).mkString(", ")
		val functionArgs = (0 until types.length).map("arg" + _)

		val methodCall = new StringBuilder()
		methodCall.append(method)
		methodCall.append("(")
		methodCall.append(functionArgs.mkString(", "))
		methodCall.append(")")

		"(function (" + functionArgs.reverse.mkString(", ") + ") {return " + methodCall.toString() + ";})(" + poppedArgs + ")"
	}

	def invokeVirtual(owner: String, name: String, desc: String) = {
		val method = NameMangler.mangleMethodName(name = name, desc = desc)
		val types = Type.getArgumentTypes(desc).toSeq
		val poppedArgs = types.map(x => popValue)
		val args  = (0 until types.length).map(index => "arg" + (index + 1))

		val methodCall = new StringBuilder()
		methodCall.append("instance.")
		methodCall.append(method)
		methodCall.append("(")
		methodCall.append((Seq("instance") ++ args).mkString(", "))
		methodCall.append(")")

		val functionArgs = (poppedArgs ++ Seq(popValue)).mkString(", ")
		val functionParams = (args.reverse ++ Seq("instance")).mkString(", ")
		"(function (" + functionParams  + ") {" +
			"if (instance === null) " + throwException(classOf[NullPointerException]) + "\n" +
				//"println(\"arg1: \" + arg1);println(\"arg2: \" + arg2);println(\"instance: \" + instance);" +
				"return " + methodCall.toString() + ";})(" + functionArgs + ")"
	}

	def invokeSpecial(owner: String, name: String, desc: String) = {
		val method = NameMangler.mangleMethodName(owner = owner, name = name, desc = desc)
		val types = Type.getArgumentTypes(desc).toSeq
		val poppedArgs = types.map(x => popValue)
		val args = (0 until types.length).map(index => "arg" + (index + 1))

		val methodCall = new StringBuilder()
		methodCall.append(method)
		methodCall.append("(")
		methodCall.append((Seq("instance") ++ args).mkString(", "))
		methodCall.append(")")

		val functionArgs = (poppedArgs ++ Seq(popValue)).mkString(", ")
		val functionParams = (args.reverse ++ Seq("instance")).mkString(", ")

		"(function (" + functionParams + ") {" +
				//"println(\"arg1: \" + arg1);println(\"arg2: \" + arg2);println(\"instance: \" + instance);" +
				"return " + methodCall.toString() + ";})(" + functionArgs + ")"
	}
}