package net.northfuse.cf2js.compiler.translators

import org.objectweb.asm.Opcodes._
import net.northfuse.cf2js.compiler.NameMangler

/**
 * @author Tyler Southwick
 */
trait FieldInstructionTranslator extends TranslatorOperandStack {

	def apply(opcode : Int,  owner : String, name : String) = opcode match {
		case GETFIELD => getField(mangleFieldName(name))
		case PUTFIELD => putField(mangleFieldName(name))
		case GETSTATIC => getStatic(owner, mangleFieldName(name))
		case PUTSTATIC => putStatic(owner, mangleFieldName(name))
		case _ => throw new OpCodeNotImplementedException(opcode)
	}

	def mangleFieldName(fieldName : String) = "_" + fieldName;
	def getField(name : String) = pushValue("(function (reference) {" +
			"   return reference." + name + ";" +
			"})(" + popValue + ")")

	def putField(name : String) = "(function (value, reference) {" +
			"   reference." + name + " = value;" +
			"})(" + popValue + ", " + popValue + ")"

	def getStatic(klass : String,  name : String) = pushValue("(function () {" +
			"   return " + NameMangler.mangleClassName(klass) + "." + name + ";" +
			"})()")

	def putStatic(klass : String,  name : String) = "(function (value) {" +
			NameMangler.mangleClassName(klass) + "." + name + " = value;" +
			"})(" + popValue + ")"
}