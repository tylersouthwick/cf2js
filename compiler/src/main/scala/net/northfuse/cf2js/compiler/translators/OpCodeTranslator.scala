package net.northfuse.cf2js.compiler.translators

import org.objectweb.asm.Opcodes
import net.northfuse.cf2js.compiler.NameMangler
import org.slf4j.LoggerFactory

trait OpCodeTranslator extends TranslatorOperandStack {

	final def `return`(value : String) = "return " + value

	final def throwException(exception : Class[_ <: Throwable]) : String = throwException("new " + NameMangler.mangleClassName(exception) + "()")
	final def throwException(exceptionReference : String) : String = "throw " + exceptionReference
}

trait TranslatorOperandStack {
	def pushValue(value : Any) : String
	def popValue : String

	def clearStack : String
}

object TranslatorOperandStack {
	val LOG = LoggerFactory.getLogger(classOf[TranslatorOperandStack])
}

trait TranslatorOperandStackArray extends TranslatorOperandStack {
	final def pushValue(value: Any) = "stack.push(" + value + ")"

	final def popValue = "stack.pop()"

	final def clearStack = "stack = [];"
}

trait TranslatorOperandStackVariables extends TranslatorOperandStack {
	import TranslatorOperandStackVariables._
	import TranslatorOperandStack.LOG

	final def pushValue(value: Any) = {
		val m = "var " + varPrefix + push() + " = " + value
		LOG.debug("push: " + m)
		m
	}

	final def popValue = {
		val m = varPrefix + pop()
		LOG.debug("pop: " + m)
		m
	}

	final def clearStack = {
		LOG.debug("clearing stack")
		clear()
		"/*clear stack*/"
	}
}

object TranslatorOperandStackVariables {
	import actors.threadpool.AtomicInteger
	import java.util.{Stack => JStack}
	val LOG = LoggerFactory.getLogger(classOf[TranslatorOperandStackVariables])
	
	val varPrefix = "stack_"
	private val data = new ThreadLocal[(JStack[Int], AtomicInteger)] {
		override def initialValue() = (new JStack[Int], new AtomicInteger)
	}

	private def counter = data.get()._2
	private def stack = data.get()._1
	def push() = {
		val i = counter.incrementAndGet()
		stack.push(i)
		i
	}

	def pop() = stack.pop() /*{
		if (stack.isEmpty) "NOSTACK" else stack.pop()
	}*/

	def clear() {
		LOG.debug("clearing stack")
		data.remove()
	}
}

class OpCodeNotImplementedException(opcode : Int) extends RuntimeException("OpCode not implemented: " + OpCodeNotImplementedException.findOpcode(opcode))

object OpCodeNotImplementedException {
	import java.lang.reflect.Modifier._
	def findOpcode(opcode : Int) : String = {
		classOf[Opcodes].getDeclaredFields.filter{field => 
			val modifier = field.getModifiers
			isStatic(modifier) && isPublic(modifier) && isFinal(modifier)
		}.filter(!_.getName.startsWith("T_")).find(_.get(null) == opcode) match {
			case Some(field) => field.getName
			case None => "[UNKNWON OPCODE: " + opcode + "]"
		}
	}
}
