package net.northfuse.cf2js.compiler.translators

import org.objectweb.asm.Label

/**
 * @author Tyler Southwick
 */
trait TableSwitchInstructionTranslator extends TranslatorOperandStack {

	def apply(min: Int, max: Int, default: Label, labels: Array[Label]) = """
		return (function (index, min, max, defaultCallback, callbacks) {
			if (index < min || index > max) {
				return defaultCallback();
			} else {
				return callbacks[index - min]();
			}
		})(""" + 
			popValue + ", " + min + ", " + max + ", " +
			LabelFunctionCreator(default) + ", " +
			TableSwitchInstructionTranslator.buildArray(labels.map(LabelFunctionCreator.buildFunctionName)) + ");"
}

object TableSwitchInstructionTranslator {
	def buildArray(labels : Array[String]) = "[" + labels.mkString(", ") + "]"
}
