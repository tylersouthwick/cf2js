package net.northfuse.cf2js.compiler

import NameMangler._

import java.io.PrintWriter
import java.lang.reflect.Method

/**
 */
class JavascriptClassWriter(className: String, superClass: String, interfaces: Array[String], implementedMethods: Seq[MethodWriter]) extends JavascriptWriter {

	val mangledName = mangleClassName(className)
	
	val inheritedMethods = {
		val klass = Class.forName(className.replaceAllLiterally("/", "."))
		/*
		System.out.println("methods: " + klass)
		klass.getMethods.map("declaring: " + _.getDeclaringClass.getName + " [" + className + "]").map(System.out.println)
		*/
		val inheritedMethods : Seq[Method] = klass.getMethods.filter(!_.getDeclaringClass.getName.equals(klass.getName))
		/*
		System.out.println("inheritedMethods: " + inheritedMethods)
		inheritedMethods.map("inherited: " + _.getDeclaringClass.getName + " [" + className + "]").map(System.out.println)
		*/
		inheritedMethods.map(method => new JavascriptInheritedMethodWriter(className, superClass, method))
	}

	/*
	System.out.println("inheritedMethods writers: " + inheritedMethods.map(_.fullMethodName))
	*/
	val methods = implementedMethods ++ inheritedMethods
	
	def write(out : PrintWriter) {
		writeClassDefinition(out)
		writeFunctions(out)
		writeFunctionPrototypes(out)
	}

	def writeClassDefinition(out : PrintWriter) {
		out.println("/** @constructor */")
		out.println("function " + mangledName + "() {}")
	}

	def writeFunctions(out : PrintWriter) {
		for (method <- methods) {
			out.print("var " + method.fullMethodName + " = ")
			method.write(out)
		}
	}

	def writeFunctionPrototypes(out : PrintWriter) {
		for (method <- methods) {
			val methodName = mangledName + {
				if (method.isStatic) {
					"."
				} else {
					".prototype."
				}
			} + method.methodName
			
			out.print(methodName + " = ")
			out.print(method.fullMethodName)
			out.println(";")
		}
	}

}

object JavascriptClassWriter {
	val LOG = org.slf4j.LoggerFactory.getLogger(classOf[JavascriptClassWriter])
}
