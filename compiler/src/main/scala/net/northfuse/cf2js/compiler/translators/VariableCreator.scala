package net.northfuse.cf2js.compiler.translators

import actors.threadpool.AtomicInteger

/**
 * @author Tyler Southwick
 */
object VariableCreator {
	private val ai = new AtomicInteger(1)
	
	def apply() = create

	def create = "temp" + ai.getAndIncrement
}
