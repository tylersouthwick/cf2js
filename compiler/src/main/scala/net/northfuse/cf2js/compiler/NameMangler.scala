package net.northfuse.cf2js.compiler

import org.objectweb.asm.{Opcodes, Type}
import java.lang.reflect.{Constructor, Method}


/**
 * @author Tyler Southwick
 */
object NameMangler {

	def mangleClassName(className: String): String = className.replaceAllLiterally("/", "_").replaceAllLiterally(".", "_")

	def mangleClassName(`class` : Class[_]): String = mangleClassName(`class`.getName)

	def mangleClassName(`type` : Type): String = mangleClassName(`type`.getClassName)

	def mangleMethodName(methodDetails: MethodDetails): String = mangleMethodName(name = methodDetails.methodName, desc = methodDetails.desc)

	def mangleMethodName(methodName: String): String = methodName

	private val ai = new java.util.concurrent.atomic.AtomicInteger

	def randomString = {
		val uuid = java.util.UUID.randomUUID.toString.hashCode
		ai.getAndIncrement + "_" + {
			if (uuid < 0) "_" + (-1 * uuid) else uuid
		}
	}

	def mangleMethodName(owner : String = "", name : String, desc : String = "()V") : String = {
		val sb = new StringBuilder
		if (!owner.isEmpty) {
			sb.append(owner.replaceAllLiterally("/", "_").replaceAllLiterally(".", "_"))
			sb.append("_$_")
		}
		sb.append(name match {
			case "<init>" => "_$$$init$$$_"
			case "<clinit>" => "_$$$clinit$$$_"
			case _ => name
		})
		sb.append("_$_")
		sb.append(desc.replaceAllLiterally("(", "$")
					.replaceAllLiterally(")", "$")
					.replaceAllLiterally("/", "_")
					.replaceAllLiterally("[", "_$$$$$_")
					.replaceAllLiterally(";", "$$$_"))
		sb.toString()
	}
}

class MethodDetails(val className: String, val name: String, val desc : String, val signature: String, val access : Int) {
	def isStatic = checkAccess(Opcodes.ACC_STATIC)

	def this(className : String, method : Method) {
		this(className.replaceAllLiterally(".", "/"), method.getName, Type.getMethodDescriptor(method), Type.getMethodDescriptor(method), method.getModifiers)
	}

	def this(className : String, method : Constructor[_]) {
		this(className.replaceAllLiterally(".", "/"), "<init>", Type.getConstructorDescriptor(method), Type.getConstructorDescriptor(method), method.getModifiers)
	}

	private def checkAccess(access : Int) = (this.access & access) == access

	val methodName = name
	val args = Type.getArgumentTypes(desc)
	def argCount = args.length + (if (isStatic) 0 else 1)
}
