package net.northfuse.cf2js.compiler.translators

import org.objectweb.asm.{Opcodes, Label}
/**
 * @author tylers2
 */
trait JumpInstructionTranslator extends TranslatorOperandStack {

	import LabelFunctionCreator.goto
	import Opcodes._

	val EQ = "=="
	val NE = "!="
	val LT = "<"
	val LE = "<="
	val GT = ">"
	val GE = ">="

	def apply(opcode : Int, label : Label) = {
		val ifStatementBuilder = ifcond(label) _
		val cmpStatementBuilder = cmpcond(label) _
		opcode match {
			case IFEQ => ifStatementBuilder(EQ)
			case IFNE => ifStatementBuilder(NE)
			case IFLT => ifStatementBuilder(LT)
			case IFLE => ifStatementBuilder(LE)
			case IFGT => ifStatementBuilder(GT)
			case IFGE => ifStatementBuilder(GE)
			case IF_ICMPEQ => cmpStatementBuilder(EQ)
			case IF_ICMPNE => cmpStatementBuilder(NE)
			case IF_ICMPLT => cmpStatementBuilder(LT)
			case IF_ICMPLE => cmpStatementBuilder(LE)
			case IF_ICMPGT => cmpStatementBuilder(GT)
			case IF_ICMPGE => cmpStatementBuilder(GE)
			case IF_ACMPEQ => cmpStatementBuilder(EQ)
			case IF_ACMPNE => cmpStatementBuilder(NE)
			case IFNONNULL => nullabilityCheck(label, "!")
			case IFNULL => nullabilityCheck(label, "")
			case GOTO => goto(label)
			case JSR => LabelFunctionCreator(label) + "();"
			case _ => throw new OpCodeNotImplementedException(opcode)
		}
	}

	def nullabilityCheck(label : Label, equality : String) = "if (" + popValue + " " + equality + "== null) {" + goto(label) + "}"
	def ifcond(label : Label)(equality : String) = "if (" + popValue + " " + equality + " 0) {" + goto(label) + "}"
	def cmpcond(label : Label)(equality : String) = {
		val value2Name = VariableCreator()
		val value1Name = VariableCreator()

		val value2Statement = "var " + value2Name + " = " + popValue
		val value1Statement = "var " + value1Name + " = " + popValue
		val ifStatement = "if (" + value1Name + " " + equality + " " + value2Name + ") {" + goto(label) + "}"
		val statements = Seq(value2Statement, value1Statement, ifStatement)
		statements.mkString(";\n")
	}
}

object LabelFunctionCreator {
	def buildFunctionName(label : Label) = label.toString
	def apply(label : Label) = buildFunctionName(label)
	def goto(label : Label) = "return " + buildFunctionName(label) + "();"
}
