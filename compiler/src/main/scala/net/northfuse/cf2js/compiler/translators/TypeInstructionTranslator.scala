package net.northfuse.cf2js.compiler.translators

import net.northfuse.cf2js.compiler.NameMangler
import org.objectweb.asm.Opcodes._

/**
 * @author Tyler Southwick
 */
trait TypeInstructionTranslator extends OpCodeTranslator with ArrayCreatorTranslator {

	def apply(opcode: Int, `type`: String) = opcode match {
		case NEW => createNew(`type`)
		case ANEWARRAY => createArray(`type`)
		case NEWARRAY => createArray(`type`)
		case CHECKCAST => "" //this is currently not tested....
		case INSTANCEOF => pushValue("true") //this is currently not tested....
		case _ => throw new OpCodeNotImplementedException(opcode)
	}

	def createNew(className: String) = pushValue("new " + NameMangler.mangleClassName(className) + "()")
}
