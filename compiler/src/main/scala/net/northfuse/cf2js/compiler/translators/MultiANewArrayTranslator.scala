package net.northfuse.cf2js.compiler.translators

import org.objectweb.asm.Type
import collection.mutable.ListBuffer

/**
 * @author tylers2
 */

trait MultiANewArrayTranslator extends TranslatorOperandStack with ArrayCreatorTranslator {

	import MultiANewArrayTranslator.LOG

	def buildMultiArray(desc : String, seq: List[String]) : String = seq match {
		case head :: Nil => createArray(desc, head, "null")
		case reference :: tail => {
			val array = buildMultiArray(desc, tail)
			createArray(desc, reference, array)
		}
		case Nil => throw new RuntimeException("shouldn't get here")
	}

	def apply(desc : String, dims : Int) = {
		LOG.info("desc : dims -> " + Type.getObjectType(desc) + " : " + dims)
		
		val list = ListBuffer[String]()
		val dimensions = (1 to dims).map({x =>
			val reference = VariableCreator.create
			reference
		})
		
		list ++= dimensions.map {"var " + _ + " = " + popValue + ";"}

		val array = VariableCreator.create
		list += "var " + array + " = " + buildMultiArray(desc, dimensions.reverse.toList)
		list += pushValue(array)

		list.mkString("\n")
	}
}

object MultiANewArrayTranslator {
	val LOG = org.slf4j.LoggerFactory.getLogger(classOf[MultiANewArrayTranslator])
}
