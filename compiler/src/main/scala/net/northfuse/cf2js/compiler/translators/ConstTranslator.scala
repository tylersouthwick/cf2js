package net.northfuse.cf2js.compiler.translators

/**
 * @author Tyler Southwick
 */
trait ConstTranslator extends  TranslatorOperandStack {

	def const(value : Any) = pushValue(value)
}