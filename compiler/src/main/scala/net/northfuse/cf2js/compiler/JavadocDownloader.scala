package net.northfuse.cf2js.compiler

import java.net.URL
import xml.{Elem, NodeSeq, Node, Source}
import java.lang.reflect.{Constructor, Method}
import java.io.FileNotFoundException

/**
 * @author tylers2
 */

class JavadocDownloader(xml: NodeSeq) {

	val body = {
		val bodies = (xml \\ "body")
		if (bodies.length == 0) {
			NodeSeq.Empty
		} else {
			bodies(0).child
		}
	}

	implicit def convert(ns: NodeSeq) = new {
		def children = ns match {
			case e: Elem => e.child
			case _ => NodeSeq.Empty
		}
	}

	def findMethod(method: Method) = findDoc(method.getName, method.getParameterTypes, false)

	def findConstructor(constructor : Constructor[_]) = findDoc(constructor.getDeclaringClass.getSimpleName, constructor.getParameterTypes, true)

	def convertType(klass : Class[_]) = {
		if (klass.isArray) {
			if (klass.getComponentType.isPrimitive) {
				klass.getSimpleName
			} else {
				klass.getName
			}
		} else {
			if (klass.isPrimitive) {
				klass.getSimpleName
			} else {
				klass.getName
			}
		}
	}

	private def findDoc(name : String, parameterTypes : Seq[Class[_]], debug : Boolean) = {
		val types = parameterTypes.map(convertType) mkString(", ")
		val sig = name + "(" + types + ")"
		/*
		if (debug) {
			println(sig)
		}
		*/
		var allow = false
		val nodes = body.filter {
			node => {
				if ("a" == node.label) {
					node.attributes.find(_.key == "name") match {
						case Some(n) => {
							val value = n.value.toString()
							if (value == sig) {
								allow = true
							} else {
								allow = false
							}
							allow
						}
						case None => false
					}
				}
				allow
			}
		}

		//println(nodes)
		val dl = {
			val temp = nodes.filter(_.label == "dl")
			if (temp.isEmpty) {
				NodeSeq.Empty
			} else {
				temp(0).child.filter(_.label == "dd")
			}
		}
		def findText(name: String) = {
			var allow = false
			val nodesToSearch: Seq[Node] = {
				if (dl.length > 2) {
					val search = (dl(2) \\ "dl")
					if (search.isEmpty) {
						NodeSeq.Empty
					} else {
						search(0).child.filter(!_.isEmpty)
					}
				} else {
					NodeSeq.Empty
				}
			}
			//println("nodesToSearch -> " + nodesToSearch + " [" + nodesToSearch.getClass + "]")
			val nodes = nodesToSearch.filter {
				node => {
					if ("dt" == node.label) {
						//println("text: " + node.text)
						if (node.text.toUpperCase.trim == name.toUpperCase + ":") {
							allow = true
						} else {
							allow = false
						}
					}
					allow
				}
			}
			//println("found: " + nodes)
			//the first node is the header, so get the rest
			if (nodes.length > 1) {
				nodes.splitAt(1)._2
			} else {
				NodeSeq.Empty
			}
		}
		new MethodDescription {
			val description = {
				if (dl.isEmpty) {
					""
				} else {
					dl(0).text.trim()
				}
			}
			val parameters = findText("parameters")
					.map(_.text) // get the text of the node - stirps out all html
					.map {
				t => t.splitAt(t.indexOf(" - "))
			} //parameters are listed as 'PARAMETER_NAME - PARMETER_DESCRIPTION'
					.map {
				case (paramName, paramDescription) => (paramName, paramDescription.substring(2).trim)
			} //trim off the ' - ' of the description

			val returns = findText("returns").text
			val throws = findText("throws").map {
				n => {
					val exceptionPackageText = (n \\ "a" \\ "@title").text
					val exceptionPackage = {
						if (exceptionPackageText == null || !exceptionPackageText.startsWith("class in ")) {
							""
						} else {
							exceptionPackageText.substring("class in ".length)
						}
					}
					val line = n.text
					val index = line.indexOf("-")
					val exception = exceptionPackage + "." + line.substring(0, index)
					val when = line.substring(index + 2)
					(exception.trim, when.trim)
				}
			}
		}
	}
}

trait MethodDescription {
	def description: String

	def parameters: Seq[(String, String)]

	def returns: String

	def throws: Seq[(String, String)]

	def toStringData = Seq(("description", description), ("parameters", parameters), ("returns", returns), ("throws", throws))

	override def toString = "{" + toStringData.map(t => t._1 + "->" + t._2).mkString(",") + "}"
}

object JavadocDownloader {
	val baseUrl = "http://docs.oracle.com/javase/6/docs/api/"

	val parser = new HTML5Parser

	def apply(klass: Class[_]) = {
		val name = klass.getName.replaceAllLiterally(".", "/")
		val url = new URL(baseUrl + name + ".html")
		try {
			val inputSource = Source.fromInputStream(url.openStream())
			val xml = parser.loadXML(source = inputSource)
			new JavadocDownloader(xml)
		} catch {
			//TODO if the javadoc is not found, create a Javadoc Object that has information from reflection (parameter names/types)
			case fne : FileNotFoundException => new JavadocDownloader(NodeSeq.Empty)
		}
	}

	def main(args: Array[String]) {
		val klass = classOf[String]
		val finder = this(klass)

		val data = finder.findMethod(klass.getMethods.filter(_.getName == "charAt").apply(0))
		println(data)
	}

	import org.xml.sax.InputSource
	import scala.xml._
	import parsing._

	class HTML5Parser extends NoBindingFactoryAdapter {
		override def loadXML(source: InputSource, _p: SAXParser) = {
			loadXML(source)
		}

		def loadXML(source: InputSource) = {
			import nu.validator.htmlparser._
			import sax.HtmlParser
			import common.XmlViolationPolicy

			val reader = new HtmlParser
			reader.setXmlnsPolicy(XmlViolationPolicy.ALLOW)
			reader.setContentHandler(this)
			reader.parse(source)
			rootElem
		}
	}

}
