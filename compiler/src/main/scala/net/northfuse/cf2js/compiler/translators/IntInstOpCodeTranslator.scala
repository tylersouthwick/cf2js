package net.northfuse.cf2js.compiler.translators

import org.objectweb.asm.Opcodes._

/**
 * @author Tyler Southwick
 */
trait IntInstOpCodeTranslator extends OpCodeTranslator with ArrayCreatorTranslator with ConstTranslator{

	def apply(opcode: Int, operand: Int) = opcode match {
		case BIPUSH => const(operand)
		case SIPUSH => const(operand)
		case NEWARRAY => createArray(operand match {
			case T_BOOLEAN => "boolean"
			case T_CHAR => "char"
			case T_FLOAT => "float"
			case T_DOUBLE => "double"
			case T_BYTE => "byte"
			case T_SHORT => "short"
			case T_INT => "int"
			case T_LONG => "long"
			case _ => throw new IllegalArgumentException("Invalid operand for NEWARRAY: " + operand)
		})
		case _ => throw new OpCodeNotImplementedException(opcode)
	}

}
