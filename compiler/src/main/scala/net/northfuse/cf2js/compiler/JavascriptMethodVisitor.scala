package net.northfuse.cf2js.compiler

import java.lang.String


import translators._

import org.slf4j.LoggerFactory
import collection.mutable.{ListBuffer, LinkedHashMap}
import org.objectweb.asm.{Type, Attribute, MethodVisitor, Label}

class JavascriptMethodVisitor(methodDetails: MethodDetails) extends MethodVisitor {
	var commands : ListBuffer[String] = null
	val labels = new LinkedHashMap[Label, ListBuffer[String]]()
	val referencedClasses = new java.util.TreeSet[String]()
	val exceptionHandlers = ListBuffer[ExceptionHandler]()
	import JavascriptMethodVisitor.LOG

	def ++(command: String) {
		LOG.trace("++(" + command + ")")
		if (commands == null) {
			commands = new ListBuffer[String]()
		}
		commands += command
	}
	
	def startLabel(label : Label) {
		LOG.trace("starting label: " + label)
		commands = ListBuffer[String]()
		labels.put(label, commands)
	}

	def getWriter = new JavascriptMethodWriter(methodDetails, labels.toSeq, exceptionHandlers.toSeq)

	def visitAnnotationDefault() = JavascriptVisitor.EMPTY_VISITOR

	def visitAnnotation(p1: String, p2: Boolean) = JavascriptVisitor.EMPTY_VISITOR

	def visitParameterAnnotation(p1: Int, p2: String, p3: Boolean) = JavascriptVisitor.EMPTY_VISITOR

	def visitAttribute(p1: Attribute) {}

	def visitCode() {}

	def visitFrame(`type`: Int, nLocal: Int, local : Array[AnyRef], nStack: Int, stack: Array[AnyRef]) {
		if (LOG.isTraceEnabled) {
			LOG.trace("visitFrame")
			LOG.trace("\ttype: " + `type`)
			LOG.trace("\tnLocal: " + nLocal)
			LOG.trace("\tlocal: " + local)
			LOG.trace("\tnStack: " + nStack)
			LOG.trace("\tstack : " + stack)
		}
	}

	def visitInsn(opcode: Int) {
		++(InstOpCodeTranslatorVisitor(opcode))
	}

	def visitIntInsn(opcode: Int, operand: Int) {
		++(IntInstOpCodeTranslatorVisitor(opcode, operand))
	}

	def visitVarInsn(opcode: Int, index: Int) {
		++(VarInstructionTranslatorVisitor(opcode, index))
	}

	def visitTypeInsn(opcode: Int, `type`: String) {
		referencedClasses.add(`type`)
		++(TypeInstructionTranslatorVisitor(opcode, `type`))
	}

	def visitFieldInsn(opcode: Int, owner: String, name: String, desc: String) {
		if (LOG.isTraceEnabled) {
			val sb = new StringBuilder()
			sb.append("field -> ").append(opcode)
			sb.append("\towner: ").append(owner)
			sb.append("\tname: ").append(name)
			sb.append("\tdesc: ").append(desc)
			LOG.trace(sb.toString())
		}
		referencedClasses.add(owner)
		++(FieldInstructionTranslatorVisitor(opcode, owner, name))
	}

	def visitMethodInsn(opcode: Int, owner: String, name: String, desc: String) {
		referencedClasses.add(owner)
		++(MethodInstructionTranslatorVisitor(opcode, owner, name, desc))
	}

	def visitJumpInsn(opcode: Int, label: Label) {
		++ (JumpInstructionTranslatorVisitor.apply(opcode, label))
	}

	def visitLabel(label: Label) {
		startLabel(label)
		//++("create Label: " + label)
	}

	def visitLdcInsn(cst: AnyRef) {
		++(LdcInstructionTranslatorVisitor(cst))
	}

	def visitIincInsn(index: Int, increment: Int) {
		++(IincInstructionTranslatorVisitor(index, increment))
	}

	def visitTableSwitchInsn(min: Int, max: Int, default: Label, labels: Array[Label]) {
		++(TableSwitchInstructionTranslatorVisitor(min, max, default, labels))
	}

	def visitLookupSwitchInsn(default: Label, keys: Array[Int], labels: Array[Label]) {
		++(LookupSwitchInstructionTranslatorVisitor(default, keys, labels))
	}

	def visitMultiANewArrayInsn(desc: String, dims : Int) {
		++(MultiANewArrayTranslatorVisitor(desc, dims))
	}

	def visitTryCatchBlock(start: Label, end: Label, handler: Label, exceptionType : String) {
		exceptionHandlers += ExceptionHandler(start, end, handler, exceptionType)
	}

	def visitLocalVariable(name: String, desc: String, signature: String, start: Label, end: Label, index: Int) {
		/*
		println("visitLocalVariable")
		println("\tname: " + name)
		println("\tdesc: " + desc)
		println("\tsignature: " + signature)
		println("\tstart: " + start)
		println("\tend: " + end)
		println("\tindex: " + index)
		*/
	}

	def visitLineNumber(p1: Int, p2: Label) {}

	def visitMaxs(p1: Int, p2: Int) {}

	def visitEnd() {}
}

object JavascriptMethodVisitor {
	val LOG = LoggerFactory.getLogger(classOf[JavascriptMethodVisitor])
}

trait VisitorStack extends TranslatorOperandStackArray
trait VisitorLocalVariables extends ArrayVariableLoader

object InstOpCodeTranslatorVisitor extends InstOpCodeTranslator with VisitorStack with VisitorLocalVariables
object IntInstOpCodeTranslatorVisitor extends IntInstOpCodeTranslator with VisitorStack
object VarInstructionTranslatorVisitor extends VarInstructionTranslator with VisitorStack with VisitorLocalVariables
object TypeInstructionTranslatorVisitor extends TypeInstructionTranslator with VisitorStack
object FieldInstructionTranslatorVisitor extends FieldInstructionTranslator with VisitorStack
object MethodInstructionTranslatorVisitor extends MethodInstructionTranslator with VisitorStack
object JumpInstructionTranslatorVisitor extends JumpInstructionTranslator with VisitorStack
object IincInstructionTranslatorVisitor extends IincInstructionTranslator with VisitorStack with VisitorLocalVariables
object LdcInstructionTranslatorVisitor extends LdcInstructionTranslator with VisitorStack
object TableSwitchInstructionTranslatorVisitor extends TableSwitchInstructionTranslator with VisitorStack
object LookupSwitchInstructionTranslatorVisitor extends LookupSwitchInstructionTranslator with VisitorStack
object MultiANewArrayTranslatorVisitor extends MultiANewArrayTranslator with VisitorStack
