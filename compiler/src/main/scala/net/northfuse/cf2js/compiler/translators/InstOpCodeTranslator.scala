package net.northfuse.cf2js.compiler.translators

import org.objectweb.asm.Opcodes._
import org.slf4j.LoggerFactory

trait InstOpCodeTranslator extends OpCodeTranslator with ConstTranslator with VariableLoader {

	import InstOpCodeTranslator._
	def apply(opcode : Int) = {
		opcode match {
			case NOP => ""
			case ACONST_NULL => const("null")
			case ICONST_M1 => const(-1)
			case ICONST_0 => const(0)
			case ICONST_1 => const(1)
			case ICONST_2 => const(2)
			case ICONST_3 => const(3)
			case ICONST_4 => const(4)
			case ICONST_5 => const(5)
			case LCONST_0 => const(0)
			case LCONST_1 => const(1)
			case DCONST_0 => const(0)
			case DCONST_1 => const(1)
			case FCONST_0 => const(0)
			case FCONST_1 => const(1)
			case FCONST_2 => const(2)
			case IALOAD => arrayLoad()
			case LALOAD => arrayLoad()
			case FALOAD => arrayLoad()
			case DALOAD => arrayLoad()
			case AALOAD => arrayLoad()
			case BALOAD => arrayLoad()
			case CALOAD => arrayLoad()
			case SALOAD => arrayLoad()
			case IASTORE => arrayStore()
			case LASTORE => arrayStore()
			case FASTORE => arrayStore()
			case DASTORE => arrayStore()
			case AASTORE => arrayStore()
			case BASTORE => arrayStore()
			case CASTORE => arrayStore()
			case SASTORE => arrayStore()
			case POP => popValue
			case POP2 => popValue
			case DUP => dup()
			case DUP_X1 => dup_x1()
			case DUP_X2 => dup_x2()
			case DUP2 => dup()
			case DUP2_X1 => dup_x1()
			case DUP2_X2 => dup_x2()
			case SWAP => swap()
			case IADD => add()
			case LADD => add()
			case FADD => add()
			case DADD => add()
			case ISUB => sub()
			case LSUB => sub()
			case FSUB => sub()
			case DSUB => sub()
			case IMUL => mul()
			case LMUL => mul()
			case DMUL => mul()
			case FMUL => mul()
			case IDIV => idiv()
			case LDIV => idiv()
			case FDIV => div()
			case DDIV => div()
			case IREM => rem()
			case LREM => rem()
			case FREM => rem()
			case DREM => rem()
			case INEG => neg()
			case LNEG => neg()
			case FNEG => neg()
			case DNEG => neg()
			case ISHL => shl()
			case LSHL => shl()
			case ISHR => shr()
			case LSHR => shr()
			case IUSHR => ushr()
			case LUSHR => ushr()
			case IAND => and()
			case LAND => and()
			case IOR => or()
			case LOR => or()
			case IXOR => xor()
			case LXOR => xor()

			case I2L => widenValue()
			case I2D => widenValue()
			case I2F => widenValue()
			case I2B => i2b()
			case I2S => i2s()
			case I2C => i2c()

			case L2I => l2i()
			case L2F => ""
			case L2D => ""

			case F2I => anything2int()
			case F2L => anything2int()
			case F2D => ""

			case D2F => ""
			case D2I => anything2int()
			case D2L => anything2int()

			case LCMP => lcmp()

			case FCMPG => fd_cmp(1)
			case FCMPL => fd_cmp(-1)
			case DCMPG => fd_cmp(1)
			case DCMPL => fd_cmp(-1)

			case ARETURN => returnValue()
			case IRETURN => returnValue()
			case LRETURN => returnValue()
			case DRETURN => returnValue()
			case FRETURN => returnValue()
			case RETURN => returnNoValue()

			case ARRAYLENGTH => arraylength()

			case ATHROW => athrow()

			case MONITORENTER => "" //we don't do anything here - there is no threading in js
			case MONITOREXIT => "" //we don't do anything here - there is no threading in js
			case _ => throw new OpCodeNotImplementedException(opcode)
		}
	}

	def sub() = pushValue(" - (" + popValue + ") + " + popValue)

	def add() = pushValue(popValue + " + " + popValue)

	def mul() = pushValue(popValue + " * " + popValue)
	def div() = pushValue("(function(divisor, dividend) {" +
		"if (divisor === 0) " + throwException(classOf[ArithmeticException]) + ";" +
		"return dividend / divisor;" +
	      	"})(" + popValue + ", " + popValue + ")")
	def idiv() = pushValue("(function(divisor, dividend) {" +
			"if (divisor === 0) " + throwException(classOf[ArithmeticException]) + ";" +
			"return Math.floor(dividend / divisor);" +
			"})(" + popValue + ", " + popValue + ")")
	def rem() = pushValue("(function(divisor, dividend) {" +
		"if (divisor === 0) " + throwException(classOf[ArithmeticException]) + ";" +
		"return dividend % divisor;" +
	      	"})(" + popValue + ", " + popValue + ")")
	def neg() = pushValue("-(" + popValue + ")")

	def shr() = pushValue("(function (b, a) {return a >> b;})(" + popValue + ", " + popValue + ")")
	def shl() = pushValue("(function (b, a) {return a << b;})(" + popValue + ", " + popValue + ")")

	def ushr() = pushValue("(function (b, a) {return a >>> b;})(" + popValue + ", " + popValue + ")")

	def and() = pushValue("(function (b, a) {return a & b;})(" + popValue + ", " + popValue + ")")
	def or() = pushValue("(function (b, a) {return a | b;})(" + popValue + ", " + popValue + ")")
	def xor() = pushValue("(function (b, a) {return a ^ b;})(" + popValue + ", " + popValue + ")")

	def arrayLoad() = pushValue(
		"(function (index, array) {" +
				"if (!array) {" + throwException(classOf[NullPointerException]) + ";}" +
				"else if (!(index >= 0 && index < array.length)) {" + throwException(classOf[ArrayIndexOutOfBoundsException]) + ";}" +
				"else {" +
				"return array[index];" +
				"}})(" + popValue + ", " + popValue + ")"
	)

	def arrayStore() =
		"(function (newValue, index, array) {" +
				"if (!array) {" + throwException(classOf[NullPointerException]) + ";}" +
				"else if (!(index >= 0 && index < array.length)) {" + throwException(classOf[ArrayIndexOutOfBoundsException]) + ";}" +
				"else {" +
				"array[index] = newValue;" +
				"}})(" + popValue + ", " + popValue + ", " + popValue + ")"

	def dup() = "(function (value) {" + pushValue("value") + ";" + pushValue("value") + "})(" + popValue + ")"

	def dup_x1() = "(function (value1, value2) {" + pushValue("value1") + ";" + pushValue("value2") + "; " + pushValue("value1") + "})(" + popValue + ", " + popValue + ")"

	def dup_x2() = "(function (value1, value2, value3) {" + pushValue("value1") + ";" + pushValue("value3") + ";" + pushValue("value2") + "; " + pushValue("value1") + "})(" + popValue + ", " + popValue + ", " + popValue + ")"

	def swap() = "(function (value1, value2) {" + pushValue("value1") + ";" + pushValue("value2") + "})(" + popValue + ", " + popValue + ")"

	def i2b() = pushValue(popValue + " & 0x000000FF")
	def i2c() = i2s()
	def i2s() = pushValue(popValue + " & 0x0000FFFF")
	def i2i() = pushValue(popValue + " & 0x0000FFFF")
	def l2i() = ""
	def anything2int() = pushValue("parseInt(" + popValue + ")")

	//widening instructions don't need to do anything
	def widenValue() = ""

	def lcmp() = pushValue("(function (value2, value1) {" +
		"if (value1 > value2) return 1;" +
		"if (value1 < value2) return -1;" +
		"return 0;" +
	  "})(" + popValue + ", " + popValue + ")")

	def fd_cmp(valueIfNaN : Int) = pushValue("(function (value2, value1) {" +
		"if (isNaN(value1) || isNaN(value2)) return " + valueIfNaN + ";" +
		"if (value1 > value2) return 1;" +
		"if (value1 < value2) return -1;" +
		"return 0;" +
		"})(" + popValue + ", " + popValue + ")")

	def returnValue() = {
		LOG.debug("return value")
		val poppedValue = popValue
		`return`("(function (value) {" +
				clearStack +
				clearVariables +
				"return value;" +
				"})(" + poppedValue + ")")
	}
	
	def returnNoValue() = {
		LOG.debug("return no value")
		`return`("(function () {" +
				clearStack +
				clearVariables +
				"return undefined;" +
				"})()")
	}

	def arraylength() = pushValue("(function (array) {" +
			"if (array === null) " + throwException(classOf[NullPointerException]) + ";" +
			"else return array.length;" +
			"})(" + popValue + ")")

	def athrow() = "(function (exception) { " +
			"if (exception === null) {" + throwException(classOf[NullPointerException]) + "}" +
			throwException("exception") + "})(" + popValue + ")"
}

object InstOpCodeTranslator {
	val LOG = LoggerFactory.getLogger(classOf[InstOpCodeTranslator])
}

