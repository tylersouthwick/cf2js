package net.northfuse.cf2js.compiler

import org.objectweb.asm._
import java.io.PrintWriter
import org.slf4j.LoggerFactory
import io.Source

object Compiler {
	val LOG = LoggerFactory.getLogger(classOf[Compiler])

	def validClass(origClassName : String) = {
		/*
		val availableJavaClasses = Seq("java.lang.Object", "java.lang.String")
		val className = cleanupClassName(origClassName)
		LOG.trace("validClass: " + className)
		if (className.startsWith("java.") || className.startsWith("javax.") || className.startsWith("sun.")) {
			availableJavaClasses.contains(className)
		} else true
		*/
		true
	}

	val baseClasses = Seq(
		classOf[String],
		classOf[NullPointerException],
		classOf[IllegalStateException],
		classOf[UnsupportedOperationException],
		classOf[CloneNotSupportedException],
		classOf[IndexOutOfBoundsException],
		classOf[NumberFormatException],
		classOf[IllegalArgumentException]
	).map(_.getName)

	def compile(name: String, methodName : String, out: PrintWriter) {
		LOG.info("classloader: "+ ClassLoader.getSystemClassLoader + " [" + classOf[NativeImplementation].getClassLoader + "]")
		LOG.info("Compiling " + name + ":" + methodName)
		copyFile("/net/northfuse/cf2js/native/exceptionHandling.js", out)

		val visited = new java.util.TreeSet[String]()
		val toVisit = new java.util.TreeSet[String]()
		for (baseClass <- baseClasses) {
			doCompile(baseClass, out, visited, toVisit, 0)
		}
		doCompile(name, out, visited, toVisit, 0)
	}
	
	private def doCompile(name : String, out : PrintWriter, visited : java.util.Set[String], toVisit : java.util.Set[String], level : Int) {
		if (!name.startsWith("[") && visited.add(name)) {
			for (nextClass <- compileClass(name, out, level).map(cleanupClassName).filter(validClass)) {
				doCompile(nextClass, out, visited, toVisit, level + 1)
			}
		}
	}

	private def cleanupClassName(name : String) = name.replaceAllLiterally("/", ".")

	private def compileClass(name : String, out : PrintWriter, level : Int) = {
		LOG.info((0 to level).map(x => " ").mkString("") + "Compiling " + name + " (" + level + ")")
		NativeImplementation(name) match {
			case Some(nativeImplementation) => {
				LOG.trace("using native implementation")
				out.println(nativeImplementation)
				out.flush()
				Seq()
			}
			case None => doCompileClass(name, out)
		}
	}
	
	private def doCompileClass(name : String,  out : PrintWriter) = {
		val in = classOf[NativeImplementation].getClassLoader.getResourceAsStream(name.replace('.', '/') + ".class")
		val reader = new ClassReader(in)
		val visitor = new JavascriptVisitor()

		reader.accept(visitor, 0)

		visitor.getClassWriter.write(out)
		out.flush()

		visitor.referencedClasses
	}
	
	private def copyFile(name : String, out : PrintWriter) {
		val source = Source.fromInputStream(classOf[ExceptionHandler].getResourceAsStream(name))
		out.println(source.mkString)
	}
}
