package net.northfuse.cf2js.compiler

import java.io.PrintWriter
import org.objectweb.asm.Label

import translators.VariableLoader
import java.lang.reflect.Method

trait MethodWriter extends JavascriptWriter {
	def methodDetails : MethodDetails
	final def methodName = NameMangler.mangleMethodName(name = methodDetails.methodName, desc = methodDetails.desc)
	final def fullMethodName = NameMangler.mangleMethodName(owner = methodDetails.className, name = methodDetails.methodName, desc = methodDetails.desc)

	final def write(out : PrintWriter) {
		out.print("function (")
		out.print((0 until methodDetails.argCount).map("arg" + _).mkString(", "))
		out.println(") {")
		doWrite(out)
		out.println("};")
	}
	def doWrite(out : PrintWriter)
	final def isStatic = methodDetails.isStatic
}

class JavascriptInheritedMethodWriter(val className : String, val parent : String, val method : Method) extends JavascriptWriter with MethodWriter {
	val methodDetails = new MethodDetails(className, method)

	final def parentMethodName = NameMangler.mangleMethodName(owner = parent, name = methodDetails.methodName, desc = methodDetails.desc)
	
	def doWrite(out : PrintWriter) {
		out.println("/*invoke parent*/")
		out.print("return ")
		out.print(parentMethodName)
		out.print("(")
		out.print((0 until methodDetails.argCount).map("arg" + _).mkString(", "))
		out.print(");")
	}
}
/**
 */
class JavascriptMethodWriter(val methodDetails: MethodDetails, labels: Seq[(Label, Seq[String])], exceptionHandlers : Seq[ExceptionHandler]) extends JavascriptWriter with MethodWriter {

	def doWrite(out : PrintWriter) {
		out.println("var stack = [];")
		out.println("var " + VariableLoader.LOCAL_VARIABLE_ARRAY_NAME + " = [];");
		for (index <- 0 until methodDetails.argCount) {
			out.println(VarInstructionTranslatorVisitor.storeArgument(index, index) + ";")
		}

		for (entry <- labels.zipWithIndex) {
			val ((label, commands), index) = entry
			out.println("var " + label + " = function () {")
			//find exceptions
			val exceptions = exceptionHandlers.filter(_.contains(labels.map(_._1), label))
			if (!exceptions.isEmpty) {
				out.println("try {")
			}
			for (command <- commands) {
				out.println(command + ";")
			}
			if (!exceptions.isEmpty) {
				out.println("} catch (ex) {")
				out.println("handleException(ex, [" + exceptions.map(ex => "[" + ex.exceptionType + ", " + ex.handler + "]").mkString(",") + "]);")
				out.println("return; //this shouldn't return - either goes to the handle or rethrows the exception")
				out.println("}")
			}
			if (index < labels.size - 1) {
				//call the next label
				out.println("/*call the next label*/");
				val nextLabel : Label = labels(index + 1)._1
				out.println("return " + nextLabel + "();")
			}
			out.println("};")
		}
		
		labels.headOption match {
			case Some(head) => {
				out.println("/*call the first label*/")
				val firstLabel : Label = head._1
				out.println("return " + firstLabel + "()")
			}
			case None =>
		}
	}
}

object JavascriptMethodWriter {
	def randomFunctionName = "function_" + NameMangler.randomString
}
