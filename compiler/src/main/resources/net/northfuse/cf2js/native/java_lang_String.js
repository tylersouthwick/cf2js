/** @constructor */
function java_lang_String(str) {
    this.str = str;
}
function java_lang_String_nullCheck(arg, callback) {
	if (arg === null) {
		var ex = new java_lang_NullPointerException();
		ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("arg cannot be null"));
		throw ex;
	} else {
		return callback();
	}
}

var java_lang_String_$__$$$init$$$__$_$$V = function (arg0) {
/*implement constructor!*/
/*
Initializes a newly created String object so that it represents
 an empty character sequence.  Note that use of this constructor is
 unnecessary since Strings are immutable.

*/
    arg0.str = "";
};
var java_lang_String_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Initializes a newly created String object so that it represents
 the same sequence of characters as the argument; in other words, the
 newly created string is a copy of the argument string. Unless an
 explicit copy of original is needed, use of this constructor is
 unnecessary since Strings are immutable.


Parameters:original - A String

*/
    arg0.str = arg1.str;
};
var java_lang_String_$__$$$init$$$__$_$Ljava_lang_StringBuffer$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Allocates a new string that contains the sequence of characters
 currently contained in the string buffer argument. The contents of the
 string buffer are copied; subsequent modification of the string buffer
 does not affect the newly created string.


Parameters:buffer - A StringBuffer

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$Ljava_lang_StringBuffer$$$_$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$Ljava_lang_StringBuilder$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Allocates a new string that contains the sequence of characters
 currently contained in the string builder argument. The contents of the
 string builder are copied; subsequent modification of the string builder
 does not affect the newly created string.

  This constructor is provided to ease migration to StringBuilder. Obtaining a string from a string builder via the toString method is likely to run faster and is generally preferred.


Parameters:builder - A StringBuilderSince:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$Ljava_lang_StringBuilder$$$_$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$_$$$$$_B$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a new String by decoding the specified array of bytes
 using the platform's default charset.  The length of the new String is a function of the charset, and hence may not be equal to the
 length of the byte array.

  The behavior of this constructor when the given bytes are not valid
 in the default charset is unspecified.  The CharsetDecoder class should be used when more control
 over the decoding process is required.


Parameters:bytes - The bytes to be decoded into charactersSince:
  JDK1.1

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$_$$$$$_B$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$_$$$$$_BI$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Deprecated. This method does not properly convert bytes into
 characters.  As of JDK 1.1, the preferred way to do this is via the
 String constructors that take a Charset, charset name, or that use the platform's
 default charset.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$_$$$$$_BI$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$_$$$$$_BII$V = function (arg0, arg1, arg2, arg3) {
/*implement constructor!*/
/*
Constructs a new String by decoding the specified subarray of
 bytes using the platform's default charset.  The length of the new
 String is a function of the charset, and hence may not be equal
 to the length of the subarray.

  The behavior of this constructor when the given bytes are not valid
 in the default charset is unspecified.  The CharsetDecoder class should be used when more control
 over the decoding process is required.


Parameters:bytes - The bytes to be decoded into charactersoffset - The index of the first byte to decodelength - The number of bytes to decode
Throws:
IndexOutOfBoundsException - If the offset and the length arguments index
          characters outside the bounds of the bytes arraySince:
  JDK1.1

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$_$$$$$_BII$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$_$$$$$_BIII$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement constructor!*/
/*
Deprecated. This method does not properly convert bytes into characters.
 As of JDK 1.1, the preferred way to do this is via the
 String constructors that take a Charset, charset name, or that use the platform's
 default charset.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$_$$$$$_BIII$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$_$$$$$_BIILjava_lang_String$$$_$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement constructor!*/
/*
Constructs a new String by decoding the specified subarray of
 bytes using the specified charset.  The length of the new String
 is a function of the charset, and hence may not be equal to the length
 of the subarray.

  The behavior of this constructor when the given bytes are not valid
 in the given charset is unspecified.  The CharsetDecoder class should be used when more control
 over the decoding process is required.


Parameters:bytes - The bytes to be decoded into charactersoffset - The index of the first byte to decodelength - The number of bytes to decodecharsetName - The name of a supported charset
Throws:
UnsupportedEncodingException - If the named charset is not supported
IndexOutOfBoundsException - If the offset and length arguments index
          characters outside the bounds of the bytes arraySince:
  JDK1.1

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$_$$$$$_BIILjava_lang_String$$$_$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$_$$$$$_BIILjava_nio_charset_Charset$$$_$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement constructor!*/
/*
Constructs a new String by decoding the specified subarray of
 bytes using the specified charset.
 The length of the new String is a function of the charset, and
 hence may not be equal to the length of the subarray.

  This method always replaces malformed-input and unmappable-character
 sequences with this charset's default replacement string.  The CharsetDecoder class should be used when more control
 over the decoding process is required.


Parameters:bytes - The bytes to be decoded into charactersoffset - The index of the first byte to decodelength - The number of bytes to decodecharset - The charset to be used to
         decode the bytes
Throws:
IndexOutOfBoundsException - If the offset and length arguments index
          characters outside the bounds of the bytes arraySince:
  1.6

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$_$$$$$_BIILjava_nio_charset_Charset$$$_$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$_$$$$$_BLjava_lang_String$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Constructs a new String by decoding the specified array of bytes
 using the specified charset.  The
 length of the new String is a function of the charset, and hence
 may not be equal to the length of the byte array.

  The behavior of this constructor when the given bytes are not valid
 in the given charset is unspecified.  The CharsetDecoder class should be used when more control
 over the decoding process is required.


Parameters:bytes - The bytes to be decoded into characterscharsetName - The name of a supported charset
Throws:
UnsupportedEncodingException - If the named charset is not supportedSince:
  JDK1.1

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$_$$$$$_BLjava_lang_String$$$_$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$_$$$$$_BLjava_nio_charset_Charset$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Constructs a new String by decoding the specified array of
 bytes using the specified charset.
 The length of the new String is a function of the charset, and
 hence may not be equal to the length of the byte array.

  This method always replaces malformed-input and unmappable-character
 sequences with this charset's default replacement string.  The CharsetDecoder class should be used when more control
 over the decoding process is required.


Parameters:bytes - The bytes to be decoded into characterscharset - The charset to be used to
         decode the bytesSince:
  1.6

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$_$$$$$_BLjava_nio_charset_Charset$$$_$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$_$$$$$_C$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Allocates a new String so that it represents the sequence of
 characters currently contained in the character array argument. The
 contents of the character array are copied; subsequent modification of
 the character array does not affect the newly created string.


Parameters:value - The initial value of the string

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$_$$$$$_C$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$_$$$$$_CII$V = function (arg0, arg1, arg2, arg3) {
/*implement constructor!*/
/*
Allocates a new String that contains characters from a subarray
 of the character array argument. The offset argument is the
 index of the first character of the subarray and the count
 argument specifies the length of the subarray. The contents of the
 subarray are copied; subsequent modification of the character array does
 not affect the newly created string.


Parameters:value - Array that is the source of charactersoffset - The initial offsetcount - The length
Throws:
IndexOutOfBoundsException - If the offset and count arguments index
          characters outside the bounds of the value array

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$_$$$$$_CII$V"));
throw ex;
};
var java_lang_String_$__$$$init$$$__$_$_$$$$$_III$V = function (arg0, arg1, arg2, arg3) {
/*implement constructor!*/
/*
Allocates a new String that contains characters from a subarray
 of the Unicode code point array argument. The offset argument
 is the index of the first code point of the subarray and the
 count argument specifies the length of the subarray. The
 contents of the subarray are converted to chars; subsequent
 modification of the int array does not affect the newly created
 string.


Parameters:codePoints - Array that is the source of Unicode code pointsoffset - The initial offsetcount - The length
Throws:
IllegalArgumentException - If any invalid Unicode code point is found in codePoints
IndexOutOfBoundsException - If the offset and count arguments index
          characters outside the bounds of the codePoints arraySince:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$__$$$init$$$__$_$_$$$$$_III$V"));
throw ex;
};
var java_lang_String_$_charAt_$_$I$C = function (arg0, arg1) {
/*implement method!*/
/*
Returns the char value at the
 specified index. An index ranges from 0 to
 length() - 1. The first char value of the sequence
 is at index 0, the next at index 1,
 and so on, as for array indexing.

 If the char value specified by the index is a
 surrogate, the surrogate
 value is returned.

Parameters:

index -> the index of the char value.

*/
//must have a return of type char
    if (arg1 < 0 || arg1 >= arg0.str.length) {
        var ex = new java_lang_IndexOutOfBoundsException();
        ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("invalid index: " + arg1 + " -> length: " + arg0.str.length));
        throw ex;
    } else {
        return arg0.str.charCodeAt(arg1);
    }
};
var java_lang_String_$_codePointAt_$_$I$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the character (Unicode code point) at the specified
 index. The index refers to char values
 (Unicode code units) and ranges from 0 to
 length() - 1.

  If the char value specified at the given index
 is in the high-surrogate range, the following index is less
 than the length of this String, and the
 char value at the following index is in the
 low-surrogate range, then the supplementary code point
 corresponding to this surrogate pair is returned. Otherwise,
 the char value at the given index is returned.

Parameters:

index -> the index to the char values

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_codePointAt_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_String_$_codePointBefore_$_$I$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the character (Unicode code point) before the specified
 index. The index refers to char values
 (Unicode code units) and ranges from 1 to length.

  If the char value at (index - 1)
 is in the low-surrogate range, (index - 2) is not
 negative, and the char value at (index -
 2) is in the high-surrogate range, then the
 supplementary code point value of the surrogate pair is
 returned. If the char value at index -
 1 is an unpaired low-surrogate or a high-surrogate, the
 surrogate value is returned.

Parameters:

index -> the index following the code point that should be returned

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_codePointBefore_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_String_$_codePointCount_$_$II$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the number of Unicode code points in the specified text
 range of this String. The text range begins at the
 specified beginIndex and extends to the
 char at index endIndex - 1. Thus the
 length (in chars) of the text range is
 endIndex-beginIndex. Unpaired surrogates within
 the text range count as one code point each.

Parameters:

beginIndex -> the index to the first char of
 the text range.

endIndex -> the index after the last char of
 the text range.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_codePointCount_$_$II$I"));
throw ex;
//must have a return of type int
};
var java_lang_String_$_compareToIgnoreCase_$_$Ljava_lang_String$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Compares two strings lexicographically, ignoring case
 differences. This method returns an integer whose sign is that of
 calling compareTo with normalized versions of the strings
 where case differences have been eliminated by calling
 Character.toLowerCase(Character.toUpperCase(character)) on
 each character.
 
 Note that this method does not take locale into account,
 and will result in an unsatisfactory ordering for certain locales.
 The java.text package provides collators to allow
 locale-sensitive ordering.

Parameters:

str -> the String to be compared.

*/
//must have a return of type int
	return java_lang_String_nullCheck(arg1, function () {
		var str1 = arg0.str.toUpperCase();
		var str2 = arg1.str.toUpperCase();
		if (str1 === str2) {
			return 0;
		} else if (str1 < str2) {
			return -1;
		} else {
			return 1;
		}
	});
};
var java_lang_String_$_compareTo_$_$Ljava_lang_Object$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_compareTo_$_$Ljava_lang_Object$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_String_$_compareTo_$_$Ljava_lang_String$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Compares two strings lexicographically.
 The comparison is based on the Unicode value of each character in
 the strings. The character sequence represented by this
 String object is compared lexicographically to the
 character sequence represented by the argument string. The result is
 a negative integer if this String object
 lexicographically precedes the argument string. The result is a
 positive integer if this String object lexicographically
 follows the argument string. The result is zero if the strings
 are equal; compareTo returns 0 exactly when
 the equals(Object) method would return true.
 
 This is the definition of lexicographic ordering. If two strings are
 different, then either they have different characters at some index
 that is a valid index for both strings, or their lengths are different,
 or both. If they have different characters at one or more index
 positions, let k be the smallest such index; then the string
 whose character at position k has the smaller value, as
 determined by using the < operator, lexicographically precedes the
 other string. In this case, compareTo returns the
 difference of the two character values at position k in
 the two string -- that is, the value:
  this.charAt(k)-anotherString.charAt(k)
 
 If there is no index position at which they differ, then the shorter
 string lexicographically precedes the longer string. In this case,
 compareTo returns the difference of the lengths of the
 strings -- that is, the value:
  this.length()-anotherString.length()

Parameters:

anotherString -> the String to be compared.

*/
//must have a return of type int
	return java_lang_String_nullCheck(arg1, function () {
		var str1 = arg0.str;
		var str2 = arg1.str;
		if (str1 === str2) {
			return 0;
		} else if (str1 < str2) {
			return -1;
		} else {
			return 1;
		}
	});
};
var java_lang_String_$_concat_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Concatenates the specified string to the end of this string.
 
 If the length of the argument string is 0, then this
 String object is returned. Otherwise, a new
 String object is created, representing a character
 sequence that is the concatenation of the character sequence
 represented by this String object and the character
 sequence represented by the argument string.
 Examples:
  "cares".concat("s") returns "caress"
 "to".concat("get").concat("her") returns "together"

Parameters:

str -> the String that is concatenated to the end
                of this String.

*/
//must have a return of type String
	return java_lang_String_nullCheck(arg1, function () {
        return new java_lang_String(arg0.str + arg1.str);
	});
};
var java_lang_String_$_contains_$_$Ljava_lang_CharSequence$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Returns true if and only if this string contains the specified
 sequence of char values.

Parameters:

s -> the sequence to search for

*/
//must have a return of type boolean
	return java_lang_String_nullCheck(arg1, function () {
        var str = arg1.toString_$_$$Ljava_lang_String$$$_(arg1).str;
        return arg0.str.indexOf(str) != -1;
    });
};
var java_lang_String_$_contentEquals_$_$Ljava_lang_CharSequence$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Compares this string to the specified CharSequence.  The result
 is true if and only if this String represents the same
 sequence of char values as the specified sequence.

Parameters:

cs -> The sequence to compare this String against

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_contentEquals_$_$Ljava_lang_CharSequence$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_String_$_contentEquals_$_$Ljava_lang_StringBuffer$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Compares this string to the specified StringBuffer.  The result
 is true if and only if this String represents the same
 sequence of characters as the specified StringBuffer.

Parameters:

sb -> The StringBuffer to compare this String against

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_contentEquals_$_$Ljava_lang_StringBuffer$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_String_$_copyValueOf_$_$_$$$$$_C$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a String that represents the character sequence in the
 array specified.

Parameters:

data -> the character array.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_copyValueOf_$_$_$$$$$_C$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_copyValueOf_$_$_$$$$$_CII$Ljava_lang_String$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a String that represents the character sequence in the
 array specified.

Parameters:

data -> the character array.

offset -> initial offset of the subarray.

count -> length of the subarray.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_copyValueOf_$_$_$$$$$_CII$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_endsWith_$_$Ljava_lang_String$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Tests if this string ends with the specified suffix.

Parameters:

suffix -> the suffix.

*/
//must have a return of type boolean
	return java_lang_String_nullCheck(arg1, function () {
		var str = arg0.str;
		var suffix = arg1.str;
		return str.indexOf(suffix, str.length - suffix.length) !== -1;
	});
};
var java_lang_String_$_equalsIgnoreCase_$_$Ljava_lang_String$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Compares this String to another String, ignoring case
 considerations.  Two strings are considered equal ignoring case if they
 are of the same length and corresponding characters in the two strings
 are equal ignoring case.

  Two characters c1 and c2 are considered the same
 ignoring case if at least one of the following is true:
 
    The two characters are the same (as compared by the
        == operator)
    Applying the method Character.toUpperCase(char) to each character
        produces the same result
    Applying the method Character.toLowerCase(char) to each character
        produces the same result

Parameters:

anotherString -> The String to compare this String against

*/
//must have a return of type boolean
	if (arg1 === null) {
		return false;
	} else {
		return arg0.str.toUpperCase() === arg1.str.toUpperCase();
	}
};
var java_lang_String_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Compares this string to the specified object.  The result is true if and only if the argument is not null and is a String object that represents the same sequence of characters as this
 object.

Parameters:

anObject -> The object to compare this String against

*/
//must have a return of type boolean
	if (arg1 === null) {
		return false;
	} else {
        return arg0.str === arg1.str;
	}
};
var java_lang_String_$_format_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Object$$$_$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_format_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Object$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_format_$_$Ljava_util_Locale$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_Object$$$_$Ljava_lang_String$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_format_$_$Ljava_util_Locale$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_Object$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_getBytes_$_$$_$$$$$_B = function (arg0) {
/*implement method!*/
/*
Encodes this String into a sequence of bytes using the
 platform's default charset, storing the result into a new byte array.

  The behavior of this method when this string cannot be encoded in
 the default charset is unspecified.  The CharsetEncoder class should be used when more control
 over the encoding process is required.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_getBytes_$_$$_$$$$$_B"));
throw ex;
//must have a return of type byte[]
};
var java_lang_String_$_getBytes_$_$II_$$$$$_BI$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*
Deprecated. This method does not properly convert characters into
 bytes.  As of JDK 1.1, the preferred way to do this is via the
 getBytes() method, which uses the platform's default charset.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_getBytes_$_$II_$$$$$_BI$V"));
throw ex;
//must have a return of type void
};
var java_lang_String_$_getBytes_$_$Ljava_lang_String$$$_$_$$$$$_B = function (arg0, arg1) {
/*implement method!*/
/*
Encodes this String into a sequence of bytes using the named
 charset, storing the result into a new byte array.

  The behavior of this method when this string cannot be encoded in
 the given charset is unspecified.  The CharsetEncoder class should be used when more control
 over the encoding process is required.

Parameters:

charsetName -> The name of a supported charset

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_getBytes_$_$Ljava_lang_String$$$_$_$$$$$_B"));
throw ex;
//must have a return of type byte[]
};
var java_lang_String_$_getBytes_$_$Ljava_nio_charset_Charset$$$_$_$$$$$_B = function (arg0, arg1) {
/*implement method!*/
/*
Encodes this String into a sequence of bytes using the given
 charset, storing the result into a
 new byte array.

  This method always replaces malformed-input and unmappable-character
 sequences with this charset's default replacement byte array.  The
 CharsetEncoder class should be used when more
 control over the encoding process is required.

Parameters:

charset -> The Charset to be used to encode
         the String

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_getBytes_$_$Ljava_nio_charset_Charset$$$_$_$$$$$_B"));
throw ex;
//must have a return of type byte[]
};
var java_lang_String_$_getChars_$_$II_$$$$$_CI$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*
Copies characters from this string into the destination character
 array.
 
 The first character to be copied is at index srcBegin;
 the last character to be copied is at index srcEnd-1
 (thus the total number of characters to be copied is
 srcEnd-srcBegin). The characters are copied into the
 subarray of dst starting at index dstBegin
 and ending at index:
      dstbegin + (srcEnd-srcBegin) - 1

Parameters:

srcBegin -> index of the first character in the string
                        to copy.

srcEnd -> index after the last character in the string
                        to copy.

dst -> the destination array.

dstBegin -> the start offset in the destination array.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_getChars_$_$II_$$$$$_CI$V"));
throw ex;
//must have a return of type void
};
var java_lang_String_$_getChars_$_$_$$$$$_CI$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_getChars_$_$_$$$$$_CI$V"));
throw ex;
//must have a return of type void
};
var java_lang_String_$_hashCode_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns a hash code for this string. The hash code for a
 String object is computed as
  s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1]
 
 using int arithmetic, where s[i] is the
 ith character of the string, n is the length of
 the string, and ^ indicates exponentiation.
 (The hash value of the empty string is zero.)

*/
//must have a return of type int
	var hashCode = 0;
	var str = arg0.str;
	var n = str.length;
	var i;
	for (i = 0; i < n; i += 1) {
		hashCode += str.charCodeAt(i) * Math.pow(31, n - i - 1);
	}
	return hashCode;
};
var java_lang_String_$_indexOf_$_$I$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the index within this string of the first occurrence of
 the specified character. If a character with value
 ch occurs in the character sequence represented by
 this String object, then the index (in Unicode
 code units) of the first such occurrence is returned. For
 values of ch in the range from 0 to 0xFFFF
 (inclusive), this is the smallest value k such that:
  this.charAt(k) == ch
 
 is true. For other values of ch, it is the
 smallest value k such that:
  this.codePointAt(k) == ch
 
 is true. In either case, if no such character occurs in this
 string, then -1 is returned.

Parameters:

ch -> a character (Unicode code point).

*/
//must have a return of type int
    return arg0.str.indexOf(String.fromCharCode(arg1));
};
var java_lang_String_$_indexOf_$_$II$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the index within this string of the first occurrence of the
 specified character, starting the search at the specified index.
 
 If a character with value ch occurs in the
 character sequence represented by this String
 object at an index no smaller than fromIndex, then
 the index of the first such occurrence is returned. For values
 of ch in the range from 0 to 0xFFFF (inclusive),
 this is the smallest value k such that:
  (this.charAt(k) == ch) && (k >= fromIndex)
 
 is true. For other values of ch, it is the
 smallest value k such that:
  (this.codePointAt(k) == ch) && (k >= fromIndex)
 
 is true. In either case, if no such character occurs in this
 string at or after position fromIndex, then
 -1 is returned.

 
 There is no restriction on the value of fromIndex. If it
 is negative, it has the same effect as if it were zero: this entire
 string may be searched. If it is greater than the length of this
 string, it has the same effect as if it were equal to the length of
 this string: -1 is returned.

 All indices are specified in char values
 (Unicode code units).

Parameters:

ch -> a character (Unicode code point).

fromIndex -> the index to start the search from.

*/
//must have a return of type int
    var sub = arg0.str.substring(arg2);
    var index = sub.indexOf(String.fromCharCode(arg1));
    if (index == -1) {
        return -1;
    } else {
        return index + arg2;
    }
};
var java_lang_String_$_indexOf_$_$Ljava_lang_String$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the index within this string of the first occurrence of the
 specified substring. The integer returned is the smallest value
 k such that:
  this.startsWith(str, k)
 
 is true.

Parameters:

str -> any string.

*/
//must have a return of type int
	return java_lang_String_nullCheck(arg1, function () {
        return arg0.str.indexOf(arg1.str);
	});
};
var java_lang_String_$_indexOf_$_$Ljava_lang_String$$$_I$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the index within this string of the first occurrence of the
 specified substring, starting at the specified index.  The integer
 returned is the smallest value k for which:
      k >= Math.min(fromIndex, this.length()) && this.startsWith(str, k)
 
 If no such value of k exists, then -1 is returned.

Parameters:

str -> the substring for which to search.

fromIndex -> the index from which to start the search.

*/
//must have a return of type int
	return java_lang_String_nullCheck(arg1, function () {
        return arg0.str.indexOf(arg1.str, arg2);
	});
};
var java_lang_String_$_indexOf_$_$_$$$$$_CII_$$$$$_CIII$I = function (arg0, arg1, arg2, arg3, arg4, arg5, arg6) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_indexOf_$_$_$$$$$_CII_$$$$$_CIII$I"));
throw ex;
//must have a return of type int
};
var java_lang_String_$_intern_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a canonical representation for the string object.
 
 A pool of strings, initially empty, is maintained privately by the
 class String.
 
 When the intern method is invoked, if the pool already contains a
 string equal to this String object as determined by
 the equals(Object) method, then the string from the pool is
 returned. Otherwise, this String object is added to the
 pool and a reference to this String object is returned.
 
 It follows that for any two strings s and t,
 s.intern() == t.intern() is true
 if and only if s.equals(t) is true.
 
 All literal strings and string-valued constant expressions are
 interned. String literals are defined in §3.10.5 of the
 Java Language
 Specification

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_intern_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_isEmpty_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if, and only if, length() is 0.

*/
//must have a return of type boolean
	return arg0.str.length == 0;
};
var java_lang_String_$_lastIndexOf_$_$I$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the index within this string of the last occurrence of
 the specified character. For values of ch in the
 range from 0 to 0xFFFF (inclusive), the index (in Unicode code
 units) returned is the largest value k such that:
  this.charAt(k) == ch
 
 is true. For other values of ch, it is the
 largest value k such that:
  this.codePointAt(k) == ch
 
 is true.  In either case, if no such character occurs in this
 string, then -1 is returned.  The
 String is searched backwards starting at the last
 character.

Parameters:

ch -> a character (Unicode code point).

*/
//must have a return of type int
	return arg0.str.lastIndexOf(String.fromCharCode(arg1));
};
var java_lang_String_$_lastIndexOf_$_$II$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the index within this string of the last occurrence of
 the specified character, searching backward starting at the
 specified index. For values of ch in the range
 from 0 to 0xFFFF (inclusive), the index returned is the largest
 value k such that:
  (this.charAt(k) == ch) && (k <= fromIndex)
 
 is true. For other values of ch, it is the
 largest value k such that:
  (this.codePointAt(k) == ch) && (k <= fromIndex)
 
 is true. In either case, if no such character occurs in this
 string at or before position fromIndex, then
 -1 is returned.

 All indices are specified in char values
 (Unicode code units).

Parameters:

ch -> a character (Unicode code point).

fromIndex -> the index to start the search from. There is no
          restriction on the value of fromIndex. If it is
          greater than or equal to the length of this string, it has
          the same effect as if it were equal to one less than the
          length of this string: this entire string may be searched.
          If it is negative, it has the same effect as if it were -1:
          -1 is returned.

*/
//must have a return of type int
	return arg0.str.lastIndexOf(String.fromCharCode(arg1), arg2);
};
var java_lang_String_$_lastIndexOf_$_$Ljava_lang_String$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the index within this string of the rightmost occurrence
 of the specified substring.  The rightmost empty string "" is
 considered to occur at the index value this.length().
 The returned index is the largest value k such that
  this.startsWith(str, k)
 
 is true.

Parameters:

str -> the substring to search for.

*/
//must have a return of type int
	return java_lang_String_nullCheck(arg1, function () {
		return arg0.str.lastIndexOf(arg1.str);
	});
};
var java_lang_String_$_lastIndexOf_$_$Ljava_lang_String$$$_I$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the index within this string of the last occurrence of the
 specified substring, searching backward starting at the specified index.
 The integer returned is the largest value k such that:
      k <= Math.min(fromIndex, this.length()) && this.startsWith(str, k)
 
 If no such value of k exists, then -1 is returned.

Parameters:

str -> the substring to search for.

fromIndex -> the index to start the search from.

*/
//must have a return of type int
	return java_lang_String_nullCheck(arg1, function () {
		return arg0.str.lastIndexOf(arg1.str, arg2);
	});
};
var java_lang_String_$_lastIndexOf_$_$_$$$$$_CII_$$$$$_CIII$I = function (arg0, arg1, arg2, arg3, arg4, arg5, arg6) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_lastIndexOf_$_$_$$$$$_CII_$$$$$_CIII$I"));
throw ex;
//must have a return of type int
};
var java_lang_String_$_length_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the length of this string.
 The length is equal to the number of Unicode
 code units in the string.

*/
//must have a return of type int
	return arg0.str.length;
};
var java_lang_String_$_matches_$_$Ljava_lang_String$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Tells whether or not this string matches the given regular expression.

  An invocation of this method of the form
 str.matches(regex) yields exactly the
 same result as the expression

  Pattern.matches(regex, str)

Parameters:

regex -> the regular expression to which this string is to be matched

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_matches_$_$Ljava_lang_String$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_String_$_offsetByCodePoints_$_$II$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the index within this String that is
 offset from the given index by
 codePointOffset code points. Unpaired surrogates
 within the text range given by index and
 codePointOffset count as one code point each.

Parameters:

index -> the index to be offset

codePointOffset -> the offset in code points

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_offsetByCodePoints_$_$II$I"));
throw ex;
//must have a return of type int
};
var java_lang_String_$_regionMatches_$_$ILjava_lang_String$$$_II$Z = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*
Tests if two string regions are equal.
 
 A substring of this String object is compared to a substring
 of the argument other. The result is true if these substrings
 represent identical character sequences. The substring of this
 String object to be compared begins at index toffset
 and has length len. The substring of other to be compared
 begins at index ooffset and has length len. The
 result is false if and only if at least one of the following
 is true:
 toffset is negative.
 ooffset is negative.
 toffset+len is greater than the length of this
 String object.
 ooffset+len is greater than the length of the other
 argument.
 There is some nonnegative integer k less than len
 such that:
 this.charAt(toffset+k) != other.charAt(ooffset+k)

Parameters:

toffset -> the starting offset of the subregion in this string.

other -> the string argument.

ooffset -> the starting offset of the subregion in the string
                    argument.

len -> the number of characters to compare.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_regionMatches_$_$ILjava_lang_String$$$_II$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_String_$_regionMatches_$_$ZILjava_lang_String$$$_II$Z = function (arg0, arg1, arg2, arg3, arg4, arg5) {
/*implement method!*/
/*
Tests if two string regions are equal.
 
 A substring of this String object is compared to a substring
 of the argument other. The result is true if these
 substrings represent character sequences that are the same, ignoring
 case if and only if ignoreCase is true. The substring of
 this String object to be compared begins at index
 toffset and has length len. The substring of
 other to be compared begins at index ooffset and
 has length len. The result is false if and only if
 at least one of the following is true:
 toffset is negative.
 ooffset is negative.
 toffset+len is greater than the length of this
 String object.
 ooffset+len is greater than the length of the other
 argument.
 ignoreCase is false and there is some nonnegative
 integer k less than len such that:
  this.charAt(toffset+k) != other.charAt(ooffset+k)
 
 ignoreCase is true and there is some nonnegative
 integer k less than len such that:
  Character.toLowerCase(this.charAt(toffset+k)) !=
               Character.toLowerCase(other.charAt(ooffset+k))
 
 and:
  Character.toUpperCase(this.charAt(toffset+k)) !=
         Character.toUpperCase(other.charAt(ooffset+k))

Parameters:

ignoreCase -> if true, ignore case when comparing
                       characters.

toffset -> the starting offset of the subregion in this
                       string.

other -> the string argument.

ooffset -> the starting offset of the subregion in the string
                       argument.

len -> the number of characters to compare.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_regionMatches_$_$ZILjava_lang_String$$$_II$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_String_$_replaceAll_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Replaces each substring of this string that matches the given regular expression with the
 given replacement.

  An invocation of this method of the form
 str.replaceAll(regex, repl)
 yields exactly the same result as the expression

 
 Pattern.compile(regex).matcher(str).replaceAll(repl)


 Note that backslashes (\) and dollar signs ($) in the
 replacement string may cause the results to be different than if it were
 being treated as a literal replacement string; see
 Matcher.replaceAll.
 Use Matcher.quoteReplacement(java.lang.String) to suppress the special
 meaning of these characters, if desired.

Parameters:

regex -> the regular expression to which this string is to be matched

replacement -> the string to be substituted for each match

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_replaceAll_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_replaceFirst_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Replaces the first substring of this string that matches the given regular expression with the
 given replacement.

  An invocation of this method of the form
 str.replaceFirst(regex, repl)
 yields exactly the same result as the expression

 
 Pattern.compile(regex).matcher(str).replaceFirst(repl)


 Note that backslashes (\) and dollar signs ($) in the
 replacement string may cause the results to be different than if it were
 being treated as a literal replacement string; see
 Matcher.replaceFirst(java.lang.String).
 Use Matcher.quoteReplacement(java.lang.String) to suppress the special
 meaning of these characters, if desired.

Parameters:

regex -> the regular expression to which this string is to be matched

replacement -> the string to be substituted for the first match

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_replaceFirst_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_replace_$_$CC$Ljava_lang_String$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a new string resulting from replacing all occurrences of
 oldChar in this string with newChar.
 
 If the character oldChar does not occur in the
 character sequence represented by this String object,
 then a reference to this String object is returned.
 Otherwise, a new String object is created that
 represents a character sequence identical to the character sequence
 represented by this String object, except that every
 occurrence of oldChar is replaced by an occurrence
 of newChar.
 
 Examples:
  "mesquite in your cellar".replace('e', 'o')
         returns "mosquito in your collar"
 "the war of baronets".replace('r', 'y')
         returns "the way of bayonets"
 "sparring with a purple porpoise".replace('p', 't')
         returns "starring with a turtle tortoise"
 "JonL".replace('q', 'x') returns "JonL" (no change)

Parameters:

oldChar -> the old character.

newChar -> the new character.

*/
//must have a return of type String
	var c1 = String.fromCharCode(arg1);
	var c2 = String.fromCharCode(arg2);
	var re = new RegExp(c1, "g");
	return new java_lang_String(arg0.str.replace(re, c2));
};
var java_lang_String_$_replace_$_$Ljava_lang_CharSequence$$$_Ljava_lang_CharSequence$$$_$Ljava_lang_String$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Replaces each substring of this string that matches the literal target
 sequence with the specified literal replacement sequence. The
 replacement proceeds from the beginning of the string to the end, for
 example, replacing "aa" with "b" in the string "aaa" will result in
 "ba" rather than "ab".

Parameters:

target -> The sequence of char values to be replaced

replacement -> The replacement sequence of char values

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_replace_$_$Ljava_lang_CharSequence$$$_Ljava_lang_CharSequence$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_split_$_$Ljava_lang_String$$$_$_$$$$$_Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Splits this string around matches of the given regular expression.

  This method works as if by invoking the two-argument split method with the given expression and a limit
 argument of zero.  Trailing empty strings are therefore not included in
 the resulting array.

  The string "boo:and:foo", for example, yields the following
 results with these expressions:

 
 
  Regex
  Result
 
 :
     { "boo", "and", "foo" }
 o
     { "b", "", ":and:f" }

Parameters:

regex -> the delimiting regular expression

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_split_$_$Ljava_lang_String$$$_$_$$$$$_Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String[]
};
var java_lang_String_$_split_$_$Ljava_lang_String$$$_I$_$$$$$_Ljava_lang_String$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Splits this string around matches of the given
 regular expression.

  The array returned by this method contains each substring of this
 string that is terminated by another substring that matches the given
 expression or is terminated by the end of the string.  The substrings in
 the array are in the order in which they occur in this string.  If the
 expression does not match any part of the input then the resulting array
 has just one element, namely this string.

  The limit parameter controls the number of times the
 pattern is applied and therefore affects the length of the resulting
 array.  If the limit n is greater than zero then the pattern
 will be applied at most n - 1 times, the array's
 length will be no greater than n, and the array's last entry
 will contain all input beyond the last matched delimiter.  If n
 is non-positive then the pattern will be applied as many times as
 possible and the array can have any length.  If n is zero then
 the pattern will be applied as many times as possible, the array can
 have any length, and trailing empty strings will be discarded.

  The string "boo:and:foo", for example, yields the
 following results with these parameters:

 
 
     Regex
     Limit
     Result
 
 :
     2
     { "boo", "and:foo" }
 :
     5
     { "boo", "and", "foo" }
 :
     -2
     { "boo", "and", "foo" }
 o
     5
     { "b", "", ":and:f", "", "" }
 o
     -2
     { "b", "", ":and:f", "", "" }
 o
     0
     { "b", "", ":and:f" }
 

  An invocation of this method of the form
 str.split(regex, n)
 yields the same result as the expression

 
 Pattern.compile(regex).split(str, n)

Parameters:

regex -> the delimiting regular expression

limit -> the result threshold, as described above

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_split_$_$Ljava_lang_String$$$_I$_$$$$$_Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String[]
};
var java_lang_String_$_startsWith_$_$Ljava_lang_String$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Tests if this string starts with the specified prefix.

Parameters:

prefix -> the prefix.

*/
//must have a return of type boolean
	return java_lang_String_nullCheck(arg1, function () {
		var str = arg0.str;
		var prefix = arg1.str;
		return str.indexOf(prefix) == 0;
	});
};
var java_lang_String_$_startsWith_$_$Ljava_lang_String$$$_I$Z = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Tests if the substring of this string beginning at the
 specified index starts with the specified prefix.

Parameters:

prefix -> the prefix.

toffset -> where to begin looking in this string.

*/
//must have a return of type boolean
	var str = new java_lang_String(arg0.str.substring(arg2));
	return java_lang_String_$_startsWith_$_$Ljava_lang_String$$$_$Z(str, arg1);
};
var java_lang_String_$_subSequence_$_$II$Ljava_lang_CharSequence$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a new character sequence that is a subsequence of this sequence.

  An invocation of this method of the form

  str.subSequence(begin, end)

 behaves in exactly the same way as the invocation

  str.substring(begin, end)

 This method is defined so that the String class can implement
 the CharSequence interface.

Parameters:

beginIndex -> the begin index, inclusive.

endIndex -> the end index, exclusive.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_subSequence_$_$II$Ljava_lang_CharSequence$$$_"));
throw ex;
//must have a return of type CharSequence
};
var java_lang_String_$_substring_$_$I$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a new string that is a substring of this string. The
 substring begins with the character at the specified index and
 extends to the end of this string. 
 Examples:
  "unhappy".substring(2) returns "happy"
 "Harbison".substring(3) returns "bison"
 "emptiness".substring(9) returns "" (an empty string)

Parameters:

beginIndex -> the beginning index, inclusive.

*/
//must have a return of type String
    if (arg1 < 0 || arg1 > arg0.str.length) {
        var ex = new java_lang_IndexOutOfBoundsException();
        ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("invalid index: " + arg1));
        throw ex;
    }
    return new java_lang_String(arg0.str.substring(arg1));
};
var java_lang_String_$_substring_$_$II$Ljava_lang_String$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a new string that is a substring of this string. The
 substring begins at the specified beginIndex and
 extends to the character at index endIndex - 1.
 Thus the length of the substring is endIndex-beginIndex.
 
 Examples:
  "hamburger".substring(4, 8) returns "urge"
 "smiles".substring(1, 5) returns "mile"

Parameters:

beginIndex -> the beginning index, inclusive.

endIndex -> the ending index, exclusive.

*/
//must have a return of type String
    if (arg1 < 0) {
        var ex = new java_lang_IndexOutOfBoundsException();
        ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("invalid begin index: " + arg1));
        throw ex;
    }
    if (arg2 > arg0.str.length) {
        var ex = new java_lang_IndexOutOfBoundsException();
        ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("invalid end index: " + arg1));
        throw ex;
    }
    if (arg1 > arg2) {
        var ex = new java_lang_IndexOutOfBoundsException();
        ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("begin must be less than end"));
        throw ex;
    }
    return new java_lang_String(arg0.str.substring(arg1, arg2));
};
var java_lang_String_$_toCharArray_$_$$_$$$$$_C = function (arg0) {
/*implement method!*/
/*
Converts this string to a new character array.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_toCharArray_$_$$_$$$$$_C"));
throw ex;
//must have a return of type char[]
};
var java_lang_String_$_toLowerCase_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Converts all of the characters in this String to lower
 case using the rules of the default locale. This is equivalent to calling
 toLowerCase(Locale.getDefault()).
 
 Note: This method is locale sensitive, and may produce unexpected
 results if used for strings that are intended to be interpreted locale
 independently.
 Examples are programming language identifiers, protocol keys, and HTML
 tags.
 For instance, "TITLE".toLowerCase() in a Turkish locale
 returns "t?tle", where '?' is the LATIN SMALL
 LETTER DOTLESS I character.
 To obtain correct results for locale insensitive strings, use
 toLowerCase(Locale.ENGLISH).

*/
//must have a return of type String
	return new java_lang_String(arg0.str.toLowerCase());
};
var java_lang_String_$_toLowerCase_$_$Ljava_util_Locale$$$_$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Converts all of the characters in this String to lower
 case using the rules of the given Locale.  Case mapping is based
 on the Unicode Standard version specified by the Character
 class. Since case mappings are not always 1:1 char mappings, the resulting
 String may be a different length than the original String.
 
 Examples of lowercase  mappings are in the following table:
 
 
   Language Code of Locale
   Upper Case
   Lower Case
   Description
 
 
   tr (Turkish)
   \u0130
   \u0069
   capital letter I with dot above -> small letter i
 
 
   tr (Turkish)
   \u0049
   \u0131
   capital letter I -> small letter dotless i 
 
 
   (all)
   French Fries
   french fries
   lowercased all chars in String
 
 
   (all)
   
       
       
   
       
       
   lowercased all chars in String

Parameters:

locale -> use the case transformation rules for this locale

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_toLowerCase_$_$Ljava_util_Locale$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
This object (which is already a string!) is itself returned.

*/
//must have a return of type String
    return arg0;
};
var java_lang_String_$_toUpperCase_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Converts all of the characters in this String to upper
 case using the rules of the default locale. This method is equivalent to
 toUpperCase(Locale.getDefault()).
 
 Note: This method is locale sensitive, and may produce unexpected
 results if used for strings that are intended to be interpreted locale
 independently.
 Examples are programming language identifiers, protocol keys, and HTML
 tags.
 For instance, "title".toUpperCase() in a Turkish locale
 returns "T?TLE", where '?' is the LATIN CAPITAL
 LETTER I WITH DOT ABOVE character.
 To obtain correct results for locale insensitive strings, use
 toUpperCase(Locale.ENGLISH).

*/
//must have a return of type String
	return new java_lang_String(arg0.str.toUpperCase());
};
var java_lang_String_$_toUpperCase_$_$Ljava_util_Locale$$$_$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Converts all of the characters in this String to upper
 case using the rules of the given Locale. Case mapping is based
 on the Unicode Standard version specified by the Character
 class. Since case mappings are not always 1:1 char mappings, the resulting
 String may be a different length than the original String.
 
 Examples of locale-sensitive and 1:M case mappings are in the following table.
 
 
 
   Language Code of Locale
   Lower Case
   Upper Case
   Description
 
 
   tr (Turkish)
   \u0069
   \u0130
   small letter i -> capital letter I with dot above
 
 
   tr (Turkish)
   \u0131
   \u0049
   small letter dotless i -> capital letter I
 
 
   (all)
   \u00df
   \u0053 \u0053
   small letter sharp s -> two letters: SS
 
 
   (all)
   Fahrvergnügen
   FAHRVERGNÜGEN

Parameters:

locale -> use the case transformation rules for this locale

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_toUpperCase_$_$Ljava_util_Locale$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_trim_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a copy of the string, with leading and trailing whitespace
 omitted.
 
 If this String object represents an empty character
 sequence, or the first and last characters of character sequence
 represented by this String object both have codes
 greater than '\u0020' (the space character), then a
 reference to this String object is returned.
 
 Otherwise, if there is no character with a code greater than
 '\u0020' in the string, then a new
 String object representing an empty string is created
 and returned.
 
 Otherwise, let k be the index of the first character in the
 string whose code is greater than '\u0020', and let
 m be the index of the last character in the string whose code
 is greater than '\u0020'. A new String
 object is created, representing the substring of this string that
 begins with the character at index k and ends with the
 character at index m-that is, the result of
 this.substring(k, m+1).
 
 This method may be used to trim whitespace (as defined above) from
 the beginning and end of a string.

*/
//must have a return of type String
	var str = arg0.str;
	var trimmed = str.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');
	if (trimmed.length === str.length) {
		return arg0;
	} else {
		return new java_lang_String(trimmed);
	}
};
var java_lang_String_$_valueOf_$_$C$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the string representation of the char
 argument.

Parameters:

c -> a char.

*/
//must have a return of type String
    return new java_lang_String(String.fromCharCode(arg0));
};
var java_lang_String_$_valueOf_$_$D$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the string representation of the double argument.
 
 The representation is exactly the one returned by the
 Double.toString method of one argument.

Parameters:

d -> a double.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_valueOf_$_$D$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_valueOf_$_$F$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the string representation of the float argument.
 
 The representation is exactly the one returned by the
 Float.toString method of one argument.

Parameters:

f -> a float.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_valueOf_$_$F$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_valueOf_$_$I$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the string representation of the int argument.
 
 The representation is exactly the one returned by the
 Integer.toString method of one argument.

Parameters:

i -> an int.

*/
//must have a return of type String

	return new java_lang_String(arg0.toString());
};
var java_lang_String_$_valueOf_$_$J$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the string representation of the long argument.
 
 The representation is exactly the one returned by the
 Long.toString method of one argument.

Parameters:

l -> a long.

*/
//must have a return of type String
	return new java_lang_String(arg0.toString());
};
var java_lang_String_$_valueOf_$_$Ljava_lang_Object$$$_$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the string representation of the Object argument.

Parameters:

obj -> an Object.

*/
//must have a return of type String
	return arg0.toString_$_$$Ljava_lang_String$$$_(arg0);
};
var java_lang_String_$_valueOf_$_$Z$Ljava_lang_String$$$_ = (function () {
    var TRUE = new java_lang_String("true");
    var FALSE = new java_lang_String("false");
    return function (arg0) {
        /*implement method!*/
        /*
         Returns the string representation of the boolean argument.

         Parameters:

         b -> a boolean.

         */
//must have a return of type String
        if (arg0) return TRUE; else return FALSE;
    }
})();
var java_lang_String_$_valueOf_$_$_$$$$$_C$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the string representation of the char array
 argument. The contents of the character array are copied; subsequent
 modification of the character array does not affect the newly
 created string.

Parameters:

data -> a char array.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_valueOf_$_$_$$$$$_C$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_valueOf_$_$_$$$$$_CII$Ljava_lang_String$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the string representation of a specific subarray of the
 char array argument.
 
 The offset argument is the index of the first
 character of the subarray. The count argument
 specifies the length of the subarray. The contents of the subarray
 are copied; subsequent modification of the character array does not
 affect the newly created string.

Parameters:

data -> the character array.

offset -> the initial offset into the value of the
                  String.

count -> the length of the value of the String.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_String_$_valueOf_$_$_$$$$$_CII$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_String_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_lang_String_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_String_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_lang_String_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_String_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_lang_String_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_lang_String.prototype._$$$init$$$__$_$$V = java_lang_String_$__$$$init$$$__$_$$V;
java_lang_String.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_lang_String_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_lang_String.prototype._$$$init$$$__$_$Ljava_lang_StringBuffer$$$_$V = java_lang_String_$__$$$init$$$__$_$Ljava_lang_StringBuffer$$$_$V;
java_lang_String.prototype._$$$init$$$__$_$Ljava_lang_StringBuilder$$$_$V = java_lang_String_$__$$$init$$$__$_$Ljava_lang_StringBuilder$$$_$V;
java_lang_String.prototype._$$$init$$$__$_$_$$$$$_B$V = java_lang_String_$__$$$init$$$__$_$_$$$$$_B$V;
java_lang_String.prototype._$$$init$$$__$_$_$$$$$_BI$V = java_lang_String_$__$$$init$$$__$_$_$$$$$_BI$V;
java_lang_String.prototype._$$$init$$$__$_$_$$$$$_BII$V = java_lang_String_$__$$$init$$$__$_$_$$$$$_BII$V;
java_lang_String.prototype._$$$init$$$__$_$_$$$$$_BIII$V = java_lang_String_$__$$$init$$$__$_$_$$$$$_BIII$V;
java_lang_String.prototype._$$$init$$$__$_$_$$$$$_BIILjava_lang_String$$$_$V = java_lang_String_$__$$$init$$$__$_$_$$$$$_BIILjava_lang_String$$$_$V;
java_lang_String.prototype._$$$init$$$__$_$_$$$$$_BIILjava_nio_charset_Charset$$$_$V = java_lang_String_$__$$$init$$$__$_$_$$$$$_BIILjava_nio_charset_Charset$$$_$V;
java_lang_String.prototype._$$$init$$$__$_$_$$$$$_BLjava_lang_String$$$_$V = java_lang_String_$__$$$init$$$__$_$_$$$$$_BLjava_lang_String$$$_$V;
java_lang_String.prototype._$$$init$$$__$_$_$$$$$_BLjava_nio_charset_Charset$$$_$V = java_lang_String_$__$$$init$$$__$_$_$$$$$_BLjava_nio_charset_Charset$$$_$V;
java_lang_String.prototype._$$$init$$$__$_$_$$$$$_C$V = java_lang_String_$__$$$init$$$__$_$_$$$$$_C$V;
java_lang_String.prototype._$$$init$$$__$_$_$$$$$_CII$V = java_lang_String_$__$$$init$$$__$_$_$$$$$_CII$V;
java_lang_String.prototype._$$$init$$$__$_$_$$$$$_III$V = java_lang_String_$__$$$init$$$__$_$_$$$$$_III$V;
java_lang_String.prototype.charAt_$_$I$C = java_lang_String_$_charAt_$_$I$C;
java_lang_String.prototype.codePointAt_$_$I$I = java_lang_String_$_codePointAt_$_$I$I;
java_lang_String.prototype.codePointBefore_$_$I$I = java_lang_String_$_codePointBefore_$_$I$I;
java_lang_String.prototype.codePointCount_$_$II$I = java_lang_String_$_codePointCount_$_$II$I;
java_lang_String.prototype.compareToIgnoreCase_$_$Ljava_lang_String$$$_$I = java_lang_String_$_compareToIgnoreCase_$_$Ljava_lang_String$$$_$I;
java_lang_String.prototype.compareTo_$_$Ljava_lang_Object$$$_$I = java_lang_String_$_compareTo_$_$Ljava_lang_Object$$$_$I;
java_lang_String.prototype.compareTo_$_$Ljava_lang_String$$$_$I = java_lang_String_$_compareTo_$_$Ljava_lang_String$$$_$I;
java_lang_String.prototype.concat_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = java_lang_String_$_concat_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_;
java_lang_String.prototype.contains_$_$Ljava_lang_CharSequence$$$_$Z = java_lang_String_$_contains_$_$Ljava_lang_CharSequence$$$_$Z;
java_lang_String.prototype.contentEquals_$_$Ljava_lang_CharSequence$$$_$Z = java_lang_String_$_contentEquals_$_$Ljava_lang_CharSequence$$$_$Z;
java_lang_String.prototype.contentEquals_$_$Ljava_lang_StringBuffer$$$_$Z = java_lang_String_$_contentEquals_$_$Ljava_lang_StringBuffer$$$_$Z;
java_lang_String.copyValueOf_$_$_$$$$$_C$Ljava_lang_String$$$_ = java_lang_String_$_copyValueOf_$_$_$$$$$_C$Ljava_lang_String$$$_;
java_lang_String.copyValueOf_$_$_$$$$$_CII$Ljava_lang_String$$$_ = java_lang_String_$_copyValueOf_$_$_$$$$$_CII$Ljava_lang_String$$$_;
java_lang_String.prototype.endsWith_$_$Ljava_lang_String$$$_$Z = java_lang_String_$_endsWith_$_$Ljava_lang_String$$$_$Z;
java_lang_String.prototype.equalsIgnoreCase_$_$Ljava_lang_String$$$_$Z = java_lang_String_$_equalsIgnoreCase_$_$Ljava_lang_String$$$_$Z;
java_lang_String.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_String_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_String.format_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Object$$$_$Ljava_lang_String$$$_ = java_lang_String_$_format_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Object$$$_$Ljava_lang_String$$$_;
java_lang_String.format_$_$Ljava_util_Locale$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_Object$$$_$Ljava_lang_String$$$_ = java_lang_String_$_format_$_$Ljava_util_Locale$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_Object$$$_$Ljava_lang_String$$$_;
java_lang_String.prototype.getBytes_$_$$_$$$$$_B = java_lang_String_$_getBytes_$_$$_$$$$$_B;
java_lang_String.prototype.getBytes_$_$II_$$$$$_BI$V = java_lang_String_$_getBytes_$_$II_$$$$$_BI$V;
java_lang_String.prototype.getBytes_$_$Ljava_lang_String$$$_$_$$$$$_B = java_lang_String_$_getBytes_$_$Ljava_lang_String$$$_$_$$$$$_B;
java_lang_String.prototype.getBytes_$_$Ljava_nio_charset_Charset$$$_$_$$$$$_B = java_lang_String_$_getBytes_$_$Ljava_nio_charset_Charset$$$_$_$$$$$_B;
java_lang_String.prototype.getChars_$_$II_$$$$$_CI$V = java_lang_String_$_getChars_$_$II_$$$$$_CI$V;
java_lang_String.prototype.getChars_$_$_$$$$$_CI$V = java_lang_String_$_getChars_$_$_$$$$$_CI$V;
java_lang_String.prototype.hashCode_$_$$I = java_lang_String_$_hashCode_$_$$I;
java_lang_String.prototype.indexOf_$_$I$I = java_lang_String_$_indexOf_$_$I$I;
java_lang_String.prototype.indexOf_$_$II$I = java_lang_String_$_indexOf_$_$II$I;
java_lang_String.prototype.indexOf_$_$Ljava_lang_String$$$_$I = java_lang_String_$_indexOf_$_$Ljava_lang_String$$$_$I;
java_lang_String.prototype.indexOf_$_$Ljava_lang_String$$$_I$I = java_lang_String_$_indexOf_$_$Ljava_lang_String$$$_I$I;
java_lang_String.indexOf_$_$_$$$$$_CII_$$$$$_CIII$I = java_lang_String_$_indexOf_$_$_$$$$$_CII_$$$$$_CIII$I;
java_lang_String.prototype.intern_$_$$Ljava_lang_String$$$_ = java_lang_String_$_intern_$_$$Ljava_lang_String$$$_;
java_lang_String.prototype.isEmpty_$_$$Z = java_lang_String_$_isEmpty_$_$$Z;
java_lang_String.prototype.lastIndexOf_$_$I$I = java_lang_String_$_lastIndexOf_$_$I$I;
java_lang_String.prototype.lastIndexOf_$_$II$I = java_lang_String_$_lastIndexOf_$_$II$I;
java_lang_String.prototype.lastIndexOf_$_$Ljava_lang_String$$$_$I = java_lang_String_$_lastIndexOf_$_$Ljava_lang_String$$$_$I;
java_lang_String.prototype.lastIndexOf_$_$Ljava_lang_String$$$_I$I = java_lang_String_$_lastIndexOf_$_$Ljava_lang_String$$$_I$I;
java_lang_String.lastIndexOf_$_$_$$$$$_CII_$$$$$_CIII$I = java_lang_String_$_lastIndexOf_$_$_$$$$$_CII_$$$$$_CIII$I;
java_lang_String.prototype.length_$_$$I = java_lang_String_$_length_$_$$I;
java_lang_String.prototype.matches_$_$Ljava_lang_String$$$_$Z = java_lang_String_$_matches_$_$Ljava_lang_String$$$_$Z;
java_lang_String.prototype.offsetByCodePoints_$_$II$I = java_lang_String_$_offsetByCodePoints_$_$II$I;
java_lang_String.prototype.regionMatches_$_$ILjava_lang_String$$$_II$Z = java_lang_String_$_regionMatches_$_$ILjava_lang_String$$$_II$Z;
java_lang_String.prototype.regionMatches_$_$ZILjava_lang_String$$$_II$Z = java_lang_String_$_regionMatches_$_$ZILjava_lang_String$$$_II$Z;
java_lang_String.prototype.replaceAll_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = java_lang_String_$_replaceAll_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_;
java_lang_String.prototype.replaceFirst_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = java_lang_String_$_replaceFirst_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_;
java_lang_String.prototype.replace_$_$CC$Ljava_lang_String$$$_ = java_lang_String_$_replace_$_$CC$Ljava_lang_String$$$_;
java_lang_String.prototype.replace_$_$Ljava_lang_CharSequence$$$_Ljava_lang_CharSequence$$$_$Ljava_lang_String$$$_ = java_lang_String_$_replace_$_$Ljava_lang_CharSequence$$$_Ljava_lang_CharSequence$$$_$Ljava_lang_String$$$_;
java_lang_String.prototype.split_$_$Ljava_lang_String$$$_$_$$$$$_Ljava_lang_String$$$_ = java_lang_String_$_split_$_$Ljava_lang_String$$$_$_$$$$$_Ljava_lang_String$$$_;
java_lang_String.prototype.split_$_$Ljava_lang_String$$$_I$_$$$$$_Ljava_lang_String$$$_ = java_lang_String_$_split_$_$Ljava_lang_String$$$_I$_$$$$$_Ljava_lang_String$$$_;
java_lang_String.prototype.startsWith_$_$Ljava_lang_String$$$_$Z = java_lang_String_$_startsWith_$_$Ljava_lang_String$$$_$Z;
java_lang_String.prototype.startsWith_$_$Ljava_lang_String$$$_I$Z = java_lang_String_$_startsWith_$_$Ljava_lang_String$$$_I$Z;
java_lang_String.prototype.subSequence_$_$II$Ljava_lang_CharSequence$$$_ = java_lang_String_$_subSequence_$_$II$Ljava_lang_CharSequence$$$_;
java_lang_String.prototype.substring_$_$I$Ljava_lang_String$$$_ = java_lang_String_$_substring_$_$I$Ljava_lang_String$$$_;
java_lang_String.prototype.substring_$_$II$Ljava_lang_String$$$_ = java_lang_String_$_substring_$_$II$Ljava_lang_String$$$_;
java_lang_String.prototype.toCharArray_$_$$_$$$$$_C = java_lang_String_$_toCharArray_$_$$_$$$$$_C;
java_lang_String.prototype.toLowerCase_$_$$Ljava_lang_String$$$_ = java_lang_String_$_toLowerCase_$_$$Ljava_lang_String$$$_;
java_lang_String.prototype.toLowerCase_$_$Ljava_util_Locale$$$_$Ljava_lang_String$$$_ = java_lang_String_$_toLowerCase_$_$Ljava_util_Locale$$$_$Ljava_lang_String$$$_;
java_lang_String.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_String_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_String.prototype.toUpperCase_$_$$Ljava_lang_String$$$_ = java_lang_String_$_toUpperCase_$_$$Ljava_lang_String$$$_;
java_lang_String.prototype.toUpperCase_$_$Ljava_util_Locale$$$_$Ljava_lang_String$$$_ = java_lang_String_$_toUpperCase_$_$Ljava_util_Locale$$$_$Ljava_lang_String$$$_;
java_lang_String.prototype.trim_$_$$Ljava_lang_String$$$_ = java_lang_String_$_trim_$_$$Ljava_lang_String$$$_;
java_lang_String.valueOf_$_$C$Ljava_lang_String$$$_ = java_lang_String_$_valueOf_$_$C$Ljava_lang_String$$$_;
java_lang_String.valueOf_$_$D$Ljava_lang_String$$$_ = java_lang_String_$_valueOf_$_$D$Ljava_lang_String$$$_;
java_lang_String.valueOf_$_$F$Ljava_lang_String$$$_ = java_lang_String_$_valueOf_$_$F$Ljava_lang_String$$$_;
java_lang_String.valueOf_$_$I$Ljava_lang_String$$$_ = java_lang_String_$_valueOf_$_$I$Ljava_lang_String$$$_;
java_lang_String.valueOf_$_$J$Ljava_lang_String$$$_ = java_lang_String_$_valueOf_$_$J$Ljava_lang_String$$$_;
java_lang_String.valueOf_$_$Ljava_lang_Object$$$_$Ljava_lang_String$$$_ = java_lang_String_$_valueOf_$_$Ljava_lang_Object$$$_$Ljava_lang_String$$$_;
java_lang_String.valueOf_$_$Z$Ljava_lang_String$$$_ = java_lang_String_$_valueOf_$_$Z$Ljava_lang_String$$$_;
java_lang_String.valueOf_$_$_$$$$$_C$Ljava_lang_String$$$_ = java_lang_String_$_valueOf_$_$_$$$$$_C$Ljava_lang_String$$$_;
java_lang_String.valueOf_$_$_$$$$$_CII$Ljava_lang_String$$$_ = java_lang_String_$_valueOf_$_$_$$$$$_CII$Ljava_lang_String$$$_;
java_lang_String.prototype.wait_$_$J$V = java_lang_String_$_wait_$_$J$V;
java_lang_String.prototype.wait_$_$JI$V = java_lang_String_$_wait_$_$JI$V;
java_lang_String.prototype.wait_$_$$V = java_lang_String_$_wait_$_$$V;
java_lang_String.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_String_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_String.prototype.notify_$_$$V = java_lang_String_$_notify_$_$$V;
java_lang_String.prototype.notifyAll_$_$$V = java_lang_String_$_notifyAll_$_$$V;
