/** @constructor */
function java_text_Normalizer() {}
var java_text_Normalizer_$_isNormalized_$_$Ljava_lang_CharSequence$$$_Ljava_text_Normalizer$Form$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_text_Normalizer_$_isNormalized_$_$Ljava_lang_CharSequence$$$_Ljava_text_Normalizer$Form$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_text_Normalizer_$_normalize_$_$Ljava_lang_CharSequence$$$_Ljava_text_Normalizer$Form$$$_$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_text_Normalizer_$_normalize_$_$Ljava_lang_CharSequence$$$_Ljava_text_Normalizer$Form$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_text_Normalizer_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_text_Normalizer_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_text_Normalizer_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_text_Normalizer_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_text_Normalizer_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_text_Normalizer_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var java_text_Normalizer_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_text_Normalizer_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_text_Normalizer_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_text_Normalizer.isNormalized_$_$Ljava_lang_CharSequence$$$_Ljava_text_Normalizer$Form$$$_$Z = java_text_Normalizer_$_isNormalized_$_$Ljava_lang_CharSequence$$$_Ljava_text_Normalizer$Form$$$_$Z;
java_text_Normalizer.normalize_$_$Ljava_lang_CharSequence$$$_Ljava_text_Normalizer$Form$$$_$Ljava_lang_String$$$_ = java_text_Normalizer_$_normalize_$_$Ljava_lang_CharSequence$$$_Ljava_text_Normalizer$Form$$$_$Ljava_lang_String$$$_;
java_text_Normalizer.prototype.wait_$_$J$V = java_text_Normalizer_$_wait_$_$J$V;
java_text_Normalizer.prototype.wait_$_$JI$V = java_text_Normalizer_$_wait_$_$JI$V;
java_text_Normalizer.prototype.wait_$_$$V = java_text_Normalizer_$_wait_$_$$V;
java_text_Normalizer.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_text_Normalizer_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_text_Normalizer.prototype.toString_$_$$Ljava_lang_String$$$_ = java_text_Normalizer_$_toString_$_$$Ljava_lang_String$$$_;
java_text_Normalizer.prototype.hashCode_$_$$I = java_text_Normalizer_$_hashCode_$_$$I;
java_text_Normalizer.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_text_Normalizer_$_getClass_$_$$Ljava_lang_Class$$$_;
java_text_Normalizer.prototype.notify_$_$$V = java_text_Normalizer_$_notify_$_$$V;
java_text_Normalizer.prototype.notifyAll_$_$$V = java_text_Normalizer_$_notifyAll_$_$$V;
