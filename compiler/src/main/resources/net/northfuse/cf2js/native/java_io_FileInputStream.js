/** @constructor */
function java_io_FileInputStream() {}
var java_io_FileInputStream_$__$$$init$$$__$_$Ljava_io_File$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Creates a FileInputStream by
 opening a connection to an actual file,
 the file named by the File
 object file in the file system.
 A new FileDescriptor object
 is created to represent this file connection.
 
 First, if there is a security manager,
 its checkRead method  is called
 with the path represented by the file
 argument as its argument.
 
 If the named file does not exist, is a directory rather than a regular
 file, or for some other reason cannot be opened for reading then a
 FileNotFoundException is thrown.


Parameters:file - the file to be opened for reading.
Throws:
FileNotFoundException - if the file does not exist,
                   is a directory rather than a regular file,
                   or for some other reason cannot be opened for
                   reading.
SecurityException - if a security manager exists and its
               checkRead method denies read access to the file.See Also:File.getPath(), 
SecurityManager.checkRead(java.lang.String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$__$$$init$$$__$_$Ljava_io_File$$$_$V"));
throw ex;
};
var java_io_FileInputStream_$__$$$init$$$__$_$Ljava_io_FileDescriptor$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Creates a FileInputStream by using the file descriptor
 fdObj, which represents an existing connection to an
 actual file in the file system.
 
 If there is a security manager, its checkRead method is
 called with the file descriptor fdObj as its argument to
 see if it's ok to read the file descriptor. If read access is denied
 to the file descriptor a SecurityException is thrown.
 
 If fdObj is null then a NullPointerException
 is thrown.


Parameters:fdObj - the file descriptor to be opened for reading.
Throws:
SecurityException - if a security manager exists and its
                 checkRead method denies read access to the
                 file descriptor.See Also:SecurityManager.checkRead(java.io.FileDescriptor)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$__$$$init$$$__$_$Ljava_io_FileDescriptor$$$_$V"));
throw ex;
};
var java_io_FileInputStream_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Creates a FileInputStream by
 opening a connection to an actual file,
 the file named by the path name name
 in the file system.  A new FileDescriptor
 object is created to represent this file
 connection.
 
 First, if there is a security
 manager, its checkRead method
 is called with the name argument
 as its argument.
 
 If the named file does not exist, is a directory rather than a regular
 file, or for some other reason cannot be opened for reading then a
 FileNotFoundException is thrown.


Parameters:name - the system-dependent file name.
Throws:
FileNotFoundException - if the file does not exist,
                   is a directory rather than a regular file,
                   or for some other reason cannot be opened for
                   reading.
SecurityException - if a security manager exists and its
               checkRead method denies read access
               to the file.See Also:SecurityManager.checkRead(java.lang.String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$__$$$init$$$__$_$Ljava_lang_String$$$_$V"));
throw ex;
};
var java_io_FileInputStream_$_available_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns an estimate of the number of remaining bytes that can be read (or
 skipped over) from this input stream without blocking by the next
 invocation of a method for this input stream. The next invocation might be
 the same thread or another thread.  A single read or skip of this
 many bytes will not block, but may read or skip fewer bytes.

  In some cases, a non-blocking read (or skip) may appear to be
 blocked when it is merely slow, for example when reading large
 files over slow networks.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$_available_$_$$I"));
throw ex;
//must have a return of type int
};
var java_io_FileInputStream_$_close_$_$$V = function (arg0) {
/*implement method!*/
/*
Closes this file input stream and releases any system resources
 associated with the stream.

  If this stream has an associated channel then the channel is closed
 as well.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$_close_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_FileInputStream_$_finalize_$_$$V = function (arg0) {
/*implement method!*/
/*
Ensures that the close method of this file input stream is
 called when there are no more references to it.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$_finalize_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_FileInputStream_$_getChannel_$_$$Ljava_nio_channels_FileChannel$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the unique FileChannel
 object associated with this file input stream.

  The initial position of the returned channel will be equal to the
 number of bytes read from the file so far.  Reading bytes from this
 stream will increment the channel's position.  Changing the channel's
 position, either explicitly or by reading, will change this stream's
 file position.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$_getChannel_$_$$Ljava_nio_channels_FileChannel$$$_"));
throw ex;
//must have a return of type FileChannel
};
var java_io_FileInputStream_$_getFD_$_$$Ljava_io_FileDescriptor$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the FileDescriptor
 object  that represents the connection to
 the actual file in the file system being
 used by this FileInputStream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$_getFD_$_$$Ljava_io_FileDescriptor$$$_"));
throw ex;
//must have a return of type FileDescriptor
};
var java_io_FileInputStream_$_read_$_$$I = function (arg0) {
/*implement method!*/
/*
Reads a byte of data from this input stream. This method blocks
 if no input is yet available.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$_read_$_$$I"));
throw ex;
//must have a return of type int
};
var java_io_FileInputStream_$_read_$_$_$$$$$_B$I = function (arg0, arg1) {
/*implement method!*/
/*
Reads up to b.length bytes of data from this input
 stream into an array of bytes. This method blocks until some input
 is available.

Parameters:

b -> the buffer into which the data is read.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$_read_$_$_$$$$$_B$I"));
throw ex;
//must have a return of type int
};
var java_io_FileInputStream_$_read_$_$_$$$$$_BII$I = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Reads up to len bytes of data from this input stream
 into an array of bytes. If len is not zero, the method
 blocks until some input is available; otherwise, no
 bytes are read and 0 is returned.

Parameters:

b -> the buffer into which the data is read.

off -> the start offset in the destination array b

len -> the maximum number of bytes read.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$_read_$_$_$$$$$_BII$I"));
throw ex;
//must have a return of type int
};
var java_io_FileInputStream_$_skip_$_$J$J = function (arg0, arg1) {
/*implement method!*/
/*
Skips over and discards n bytes of data from the
 input stream.

 The skip method may, for a variety of
 reasons, end up skipping over some smaller number of bytes,
 possibly 0. If n is negative, an
 IOException is thrown, even though the skip
 method of the InputStream superclass does nothing in this case.
 The actual number of bytes skipped is returned.

 This method may skip more bytes than are remaining in the backing
 file. This produces no exception and the number of bytes skipped
 may include some number of bytes that were beyond the EOF of the
 backing file. Attempting to read from the stream after skipping past
 the end will result in -1 indicating the end of the file.

Parameters:

n -> the number of bytes to be skipped.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileInputStream_$_skip_$_$J$J"));
throw ex;
//must have a return of type long
};
var java_io_FileInputStream_$_mark_$_$I$V = function (arg0, arg1) {
/*invoke parent*/
return java_io_InputStream_$_mark_$_$I$V(arg0, arg1);};
var java_io_FileInputStream_$_reset_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_reset_$_$$V(arg0);};
var java_io_FileInputStream_$_markSupported_$_$$Z = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_markSupported_$_$$Z(arg0);};
var java_io_FileInputStream_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_io_InputStream_$_wait_$_$J$V(arg0, arg1);};
var java_io_FileInputStream_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_io_InputStream_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_io_FileInputStream_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_wait_$_$$V(arg0);};
var java_io_FileInputStream_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_io_InputStream_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_io_FileInputStream_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_io_FileInputStream_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_hashCode_$_$$I(arg0);};
var java_io_FileInputStream_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_io_FileInputStream_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_notify_$_$$V(arg0);};
var java_io_FileInputStream_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_notifyAll_$_$$V(arg0);};
java_io_FileInputStream.prototype._$$$init$$$__$_$Ljava_io_File$$$_$V = java_io_FileInputStream_$__$$$init$$$__$_$Ljava_io_File$$$_$V;
java_io_FileInputStream.prototype._$$$init$$$__$_$Ljava_io_FileDescriptor$$$_$V = java_io_FileInputStream_$__$$$init$$$__$_$Ljava_io_FileDescriptor$$$_$V;
java_io_FileInputStream.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_io_FileInputStream_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_io_FileInputStream.prototype.available_$_$$I = java_io_FileInputStream_$_available_$_$$I;
java_io_FileInputStream.prototype.close_$_$$V = java_io_FileInputStream_$_close_$_$$V;
java_io_FileInputStream.prototype.finalize_$_$$V = java_io_FileInputStream_$_finalize_$_$$V;
java_io_FileInputStream.prototype.getChannel_$_$$Ljava_nio_channels_FileChannel$$$_ = java_io_FileInputStream_$_getChannel_$_$$Ljava_nio_channels_FileChannel$$$_;
java_io_FileInputStream.prototype.getFD_$_$$Ljava_io_FileDescriptor$$$_ = java_io_FileInputStream_$_getFD_$_$$Ljava_io_FileDescriptor$$$_;
java_io_FileInputStream.prototype.read_$_$$I = java_io_FileInputStream_$_read_$_$$I;
java_io_FileInputStream.prototype.read_$_$_$$$$$_B$I = java_io_FileInputStream_$_read_$_$_$$$$$_B$I;
java_io_FileInputStream.prototype.read_$_$_$$$$$_BII$I = java_io_FileInputStream_$_read_$_$_$$$$$_BII$I;
java_io_FileInputStream.prototype.skip_$_$J$J = java_io_FileInputStream_$_skip_$_$J$J;
java_io_FileInputStream.prototype.mark_$_$I$V = java_io_FileInputStream_$_mark_$_$I$V;
java_io_FileInputStream.prototype.reset_$_$$V = java_io_FileInputStream_$_reset_$_$$V;
java_io_FileInputStream.prototype.markSupported_$_$$Z = java_io_FileInputStream_$_markSupported_$_$$Z;
java_io_FileInputStream.prototype.wait_$_$J$V = java_io_FileInputStream_$_wait_$_$J$V;
java_io_FileInputStream.prototype.wait_$_$JI$V = java_io_FileInputStream_$_wait_$_$JI$V;
java_io_FileInputStream.prototype.wait_$_$$V = java_io_FileInputStream_$_wait_$_$$V;
java_io_FileInputStream.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_io_FileInputStream_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_io_FileInputStream.prototype.toString_$_$$Ljava_lang_String$$$_ = java_io_FileInputStream_$_toString_$_$$Ljava_lang_String$$$_;
java_io_FileInputStream.prototype.hashCode_$_$$I = java_io_FileInputStream_$_hashCode_$_$$I;
java_io_FileInputStream.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_io_FileInputStream_$_getClass_$_$$Ljava_lang_Class$$$_;
java_io_FileInputStream.prototype.notify_$_$$V = java_io_FileInputStream_$_notify_$_$$V;
java_io_FileInputStream.prototype.notifyAll_$_$$V = java_io_FileInputStream_$_notifyAll_$_$$V;
