/** @constructor */
function java_lang_Math() {}
var java_lang_Math_$_IEEEremainder_$_$DD$D = function (arg0, arg1) {
/*implement method!*/
/*
Computes the remainder operation on two arguments as prescribed 
 by the IEEE 754 standard.
 The remainder value is mathematically equal to 
 f1 - f2 × n,
 where n is the mathematical integer closest to the exact 
 mathematical value of the quotient f1/f2, and if two 
 mathematical integers are equally close to f1/f2, 
 then n is the integer that is even. If the remainder is 
 zero, its sign is the same as the sign of the first argument. 
 Special cases:
 If either argument is NaN, or the first argument is infinite, 
 or the second argument is positive zero or negative zero, then the 
 result is NaN.
 If the first argument is finite and the second argument is 
 infinite, then the result is the same as the first argument.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_IEEEremainder_$_$DD$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_abs_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the absolute value of a double value.
 If the argument is not negative, the argument is returned.
 If the argument is negative, the negation of the argument is returned.
 Special cases:
 If the argument is positive zero or negative zero, the result 
 is positive zero. 
 If the argument is infinite, the result is positive infinity. 
 If the argument is NaN, the result is NaN.
 In other words, the result is the same as the value of the expression: 
 Double.longBitsToDouble((Double.doubleToLongBits(a)<<1)>>>1)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_abs_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_abs_$_$F$F = function (arg0) {
/*implement method!*/
/*
Returns the absolute value of a float value.
 If the argument is not negative, the argument is returned.
 If the argument is negative, the negation of the argument is returned.
 Special cases:
 If the argument is positive zero or negative zero, the 
 result is positive zero. 
 If the argument is infinite, the result is positive infinity. 
 If the argument is NaN, the result is NaN.
 In other words, the result is the same as the value of the expression: 
 Float.intBitsToFloat(0x7fffffff & Float.floatToIntBits(a))

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_abs_$_$F$F"));
throw ex;
//must have a return of type float
};
var java_lang_Math_$_abs_$_$I$I = function (arg0) {
/*implement method!*/
/*
Returns the absolute value of an int value.
 If the argument is not negative, the argument is returned.
 If the argument is negative, the negation of the argument is returned. 
 
 Note that if the argument is equal to the value of
 Integer.MIN_VALUE, the most negative representable
 int value, the result is that same value, which is
 negative.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_abs_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Math_$_abs_$_$J$J = function (arg0) {
/*implement method!*/
/*
Returns the absolute value of a long value.
 If the argument is not negative, the argument is returned.
 If the argument is negative, the negation of the argument is returned. 
 
 Note that if the argument is equal to the value of
 Long.MIN_VALUE, the most negative representable
 long value, the result is that same value, which
 is negative.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_abs_$_$J$J"));
throw ex;
//must have a return of type long
};
var java_lang_Math_$_acos_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the arc cosine of a value; the returned angle is in the
 range 0.0 through pi.  Special case:
 If the argument is NaN or its absolute value is greater 
 than 1, then the result is NaN.
 
 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_acos_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_asin_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the arc sine of a value; the returned angle is in the
 range -pi/2 through pi/2.  Special cases:
 If the argument is NaN or its absolute value is greater 
 than 1, then the result is NaN.
 If the argument is zero, then the result is a zero with the
 same sign as the argument.
 
 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_asin_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_atan2_$_$DD$D = function (arg0, arg1) {
/*implement method!*/
/*
Returns the angle theta from the conversion of rectangular
 coordinates (x, y) to polar
 coordinates (r, theta).
 This method computes the phase theta by computing an arc tangent
 of y/x in the range of -pi to pi. Special 
 cases:
 If either argument is NaN, then the result is NaN. 
 If the first argument is positive zero and the second argument 
 is positive, or the first argument is positive and finite and the 
 second argument is positive infinity, then the result is positive 
 zero. 
 If the first argument is negative zero and the second argument 
 is positive, or the first argument is negative and finite and the 
 second argument is positive infinity, then the result is negative zero. 
 If the first argument is positive zero and the second argument 
 is negative, or the first argument is positive and finite and the 
 second argument is negative infinity, then the result is the 
 double value closest to pi. 
 If the first argument is negative zero and the second argument 
 is negative, or the first argument is negative and finite and the 
 second argument is negative infinity, then the result is the 
 double value closest to -pi. 
 If the first argument is positive and the second argument is 
 positive zero or negative zero, or the first argument is positive 
 infinity and the second argument is finite, then the result is the 
 double value closest to pi/2. 
 If the first argument is negative and the second argument is 
 positive zero or negative zero, or the first argument is negative 
 infinity and the second argument is finite, then the result is the 
 double value closest to -pi/2. 
 If both arguments are positive infinity, then the result is the 
 double value closest to pi/4. 
 If the first argument is positive infinity and the second argument 
 is negative infinity, then the result is the double 
 value closest to 3*pi/4. 
 If the first argument is negative infinity and the second argument 
 is positive infinity, then the result is the double value 
 closest to -pi/4. 
 If both arguments are negative infinity, then the result is the 
 double value closest to -3*pi/4.
 
 The computed result must be within 2 ulps of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_atan2_$_$DD$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_atan_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the arc tangent of a value; the returned angle is in the
 range -pi/2 through pi/2.  Special cases:
 If the argument is NaN, then the result is NaN.
 If the argument is zero, then the result is a zero with the
 same sign as the argument.
 
 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_atan_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_cbrt_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the cube root of a double value.  For
 positive finite x, cbrt(-x) ==
 -cbrt(x); that is, the cube root of a negative value is
 the negative of the cube root of that value's magnitude.
 
 Special cases: 

 
 
 If the argument is NaN, then the result is NaN.

 If the argument is infinite, then the result is an infinity
 with the same sign as the argument.

 If the argument is zero, then the result is a zero with the
 same sign as the argument.
 
 

 The computed result must be within 1 ulp of the exact result.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_cbrt_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_ceil_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the smallest (closest to negative infinity)
 double value that is greater than or equal to the
 argument and is equal to a mathematical integer. Special cases:
 If the argument value is already equal to a
 mathematical integer, then the result is the same as the
 argument.  If the argument is NaN or an infinity or
 positive zero or negative zero, then the result is the same as
 the argument.  If the argument value is less than zero but
 greater than -1.0, then the result is negative zero. Note
 that the value of Math.ceil(x) is exactly the
 value of -Math.floor(-x).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_ceil_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_copySign_$_$DD$D = function (arg0, arg1) {
/*implement method!*/
/*
Returns the first floating-point argument with the sign of the
 second floating-point argument.  Note that unlike the StrictMath.copySign
 method, this method does not require NaN sign
 arguments to be treated as positive values; implementations are
 permitted to treat some NaN arguments as positive and other NaN
 arguments as negative to allow greater performance.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_copySign_$_$DD$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_copySign_$_$FF$F = function (arg0, arg1) {
/*implement method!*/
/*
Returns the first floating-point argument with the sign of the
 second floating-point argument.  Note that unlike the StrictMath.copySign
 method, this method does not require NaN sign
 arguments to be treated as positive values; implementations are
 permitted to treat some NaN arguments as positive and other NaN
 arguments as negative to allow greater performance.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_copySign_$_$FF$F"));
throw ex;
//must have a return of type float
};
var java_lang_Math_$_cos_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the trigonometric cosine of an angle. Special cases:
 If the argument is NaN or an infinity, then the 
 result is NaN.
 
 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_cos_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_cosh_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the hyperbolic cosine of a double value.
 The hyperbolic cosine of x is defined to be
 (ex + e-x)/2
 where e is Euler's number.

 Special cases:
 

 If the argument is NaN, then the result is NaN.

 If the argument is infinite, then the result is positive
 infinity.

 If the argument is zero, then the result is 1.0.

 

 The computed result must be within 2.5 ulps of the exact result.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_cosh_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_exp_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns Euler's number e raised to the power of a
 double value.  Special cases:
 If the argument is NaN, the result is NaN.
 If the argument is positive infinity, then the result is 
 positive infinity.
 If the argument is negative infinity, then the result is 
 positive zero.
 
 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_exp_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_expm1_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns ex -1.  Note that for values of
 x near 0, the exact sum of
 expm1(x) + 1 is much closer to the true
 result of ex than exp(x).

 Special cases:
 
 If the argument is NaN, the result is NaN.

 If the argument is positive infinity, then the result is
 positive infinity.

 If the argument is negative infinity, then the result is
 -1.0.

 If the argument is zero, then the result is a zero with the
 same sign as the argument.

 

 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.  The result of
 expm1 for any finite input must be greater than or
 equal to -1.0.  Note that once the exact result of
 ex - 1 is within 1/2
 ulp of the limit value -1, -1.0 should be
 returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_expm1_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_floor_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the largest (closest to positive infinity)
 double value that is less than or equal to the
 argument and is equal to a mathematical integer. Special cases:
 If the argument value is already equal to a
 mathematical integer, then the result is the same as the
 argument.  If the argument is NaN or an infinity or
 positive zero or negative zero, then the result is the same as
 the argument.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_floor_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_getExponent_$_$D$I = function (arg0) {
/*implement method!*/
/*
Returns the unbiased exponent used in the representation of a
 double.  Special cases:

 
 If the argument is NaN or infinite, then the result is
 Double.MAX_EXPONENT + 1.
 If the argument is zero or subnormal, then the result is
 Double.MIN_EXPONENT -1.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_getExponent_$_$D$I"));
throw ex;
//must have a return of type int
};
var java_lang_Math_$_getExponent_$_$F$I = function (arg0) {
/*implement method!*/
/*
Returns the unbiased exponent used in the representation of a
 float.  Special cases:

 
 If the argument is NaN or infinite, then the result is
 Float.MAX_EXPONENT + 1.
 If the argument is zero or subnormal, then the result is
 Float.MIN_EXPONENT -1.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_getExponent_$_$F$I"));
throw ex;
//must have a return of type int
};
var java_lang_Math_$_hypot_$_$DD$D = function (arg0, arg1) {
/*implement method!*/
/*
Returns sqrt(x2 +y2)
 without intermediate overflow or underflow.

 Special cases:
 

  If either argument is infinite, then the result
 is positive infinity.

  If either argument is NaN and neither argument is infinite,
 then the result is NaN.

 

 The computed result must be within 1 ulp of the exact
 result.  If one parameter is held constant, the results must be
 semi-monotonic in the other parameter.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_hypot_$_$DD$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_log10_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the base 10 logarithm of a double value.
 Special cases:

 If the argument is NaN or less than zero, then the result 
 is NaN.
 If the argument is positive infinity, then the result is 
 positive infinity.
 If the argument is positive zero or negative zero, then the 
 result is negative infinity.
  If the argument is equal to 10n for
 integer n, then the result is n.
 
 
 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_log10_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_log1p_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the natural logarithm of the sum of the argument and 1.
 Note that for small values x, the result of
 log1p(x) is much closer to the true result of ln(1
 + x) than the floating-point evaluation of
 log(1.0+x).

 Special cases:

 

 If the argument is NaN or less than -1, then the result is
 NaN.

 If the argument is positive infinity, then the result is
 positive infinity.

 If the argument is negative one, then the result is
 negative infinity.

 If the argument is zero, then the result is a zero with the
 same sign as the argument.

 

 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_log1p_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_log_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the natural logarithm (base e) of a double
 value.  Special cases:
 If the argument is NaN or less than zero, then the result 
 is NaN.
 If the argument is positive infinity, then the result is 
 positive infinity.
 If the argument is positive zero or negative zero, then the 
 result is negative infinity.
 
 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_log_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_max_$_$DD$D = function (arg0, arg1) {
/*implement method!*/
/*
Returns the greater of two double values.  That
 is, the result is the argument closer to positive infinity. If
 the arguments have the same value, the result is that same
 value. If either value is NaN, then the result is NaN.  Unlike
 the numerical comparison operators, this method considers
 negative zero to be strictly smaller than positive zero. If one
 argument is positive zero and the other negative zero, the
 result is positive zero.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_max_$_$DD$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_max_$_$FF$F = function (arg0, arg1) {
/*implement method!*/
/*
Returns the greater of two float values.  That is,
 the result is the argument closer to positive infinity. If the
 arguments have the same value, the result is that same
 value. If either value is NaN, then the result is NaN.  Unlike
 the numerical comparison operators, this method considers
 negative zero to be strictly smaller than positive zero. If one
 argument is positive zero and the other negative zero, the
 result is positive zero.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_max_$_$FF$F"));
throw ex;
//must have a return of type float
};
var java_lang_Math_$_max_$_$II$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the greater of two int values. That is, the 
 result is the argument closer to the value of 
 Integer.MAX_VALUE. If the arguments have the same value, 
 the result is that same value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_max_$_$II$I"));
throw ex;
//must have a return of type int
};
var java_lang_Math_$_max_$_$JJ$J = function (arg0, arg1) {
/*implement method!*/
/*
Returns the greater of two long values. That is, the 
 result is the argument closer to the value of 
 Long.MAX_VALUE. If the arguments have the same value, 
 the result is that same value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_max_$_$JJ$J"));
throw ex;
//must have a return of type long
};
var java_lang_Math_$_min_$_$DD$D = function (arg0, arg1) {
/*implement method!*/
/*
Returns the smaller of two double values.  That
 is, the result is the value closer to negative infinity. If the
 arguments have the same value, the result is that same
 value. If either value is NaN, then the result is NaN.  Unlike
 the numerical comparison operators, this method considers
 negative zero to be strictly smaller than positive zero. If one
 argument is positive zero and the other is negative zero, the
 result is negative zero.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_min_$_$DD$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_min_$_$FF$F = function (arg0, arg1) {
/*implement method!*/
/*
Returns the smaller of two float values.  That is,
 the result is the value closer to negative infinity. If the
 arguments have the same value, the result is that same
 value. If either value is NaN, then the result is NaN.  Unlike
 the numerical comparison operators, this method considers
 negative zero to be strictly smaller than positive zero.  If
 one argument is positive zero and the other is negative zero,
 the result is negative zero.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_min_$_$FF$F"));
throw ex;
//must have a return of type float
};
var java_lang_Math_$_min_$_$II$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the smaller of two int values. That is,
 the result the argument closer to the value of
 Integer.MIN_VALUE.  If the arguments have the same
 value, the result is that same value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_min_$_$II$I"));
throw ex;
//must have a return of type int
};
var java_lang_Math_$_min_$_$JJ$J = function (arg0, arg1) {
/*implement method!*/
/*
Returns the smaller of two long values. That is,
 the result is the argument closer to the value of
 Long.MIN_VALUE. If the arguments have the same
 value, the result is that same value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_min_$_$JJ$J"));
throw ex;
//must have a return of type long
};
var java_lang_Math_$_nextAfter_$_$DD$D = function (arg0, arg1) {
/*implement method!*/
/*
Returns the floating-point number adjacent to the first
 argument in the direction of the second argument.  If both
 arguments compare as equal the second argument is returned.

 
 Special cases:
 
  If either argument is a NaN, then NaN is returned.

  If both arguments are signed zeros, direction
 is returned unchanged (as implied by the requirement of
 returning the second argument if the arguments compare as
 equal).

  If start is
 ±Double.MIN_VALUE and direction
 has a value such that the result should have a smaller
 magnitude, then a zero with the same sign as start
 is returned.

  If start is infinite and
 direction has a value such that the result should
 have a smaller magnitude, Double.MAX_VALUE with the
 same sign as start is returned.

  If start is equal to ±
 Double.MAX_VALUE and direction has a
 value such that the result should have a larger magnitude, an
 infinity with same sign as start is returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_nextAfter_$_$DD$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_nextAfter_$_$FD$F = function (arg0, arg1) {
/*implement method!*/
/*
Returns the floating-point number adjacent to the first
 argument in the direction of the second argument.  If both
 arguments compare as equal a value equivalent to the second argument
 is returned.

 
 Special cases:
 
  If either argument is a NaN, then NaN is returned.

  If both arguments are signed zeros, a value equivalent
 to direction is returned.

  If start is
 ±Float.MIN_VALUE and direction
 has a value such that the result should have a smaller
 magnitude, then a zero with the same sign as start
 is returned.

  If start is infinite and
 direction has a value such that the result should
 have a smaller magnitude, Float.MAX_VALUE with the
 same sign as start is returned.

  If start is equal to ±
 Float.MAX_VALUE and direction has a
 value such that the result should have a larger magnitude, an
 infinity with same sign as start is returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_nextAfter_$_$FD$F"));
throw ex;
//must have a return of type float
};
var java_lang_Math_$_nextUp_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the floating-point value adjacent to d in
 the direction of positive infinity.  This method is
 semantically equivalent to nextAfter(d,
 Double.POSITIVE_INFINITY); however, a nextUp
 implementation may run faster than its equivalent
 nextAfter call.

 Special Cases:
 
  If the argument is NaN, the result is NaN.

  If the argument is positive infinity, the result is
 positive infinity.

  If the argument is zero, the result is
 Double.MIN_VALUE

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_nextUp_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_nextUp_$_$F$F = function (arg0) {
/*implement method!*/
/*
Returns the floating-point value adjacent to f in
 the direction of positive infinity.  This method is
 semantically equivalent to nextAfter(f,
 Float.POSITIVE_INFINITY); however, a nextUp
 implementation may run faster than its equivalent
 nextAfter call.

 Special Cases:
 
  If the argument is NaN, the result is NaN.

  If the argument is positive infinity, the result is
 positive infinity.

  If the argument is zero, the result is
 Float.MIN_VALUE

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_nextUp_$_$F$F"));
throw ex;
//must have a return of type float
};
var java_lang_Math_$_pow_$_$DD$D = function (arg0, arg1) {
/*implement method!*/
/*
Returns the value of the first argument raised to the power of the
 second argument. Special cases:

 If the second argument is positive or negative zero, then the 
 result is 1.0. 
 If the second argument is 1.0, then the result is the same as the 
 first argument.
 If the second argument is NaN, then the result is NaN. 
 If the first argument is NaN and the second argument is nonzero, 
 then the result is NaN. 

 If
 
 the absolute value of the first argument is greater than 1
 and the second argument is positive infinity, or
 the absolute value of the first argument is less than 1 and
 the second argument is negative infinity,
 
 then the result is positive infinity. 

 If 
 
 the absolute value of the first argument is greater than 1 and 
 the second argument is negative infinity, or 
 the absolute value of the 
 first argument is less than 1 and the second argument is positive 
 infinity,
 
 then the result is positive zero. 

 If the absolute value of the first argument equals 1 and the 
 second argument is infinite, then the result is NaN. 

 If 
 
 the first argument is positive zero and the second argument
 is greater than zero, or
 the first argument is positive infinity and the second
 argument is less than zero,
 
 then the result is positive zero. 

 If 
 
 the first argument is positive zero and the second argument
 is less than zero, or
 the first argument is positive infinity and the second
 argument is greater than zero,
 
 then the result is positive infinity.

 If 
 
 the first argument is negative zero and the second argument
 is greater than zero but not a finite odd integer, or
 the first argument is negative infinity and the second
 argument is less than zero but not a finite odd integer,
 
 then the result is positive zero. 

 If 
 
 the first argument is negative zero and the second argument
 is a positive finite odd integer, or
 the first argument is negative infinity and the second
 argument is a negative finite odd integer,
 
 then the result is negative zero. 

 If
 
 the first argument is negative zero and the second argument
 is less than zero but not a finite odd integer, or
 the first argument is negative infinity and the second
 argument is greater than zero but not a finite odd integer,
 
 then the result is positive infinity. 

 If 
 
 the first argument is negative zero and the second argument
 is a negative finite odd integer, or
 the first argument is negative infinity and the second
 argument is a positive finite odd integer,
 
 then the result is negative infinity. 

 If the first argument is finite and less than zero
 
  if the second argument is a finite even integer, the
 result is equal to the result of raising the absolute value of
 the first argument to the power of the second argument

 if the second argument is a finite odd integer, the result
 is equal to the negative of the result of raising the absolute
 value of the first argument to the power of the second
 argument

 if the second argument is finite and not an integer, then
 the result is NaN.
 

 If both arguments are integers, then the result is exactly equal 
 to the mathematical result of raising the first argument to the power 
 of the second argument if that result can in fact be represented 
 exactly as a double value.
 
 (In the foregoing descriptions, a floating-point value is
 considered to be an integer if and only if it is finite and a
 fixed point of the method ceil or,
 equivalently, a fixed point of the method floor. A value is a fixed point of a one-argument
 method if and only if the result of applying the method to the
 value is equal to the value.)

 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_pow_$_$DD$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_random_$_$$D = function () {
/*implement method!*/
/*
Returns a double value with a positive sign, greater 
 than or equal to 0.0 and less than 1.0. 
 Returned values are chosen pseudorandomly with (approximately) 
 uniform distribution from that range. 
 
 When this method is first called, it creates a single new
 pseudorandom-number generator, exactly as if by the expression
 new java.util.Random This
 new pseudorandom-number generator is used thereafter for all
 calls to this method and is used nowhere else.
 
 This method is properly synchronized to allow correct use by
 more than one thread. However, if many threads need to generate
 pseudorandom numbers at a great rate, it may reduce contention
 for each thread to have its own pseudorandom-number generator.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_random_$_$$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_rint_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the double value that is closest in value
 to the argument and is equal to a mathematical integer. If two
 double values that are mathematical integers are
 equally close, the result is the integer value that is
 even. Special cases:
 If the argument value is already equal to a mathematical 
 integer, then the result is the same as the argument. 
 If the argument is NaN or an infinity or positive zero or negative 
 zero, then the result is the same as the argument.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_rint_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_round_$_$D$J = function (arg0) {
/*implement method!*/
/*
Returns the closest long to the argument. The result 
 is rounded to an integer by adding 1/2, taking the floor of the 
 result, and casting the result to type long. In other 
 words, the result is equal to the value of the expression:
 (long)Math.floor(a + 0.5d)
 
 Special cases:
 If the argument is NaN, the result is 0.
 If the argument is negative infinity or any value less than or 
 equal to the value of Long.MIN_VALUE, the result is 
 equal to the value of Long.MIN_VALUE. 
 If the argument is positive infinity or any value greater than or 
 equal to the value of Long.MAX_VALUE, the result is 
 equal to the value of Long.MAX_VALUE.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_round_$_$D$J"));
throw ex;
//must have a return of type long
};
var java_lang_Math_$_round_$_$F$I = function (arg0) {
/*implement method!*/
/*
Returns the closest int to the argument. The 
 result is rounded to an integer by adding 1/2, taking the 
 floor of the result, and casting the result to type int. 
 In other words, the result is equal to the value of the expression:
 (int)Math.floor(a + 0.5f)
 
 Special cases:
 If the argument is NaN, the result is 0.
 If the argument is negative infinity or any value less than or 
 equal to the value of Integer.MIN_VALUE, the result is 
 equal to the value of Integer.MIN_VALUE. 
 If the argument is positive infinity or any value greater than or 
 equal to the value of Integer.MAX_VALUE, the result is 
 equal to the value of Integer.MAX_VALUE.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_round_$_$F$I"));
throw ex;
//must have a return of type int
};
var java_lang_Math_$_scalb_$_$DI$D = function (arg0, arg1) {
/*implement method!*/
/*
Return d ×
 2scaleFactor rounded as if performed
 by a single correctly rounded floating-point multiply to a
 member of the double value set.  See the Java
 Language Specification for a discussion of floating-point
 value sets.  If the exponent of the result is between Double.MIN_EXPONENT and Double.MAX_EXPONENT, the
 answer is calculated exactly.  If the exponent of the result
 would be larger than Double.MAX_EXPONENT, an
 infinity is returned.  Note that if the result is subnormal,
 precision may be lost; that is, when scalb(x, n)
 is subnormal, scalb(scalb(x, n), -n) may not equal
 x.  When the result is non-NaN, the result has the same
 sign as d.


 Special cases:
 
  If the first argument is NaN, NaN is returned.
  If the first argument is infinite, then an infinity of the
 same sign is returned.
  If the first argument is zero, then a zero of the same
 sign is returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_scalb_$_$DI$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_scalb_$_$FI$F = function (arg0, arg1) {
/*implement method!*/
/*
Return f ×
 2scaleFactor rounded as if performed
 by a single correctly rounded floating-point multiply to a
 member of the float value set.  See the Java
 Language Specification for a discussion of floating-point
 value sets.  If the exponent of the result is between Float.MIN_EXPONENT and Float.MAX_EXPONENT, the
 answer is calculated exactly.  If the exponent of the result
 would be larger than Float.MAX_EXPONENT, an
 infinity is returned.  Note that if the result is subnormal,
 precision may be lost; that is, when scalb(x, n)
 is subnormal, scalb(scalb(x, n), -n) may not equal
 x.  When the result is non-NaN, the result has the same
 sign as f.


 Special cases:
 
  If the first argument is NaN, NaN is returned.
  If the first argument is infinite, then an infinity of the
 same sign is returned.
  If the first argument is zero, then a zero of the same
 sign is returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_scalb_$_$FI$F"));
throw ex;
//must have a return of type float
};
var java_lang_Math_$_signum_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the signum function of the argument; zero if the argument
 is zero, 1.0 if the argument is greater than zero, -1.0 if the
 argument is less than zero.

 Special Cases:
 
  If the argument is NaN, then the result is NaN.
  If the argument is positive zero or negative zero, then the
      result is the same as the argument.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_signum_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_signum_$_$F$F = function (arg0) {
/*implement method!*/
/*
Returns the signum function of the argument; zero if the argument
 is zero, 1.0f if the argument is greater than zero, -1.0f if the
 argument is less than zero.

 Special Cases:
 
  If the argument is NaN, then the result is NaN.
  If the argument is positive zero or negative zero, then the
      result is the same as the argument.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_signum_$_$F$F"));
throw ex;
//must have a return of type float
};
var java_lang_Math_$_sin_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the trigonometric sine of an angle.  Special cases:
 If the argument is NaN or an infinity, then the 
 result is NaN.
 If the argument is zero, then the result is a zero with the
 same sign as the argument.
 
 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_sin_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_sinh_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the hyperbolic sine of a double value.
 The hyperbolic sine of x is defined to be
 (ex - e-x)/2
 where e is Euler's number.

 Special cases:
 

 If the argument is NaN, then the result is NaN.

 If the argument is infinite, then the result is an infinity
 with the same sign as the argument.

 If the argument is zero, then the result is a zero with the
 same sign as the argument.

 

 The computed result must be within 2.5 ulps of the exact result.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_sinh_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_sqrt_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the correctly rounded positive square root of a 
 double value.
 Special cases:
 If the argument is NaN or less than zero, then the result 
 is NaN. 
 If the argument is positive infinity, then the result is positive 
 infinity. 
 If the argument is positive zero or negative zero, then the 
 result is the same as the argument.
 Otherwise, the result is the double value closest to 
 the true mathematical square root of the argument value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_sqrt_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_tan_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the trigonometric tangent of an angle.  Special cases:
 If the argument is NaN or an infinity, then the result 
 is NaN.
 If the argument is zero, then the result is a zero with the
 same sign as the argument.
 
 The computed result must be within 1 ulp of the exact result.
 Results must be semi-monotonic.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_tan_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_tanh_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the hyperbolic tangent of a double value.
 The hyperbolic tangent of x is defined to be
 (ex - e-x)/(ex + e-x),
 in other words, sinh(x)/cosh(x).  Note
 that the absolute value of the exact tanh is always less than
 1.

 Special cases:
 

 If the argument is NaN, then the result is NaN.

 If the argument is zero, then the result is a zero with the
 same sign as the argument.

 If the argument is positive infinity, then the result is
 +1.0.

 If the argument is negative infinity, then the result is
 -1.0.
  
 

 The computed result must be within 2.5 ulps of the exact result.
 The result of tanh for any finite input must have
 an absolute value less than or equal to 1.  Note that once the
 exact result of tanh is within 1/2 of an ulp of the limit value
 of ±1, correctly signed ±1.0 should
 be returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_tanh_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_toDegrees_$_$D$D = function (arg0) {
/*implement method!*/
/*
Converts an angle measured in radians to an approximately
 equivalent angle measured in degrees.  The conversion from
 radians to degrees is generally inexact; users should
 not expect cos(toRadians(90.0)) to exactly
 equal 0.0.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_toDegrees_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_toRadians_$_$D$D = function (arg0) {
/*implement method!*/
/*
Converts an angle measured in degrees to an approximately
 equivalent angle measured in radians.  The conversion from
 degrees to radians is generally inexact.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_toRadians_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_ulp_$_$D$D = function (arg0) {
/*implement method!*/
/*
Returns the size of an ulp of the argument.  An ulp of a
 double value is the positive distance between this
 floating-point value and the double value next
 larger in magnitude.  Note that for non-NaN x,
 ulp(-x) == ulp(x).
 
 Special Cases:
 
  If the argument is NaN, then the result is NaN.
  If the argument is positive or negative infinity, then the
 result is positive infinity.
  If the argument is positive or negative zero, then the result is
 Double.MIN_VALUE.
  If the argument is ±Double.MAX_VALUE, then
 the result is equal to 2971.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_ulp_$_$D$D"));
throw ex;
//must have a return of type double
};
var java_lang_Math_$_ulp_$_$F$F = function (arg0) {
/*implement method!*/
/*
Returns the size of an ulp of the argument.  An ulp of a
 float value is the positive distance between this
 floating-point value and the float value next
 larger in magnitude.  Note that for non-NaN x,
 ulp(-x) == ulp(x).
 
 Special Cases:
 
  If the argument is NaN, then the result is NaN.
  If the argument is positive or negative infinity, then the
 result is positive infinity.
  If the argument is positive or negative zero, then the result is
 Float.MIN_VALUE.
  If the argument is ±Float.MAX_VALUE, then
 the result is equal to 2104.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Math_$_ulp_$_$F$F"));
throw ex;
//must have a return of type float
};
var java_lang_Math_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_lang_Math_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_Math_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_lang_Math_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_lang_Math_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_lang_Math_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var java_lang_Math_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_Math_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_lang_Math_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_lang_Math.IEEEremainder_$_$DD$D = java_lang_Math_$_IEEEremainder_$_$DD$D;
java_lang_Math.abs_$_$D$D = java_lang_Math_$_abs_$_$D$D;
java_lang_Math.abs_$_$F$F = java_lang_Math_$_abs_$_$F$F;
java_lang_Math.abs_$_$I$I = java_lang_Math_$_abs_$_$I$I;
java_lang_Math.abs_$_$J$J = java_lang_Math_$_abs_$_$J$J;
java_lang_Math.acos_$_$D$D = java_lang_Math_$_acos_$_$D$D;
java_lang_Math.asin_$_$D$D = java_lang_Math_$_asin_$_$D$D;
java_lang_Math.atan2_$_$DD$D = java_lang_Math_$_atan2_$_$DD$D;
java_lang_Math.atan_$_$D$D = java_lang_Math_$_atan_$_$D$D;
java_lang_Math.cbrt_$_$D$D = java_lang_Math_$_cbrt_$_$D$D;
java_lang_Math.ceil_$_$D$D = java_lang_Math_$_ceil_$_$D$D;
java_lang_Math.copySign_$_$DD$D = java_lang_Math_$_copySign_$_$DD$D;
java_lang_Math.copySign_$_$FF$F = java_lang_Math_$_copySign_$_$FF$F;
java_lang_Math.cos_$_$D$D = java_lang_Math_$_cos_$_$D$D;
java_lang_Math.cosh_$_$D$D = java_lang_Math_$_cosh_$_$D$D;
java_lang_Math.exp_$_$D$D = java_lang_Math_$_exp_$_$D$D;
java_lang_Math.expm1_$_$D$D = java_lang_Math_$_expm1_$_$D$D;
java_lang_Math.floor_$_$D$D = java_lang_Math_$_floor_$_$D$D;
java_lang_Math.getExponent_$_$D$I = java_lang_Math_$_getExponent_$_$D$I;
java_lang_Math.getExponent_$_$F$I = java_lang_Math_$_getExponent_$_$F$I;
java_lang_Math.hypot_$_$DD$D = java_lang_Math_$_hypot_$_$DD$D;
java_lang_Math.log10_$_$D$D = java_lang_Math_$_log10_$_$D$D;
java_lang_Math.log1p_$_$D$D = java_lang_Math_$_log1p_$_$D$D;
java_lang_Math.log_$_$D$D = java_lang_Math_$_log_$_$D$D;
java_lang_Math.max_$_$DD$D = java_lang_Math_$_max_$_$DD$D;
java_lang_Math.max_$_$FF$F = java_lang_Math_$_max_$_$FF$F;
java_lang_Math.max_$_$II$I = java_lang_Math_$_max_$_$II$I;
java_lang_Math.max_$_$JJ$J = java_lang_Math_$_max_$_$JJ$J;
java_lang_Math.min_$_$DD$D = java_lang_Math_$_min_$_$DD$D;
java_lang_Math.min_$_$FF$F = java_lang_Math_$_min_$_$FF$F;
java_lang_Math.min_$_$II$I = java_lang_Math_$_min_$_$II$I;
java_lang_Math.min_$_$JJ$J = java_lang_Math_$_min_$_$JJ$J;
java_lang_Math.nextAfter_$_$DD$D = java_lang_Math_$_nextAfter_$_$DD$D;
java_lang_Math.nextAfter_$_$FD$F = java_lang_Math_$_nextAfter_$_$FD$F;
java_lang_Math.nextUp_$_$D$D = java_lang_Math_$_nextUp_$_$D$D;
java_lang_Math.nextUp_$_$F$F = java_lang_Math_$_nextUp_$_$F$F;
java_lang_Math.pow_$_$DD$D = java_lang_Math_$_pow_$_$DD$D;
java_lang_Math.random_$_$$D = java_lang_Math_$_random_$_$$D;
java_lang_Math.rint_$_$D$D = java_lang_Math_$_rint_$_$D$D;
java_lang_Math.round_$_$D$J = java_lang_Math_$_round_$_$D$J;
java_lang_Math.round_$_$F$I = java_lang_Math_$_round_$_$F$I;
java_lang_Math.scalb_$_$DI$D = java_lang_Math_$_scalb_$_$DI$D;
java_lang_Math.scalb_$_$FI$F = java_lang_Math_$_scalb_$_$FI$F;
java_lang_Math.signum_$_$D$D = java_lang_Math_$_signum_$_$D$D;
java_lang_Math.signum_$_$F$F = java_lang_Math_$_signum_$_$F$F;
java_lang_Math.sin_$_$D$D = java_lang_Math_$_sin_$_$D$D;
java_lang_Math.sinh_$_$D$D = java_lang_Math_$_sinh_$_$D$D;
java_lang_Math.sqrt_$_$D$D = java_lang_Math_$_sqrt_$_$D$D;
java_lang_Math.tan_$_$D$D = java_lang_Math_$_tan_$_$D$D;
java_lang_Math.tanh_$_$D$D = java_lang_Math_$_tanh_$_$D$D;
java_lang_Math.toDegrees_$_$D$D = java_lang_Math_$_toDegrees_$_$D$D;
java_lang_Math.toRadians_$_$D$D = java_lang_Math_$_toRadians_$_$D$D;
java_lang_Math.ulp_$_$D$D = java_lang_Math_$_ulp_$_$D$D;
java_lang_Math.ulp_$_$F$F = java_lang_Math_$_ulp_$_$F$F;
java_lang_Math.prototype.wait_$_$J$V = java_lang_Math_$_wait_$_$J$V;
java_lang_Math.prototype.wait_$_$JI$V = java_lang_Math_$_wait_$_$JI$V;
java_lang_Math.prototype.wait_$_$$V = java_lang_Math_$_wait_$_$$V;
java_lang_Math.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_Math_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_Math.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_Math_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_Math.prototype.hashCode_$_$$I = java_lang_Math_$_hashCode_$_$$I;
java_lang_Math.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_Math_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_Math.prototype.notify_$_$$V = java_lang_Math_$_notify_$_$$V;
java_lang_Math.prototype.notifyAll_$_$$V = java_lang_Math_$_notifyAll_$_$$V;
