/** @constructor */
function java_lang_Double() {}
var java_lang_Double_$__$$$init$$$__$_$D$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Double object that
 represents the primitive double argument.


Parameters:value - the value to be represented by the Double.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$__$$$init$$$__$_$D$V"));
throw ex;
};
var java_lang_Double_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Double object that
 represents the floating-point value of type double
 represented by the string. The string is converted to a
 double value as if by the valueOf method.


Parameters:s - a string to be converted to a Double.
Throws:
NumberFormatException - if the string does not contain a
               parsable number.See Also:valueOf(java.lang.String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$__$$$init$$$__$_$Ljava_lang_String$$$_$V"));
throw ex;
};
var java_lang_Double_$_byteValue_$_$$B = function (arg0) {
/*implement method!*/
/*
Returns the value of this Double as a byte (by
 casting to a byte).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_byteValue_$_$$B"));
throw ex;
//must have a return of type byte
};
var java_lang_Double_$_compareTo_$_$Ljava_lang_Double$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Compares two Double objects numerically.  There
 are two ways in which comparisons performed by this method
 differ from those performed by the Java language numerical
 comparison operators (<, <=, ==, >= >)
 when applied to primitive double values:
 
                Double.NaN is considered by this method
                to be equal to itself and greater than all other
                double values (including
                Double.POSITIVE_INFINITY).
 
                0.0d is considered by this method to be greater
                than -0.0d.
 
 This ensures that the natural ordering of
 Double objects imposed by this method is consistent
 with equals.

Parameters:

anotherDouble -> the Double to be compared.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_compareTo_$_$Ljava_lang_Double$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Double_$_compareTo_$_$Ljava_lang_Object$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_compareTo_$_$Ljava_lang_Object$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Double_$_compare_$_$DD$I = function (arg0, arg1) {
/*implement method!*/
/*
Compares the two specified double values. The sign
 of the integer value returned is the same as that of the
 integer that would be returned by the call:
     new Double(d1).compareTo(new Double(d2))

Parameters:

d1 -> the first double to compare

d2 -> the second double to compare

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_compare_$_$DD$I"));
throw ex;
//must have a return of type int
};
var java_lang_Double_$_doubleToLongBits_$_$D$J = function (arg0) {
/*implement method!*/
/*
Returns a representation of the specified floating-point value
 according to the IEEE 754 floating-point "double
 format" bit layout.
 
 Bit 63 (the bit that is selected by the mask 
 0x8000000000000000L) represents the sign of the 
 floating-point number. Bits 
 62-52 (the bits that are selected by the mask 
 0x7ff0000000000000L) represent the exponent. Bits 51-0 
 (the bits that are selected by the mask 
 0x000fffffffffffffL) represent the significand 
 (sometimes called the mantissa) of the floating-point number. 
 
 If the argument is positive infinity, the result is
 0x7ff0000000000000L.
 
 If the argument is negative infinity, the result is
 0xfff0000000000000L.
 
 If the argument is NaN, the result is 
 0x7ff8000000000000L. 
 
 In all cases, the result is a long integer that, when 
 given to the longBitsToDouble(long) method, will produce a 
 floating-point value the same as the argument to 
 doubleToLongBits (except all NaN values are
 collapsed to a single "canonical" NaN value).

Parameters:

value -> a double precision floating-point number.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_doubleToLongBits_$_$D$J"));
throw ex;
//must have a return of type long
};
var java_lang_Double_$_doubleToRawLongBits_$_$D$J = function (arg0) {
/*implement method!*/
/*
Returns a representation of the specified floating-point value
 according to the IEEE 754 floating-point "double
 format" bit layout, preserving Not-a-Number (NaN) values.
 
 Bit 63 (the bit that is selected by the mask 
 0x8000000000000000L) represents the sign of the 
 floating-point number. Bits 
 62-52 (the bits that are selected by the mask 
 0x7ff0000000000000L) represent the exponent. Bits 51-0 
 (the bits that are selected by the mask 
 0x000fffffffffffffL) represent the significand 
 (sometimes called the mantissa) of the floating-point number. 
 
 If the argument is positive infinity, the result is
 0x7ff0000000000000L.
 
 If the argument is negative infinity, the result is
 0xfff0000000000000L.
 
 If the argument is NaN, the result is the long
 integer representing the actual NaN value.  Unlike the
 doubleToLongBits method,
 doubleToRawLongBits does not collapse all the bit
 patterns encoding a NaN to a single "canonical" NaN
 value.
 
 In all cases, the result is a long integer that,
 when given to the longBitsToDouble(long) method, will
 produce a floating-point value the same as the argument to
 doubleToRawLongBits.

Parameters:

value -> a double precision floating-point number.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_doubleToRawLongBits_$_$D$J"));
throw ex;
//must have a return of type long
};
var java_lang_Double_$_doubleValue_$_$$D = function (arg0) {
/*implement method!*/
/*
Returns the double value of this
 Double object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_doubleValue_$_$$D"));
throw ex;
//must have a return of type double
};
var java_lang_Double_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Compares this object against the specified object.  The result
 is true if and only if the argument is not
 null and is a Double object that
 represents a double that has the same value as the
 double represented by this object. For this
 purpose, two double values are considered to be
 the same if and only if the method doubleToLongBits(double) returns the identical
 long value when applied to each.
 
 Note that in most cases, for two instances of class
 Double, d1 and d2, the
 value of d1.equals(d2) is true if and
 only if
    d1.doubleValue() == d2.doubleValue()
 
 
 also has the value true. However, there are two
 exceptions:
 
 If d1 and d2 both represent
     Double.NaN, then the equals method
     returns true, even though
     Double.NaN==Double.NaN has the value
     false.
 If d1 represents +0.0 while
     d2 represents -0.0, or vice versa,
     the equal test has the value false,
     even though +0.0==-0.0 has the value true.
 
 This definition allows hash tables to operate properly.

Parameters:

obj -> the object to compare with.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_equals_$_$Ljava_lang_Object$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Double_$_floatValue_$_$$F = function (arg0) {
/*implement method!*/
/*
Returns the float value of this
 Double object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_floatValue_$_$$F"));
throw ex;
//must have a return of type float
};
var java_lang_Double_$_hashCode_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns a hash code for this Double object. The
 result is the exclusive OR of the two halves of the
 long integer bit representation, exactly as
 produced by the method doubleToLongBits(double), of
 the primitive double value represented by this
 Double object. That is, the hash code is the value
 of the expression:
  (int)(v^(v>>>32))
 
 where v is defined by: 
  long v = Double.doubleToLongBits(this.doubleValue());

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_hashCode_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Double_$_intValue_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the value of this Double as an
 int (by casting to type int).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_intValue_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Double_$_isInfinite_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if this Double value is
 infinitely large in magnitude, false otherwise.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_isInfinite_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Double_$_isInfinite_$_$D$Z = function (arg0) {
/*implement method!*/
/*
Returns true if the specified number is infinitely
 large in magnitude, false otherwise.

Parameters:

v -> the value to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_isInfinite_$_$D$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Double_$_isNaN_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if this Double value is
 a Not-a-Number (NaN), false otherwise.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_isNaN_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Double_$_isNaN_$_$D$Z = function (arg0) {
/*implement method!*/
/*
Returns true if the specified number is a
 Not-a-Number (NaN) value, false otherwise.

Parameters:

v -> the value to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_isNaN_$_$D$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Double_$_longBitsToDouble_$_$J$D = function (arg0) {
/*implement method!*/
/*
Returns the double value corresponding to a given
 bit representation.
 The argument is considered to be a representation of a
 floating-point value according to the IEEE 754 floating-point
 "double format" bit layout.
 
 If the argument is 0x7ff0000000000000L, the result 
 is positive infinity. 
 
 If the argument is 0xfff0000000000000L, the result 
 is negative infinity. 
 
 If the argument is any value in the range
 0x7ff0000000000001L through
 0x7fffffffffffffffL or in the range
 0xfff0000000000001L through
 0xffffffffffffffffL, the result is a NaN.  No IEEE
 754 floating-point operation provided by Java can distinguish
 between two NaN values of the same type with different bit
 patterns.  Distinct values of NaN are only distinguishable by
 use of the Double.doubleToRawLongBits method.
 
 In all other cases, let s, e, and m be three 
 values that can be computed from the argument: 
  int s = ((bits >> 63) == 0) ? 1 : -1;
 int e = (int)((bits >> 52) & 0x7ffL);
 long m = (e == 0) ?
                 (bits & 0xfffffffffffffL) << 1 :
                 (bits & 0xfffffffffffffL) | 0x10000000000000L;
 
 Then the floating-point result equals the value of the mathematical 
 expression s·m·2e-1075.

 Note that this method may not be able to return a
 double NaN with exactly same bit pattern as the
 long argument.  IEEE 754 distinguishes between two
 kinds of NaNs, quiet NaNs and signaling NaNs.  The
 differences between the two kinds of NaN are generally not
 visible in Java.  Arithmetic operations on signaling NaNs turn
 them into quiet NaNs with a different, but often similar, bit
 pattern.  However, on some processors merely copying a
 signaling NaN also performs that conversion.  In particular,
 copying a signaling NaN to return it to the calling method
 may perform this conversion.  So longBitsToDouble
 may not be able to return a double with a
 signaling NaN bit pattern.  Consequently, for some
 long values,
 doubleToRawLongBits(longBitsToDouble(start)) may
 not equal start.  Moreover, which
 particular bit patterns represent signaling NaNs is platform
 dependent; although all NaN bit patterns, quiet or signaling,
 must be in the NaN range identified above.

Parameters:

bits -> any long integer.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_longBitsToDouble_$_$J$D"));
throw ex;
//must have a return of type double
};
var java_lang_Double_$_longValue_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the value of this Double as a
 long (by casting to type long).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_longValue_$_$$J"));
throw ex;
//must have a return of type long
};
var java_lang_Double_$_parseDouble_$_$Ljava_lang_String$$$_$D = function (arg0) {
/*implement method!*/
/*
Returns a new double initialized to the value
 represented by the specified String, as performed
 by the valueOf method of class
 Double.

Parameters:

s -> the string to be parsed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_parseDouble_$_$Ljava_lang_String$$$_$D"));
throw ex;
//must have a return of type double
};
var java_lang_Double_$_shortValue_$_$$S = function (arg0) {
/*implement method!*/
/*
Returns the value of this Double as a
 short (by casting to a short).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_shortValue_$_$$S"));
throw ex;
//must have a return of type short
};
var java_lang_Double_$_toHexString_$_$D$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a hexadecimal string representation of the
 double argument. All characters mentioned below
 are ASCII characters.

 
 If the argument is NaN, the result is the string
     "NaN".
 Otherwise, the result is a string that represents the sign
 and magnitude of the argument. If the sign is negative, the
 first character of the result is '-'
 ('\u002D'); if the sign is positive, no sign
 character appears in the result. As for the magnitude m:

  
 If m is infinity, it is represented by the string
 "Infinity"; thus, positive infinity produces the
 result "Infinity" and negative infinity produces
 the result "-Infinity".

 If m is zero, it is represented by the string
 "0x0.0p0"; thus, negative zero produces the result
 "-0x0.0p0" and positive zero produces the result
 "0x0.0p0".

 If m is a double value with a
 normalized representation, substrings are used to represent the
 significand and exponent fields.  The significand is
 represented by the characters "0x1."
 followed by a lowercase hexadecimal representation of the rest
 of the significand as a fraction.  Trailing zeros in the
 hexadecimal representation are removed unless all the digits
 are zero, in which case a single zero is used. Next, the
 exponent is represented by "p" followed
 by a decimal string of the unbiased exponent as if produced by
 a call to Integer.toString on the
 exponent value.

 If m is a double value with a subnormal
 representation, the significand is represented by the
 characters "0x0." followed by a
 hexadecimal representation of the rest of the significand as a
 fraction.  Trailing zeros in the hexadecimal representation are
 removed. Next, the exponent is represented by
 "p-1022".  Note that there must be at
 least one nonzero digit in a subnormal significand.

 
 
 

 
 Examples
 Floating-point ValueHexadecimal String
 1.0  0x1.0p0
 -1.0 -0x1.0p0
 2.0  0x1.0p1
 3.0  0x1.8p1
 0.5  0x1.0p-1
 0.25 0x1.0p-2
 Double.MAX_VALUE
     0x1.fffffffffffffp1023
 Minimum Normal Value
     0x1.0p-1022
 Maximum Subnormal Value
     0x0.fffffffffffffp-1022
 Double.MIN_VALUE
     0x0.0000000000001p-1022

Parameters:

d -> the double to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_toHexString_$_$D$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Double_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of this Double object.
 The primitive double value represented by this
 object is converted to a string exactly as if by the method
 toString of one argument.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_toString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Double_$_toString_$_$D$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of the double 
 argument. All characters mentioned below are ASCII characters.
 
 If the argument is NaN, the result is the string
     "NaN".
 Otherwise, the result is a string that represents the sign and 
 magnitude (absolute value) of the argument. If the sign is negative, 
 the first character of the result is '-' 
 ('\u002D'); if the sign is positive, no sign character 
 appears in the result. As for the magnitude m:
 
 If m is infinity, it is represented by the characters 
 "Infinity"; thus, positive infinity produces the result 
 "Infinity" and negative infinity produces the result 
 "-Infinity".

 If m is zero, it is represented by the characters 
 "0.0"; thus, negative zero produces the result 
 "-0.0" and positive zero produces the result 
 "0.0". 

 If m is greater than or equal to 10-3 but less 
 than 107, then it is represented as the integer part of 
 m, in decimal form with no leading zeroes, followed by 
 '.' ('\u002E'), followed by one or 
 more decimal digits representing the fractional part of m. 

 If m is less than 10-3 or greater than or
 equal to 107, then it is represented in so-called
 "computerized scientific notation." Let n be the unique
 integer such that 10n <= m <
 10n+1; then let a be the
 mathematically exact quotient of m and
 10n so that 1 <= a < 10. The
 magnitude is then represented as the integer part of a,
 as a single decimal digit, followed by '.'
 ('\u002E'), followed by decimal digits
 representing the fractional part of a, followed by the
 letter 'E' ('\u0045'), followed
 by a representation of n as a decimal integer, as
 produced by the method Integer.toString(int).
 
 
 How many digits must be printed for the fractional part of 
 m or a? There must be at least one digit to represent 
 the fractional part, and beyond that as many, but only as many, more 
 digits as are needed to uniquely distinguish the argument value from
 adjacent values of type double. That is, suppose that 
 x is the exact mathematical value represented by the decimal 
 representation produced by this method for a finite nonzero argument 
 d. Then d must be the double value nearest 
 to x; or if two double values are equally close 
 to x, then d must be one of them and the least
 significant bit of the significand of d must be 0.
 
 To create localized string representations of a floating-point
 value, use subclasses of NumberFormat.

Parameters:

d -> the double to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_toString_$_$D$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Double_$_valueOf_$_$D$Ljava_lang_Double$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a Double instance representing the specified
 double value.
 If a new Double instance is not required, this method
 should generally be used in preference to the constructor
 Double(double), as this method is likely to yield
 significantly better space and time performance by caching
 frequently requested values.

Parameters:

d -> a double value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_valueOf_$_$D$Ljava_lang_Double$$$_"));
throw ex;
//must have a return of type Double
};
var java_lang_Double_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Double$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a Double object holding the
 double value represented by the argument string
 s.
 
 If s is null, then a 
 NullPointerException is thrown.

 Leading and trailing whitespace characters in s
 are ignored.  Whitespace is removed as if by the String.trim() method; that is, both ASCII space and control
 characters are removed. The rest of s should
 constitute a FloatValue as described by the lexical
 syntax rules:

 
 
 FloatValue:
 Signopt NaN
 Signopt Infinity
 Signopt FloatingPointLiteral
 Signopt HexFloatingPointLiteral
 SignedInteger
 

 

 
 HexFloatingPointLiteral:
  HexSignificand BinaryExponent FloatTypeSuffixopt
 

 

 
 HexSignificand:
 HexNumeral
 HexNumeral .
 0x HexDigitsopt 
     . HexDigits
 0X HexDigitsopt 
     . HexDigits
 

 

 
 BinaryExponent:
 BinaryExponentIndicator SignedInteger
 

 

 
 BinaryExponentIndicator:
 p
 P
 

 

 where Sign, FloatingPointLiteral,
 HexNumeral, HexDigits, SignedInteger and
 FloatTypeSuffix are as defined in the lexical structure
 sections of the of the Java Language
 Specification. If s does not have the form of
 a FloatValue, then a NumberFormatException
 is thrown. Otherwise, s is regarded as
 representing an exact decimal value in the usual
 "computerized scientific notation" or as an exact
 hexadecimal value; this exact numerical value is then
 conceptually converted to an "infinitely precise"
 binary value that is then rounded to type double
 by the usual round-to-nearest rule of IEEE 754 floating-point
 arithmetic, which includes preserving the sign of a zero
 value. Finally, a Double object representing this
 double value is returned.

  To interpret localized string representations of a
 floating-point value, use subclasses of NumberFormat.

 Note that trailing format specifiers, specifiers that
 determine the type of a floating-point literal
 (1.0f is a float value;
 1.0d is a double value), do
 not influence the results of this method.  In other
 words, the numerical value of the input string is converted
 directly to the target floating-point type.  The two-step
 sequence of conversions, string to float followed
 by float to double, is not
 equivalent to converting a string directly to
 double. For example, the float
 literal 0.1f is equal to the double
 value 0.10000000149011612; the float
 literal 0.1f represents a different numerical
 value than the double literal
 0.1. (The numerical value 0.1 cannot be exactly
 represented in a binary floating-point number.)

 To avoid calling this method on an invalid string and having
 a NumberFormatException be thrown, the regular
 expression below can be used to screen the input string:

 
         final String Digits     = "(\\p{Digit}+)";
  final String HexDigits  = "(\\p{XDigit}+)";
        // an exponent is 'e' or 'E' followed by an optionally 
        // signed decimal integer.
        final String Exp        = "[eE][+-]?"+Digits;
        final String fpRegex    =
            ("[\\x00-\\x20]*"+  // Optional leading "whitespace"
             "[+-]?(" + // Optional sign character
             "NaN|" +           // "NaN" string
             "Infinity|" +      // "Infinity" string

             // A decimal floating-point string representing a finite positive
             // number without a leading sign has at most five basic pieces:
             // Digits . Digits ExponentPart FloatTypeSuffix
             // 
             // Since this method allows integer-only strings as input
             // in addition to strings of floating-point literals, the
             // two sub-patterns below are simplifications of the grammar
             // productions from the Java Language Specification, 2nd 
             // edition, section 3.10.2.

             // Digits ._opt Digits_opt ExponentPart_opt FloatTypeSuffix_opt
             "((("+Digits+"(\\.)?("+Digits+"?)("+Exp+")?)|"+

             // . Digits ExponentPart_opt FloatTypeSuffix_opt
             "(\\.("+Digits+")("+Exp+")?)|"+

       // Hexadecimal strings
       "((" +
        // 0[xX] HexDigits ._opt BinaryExponent FloatTypeSuffix_opt
        "(0[xX]" + HexDigits + "(\\.)?)|" +

        // 0[xX] HexDigits_opt . HexDigits BinaryExponent FloatTypeSuffix_opt
        "(0[xX]" + HexDigits + "?(\\.)" + HexDigits + ")" +

        ")[pP][+-]?" + Digits + "))" +
             "[fFdD]?))" +
             "[\\x00-\\x20]*");// Optional trailing "whitespace"
            
  if (Pattern.matches(fpRegex, myString))
            Double.valueOf(myString); // Will not throw NumberFormatException
        else {
            // Perform suitable alternative action
        }

Parameters:

s -> the string to be parsed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Double_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Double$$$_"));
throw ex;
//must have a return of type Double
};
var java_lang_Double_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$J$V(arg0, arg1);};
var java_lang_Double_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_Double_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$$V(arg0);};
var java_lang_Double_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_Double_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notify_$_$$V(arg0);};
var java_lang_Double_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notifyAll_$_$$V(arg0);};
java_lang_Double.prototype._$$$init$$$__$_$D$V = java_lang_Double_$__$$$init$$$__$_$D$V;
java_lang_Double.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_lang_Double_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_lang_Double.prototype.byteValue_$_$$B = java_lang_Double_$_byteValue_$_$$B;
java_lang_Double.prototype.compareTo_$_$Ljava_lang_Double$$$_$I = java_lang_Double_$_compareTo_$_$Ljava_lang_Double$$$_$I;
java_lang_Double.prototype.compareTo_$_$Ljava_lang_Object$$$_$I = java_lang_Double_$_compareTo_$_$Ljava_lang_Object$$$_$I;
java_lang_Double.compare_$_$DD$I = java_lang_Double_$_compare_$_$DD$I;
java_lang_Double.doubleToLongBits_$_$D$J = java_lang_Double_$_doubleToLongBits_$_$D$J;
java_lang_Double.doubleToRawLongBits_$_$D$J = java_lang_Double_$_doubleToRawLongBits_$_$D$J;
java_lang_Double.prototype.doubleValue_$_$$D = java_lang_Double_$_doubleValue_$_$$D;
java_lang_Double.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_Double_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_Double.prototype.floatValue_$_$$F = java_lang_Double_$_floatValue_$_$$F;
java_lang_Double.prototype.hashCode_$_$$I = java_lang_Double_$_hashCode_$_$$I;
java_lang_Double.prototype.intValue_$_$$I = java_lang_Double_$_intValue_$_$$I;
java_lang_Double.prototype.isInfinite_$_$$Z = java_lang_Double_$_isInfinite_$_$$Z;
java_lang_Double.isInfinite_$_$D$Z = java_lang_Double_$_isInfinite_$_$D$Z;
java_lang_Double.prototype.isNaN_$_$$Z = java_lang_Double_$_isNaN_$_$$Z;
java_lang_Double.isNaN_$_$D$Z = java_lang_Double_$_isNaN_$_$D$Z;
java_lang_Double.longBitsToDouble_$_$J$D = java_lang_Double_$_longBitsToDouble_$_$J$D;
java_lang_Double.prototype.longValue_$_$$J = java_lang_Double_$_longValue_$_$$J;
java_lang_Double.parseDouble_$_$Ljava_lang_String$$$_$D = java_lang_Double_$_parseDouble_$_$Ljava_lang_String$$$_$D;
java_lang_Double.prototype.shortValue_$_$$S = java_lang_Double_$_shortValue_$_$$S;
java_lang_Double.toHexString_$_$D$Ljava_lang_String$$$_ = java_lang_Double_$_toHexString_$_$D$Ljava_lang_String$$$_;
java_lang_Double.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_Double_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_Double.toString_$_$D$Ljava_lang_String$$$_ = java_lang_Double_$_toString_$_$D$Ljava_lang_String$$$_;
java_lang_Double.valueOf_$_$D$Ljava_lang_Double$$$_ = java_lang_Double_$_valueOf_$_$D$Ljava_lang_Double$$$_;
java_lang_Double.valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Double$$$_ = java_lang_Double_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Double$$$_;
java_lang_Double.prototype.wait_$_$J$V = java_lang_Double_$_wait_$_$J$V;
java_lang_Double.prototype.wait_$_$JI$V = java_lang_Double_$_wait_$_$JI$V;
java_lang_Double.prototype.wait_$_$$V = java_lang_Double_$_wait_$_$$V;
java_lang_Double.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_Double_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_Double.prototype.notify_$_$$V = java_lang_Double_$_notify_$_$$V;
java_lang_Double.prototype.notifyAll_$_$$V = java_lang_Double_$_notifyAll_$_$$V;
