/** @constructor */
function java_io_OutputStream() {}
var java_io_OutputStream_$__$$$init$$$__$_$$V = function (arg0) {
/*implement constructor!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_OutputStream_$__$$$init$$$__$_$$V"));
throw ex;
};
var java_io_OutputStream_$_close_$_$$V = function (arg0) {
/*implement method!*/
/*
Closes this output stream and releases any system resources 
 associated with this stream. The general contract of close 
 is that it closes the output stream. A closed stream cannot perform 
 output operations and cannot be reopened.
 
 The close method of OutputStream does nothing.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_OutputStream_$_close_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_OutputStream_$_flush_$_$$V = function (arg0) {
/*implement method!*/
/*
Flushes this output stream and forces any buffered output bytes 
 to be written out. The general contract of flush is 
 that calling it is an indication that, if any bytes previously 
 written have been buffered by the implementation of the output 
 stream, such bytes should immediately be written to their 
 intended destination.
 
 If the intended destination of this stream is an abstraction provided by
 the underlying operating system, for example a file, then flushing the
 stream guarantees only that bytes previously written to the stream are
 passed to the operating system for writing; it does not guarantee that
 they are actually written to a physical device such as a disk drive.
 
 The flush method of OutputStream does nothing.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_OutputStream_$_flush_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_OutputStream_$_write_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes the specified byte to this output stream. The general 
 contract for write is that one byte is written 
 to the output stream. The byte to be written is the eight 
 low-order bits of the argument b. The 24 
 high-order bits of b are ignored.
 
 Subclasses of OutputStream must provide an 
 implementation for this method.

Parameters:

b -> the byte.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_OutputStream_$_write_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_io_OutputStream_$_write_$_$_$$$$$_B$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes b.length bytes from the specified byte array 
 to this output stream. The general contract for write(b) 
 is that it should have exactly the same effect as the call 
 write(b, 0, b.length).

Parameters:

b -> the data.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_OutputStream_$_write_$_$_$$$$$_B$V"));
throw ex;
//must have a return of type void
};
var java_io_OutputStream_$_write_$_$_$$$$$_BII$V = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Writes len bytes from the specified byte array 
 starting at offset off to this output stream. 
 The general contract for write(b, off, len) is that 
 some of the bytes in the array b are written to the 
 output stream in order; element b[off] is the first 
 byte written and b[off+len-1] is the last byte written 
 by this operation.
 
 The write method of OutputStream calls 
 the write method of one argument on each of the bytes to be 
 written out. Subclasses are encouraged to override this method and 
 provide a more efficient implementation. 
 
 If b is null, a 
 NullPointerException is thrown.
 
 If off is negative, or len is negative, or 
 off+len is greater than the length of the array 
 b, then an IndexOutOfBoundsException is thrown.

Parameters:

b -> the data.

off -> the start offset in the data.

len -> the number of bytes to write.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_OutputStream_$_write_$_$_$$$$$_BII$V"));
throw ex;
//must have a return of type void
};
var java_io_OutputStream_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_io_OutputStream_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_io_OutputStream_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_io_OutputStream_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_io_OutputStream_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_io_OutputStream_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var java_io_OutputStream_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_io_OutputStream_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_io_OutputStream_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_io_OutputStream.prototype._$$$init$$$__$_$$V = java_io_OutputStream_$__$$$init$$$__$_$$V;
java_io_OutputStream.prototype.close_$_$$V = java_io_OutputStream_$_close_$_$$V;
java_io_OutputStream.prototype.flush_$_$$V = java_io_OutputStream_$_flush_$_$$V;
java_io_OutputStream.prototype.write_$_$I$V = java_io_OutputStream_$_write_$_$I$V;
java_io_OutputStream.prototype.write_$_$_$$$$$_B$V = java_io_OutputStream_$_write_$_$_$$$$$_B$V;
java_io_OutputStream.prototype.write_$_$_$$$$$_BII$V = java_io_OutputStream_$_write_$_$_$$$$$_BII$V;
java_io_OutputStream.prototype.wait_$_$J$V = java_io_OutputStream_$_wait_$_$J$V;
java_io_OutputStream.prototype.wait_$_$JI$V = java_io_OutputStream_$_wait_$_$JI$V;
java_io_OutputStream.prototype.wait_$_$$V = java_io_OutputStream_$_wait_$_$$V;
java_io_OutputStream.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_io_OutputStream_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_io_OutputStream.prototype.toString_$_$$Ljava_lang_String$$$_ = java_io_OutputStream_$_toString_$_$$Ljava_lang_String$$$_;
java_io_OutputStream.prototype.hashCode_$_$$I = java_io_OutputStream_$_hashCode_$_$$I;
java_io_OutputStream.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_io_OutputStream_$_getClass_$_$$Ljava_lang_Class$$$_;
java_io_OutputStream.prototype.notify_$_$$V = java_io_OutputStream_$_notify_$_$$V;
java_io_OutputStream.prototype.notifyAll_$_$$V = java_io_OutputStream_$_notifyAll_$_$$V;
