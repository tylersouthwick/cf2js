function handleException(ex, handlers) {
    var con = ex.constructor;
    var i;

    for (i = 0; i < handlers.length; i++) {
        var handler = handlers[i];
        if (con === handler[0]) {
            handler[1]();
            return;
        }
    }
    //if no handler is found, rethrow the exception
    throw ex;
}