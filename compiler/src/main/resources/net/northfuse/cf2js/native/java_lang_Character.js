/** @constructor */
function java_lang_Character() {}
var java_lang_Character_$__$$$init$$$__$_$C$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Character object that
 represents the specified char value.


Parameters:value - the value to be represented by the 
                  Character object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$__$$$init$$$__$_$C$V"));
throw ex;
};
var java_lang_Character_$_charCount_$_$I$I = function (arg0) {
/*implement method!*/
/*
Determines the number of char values needed to
 represent the specified character (Unicode code point). If the
 specified character is equal to or greater than 0x10000, then
 the method returns 2. Otherwise, the method returns 1.

 This method doesn't validate the specified character to be a
 valid Unicode code point. The caller must validate the
 character value using isValidCodePoint
 if necessary.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_charCount_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_charValue_$_$$C = function (arg0) {
/*implement method!*/
/*
Returns the value of this Character object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_charValue_$_$$C"));
throw ex;
//must have a return of type char
};
var java_lang_Character_$_codePointAtImpl_$_$_$$$$$_CII$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_codePointAtImpl_$_$_$$$$$_CII$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_codePointAt_$_$Ljava_lang_CharSequence$$$_I$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the code point at the given index of the
 CharSequence. If the char value at
 the given index in the CharSequence is in the
 high-surrogate range, the following index is less than the
 length of the CharSequence, and the
 char value at the following index is in the
 low-surrogate range, then the supplementary code point
 corresponding to this surrogate pair is returned. Otherwise,
 the char value at the given index is returned.

Parameters:

seq -> a sequence of char values (Unicode code
 units)

index -> the index to the char values (Unicode
 code units) in seq to be converted

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_codePointAt_$_$Ljava_lang_CharSequence$$$_I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_codePointAt_$_$_$$$$$_CI$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the code point at the given index of the
 char array. If the char value at
 the given index in the char array is in the
 high-surrogate range, the following index is less than the
 length of the char array, and the
 char value at the following index is in the
 low-surrogate range, then the supplementary code point
 corresponding to this surrogate pair is returned. Otherwise,
 the char value at the given index is returned.

Parameters:

a -> the char array

index -> the index to the char values (Unicode
 code units) in the char array to be converted

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_codePointAt_$_$_$$$$$_CI$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_codePointAt_$_$_$$$$$_CII$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the code point at the given index of the
 char array, where only array elements with
 index less than limit can be used. If
 the char value at the given index in the
 char array is in the high-surrogate range, the
 following index is less than the limit, and the
 char value at the following index is in the
 low-surrogate range, then the supplementary code point
 corresponding to this surrogate pair is returned. Otherwise,
 the char value at the given index is returned.

Parameters:

a -> the char array

index -> the index to the char values (Unicode
 code units) in the char array to be converted

limit -> the index after the last array element that can be used in the
 char array

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_codePointAt_$_$_$$$$$_CII$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_codePointBeforeImpl_$_$_$$$$$_CII$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_codePointBeforeImpl_$_$_$$$$$_CII$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_codePointBefore_$_$Ljava_lang_CharSequence$$$_I$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the code point preceding the given index of the
 CharSequence. If the char value at
 (index - 1) in the CharSequence is in
 the low-surrogate range, (index - 2) is not
 negative, and the char value at (index -
 2) in the CharSequence is in the
 high-surrogate range, then the supplementary code point
 corresponding to this surrogate pair is returned. Otherwise,
 the char value at (index - 1) is
 returned.

Parameters:

seq -> the CharSequence instance

index -> the index following the code point that should be returned

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_codePointBefore_$_$Ljava_lang_CharSequence$$$_I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_codePointBefore_$_$_$$$$$_CI$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the code point preceding the given index of the
 char array. If the char value at
 (index - 1) in the char array is in
 the low-surrogate range, (index - 2) is not
 negative, and the char value at (index -
 2) in the char array is in the
 high-surrogate range, then the supplementary code point
 corresponding to this surrogate pair is returned. Otherwise,
 the char value at (index - 1) is
 returned.

Parameters:

a -> the char array

index -> the index following the code point that should be returned

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_codePointBefore_$_$_$$$$$_CI$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_codePointBefore_$_$_$$$$$_CII$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the code point preceding the given index of the
 char array, where only array elements with
 index greater than or equal to start
 can be used. If the char value at (index -
 1) in the char array is in the
 low-surrogate range, (index - 2) is not less than
 start, and the char value at
 (index - 2) in the char array is in
 the high-surrogate range, then the supplementary code point
 corresponding to this surrogate pair is returned. Otherwise,
 the char value at (index - 1) is
 returned.

Parameters:

a -> the char array

index -> the index following the code point that should be returned

start -> the index of the first array element in the
 char array

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_codePointBefore_$_$_$$$$$_CII$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_codePointCountImpl_$_$_$$$$$_CII$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_codePointCountImpl_$_$_$$$$$_CII$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_codePointCount_$_$Ljava_lang_CharSequence$$$_II$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the number of Unicode code points in the text range of
 the specified char sequence. The text range begins at the
 specified beginIndex and extends to the
 char at index endIndex - 1. Thus the
 length (in chars) of the text range is
 endIndex-beginIndex. Unpaired surrogates within
 the text range count as one code point each.

Parameters:

seq -> the char sequence

beginIndex -> the index to the first char of
 the text range.

endIndex -> the index after the last char of
 the text range.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_codePointCount_$_$Ljava_lang_CharSequence$$$_II$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_codePointCount_$_$_$$$$$_CII$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the number of Unicode code points in a subarray of the
 char array argument. The offset
 argument is the index of the first char of the
 subarray and the count argument specifies the
 length of the subarray in chars. Unpaired
 surrogates within the subarray count as one code point each.

Parameters:

a -> the char array

offset -> the index of the first char in the
 given char array

count -> the length of the subarray in chars

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_codePointCount_$_$_$$$$$_CII$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_compareTo_$_$Ljava_lang_Character$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Compares two Character objects numerically.

Parameters:

anotherCharacter -> the Character to be compared.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_compareTo_$_$Ljava_lang_Character$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_compareTo_$_$Ljava_lang_Object$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_compareTo_$_$Ljava_lang_Object$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_digit_$_$CI$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the numeric value of the character ch in the
 specified radix.
 
 If the radix is not in the range MIN_RADIX <=
 radix <= MAX_RADIX or if the
 value of ch is not a valid digit in the specified
 radix, -1 is returned. A character is a valid digit
 if at least one of the following is true:
 
 The method isDigit is true of the character
     and the Unicode decimal digit value of the character (or its
     single-character decomposition) is less than the specified radix.
     In this case the decimal digit value is returned.
 The character is one of the uppercase Latin letters
     'A' through 'Z' and its code is less than
     radix + 'A' - 10.
     In this case, ch - 'A' + 10
     is returned.
 The character is one of the lowercase Latin letters
     'a' through 'z' and its code is less than
     radix + 'a' - 10.
     In this case, ch - 'a' + 10
     is returned.
 

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the digit(int, int) method.

Parameters:

ch -> the character to be converted.

radix -> the radix.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_digit_$_$CI$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_digit_$_$II$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the numeric value of the specified character (Unicode
 code point) in the specified radix.
 
 If the radix is not in the range MIN_RADIX <=
 radix <= MAX_RADIX or if the
 character is not a valid digit in the specified
 radix, -1 is returned. A character is a valid digit
 if at least one of the following is true:
 
 The method isDigit(codePoint) is true of the character
     and the Unicode decimal digit value of the character (or its
     single-character decomposition) is less than the specified radix.
     In this case the decimal digit value is returned.
 The character is one of the uppercase Latin letters
     'A' through 'Z' and its code is less than
     radix + 'A' - 10.
     In this case, ch - 'A' + 10
     is returned.
 The character is one of the lowercase Latin letters
     'a' through 'z' and its code is less than
     radix + 'a' - 10.
     In this case, ch - 'a' + 10
     is returned.

Parameters:

codePoint -> the character (Unicode code point) to be converted.

radix -> the radix.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_digit_$_$II$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Compares this object against the specified object.
 The result is true if and only if the argument is not
 null and is a Character object that
 represents the same char value as this object.

Parameters:

obj -> the object to compare with.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_equals_$_$Ljava_lang_Object$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_forDigit_$_$II$C = function (arg0, arg1) {
/*implement method!*/
/*
Determines the character representation for a specific digit in
 the specified radix. If the value of radix is not a
 valid radix, or the value of digit is not a valid
 digit in the specified radix, the null character
 ('\u0000') is returned.
 
 The radix argument is valid if it is greater than or
 equal to MIN_RADIX and less than or equal to
 MAX_RADIX. The digit argument is valid if
 0 <=digit < radix.
 
 If the digit is less than 10, then
 '0' + digit is returned. Otherwise, the value
 'a' + digit - 10 is returned.

Parameters:

digit -> the number to convert to a character.

radix -> the radix.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_forDigit_$_$II$C"));
throw ex;
//must have a return of type char
};
var java_lang_Character_$_getDirectionality_$_$C$B = function (arg0) {
/*implement method!*/
/*
Returns the Unicode directionality property for the given
 character.  Character directionality is used to calculate the
 visual ordering of text. The directionality value of undefined
 char values is DIRECTIONALITY_UNDEFINED.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the getDirectionality(int) method.

Parameters:

ch -> char for which the directionality property 
            is requested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_getDirectionality_$_$C$B"));
throw ex;
//must have a return of type byte
};
var java_lang_Character_$_getDirectionality_$_$I$B = function (arg0) {
/*implement method!*/
/*
Returns the Unicode directionality property for the given
 character (Unicode code point).  Character directionality is
 used to calculate the visual ordering of text. The
 directionality value of undefined character is DIRECTIONALITY_UNDEFINED.

Parameters:

codePoint -> the character (Unicode code point) for which
          the directionality property is requested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_getDirectionality_$_$I$B"));
throw ex;
//must have a return of type byte
};
var java_lang_Character_$_getNumericValue_$_$C$I = function (arg0) {
/*implement method!*/
/*
Returns the int value that the specified Unicode
 character represents. For example, the character
 '\u216C' (the roman numeral fifty) will return
 an int with a value of 50.
 
 The letters A-Z in their uppercase ('\u0041' through
 '\u005A'), lowercase
 ('\u0061' through '\u007A'), and
 full width variant ('\uFF21' through
 '\uFF3A' and '\uFF41' through
 '\uFF5A') forms have numeric values from 10
 through 35. This is independent of the Unicode specification,
 which does not assign numeric values to these char
 values.
 
 If the character does not have a numeric value, then -1 is returned.
 If the character has a numeric value that cannot be represented as a
 nonnegative integer (for example, a fractional value), then -2
 is returned.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the getNumericValue(int) method.

Parameters:

ch -> the character to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_getNumericValue_$_$C$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_getNumericValue_$_$I$I = function (arg0) {
/*implement method!*/
/*
Returns the int value that the specified 
 character (Unicode code point) represents. For example, the character
 '\u216C' (the Roman numeral fifty) will return
 an int with a value of 50.
 
 The letters A-Z in their uppercase ('\u0041' through
 '\u005A'), lowercase
 ('\u0061' through '\u007A'), and
 full width variant ('\uFF21' through
 '\uFF3A' and '\uFF41' through
 '\uFF5A') forms have numeric values from 10
 through 35. This is independent of the Unicode specification,
 which does not assign numeric values to these char
 values.
 
 If the character does not have a numeric value, then -1 is returned.
 If the character has a numeric value that cannot be represented as a
 nonnegative integer (for example, a fractional value), then -2
 is returned.

Parameters:

codePoint -> the character (Unicode code point) to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_getNumericValue_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_getType_$_$C$I = function (arg0) {
/*implement method!*/
/*
Returns a value indicating a character's general category.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the getType(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_getType_$_$C$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_getType_$_$I$I = function (arg0) {
/*implement method!*/
/*
Returns a value indicating a character's general category.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_getType_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_hashCode_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns a hash code for this Character.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_hashCode_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_isDefined_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if a character is defined in Unicode.
 
 A character is defined if at least one of the following is true:
 
 It has an entry in the UnicodeData file.
 It has a value in a range defined by the UnicodeData file.
 

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isDefined(int) method.

Parameters:

ch -> the character to be tested

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isDefined_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isDefined_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if a character (Unicode code point) is defined in Unicode.
 
 A character is defined if at least one of the following is true:
 
 It has an entry in the UnicodeData file.
 It has a value in a range defined by the UnicodeData file.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isDefined_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isDigit_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character is a digit.
 
 A character is a digit if its general category type, provided
 by Character.getType(ch), is
 DECIMAL_DIGIT_NUMBER.
 
 Some Unicode character ranges that contain digits:
 
 '\u0030' through '\u0039', 
     ISO-LATIN-1 digits ('0' through '9')
 '\u0660' through '\u0669',
     Arabic-Indic digits
 '\u06F0' through '\u06F9',
     Extended Arabic-Indic digits
 '\u0966' through '\u096F',
     Devanagari digits
 '\uFF10' through '\uFF19',
     Fullwidth digits
 

 Many other character ranges contain digits as well.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isDigit(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isDigit_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isDigit_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character (Unicode code point) is a digit.
 
 A character is a digit if its general category type, provided
 by getType(codePoint), is
 DECIMAL_DIGIT_NUMBER.
 
 Some Unicode character ranges that contain digits:
 
 '\u0030' through '\u0039', 
     ISO-LATIN-1 digits ('0' through '9')
 '\u0660' through '\u0669',
     Arabic-Indic digits
 '\u06F0' through '\u06F9',
     Extended Arabic-Indic digits
 '\u0966' through '\u096F',
     Devanagari digits
 '\uFF10' through '\uFF19',
     Fullwidth digits
 

 Many other character ranges contain digits as well.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isDigit_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isHighSurrogate_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the given char value is a
 high-surrogate code unit (also known as leading-surrogate
 code unit). Such values do not represent characters by
 themselves, but are used in the representation of supplementary characters in the
 UTF-16 encoding.

 This method returns true if and only if
 ch >= '\uD800' && ch <= '\uDBFF'
 
 is true.

Parameters:

ch -> the char value to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isHighSurrogate_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isISOControl_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character is an ISO control
 character.  A character is considered to be an ISO control
 character if its code is in the range '\u0000'
 through '\u001F' or in the range
 '\u007F' through '\u009F'.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isISOControl(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isISOControl_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isISOControl_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the referenced character (Unicode code point) is an ISO control
 character.  A character is considered to be an ISO control
 character if its code is in the range '\u0000'
 through '\u001F' or in the range
 '\u007F' through '\u009F'.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isISOControl_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isIdentifierIgnorable_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character should be regarded as
 an ignorable character in a Java identifier or a Unicode identifier.
 
 The following Unicode characters are ignorable in a Java identifier
 or a Unicode identifier:
 
 ISO control characters that are not whitespace
 
 '\u0000' through '\u0008'
 '\u000E' through '\u001B'
 '\u007F' through '\u009F'
 

 all characters that have the FORMAT general
 category value
 

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isIdentifierIgnorable(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isIdentifierIgnorable_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isIdentifierIgnorable_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character (Unicode code point) should be regarded as
 an ignorable character in a Java identifier or a Unicode identifier.
 
 The following Unicode characters are ignorable in a Java identifier
 or a Unicode identifier:
 
 ISO control characters that are not whitespace
 
 '\u0000' through '\u0008'
 '\u000E' through '\u001B'
 '\u007F' through '\u009F'
 

 all characters that have the FORMAT general
 category value

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isIdentifierIgnorable_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isJavaIdentifierPart_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character may be part of a Java
 identifier as other than the first character.
 
 A character may be part of a Java identifier if any of the following
 are true:
 
   it is a letter
   it is a currency symbol (such as '$')
   it is a connecting punctuation character (such as '_')
   it is a digit
   it is a numeric letter (such as a Roman numeral character)
   it is a combining mark
   it is a non-spacing mark
  isIdentifierIgnorable returns
 true for the character
 

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isJavaIdentifierPart(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isJavaIdentifierPart_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isJavaIdentifierPart_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the character (Unicode code point) may be part of a Java
 identifier as other than the first character.
 
 A character may be part of a Java identifier if any of the following
 are true:
 
   it is a letter
   it is a currency symbol (such as '$')
   it is a connecting punctuation character (such as '_')
   it is a digit
   it is a numeric letter (such as a Roman numeral character)
   it is a combining mark
   it is a non-spacing mark
  isIdentifierIgnorable(codePoint) returns true for
 the character

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isJavaIdentifierPart_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isJavaIdentifierStart_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character is
 permissible as the first character in a Java identifier.
 
 A character may start a Java identifier if and only if
 one of the following conditions is true:
 
  isLetter(ch) returns true
  getType(ch) returns LETTER_NUMBER
  ch is a currency symbol (such as "$")
  ch is a connecting punctuation character (such as "_").
 

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isJavaIdentifierStart(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isJavaIdentifierStart_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isJavaIdentifierStart_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the character (Unicode code point) is
 permissible as the first character in a Java identifier.
 
 A character may start a Java identifier if and only if
 one of the following conditions is true:
 
  isLetter(codePoint)
      returns true
  getType(codePoint)
      returns LETTER_NUMBER
  the referenced character is a currency symbol (such as "$")
  the referenced character is a connecting punctuation character
      (such as "_").

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isJavaIdentifierStart_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isJavaLetterOrDigit_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Deprecated. Replaced by isJavaIdentifierPart(char).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isJavaLetterOrDigit_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isJavaLetter_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Deprecated. Replaced by isJavaIdentifierStart(char).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isJavaLetter_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isLetterOrDigit_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character is a letter or digit.
 
 A character is considered to be a letter or digit if either
 Character.isLetter(char ch) or
 Character.isDigit(char ch) returns
 true for the character.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isLetterOrDigit(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isLetterOrDigit_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isLetterOrDigit_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character (Unicode code point) is a letter or digit.
 
 A character is considered to be a letter or digit if either
 isLetter(codePoint) or
 isDigit(codePoint) returns
 true for the character.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isLetterOrDigit_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isLetter_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character is a letter.
 
 A character is considered to be a letter if its general
 category type, provided by Character.getType(ch),
 is any of the following:
 
  UPPERCASE_LETTER
  LOWERCASE_LETTER
  TITLECASE_LETTER
  MODIFIER_LETTER
  OTHER_LETTER
 

 Not all letters have case. Many characters are
 letters but are neither uppercase nor lowercase nor titlecase.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isLetter(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isLetter_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isLetter_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character (Unicode code point) is a letter.
 
 A character is considered to be a letter if its general
 category type, provided by getType(codePoint),
 is any of the following:
 
  UPPERCASE_LETTER
  LOWERCASE_LETTER
  TITLECASE_LETTER
  MODIFIER_LETTER
  OTHER_LETTER
 

 Not all letters have case. Many characters are
 letters but are neither uppercase nor lowercase nor titlecase.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isLetter_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isLowSurrogate_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the given char value is a
 low-surrogate code unit (also known as trailing-surrogate code
 unit). Such values do not represent characters by themselves,
 but are used in the representation of supplementary characters in the UTF-16 encoding.

  This method returns true if and only if
 ch >= '\uDC00' && ch <= '\uDFFF'
  is true.

Parameters:

ch -> the char value to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isLowSurrogate_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isLowerCase_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character is a lowercase character.
 
 A character is lowercase if its general category type, provided
 by Character.getType(ch), is
 LOWERCASE_LETTER.
 
 The following are examples of lowercase characters:
  a b c d e f g h i j k l m n o p q r s t u v w x y z
 '\u00DF' '\u00E0' '\u00E1' '\u00E2' '\u00E3' '\u00E4' '\u00E5' '\u00E6' 
 '\u00E7' '\u00E8' '\u00E9' '\u00EA' '\u00EB' '\u00EC' '\u00ED' '\u00EE'
 '\u00EF' '\u00F0' '\u00F1' '\u00F2' '\u00F3' '\u00F4' '\u00F5' '\u00F6'
 '\u00F8' '\u00F9' '\u00FA' '\u00FB' '\u00FC' '\u00FD' '\u00FE' '\u00FF'
 
  Many other Unicode characters are lowercase too.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isLowerCase(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isLowerCase_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isLowerCase_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character (Unicode code point) is a
 lowercase character.
 
 A character is lowercase if its general category type, provided
 by getType(codePoint), is
 LOWERCASE_LETTER.
 
 The following are examples of lowercase characters:
  a b c d e f g h i j k l m n o p q r s t u v w x y z
 '\u00DF' '\u00E0' '\u00E1' '\u00E2' '\u00E3' '\u00E4' '\u00E5' '\u00E6' 
 '\u00E7' '\u00E8' '\u00E9' '\u00EA' '\u00EB' '\u00EC' '\u00ED' '\u00EE'
 '\u00EF' '\u00F0' '\u00F1' '\u00F2' '\u00F3' '\u00F4' '\u00F5' '\u00F6'
 '\u00F8' '\u00F9' '\u00FA' '\u00FB' '\u00FC' '\u00FD' '\u00FE' '\u00FF'
 
  Many other Unicode characters are lowercase too.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isLowerCase_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isMirrored_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines whether the character is mirrored according to the
 Unicode specification.  Mirrored characters should have their
 glyphs horizontally mirrored when displayed in text that is
 right-to-left.  For example, '\u0028' LEFT
 PARENTHESIS is semantically defined to be an opening
 parenthesis.  This will appear as a "(" in text that is
 left-to-right but as a ")" in text that is right-to-left.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isMirrored(int) method.

Parameters:

ch -> char for which the mirrored property is requested

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isMirrored_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isMirrored_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines whether the specified character (Unicode code point)
 is mirrored according to the Unicode specification.  Mirrored
 characters should have their glyphs horizontally mirrored when
 displayed in text that is right-to-left.  For example,
 '\u0028' LEFT PARENTHESIS is semantically
 defined to be an opening parenthesis.  This will appear
 as a "(" in text that is left-to-right but as a ")" in text
 that is right-to-left.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isMirrored_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isSpaceChar_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character is a Unicode space character.
 A character is considered to be a space character if and only if
 it is specified to be a space character by the Unicode standard. This
 method returns true if the character's general category type is any of
 the following:
 
  SPACE_SEPARATOR
  LINE_SEPARATOR
  PARAGRAPH_SEPARATOR
 

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isSpaceChar(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isSpaceChar_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isSpaceChar_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character (Unicode code point) is a
 Unicode space character.  A character is considered to be a
 space character if and only if it is specified to be a space
 character by the Unicode standard. This method returns true if
 the character's general category type is any of the following:

 
  SPACE_SEPARATOR
  LINE_SEPARATOR
  PARAGRAPH_SEPARATOR

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isSpaceChar_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isSpace_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Deprecated. Replaced by isWhitespace(char).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isSpace_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isSupplementaryCodePoint_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines whether the specified character (Unicode code point)
 is in the supplementary character range. The method call is
 equivalent to the expression:
  codePoint >= 0x10000 && codePoint <= 0x10FFFF

Parameters:

codePoint -> the character (Unicode code point) to be tested

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isSupplementaryCodePoint_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isSurrogatePair_$_$CC$Z = function (arg0, arg1) {
/*implement method!*/
/*
Determines whether the specified pair of char
 values is a valid surrogate pair. This method is equivalent to
 the expression:
  isHighSurrogate(high) && isLowSurrogate(low)

Parameters:

high -> the high-surrogate code value to be tested

low -> the low-surrogate code value to be tested

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isSurrogatePair_$_$CC$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isTitleCase_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character is a titlecase character.
  
 A character is a titlecase character if its general
 category type, provided by Character.getType(ch),
 is TITLECASE_LETTER.
 
 Some characters look like pairs of Latin letters. For example, there
 is an uppercase letter that looks like "LJ" and has a corresponding
 lowercase letter that looks like "lj". A third form, which looks like "Lj",
 is the appropriate form to use when rendering a word in lowercase
 with initial capitals, as for a book title.
 
 These are some of the Unicode characters for which this method returns
 true:
 
 LATIN CAPITAL LETTER D WITH SMALL LETTER Z WITH CARON
 LATIN CAPITAL LETTER L WITH SMALL LETTER J
 LATIN CAPITAL LETTER N WITH SMALL LETTER J
 LATIN CAPITAL LETTER D WITH SMALL LETTER Z
 
  Many other Unicode characters are titlecase too.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isTitleCase(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isTitleCase_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isTitleCase_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character (Unicode code point) is a titlecase character.
  
 A character is a titlecase character if its general
 category type, provided by getType(codePoint),
 is TITLECASE_LETTER.
 
 Some characters look like pairs of Latin letters. For example, there
 is an uppercase letter that looks like "LJ" and has a corresponding
 lowercase letter that looks like "lj". A third form, which looks like "Lj",
 is the appropriate form to use when rendering a word in lowercase
 with initial capitals, as for a book title.
 
 These are some of the Unicode characters for which this method returns
 true:
 
 LATIN CAPITAL LETTER D WITH SMALL LETTER Z WITH CARON
 LATIN CAPITAL LETTER L WITH SMALL LETTER J
 LATIN CAPITAL LETTER N WITH SMALL LETTER J
 LATIN CAPITAL LETTER D WITH SMALL LETTER Z
 
  Many other Unicode characters are titlecase too.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isTitleCase_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isUnicodeIdentifierPart_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character may be part of a Unicode
 identifier as other than the first character.
 
 A character may be part of a Unicode identifier if and only if
 one of the following statements is true:
 
   it is a letter
   it is a connecting punctuation character (such as '_')
   it is a digit
   it is a numeric letter (such as a Roman numeral character)
   it is a combining mark
   it is a non-spacing mark
  isIdentifierIgnorable returns
 true for this character.
 
 
 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isUnicodeIdentifierPart(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isUnicodeIdentifierPart_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isUnicodeIdentifierPart_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character (Unicode code point) may be part of a Unicode
 identifier as other than the first character.
 
 A character may be part of a Unicode identifier if and only if
 one of the following statements is true:
 
   it is a letter
   it is a connecting punctuation character (such as '_')
   it is a digit
   it is a numeric letter (such as a Roman numeral character)
   it is a combining mark
   it is a non-spacing mark
  isIdentifierIgnorable returns
 true for this character.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isUnicodeIdentifierPart_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isUnicodeIdentifierStart_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character is permissible as the
 first character in a Unicode identifier.
 
 A character may start a Unicode identifier if and only if
 one of the following conditions is true:
 
  isLetter(ch) returns true
  getType(ch) returns 
      LETTER_NUMBER.
 

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isUnicodeIdentifierStart(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isUnicodeIdentifierStart_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isUnicodeIdentifierStart_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character (Unicode code point) is permissible as the
 first character in a Unicode identifier.
 
 A character may start a Unicode identifier if and only if
 one of the following conditions is true:
 
  isLetter(codePoint)
      returns true
  getType(codePoint)
      returns LETTER_NUMBER.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isUnicodeIdentifierStart_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isUpperCase_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character is an uppercase character.
 
 A character is uppercase if its general category type, provided by
 Character.getType(ch), is UPPERCASE_LETTER.
 
 The following are examples of uppercase characters:
  A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
 '\u00C0' '\u00C1' '\u00C2' '\u00C3' '\u00C4' '\u00C5' '\u00C6' '\u00C7'
 '\u00C8' '\u00C9' '\u00CA' '\u00CB' '\u00CC' '\u00CD' '\u00CE' '\u00CF'
 '\u00D0' '\u00D1' '\u00D2' '\u00D3' '\u00D4' '\u00D5' '\u00D6' '\u00D8'
 '\u00D9' '\u00DA' '\u00DB' '\u00DC' '\u00DD' '\u00DE'
 
  Many other Unicode characters are uppercase too.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isUpperCase(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isUpperCase_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isUpperCase_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character (Unicode code point) is an uppercase character.
 
 A character is uppercase if its general category type, provided by
 getType(codePoint), is UPPERCASE_LETTER.
 
 The following are examples of uppercase characters:
  A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
 '\u00C0' '\u00C1' '\u00C2' '\u00C3' '\u00C4' '\u00C5' '\u00C6' '\u00C7'
 '\u00C8' '\u00C9' '\u00CA' '\u00CB' '\u00CC' '\u00CD' '\u00CE' '\u00CF'
 '\u00D0' '\u00D1' '\u00D2' '\u00D3' '\u00D4' '\u00D5' '\u00D6' '\u00D8'
 '\u00D9' '\u00DA' '\u00DB' '\u00DC' '\u00DD' '\u00DE'
 
  Many other Unicode characters are uppercase too.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isUpperCase_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isValidCodePoint_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines whether the specified code point is a valid Unicode
 code point value in the range of 0x0000 to
 0x10FFFF inclusive. This method is equivalent to
 the expression:

  codePoint >= 0x0000 && codePoint <= 0x10FFFF

Parameters:

codePoint -> the Unicode code point to be tested

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isValidCodePoint_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isWhitespace_$_$C$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character is white space according to Java.
 A character is a Java whitespace character if and only if it satisfies
 one of the following criteria:
 
  It is a Unicode space character (SPACE_SEPARATOR,
      LINE_SEPARATOR, or PARAGRAPH_SEPARATOR) 
      but is not also a non-breaking space ('\u00A0',
      '\u2007', '\u202F').
  It is '\u0009', HORIZONTAL TABULATION.
  It is '\u000A', LINE FEED.
  It is '\u000B', VERTICAL TABULATION.
  It is '\u000C', FORM FEED.
  It is '\u000D', CARRIAGE RETURN.
  It is '\u001C', FILE SEPARATOR.
  It is '\u001D', GROUP SEPARATOR.
  It is '\u001E', RECORD SEPARATOR.
  It is '\u001F', UNIT SEPARATOR.
 

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the isWhitespace(int) method.

Parameters:

ch -> the character to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isWhitespace_$_$C$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_isWhitespace_$_$I$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified character (Unicode code point) is
 white space according to Java.  A character is a Java
 whitespace character if and only if it satisfies one of the
 following criteria:
 
  It is a Unicode space character (SPACE_SEPARATOR,
      LINE_SEPARATOR, or PARAGRAPH_SEPARATOR) 
      but is not also a non-breaking space ('\u00A0',
      '\u2007', '\u202F').
  It is '\u0009', HORIZONTAL TABULATION.
  It is '\u000A', LINE FEED.
  It is '\u000B', VERTICAL TABULATION.
  It is '\u000C', FORM FEED.
  It is '\u000D', CARRIAGE RETURN.
  It is '\u001C', FILE SEPARATOR.
  It is '\u001D', GROUP SEPARATOR.
  It is '\u001E', RECORD SEPARATOR.
  It is '\u001F', UNIT SEPARATOR.

Parameters:

codePoint -> the character (Unicode code point) to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_isWhitespace_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Character_$_offsetByCodePointsImpl_$_$_$$$$$_CIIII$I = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_offsetByCodePointsImpl_$_$_$$$$$_CIIII$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_offsetByCodePoints_$_$Ljava_lang_CharSequence$$$_II$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the index within the given char sequence that is offset
 from the given index by codePointOffset
 code points. Unpaired surrogates within the text range given by
 index and codePointOffset count as
 one code point each.

Parameters:

seq -> the char sequence

index -> the index to be offset

codePointOffset -> the offset in code points

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_offsetByCodePoints_$_$Ljava_lang_CharSequence$$$_II$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_offsetByCodePoints_$_$_$$$$$_CIIII$I = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*
Returns the index within the given char subarray
 that is offset from the given index by
 codePointOffset code points. The
 start and count arguments specify a
 subarray of the char array. Unpaired surrogates
 within the text range given by index and
 codePointOffset count as one code point each.

Parameters:

a -> the char array

start -> the index of the first char of the
 subarray

count -> the length of the subarray in chars

index -> the index to be offset

codePointOffset -> the offset in code points

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_offsetByCodePoints_$_$_$$$$$_CIIII$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_reverseBytes_$_$C$C = function (arg0) {
/*implement method!*/
/*
Returns the value obtained by reversing the order of the bytes in the
 specified char value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_reverseBytes_$_$C$C"));
throw ex;
//must have a return of type char
};
var java_lang_Character_$_toChars_$_$I$_$$$$$_C = function (arg0) {
/*implement method!*/
/*
Converts the specified character (Unicode code point) to its
 UTF-16 representation stored in a char array. If
 the specified code point is a BMP (Basic Multilingual Plane or
 Plane 0) value, the resulting char array has
 the same value as codePoint. If the specified code
 point is a supplementary code point, the resulting
 char array has the corresponding surrogate pair.

Parameters:

codePoint -> a Unicode code point

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toChars_$_$I$_$$$$$_C"));
throw ex;
//must have a return of type char[]
};
var java_lang_Character_$_toChars_$_$I_$$$$$_CI$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Converts the specified character (Unicode code point) to its
 UTF-16 representation. If the specified code point is a BMP
 (Basic Multilingual Plane or Plane 0) value, the same value is
 stored in dst[dstIndex], and 1 is returned. If the
 specified code point is a supplementary character, its
 surrogate values are stored in dst[dstIndex]
 (high-surrogate) and dst[dstIndex+1]
 (low-surrogate), and 2 is returned.

Parameters:

codePoint -> the character (Unicode code point) to be converted.

dst -> an array of char in which the
 codePoint's UTF-16 value is stored.

dstIndex -> the start index into the dst
 array where the converted value is stored.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toChars_$_$I_$$$$$_CI$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_toCodePoint_$_$CC$I = function (arg0, arg1) {
/*implement method!*/
/*
Converts the specified surrogate pair to its supplementary code
 point value. This method does not validate the specified
 surrogate pair. The caller must validate it using isSurrogatePair if necessary.

Parameters:

high -> the high-surrogate code unit

low -> the low-surrogate code unit

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toCodePoint_$_$CC$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_toLowerCase_$_$C$C = function (arg0) {
/*implement method!*/
/*
Converts the character argument to lowercase using case
 mapping information from the UnicodeData file.
 
 Note that
 Character.isLowerCase(Character.toLowerCase(ch))
 does not always return true for some ranges of
 characters, particularly those that are symbols or ideographs.

 In general, String.toLowerCase() should be used to map
 characters to lowercase. String case mapping methods
 have several benefits over Character case mapping methods.
 String case mapping methods can perform locale-sensitive
 mappings, context-sensitive mappings, and 1:M character mappings, whereas
 the Character case mapping methods cannot.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the toLowerCase(int) method.

Parameters:

ch -> the character to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toLowerCase_$_$C$C"));
throw ex;
//must have a return of type char
};
var java_lang_Character_$_toLowerCase_$_$I$I = function (arg0) {
/*implement method!*/
/*
Converts the character (Unicode code point) argument to
 lowercase using case mapping information from the UnicodeData
 file.

  Note that
 Character.isLowerCase(Character.toLowerCase(codePoint))
 does not always return true for some ranges of
 characters, particularly those that are symbols or ideographs.

 In general, String.toLowerCase() should be used to map
 characters to lowercase. String case mapping methods
 have several benefits over Character case mapping methods.
 String case mapping methods can perform locale-sensitive
 mappings, context-sensitive mappings, and 1:M character mappings, whereas
 the Character case mapping methods cannot.

Parameters:

codePoint -> the character (Unicode code point) to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toLowerCase_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a String object representing this
 Character's value.  The result is a string of
 length 1 whose sole component is the primitive
 char value represented by this
 Character object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Character_$_toString_$_$C$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a String object representing the
 specified char.  The result is a string of length
 1 consisting solely of the specified char.

Parameters:

c -> the char to be converted

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toString_$_$C$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Character_$_toSurrogates_$_$I_$$$$$_CI$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toSurrogates_$_$I_$$$$$_CI$V"));
throw ex;
//must have a return of type void
};
var java_lang_Character_$_toTitleCase_$_$C$C = function (arg0) {
/*implement method!*/
/*
Converts the character argument to titlecase using case mapping
 information from the UnicodeData file. If a character has no
 explicit titlecase mapping and is not itself a titlecase char
 according to UnicodeData, then the uppercase mapping is
 returned as an equivalent titlecase mapping. If the
 char argument is already a titlecase
 char, the same char value will be
 returned.
 
 Note that
 Character.isTitleCase(Character.toTitleCase(ch))
 does not always return true for some ranges of
 characters.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the toTitleCase(int) method.

Parameters:

ch -> the character to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toTitleCase_$_$C$C"));
throw ex;
//must have a return of type char
};
var java_lang_Character_$_toTitleCase_$_$I$I = function (arg0) {
/*implement method!*/
/*
Converts the character (Unicode code point) argument to titlecase using case mapping
 information from the UnicodeData file. If a character has no
 explicit titlecase mapping and is not itself a titlecase char
 according to UnicodeData, then the uppercase mapping is
 returned as an equivalent titlecase mapping. If the
 character argument is already a titlecase
 character, the same character value will be
 returned.
 
 Note that
 Character.isTitleCase(Character.toTitleCase(codePoint))
 does not always return true for some ranges of
 characters.

Parameters:

codePoint -> the character (Unicode code point) to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toTitleCase_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_toUpperCaseCharArray_$_$I$_$$$$$_C = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toUpperCaseCharArray_$_$I$_$$$$$_C"));
throw ex;
//must have a return of type char[]
};
var java_lang_Character_$_toUpperCaseEx_$_$I$I = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toUpperCaseEx_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_toUpperCase_$_$C$C = function (arg0) {
/*implement method!*/
/*
Converts the character argument to uppercase using case mapping
 information from the UnicodeData file.
 
 Note that
 Character.isUpperCase(Character.toUpperCase(ch))
 does not always return true for some ranges of
 characters, particularly those that are symbols or ideographs.

 In general, String.toUpperCase() should be used to map
 characters to uppercase. String case mapping methods
 have several benefits over Character case mapping methods.
 String case mapping methods can perform locale-sensitive
 mappings, context-sensitive mappings, and 1:M character mappings, whereas
 the Character case mapping methods cannot.

 Note: This method cannot handle  supplementary characters. To support
 all Unicode characters, including supplementary characters, use
 the toUpperCase(int) method.

Parameters:

ch -> the character to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toUpperCase_$_$C$C"));
throw ex;
//must have a return of type char
};
var java_lang_Character_$_toUpperCase_$_$I$I = function (arg0) {
/*implement method!*/
/*
Converts the character (Unicode code point) argument to
 uppercase using case mapping information from the UnicodeData
 file.
 
 Note that
 Character.isUpperCase(Character.toUpperCase(codePoint))
 does not always return true for some ranges of
 characters, particularly those that are symbols or ideographs.

 In general, String.toUpperCase() should be used to map
 characters to uppercase. String case mapping methods
 have several benefits over Character case mapping methods.
 String case mapping methods can perform locale-sensitive
 mappings, context-sensitive mappings, and 1:M character mappings, whereas
 the Character case mapping methods cannot.

Parameters:

codePoint -> the character (Unicode code point) to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_toUpperCase_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Character_$_valueOf_$_$C$Ljava_lang_Character$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a Character instance representing the specified
 char value.
 If a new Character instance is not required, this method
 should generally be used in preference to the constructor
 Character(char), as this method is likely to yield
 significantly better space and time performance by caching
 frequently requested values.

Parameters:

c -> a char value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Character_$_valueOf_$_$C$Ljava_lang_Character$$$_"));
throw ex;
//must have a return of type Character
};
var java_lang_Character_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_lang_Character_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_Character_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_lang_Character_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_Character_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_lang_Character_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_lang_Character.prototype._$$$init$$$__$_$C$V = java_lang_Character_$__$$$init$$$__$_$C$V;
java_lang_Character.charCount_$_$I$I = java_lang_Character_$_charCount_$_$I$I;
java_lang_Character.prototype.charValue_$_$$C = java_lang_Character_$_charValue_$_$$C;
java_lang_Character.codePointAtImpl_$_$_$$$$$_CII$I = java_lang_Character_$_codePointAtImpl_$_$_$$$$$_CII$I;
java_lang_Character.codePointAt_$_$Ljava_lang_CharSequence$$$_I$I = java_lang_Character_$_codePointAt_$_$Ljava_lang_CharSequence$$$_I$I;
java_lang_Character.codePointAt_$_$_$$$$$_CI$I = java_lang_Character_$_codePointAt_$_$_$$$$$_CI$I;
java_lang_Character.codePointAt_$_$_$$$$$_CII$I = java_lang_Character_$_codePointAt_$_$_$$$$$_CII$I;
java_lang_Character.codePointBeforeImpl_$_$_$$$$$_CII$I = java_lang_Character_$_codePointBeforeImpl_$_$_$$$$$_CII$I;
java_lang_Character.codePointBefore_$_$Ljava_lang_CharSequence$$$_I$I = java_lang_Character_$_codePointBefore_$_$Ljava_lang_CharSequence$$$_I$I;
java_lang_Character.codePointBefore_$_$_$$$$$_CI$I = java_lang_Character_$_codePointBefore_$_$_$$$$$_CI$I;
java_lang_Character.codePointBefore_$_$_$$$$$_CII$I = java_lang_Character_$_codePointBefore_$_$_$$$$$_CII$I;
java_lang_Character.codePointCountImpl_$_$_$$$$$_CII$I = java_lang_Character_$_codePointCountImpl_$_$_$$$$$_CII$I;
java_lang_Character.codePointCount_$_$Ljava_lang_CharSequence$$$_II$I = java_lang_Character_$_codePointCount_$_$Ljava_lang_CharSequence$$$_II$I;
java_lang_Character.codePointCount_$_$_$$$$$_CII$I = java_lang_Character_$_codePointCount_$_$_$$$$$_CII$I;
java_lang_Character.prototype.compareTo_$_$Ljava_lang_Character$$$_$I = java_lang_Character_$_compareTo_$_$Ljava_lang_Character$$$_$I;
java_lang_Character.prototype.compareTo_$_$Ljava_lang_Object$$$_$I = java_lang_Character_$_compareTo_$_$Ljava_lang_Object$$$_$I;
java_lang_Character.digit_$_$CI$I = java_lang_Character_$_digit_$_$CI$I;
java_lang_Character.digit_$_$II$I = java_lang_Character_$_digit_$_$II$I;
java_lang_Character.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_Character_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_Character.forDigit_$_$II$C = java_lang_Character_$_forDigit_$_$II$C;
java_lang_Character.getDirectionality_$_$C$B = java_lang_Character_$_getDirectionality_$_$C$B;
java_lang_Character.getDirectionality_$_$I$B = java_lang_Character_$_getDirectionality_$_$I$B;
java_lang_Character.getNumericValue_$_$C$I = java_lang_Character_$_getNumericValue_$_$C$I;
java_lang_Character.getNumericValue_$_$I$I = java_lang_Character_$_getNumericValue_$_$I$I;
java_lang_Character.getType_$_$C$I = java_lang_Character_$_getType_$_$C$I;
java_lang_Character.getType_$_$I$I = java_lang_Character_$_getType_$_$I$I;
java_lang_Character.prototype.hashCode_$_$$I = java_lang_Character_$_hashCode_$_$$I;
java_lang_Character.isDefined_$_$C$Z = java_lang_Character_$_isDefined_$_$C$Z;
java_lang_Character.isDefined_$_$I$Z = java_lang_Character_$_isDefined_$_$I$Z;
java_lang_Character.isDigit_$_$C$Z = java_lang_Character_$_isDigit_$_$C$Z;
java_lang_Character.isDigit_$_$I$Z = java_lang_Character_$_isDigit_$_$I$Z;
java_lang_Character.isHighSurrogate_$_$C$Z = java_lang_Character_$_isHighSurrogate_$_$C$Z;
java_lang_Character.isISOControl_$_$C$Z = java_lang_Character_$_isISOControl_$_$C$Z;
java_lang_Character.isISOControl_$_$I$Z = java_lang_Character_$_isISOControl_$_$I$Z;
java_lang_Character.isIdentifierIgnorable_$_$C$Z = java_lang_Character_$_isIdentifierIgnorable_$_$C$Z;
java_lang_Character.isIdentifierIgnorable_$_$I$Z = java_lang_Character_$_isIdentifierIgnorable_$_$I$Z;
java_lang_Character.isJavaIdentifierPart_$_$C$Z = java_lang_Character_$_isJavaIdentifierPart_$_$C$Z;
java_lang_Character.isJavaIdentifierPart_$_$I$Z = java_lang_Character_$_isJavaIdentifierPart_$_$I$Z;
java_lang_Character.isJavaIdentifierStart_$_$C$Z = java_lang_Character_$_isJavaIdentifierStart_$_$C$Z;
java_lang_Character.isJavaIdentifierStart_$_$I$Z = java_lang_Character_$_isJavaIdentifierStart_$_$I$Z;
java_lang_Character.isJavaLetterOrDigit_$_$C$Z = java_lang_Character_$_isJavaLetterOrDigit_$_$C$Z;
java_lang_Character.isJavaLetter_$_$C$Z = java_lang_Character_$_isJavaLetter_$_$C$Z;
java_lang_Character.isLetterOrDigit_$_$C$Z = java_lang_Character_$_isLetterOrDigit_$_$C$Z;
java_lang_Character.isLetterOrDigit_$_$I$Z = java_lang_Character_$_isLetterOrDigit_$_$I$Z;
java_lang_Character.isLetter_$_$C$Z = java_lang_Character_$_isLetter_$_$C$Z;
java_lang_Character.isLetter_$_$I$Z = java_lang_Character_$_isLetter_$_$I$Z;
java_lang_Character.isLowSurrogate_$_$C$Z = java_lang_Character_$_isLowSurrogate_$_$C$Z;
java_lang_Character.isLowerCase_$_$C$Z = java_lang_Character_$_isLowerCase_$_$C$Z;
java_lang_Character.isLowerCase_$_$I$Z = java_lang_Character_$_isLowerCase_$_$I$Z;
java_lang_Character.isMirrored_$_$C$Z = java_lang_Character_$_isMirrored_$_$C$Z;
java_lang_Character.isMirrored_$_$I$Z = java_lang_Character_$_isMirrored_$_$I$Z;
java_lang_Character.isSpaceChar_$_$C$Z = java_lang_Character_$_isSpaceChar_$_$C$Z;
java_lang_Character.isSpaceChar_$_$I$Z = java_lang_Character_$_isSpaceChar_$_$I$Z;
java_lang_Character.isSpace_$_$C$Z = java_lang_Character_$_isSpace_$_$C$Z;
java_lang_Character.isSupplementaryCodePoint_$_$I$Z = java_lang_Character_$_isSupplementaryCodePoint_$_$I$Z;
java_lang_Character.isSurrogatePair_$_$CC$Z = java_lang_Character_$_isSurrogatePair_$_$CC$Z;
java_lang_Character.isTitleCase_$_$C$Z = java_lang_Character_$_isTitleCase_$_$C$Z;
java_lang_Character.isTitleCase_$_$I$Z = java_lang_Character_$_isTitleCase_$_$I$Z;
java_lang_Character.isUnicodeIdentifierPart_$_$C$Z = java_lang_Character_$_isUnicodeIdentifierPart_$_$C$Z;
java_lang_Character.isUnicodeIdentifierPart_$_$I$Z = java_lang_Character_$_isUnicodeIdentifierPart_$_$I$Z;
java_lang_Character.isUnicodeIdentifierStart_$_$C$Z = java_lang_Character_$_isUnicodeIdentifierStart_$_$C$Z;
java_lang_Character.isUnicodeIdentifierStart_$_$I$Z = java_lang_Character_$_isUnicodeIdentifierStart_$_$I$Z;
java_lang_Character.isUpperCase_$_$C$Z = java_lang_Character_$_isUpperCase_$_$C$Z;
java_lang_Character.isUpperCase_$_$I$Z = java_lang_Character_$_isUpperCase_$_$I$Z;
java_lang_Character.isValidCodePoint_$_$I$Z = java_lang_Character_$_isValidCodePoint_$_$I$Z;
java_lang_Character.isWhitespace_$_$C$Z = java_lang_Character_$_isWhitespace_$_$C$Z;
java_lang_Character.isWhitespace_$_$I$Z = java_lang_Character_$_isWhitespace_$_$I$Z;
java_lang_Character.offsetByCodePointsImpl_$_$_$$$$$_CIIII$I = java_lang_Character_$_offsetByCodePointsImpl_$_$_$$$$$_CIIII$I;
java_lang_Character.offsetByCodePoints_$_$Ljava_lang_CharSequence$$$_II$I = java_lang_Character_$_offsetByCodePoints_$_$Ljava_lang_CharSequence$$$_II$I;
java_lang_Character.offsetByCodePoints_$_$_$$$$$_CIIII$I = java_lang_Character_$_offsetByCodePoints_$_$_$$$$$_CIIII$I;
java_lang_Character.reverseBytes_$_$C$C = java_lang_Character_$_reverseBytes_$_$C$C;
java_lang_Character.toChars_$_$I$_$$$$$_C = java_lang_Character_$_toChars_$_$I$_$$$$$_C;
java_lang_Character.toChars_$_$I_$$$$$_CI$I = java_lang_Character_$_toChars_$_$I_$$$$$_CI$I;
java_lang_Character.toCodePoint_$_$CC$I = java_lang_Character_$_toCodePoint_$_$CC$I;
java_lang_Character.toLowerCase_$_$C$C = java_lang_Character_$_toLowerCase_$_$C$C;
java_lang_Character.toLowerCase_$_$I$I = java_lang_Character_$_toLowerCase_$_$I$I;
java_lang_Character.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_Character_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_Character.toString_$_$C$Ljava_lang_String$$$_ = java_lang_Character_$_toString_$_$C$Ljava_lang_String$$$_;
java_lang_Character.toSurrogates_$_$I_$$$$$_CI$V = java_lang_Character_$_toSurrogates_$_$I_$$$$$_CI$V;
java_lang_Character.toTitleCase_$_$C$C = java_lang_Character_$_toTitleCase_$_$C$C;
java_lang_Character.toTitleCase_$_$I$I = java_lang_Character_$_toTitleCase_$_$I$I;
java_lang_Character.toUpperCaseCharArray_$_$I$_$$$$$_C = java_lang_Character_$_toUpperCaseCharArray_$_$I$_$$$$$_C;
java_lang_Character.toUpperCaseEx_$_$I$I = java_lang_Character_$_toUpperCaseEx_$_$I$I;
java_lang_Character.toUpperCase_$_$C$C = java_lang_Character_$_toUpperCase_$_$C$C;
java_lang_Character.toUpperCase_$_$I$I = java_lang_Character_$_toUpperCase_$_$I$I;
java_lang_Character.valueOf_$_$C$Ljava_lang_Character$$$_ = java_lang_Character_$_valueOf_$_$C$Ljava_lang_Character$$$_;
java_lang_Character.prototype.wait_$_$J$V = java_lang_Character_$_wait_$_$J$V;
java_lang_Character.prototype.wait_$_$JI$V = java_lang_Character_$_wait_$_$JI$V;
java_lang_Character.prototype.wait_$_$$V = java_lang_Character_$_wait_$_$$V;
java_lang_Character.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_Character_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_Character.prototype.notify_$_$$V = java_lang_Character_$_notify_$_$$V;
java_lang_Character.prototype.notifyAll_$_$$V = java_lang_Character_$_notifyAll_$_$$V;
