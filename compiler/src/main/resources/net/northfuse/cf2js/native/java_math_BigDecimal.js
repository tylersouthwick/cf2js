/** @constructor */
function java_math_BigDecimal() {}
var java_math_BigDecimal_$__$$$init$$$__$_$D$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Translates a double into a BigDecimal which
 is the exact decimal representation of the double's
 binary floating-point value.  The scale of the returned
 BigDecimal is the smallest value such that
 (10scale × val) is an integer.
 
 Notes:
 
 
 The results of this constructor can be somewhat unpredictable.
 One might assume that writing new BigDecimal(0.1) in
 Java creates a BigDecimal which is exactly equal to
 0.1 (an unscaled value of 1, with a scale of 1), but it is
 actually equal to
 0.1000000000000000055511151231257827021181583404541015625.
 This is because 0.1 cannot be represented exactly as a
 double (or, for that matter, as a binary fraction of
 any finite length).  Thus, the value that is being passed
 in to the constructor is not exactly equal to 0.1,
 appearances notwithstanding.

 
 The String constructor, on the other hand, is
 perfectly predictable: writing new BigDecimal("0.1")
 creates a BigDecimal which is exactly equal to
 0.1, as one would expect.  Therefore, it is generally
 recommended that the String constructor be used in preference to this one.

 
 When a double must be used as a source for a
 BigDecimal, note that this constructor provides an
 exact conversion; it does not give the same result as
 converting the double to a String using the
 Double.toString(double) method and then using the
 BigDecimal(String) constructor.  To get that result,
 use the static valueOf(double) method.
 


Parameters:val - double value to be converted to 
        BigDecimal.
Throws:
NumberFormatException - if val is infinite or NaN.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$D$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$DLjava_math_MathContext$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Translates a double into a BigDecimal, with
 rounding according to the context settings.  The scale of the
 BigDecimal is the smallest value such that
 (10scale × val) is an integer.
 
 The results of this constructor can be somewhat unpredictable
 and its use is generally not recommended; see the notes under
 the BigDecimal(double) constructor.


Parameters:val - double value to be converted to 
         BigDecimal.mc - the context to use.
Throws:
ArithmeticException - if the result is inexact but the
         RoundingMode is UNNECESSARY.
NumberFormatException - if val is infinite or NaN.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$DLjava_math_MathContext$$$_$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$I$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Translates an int into a BigDecimal.  The
 scale of the BigDecimal is zero.


Parameters:val - int value to be converted to
            BigDecimal.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$I$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$ILjava_math_MathContext$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Translates an int into a BigDecimal, with
 rounding according to the context settings.  The scale of the
 BigDecimal, before any rounding, is zero.


Parameters:val - int value to be converted to BigDecimal.mc - the context to use.
Throws:
ArithmeticException - if the result is inexact but the
         rounding mode is UNNECESSARY.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$ILjava_math_MathContext$$$_$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$J$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Translates a long into a BigDecimal.  The
 scale of the BigDecimal is zero.


Parameters:val - long value to be converted to BigDecimal.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$J$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$JLjava_math_MathContext$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Translates a long into a BigDecimal, with
 rounding according to the context settings.  The scale of the
 BigDecimal, before any rounding, is zero.


Parameters:val - long value to be converted to BigDecimal.mc - the context to use.
Throws:
ArithmeticException - if the result is inexact but the
         rounding mode is UNNECESSARY.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$JLjava_math_MathContext$$$_$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Translates the string representation of a BigDecimal
 into a BigDecimal.  The string representation consists
 of an optional sign, '+' ( '\u002B') or
 '-' ('\u002D'), followed by a sequence of
 zero or more decimal digits ("the integer"), optionally
 followed by a fraction, optionally followed by an exponent.
 
 The fraction consists of a decimal point followed by zero
 or more decimal digits.  The string must contain at least one
 digit in either the integer or the fraction.  The number formed
 by the sign, the integer and the fraction is referred to as the
 significand.

 The exponent consists of the character 'e'
 ('\u0065') or 'E' ('\u0045')
 followed by one or more decimal digits.  The value of the
 exponent must lie between -Integer.MAX_VALUE (Integer.MIN_VALUE+1) and Integer.MAX_VALUE, inclusive.

 More formally, the strings this constructor accepts are
 described by the following grammar:
 
 
 BigDecimalString:
 Signopt Significand Exponentopt
 
 Sign:
 +
 -
 
 Significand:
 IntegerPart . FractionPartopt
 . FractionPart
 IntegerPart
 
 IntegerPart:
 Digits
 
 FractionPart:
 Digits
 
 Exponent:
 ExponentIndicator SignedInteger
 
 ExponentIndicator:
 e
 E
 
 SignedInteger:
 Signopt Digits
 
 Digits:
 Digit
 Digits Digit
 
 Digit:
 any character for which Character.isDigit(char)
 returns true, including 0, 1, 2 ...
 
 

 The scale of the returned BigDecimal will be the
 number of digits in the fraction, or zero if the string
 contains no decimal point, subject to adjustment for any
 exponent; if the string contains an exponent, the exponent is
 subtracted from the scale.  The value of the resulting scale
 must lie between Integer.MIN_VALUE and
 Integer.MAX_VALUE, inclusive.

 The character-to-digit mapping is provided by Character.digit(char, int) set to convert to radix 10.  The
 String may not contain any extraneous characters (whitespace,
 for example).

 Examples:
 The value of the returned BigDecimal is equal to
 significand × 10 exponent.  
 For each string on the left, the resulting representation
 [BigInteger, scale] is shown on the right.
  "0"            [0,0]
 "0.00"         [0,2]
 "123"          [123,0]
 "-123"         [-123,0]
 "1.23E3"       [123,-1]
 "1.23E+3"      [123,-1]
 "12.3E+7"      [123,-6]
 "12.0"         [120,1]
 "12.3"         [123,1]
 "0.00123"      [123,5]
 "-1.23E-12"    [-123,14]
 "1234.5E-4"    [12345,5]
 "0E+7"         [0,-7]
 "-0"           [0,0]
 

 Note: For values other than float and
 double NaN and ±Infinity, this constructor is
 compatible with the values returned by Float.toString(float)
 and Double.toString(double).  This is generally the preferred
 way to convert a float or double into a
 BigDecimal, as it doesn't suffer from the unpredictability of
 the BigDecimal(double) constructor.


Parameters:val - String representation of BigDecimal.
Throws:
NumberFormatException - if val is not a valid 
               representation of a BigDecimal.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$Ljava_lang_String$$$_$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$Ljava_lang_String$$$_Ljava_math_MathContext$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Translates the string representation of a BigDecimal
 into a BigDecimal, accepting the same strings as the
 BigDecimal(String) constructor, with rounding
 according to the context settings.


Parameters:val - string representation of a BigDecimal.mc - the context to use.
Throws:
ArithmeticException - if the result is inexact but the
         rounding mode is UNNECESSARY.
NumberFormatException - if val is not a valid
         representation of a BigDecimal.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$Ljava_lang_String$$$_Ljava_math_MathContext$$$_$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Translates a BigInteger into a BigDecimal.
 The scale of the BigDecimal is zero.


Parameters:val - BigInteger value to be converted to
            BigDecimal.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_I$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Translates a BigInteger unscaled value and an
 int scale into a BigDecimal.  The value of
 the BigDecimal is
 (unscaledVal × 10-scale).


Parameters:unscaledVal - unscaled value of the BigDecimal.scale - scale of the BigDecimal.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_I$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_ILjava_math_MathContext$$$_$V = function (arg0, arg1, arg2, arg3) {
/*implement constructor!*/
/*
Translates a BigInteger unscaled value and an
 int scale into a BigDecimal, with rounding
 according to the context settings.  The value of the
 BigDecimal is (unscaledVal ×
 10-scale), rounded according to the
 precision and rounding mode settings.


Parameters:unscaledVal - unscaled value of the BigDecimal.scale - scale of the BigDecimal.mc - the context to use.
Throws:
ArithmeticException - if the result is inexact but the
         rounding mode is UNNECESSARY.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_ILjava_math_MathContext$$$_$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_Ljava_math_MathContext$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Translates a BigInteger into a BigDecimal
 rounding according to the context settings.  The scale of the
 BigDecimal is zero.


Parameters:val - BigInteger value to be converted to
            BigDecimal.mc - the context to use.
Throws:
ArithmeticException - if the result is inexact but the
         rounding mode is UNNECESSARY.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_Ljava_math_MathContext$$$_$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_C$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Translates a character array representation of a
 BigDecimal into a BigDecimal, accepting the
 same sequence of characters as the BigDecimal(String)
 constructor.
 
 Note that if the sequence of characters is already available
 as a character array, using this constructor is faster than
 converting the char array to string and using the
 BigDecimal(String) constructor .


Parameters:in - char array that is the source of characters.
Throws:
NumberFormatException - if in is not a valid
         representation of a BigDecimal.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_C$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_CII$V = function (arg0, arg1, arg2, arg3) {
/*implement constructor!*/
/*
Translates a character array representation of a
 BigDecimal into a BigDecimal, accepting the
 same sequence of characters as the BigDecimal(String)
 constructor, while allowing a sub-array to be specified.
 
 Note that if the sequence of characters is already available
 within a character array, using this constructor is faster than
 converting the char array to string and using the
 BigDecimal(String) constructor .


Parameters:in - char array that is the source of characters.offset - first character in the array to inspect.len - number of characters to consider.
Throws:
NumberFormatException - if in is not a valid
         representation of a BigDecimal or the defined subarray
         is not wholly within in.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_CII$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_CIILjava_math_MathContext$$$_$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement constructor!*/
/*
Translates a character array representation of a
 BigDecimal into a BigDecimal, accepting the
 same sequence of characters as the BigDecimal(String)
 constructor, while allowing a sub-array to be specified and
 with rounding according to the context settings.
 
 Note that if the sequence of characters is already available
 within a character array, using this constructor is faster than
 converting the char array to string and using the
 BigDecimal(String) constructor .


Parameters:in - char array that is the source of characters.offset - first character in the array to inspect.len - number of characters to consider..mc - the context to use.
Throws:
ArithmeticException - if the result is inexact but the
         rounding mode is UNNECESSARY.
NumberFormatException - if in is not a valid
         representation of a BigDecimal or the defined subarray
         is not wholly within in.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_CIILjava_math_MathContext$$$_$V"));
throw ex;
};
var java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_CLjava_math_MathContext$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Translates a character array representation of a
 BigDecimal into a BigDecimal, accepting the
 same sequence of characters as the BigDecimal(String)
 constructor and with rounding according to the context
 settings.
 
 Note that if the sequence of characters is already available
 as a character array, using this constructor is faster than
 converting the char array to string and using the
 BigDecimal(String) constructor .


Parameters:in - char array that is the source of characters.mc - the context to use.
Throws:
ArithmeticException - if the result is inexact but the
         rounding mode is UNNECESSARY.
NumberFormatException - if in is not a valid
         representation of a BigDecimal.Since:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_CLjava_math_MathContext$$$_$V"));
throw ex;
};
var java_math_BigDecimal_$_abs_$_$$Ljava_math_BigDecimal$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a BigDecimal whose value is the absolute value
 of this BigDecimal, and whose scale is
 this.scale().

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_abs_$_$$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_abs_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose value is the absolute value
 of this BigDecimal, with rounding according to the
 context settings.

Parameters:

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_abs_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_access$000_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigInteger$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_access$000_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigInteger$$$_"));
throw ex;
//must have a return of type BigInteger
};
var java_math_BigDecimal_$_access$100_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigInteger$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_access$100_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigInteger$$$_"));
throw ex;
//must have a return of type BigInteger
};
var java_math_BigDecimal_$_add_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this +
 augend), and whose scale is max(this.scale(),
 augend.scale()).

Parameters:

augend -> value to be added to this BigDecimal.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_add_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_add_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this + augend),
 with rounding according to the context settings.

 If either number is zero and the precision setting is nonzero then
 the other number, rounded if necessary, is used as the result.

Parameters:

augend -> value to be added to this BigDecimal.

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_add_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_byteValueExact_$_$$B = function (arg0) {
/*implement method!*/
/*
Converts this BigDecimal to a byte, checking
 for lost information.  If this BigDecimal has a
 nonzero fractional part or is out of the possible range for a
 byte result then an ArithmeticException is
 thrown.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_byteValueExact_$_$$B"));
throw ex;
//must have a return of type byte
};
var java_math_BigDecimal_$_compareTo_$_$Ljava_lang_Object$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_compareTo_$_$Ljava_lang_Object$$$_$I"));
throw ex;
//must have a return of type int
};
var java_math_BigDecimal_$_compareTo_$_$Ljava_math_BigDecimal$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Compares this BigDecimal with the specified
 BigDecimal.  Two BigDecimal objects that are
 equal in value but have a different scale (like 2.0 and 2.00)
 are considered equal by this method.  This method is provided
 in preference to individual methods for each of the six boolean
 comparison operators (<, ==,
 >, >=, !=, <=).  The
 suggested idiom for performing these comparisons is:
 (x.compareTo(y) <op> 0), where
 <op> is one of the six comparison operators.

Parameters:

val -> BigDecimal to which this BigDecimal is 
         to be compared.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_compareTo_$_$Ljava_math_BigDecimal$$$_$I"));
throw ex;
//must have a return of type int
};
var java_math_BigDecimal_$_divideAndRemainder_$_$Ljava_math_BigDecimal$$$_$_$$$$$_Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a two-element BigDecimal array containing the
 result of divideToIntegralValue followed by the result of
 remainder on the two operands.
 
 Note that if both the integer quotient and remainder are
 needed, this method is faster than using the
 divideToIntegralValue and remainder methods
 separately because the division need only be carried out once.

Parameters:

divisor -> value by which this BigDecimal is to be divided, 
         and the remainder computed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_divideAndRemainder_$_$Ljava_math_BigDecimal$$$_$_$$$$$_Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal[]
};
var java_math_BigDecimal_$_divideAndRemainder_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$_$$$$$_Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a two-element BigDecimal array containing the
 result of divideToIntegralValue followed by the result of
 remainder on the two operands calculated with rounding
 according to the context settings.
 
 Note that if both the integer quotient and remainder are
 needed, this method is faster than using the
 divideToIntegralValue and remainder methods
 separately because the division need only be carried out once.

Parameters:

divisor -> value by which this BigDecimal is to be divided, 
         and the remainder computed.

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_divideAndRemainder_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$_$$$$$_Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal[]
};
var java_math_BigDecimal_$_divideToIntegralValue_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose value is the integer part
 of the quotient (this / divisor) rounded down.  The
 preferred scale of the result is (this.scale() -
 divisor.scale()).

Parameters:

divisor -> value by which this BigDecimal is to be divided.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_divideToIntegralValue_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_divideToIntegralValue_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a BigDecimal whose value is the integer part
 of (this / divisor).  Since the integer part of the
 exact quotient does not depend on the rounding mode, the
 rounding mode does not affect the values returned by this
 method.  The preferred scale of the result is
 (this.scale() - divisor.scale()).  An
 ArithmeticException is thrown if the integer part of
 the exact quotient needs more than mc.precision
 digits.

Parameters:

divisor -> value by which this BigDecimal is to be divided.

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_divideToIntegralValue_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this /
 divisor), and whose preferred scale is (this.scale() -
 divisor.scale()); if the exact quotient cannot be
 represented (because it has a non-terminating decimal
 expansion) an ArithmeticException is thrown.

Parameters:

divisor -> value by which this BigDecimal is to be divided.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_I$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this /
 divisor), and whose scale is this.scale().  If
 rounding must be performed to generate a result with the given
 scale, the specified rounding mode is applied.
 
 The new divide(BigDecimal, RoundingMode) method
 should be used in preference to this legacy method.

Parameters:

divisor -> value by which this BigDecimal is to be divided.

roundingMode -> rounding mode to apply.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_I$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_II$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this /
 divisor), and whose scale is as specified.  If rounding must
 be performed to generate a result with the specified scale, the
 specified rounding mode is applied.
 
 The new divide(BigDecimal, int, RoundingMode) method
 should be used in preference to this legacy method.

Parameters:

divisor -> value by which this BigDecimal is to be divided.

scale -> scale of the BigDecimal quotient to be returned.

roundingMode -> rounding mode to apply.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_II$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_ILjava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this /
 divisor), and whose scale is as specified.  If rounding must
 be performed to generate a result with the specified scale, the
 specified rounding mode is applied.

Parameters:

divisor -> value by which this BigDecimal is to be divided.

scale -> scale of the BigDecimal quotient to be returned.

roundingMode -> rounding mode to apply.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_ILjava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this /
 divisor), with rounding according to the context settings.

Parameters:

divisor -> value by which this BigDecimal is to be divided.

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_Ljava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this /
 divisor), and whose scale is this.scale().  If
 rounding must be performed to generate a result with the given
 scale, the specified rounding mode is applied.

Parameters:

divisor -> value by which this BigDecimal is to be divided.

roundingMode -> rounding mode to apply.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_Ljava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_doubleValue_$_$$D = function (arg0) {
/*implement method!*/
/*
Converts this BigDecimal to a double.
 This conversion is similar to the narrowing
 primitive conversion from double to
 float as defined in the Java Language
 Specification: if this BigDecimal has too great a
 magnitude represent as a double, it will be
 converted to Double.NEGATIVE_INFINITY or Double.POSITIVE_INFINITY as appropriate.  Note that even when
 the return value is finite, this conversion can lose
 information about the precision of the BigDecimal
 value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_doubleValue_$_$$D"));
throw ex;
//must have a return of type double
};
var java_math_BigDecimal_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Compares this BigDecimal with the specified
 Object for equality.  Unlike compareTo, this method considers two
 BigDecimal objects equal only if they are equal in
 value and scale (thus 2.0 is not equal to 2.00 when compared by
 this method).

Parameters:

x -> Object to which this BigDecimal is 
         to be compared.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_equals_$_$Ljava_lang_Object$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_math_BigDecimal_$_floatValue_$_$$F = function (arg0) {
/*implement method!*/
/*
Converts this BigDecimal to a float.
 This conversion is similar to the narrowing
 primitive conversion from double to
 float defined in the Java Language
 Specification: if this BigDecimal has too great a
 magnitude to represent as a float, it will be
 converted to Float.NEGATIVE_INFINITY or Float.POSITIVE_INFINITY as appropriate.  Note that even when
 the return value is finite, this conversion can lose
 information about the precision of the BigDecimal
 value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_floatValue_$_$$F"));
throw ex;
//must have a return of type float
};
var java_math_BigDecimal_$_hashCode_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the hash code for this BigDecimal.  Note that
 two BigDecimal objects that are numerically equal but
 differ in scale (like 2.0 and 2.00) will generally not
 have the same hash code.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_hashCode_$_$$I"));
throw ex;
//must have a return of type int
};
var java_math_BigDecimal_$_intValueExact_$_$$I = function (arg0) {
/*implement method!*/
/*
Converts this BigDecimal to an int, checking
 for lost information.  If this BigDecimal has a
 nonzero fractional part or is out of the possible range for an
 int result then an ArithmeticException is
 thrown.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_intValueExact_$_$$I"));
throw ex;
//must have a return of type int
};
var java_math_BigDecimal_$_intValue_$_$$I = function (arg0) {
/*implement method!*/
/*
Converts this BigDecimal to an int.  This
 conversion is analogous to a narrowing
 primitive conversion from double to
 short as defined in the Java Language
 Specification: any fractional part of this
 BigDecimal will be discarded, and if the resulting
 "BigInteger" is too big to fit in an
 int, only the low-order 32 bits are returned.
 Note that this conversion can lose information about the
 overall magnitude and precision of this BigDecimal
 value as well as return a result with the opposite sign.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_intValue_$_$$I"));
throw ex;
//must have a return of type int
};
var java_math_BigDecimal_$_longValueExact_$_$$J = function (arg0) {
/*implement method!*/
/*
Converts this BigDecimal to a long, checking
 for lost information.  If this BigDecimal has a
 nonzero fractional part or is out of the possible range for a
 long result then an ArithmeticException is
 thrown.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_longValueExact_$_$$J"));
throw ex;
//must have a return of type long
};
var java_math_BigDecimal_$_longValue_$_$$J = function (arg0) {
/*implement method!*/
/*
Converts this BigDecimal to a long.  This
 conversion is analogous to a narrowing
 primitive conversion from double to
 short as defined in the Java Language
 Specification: any fractional part of this
 BigDecimal will be discarded, and if the resulting
 "BigInteger" is too big to fit in a
 long, only the low-order 64 bits are returned.
 Note that this conversion can lose information about the
 overall magnitude and precision of this BigDecimal value as well
 as return a result with the opposite sign.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_longValue_$_$$J"));
throw ex;
//must have a return of type long
};
var java_math_BigDecimal_$_max_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns the maximum of this BigDecimal and val.

Parameters:

val -> value with which the maximum is to be computed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_max_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_min_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns the minimum of this BigDecimal and
 val.

Parameters:

val -> value with which the minimum is to be computed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_min_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_movePointLeft_$_$I$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal which is equivalent to this one
 with the decimal point moved n places to the left.  If
 n is non-negative, the call merely adds n to
 the scale.  If n is negative, the call is equivalent
 to movePointRight(-n).  The BigDecimal
 returned by this call has value (this ×
 10-n) and scale max(this.scale()+n,
 0).

Parameters:

n -> number of places to move the decimal point to the left.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_movePointLeft_$_$I$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_movePointRight_$_$I$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal which is equivalent to this one
 with the decimal point moved n places to the right.
 If n is non-negative, the call merely subtracts
 n from the scale.  If n is negative, the call
 is equivalent to movePointLeft(-n).  The
 BigDecimal returned by this call has value (this
 × 10n) and scale max(this.scale()-n,
 0).

Parameters:

n -> number of places to move the decimal point to the right.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_movePointRight_$_$I$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_multiply_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this ×
 multiplicand), and whose scale is (this.scale() +
 multiplicand.scale()).

Parameters:

multiplicand -> value to be multiplied by this BigDecimal.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_multiply_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_multiply_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this ×
 multiplicand), with rounding according to the context settings.

Parameters:

multiplicand -> value to be multiplied by this BigDecimal.

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_multiply_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_negate_$_$$Ljava_math_BigDecimal$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (-this),
 and whose scale is this.scale().

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_negate_$_$$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_negate_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (-this),
 with rounding according to the context settings.

Parameters:

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_negate_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_plus_$_$$Ljava_math_BigDecimal$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (+this), and whose
 scale is this.scale().
 
 This method, which simply returns this BigDecimal
 is included for symmetry with the unary minus method negate().

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_plus_$_$$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_plus_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (+this),
 with rounding according to the context settings.
 
 The effect of this method is identical to that of the round(MathContext) method.

Parameters:

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_plus_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_pow_$_$I$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose value is
 (thisn), The power is computed exactly, to
 unlimited precision.
 
 The parameter n must be in the range 0 through
 999999999, inclusive.  ZERO.pow(0) returns ONE.

 Note that future releases may expand the allowable exponent
 range of this method.

Parameters:

n -> power to raise this BigDecimal to.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_pow_$_$I$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_pow_$_$ILjava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a BigDecimal whose value is
 (thisn).  The current implementation uses
 the core algorithm defined in ANSI standard X3.274-1996 with
 rounding according to the context settings.  In general, the
 returned numerical value is within two ulps of the exact
 numerical value for the chosen precision.  Note that future
 releases may use a different algorithm with a decreased
 allowable error bound and increased allowable exponent range.

 The X3.274-1996 algorithm is:

 
  An ArithmeticException exception is thrown if
  
    abs(n) > 999999999
    mc.precision == 0 and n < 0
    mc.precision > 0 and n has more than
    mc.precision decimal digits
  

  if n is zero, ONE is returned even if
 this is zero, otherwise
 
    if n is positive, the result is calculated via
   the repeated squaring technique into a single accumulator.
   The individual multiplications with the accumulator use the
   same math context settings as in mc except for a
   precision increased to mc.precision + elength + 1
   where elength is the number of decimal digits in
   n.

    if n is negative, the result is calculated as if
   n were positive; this value is then divided into one
   using the working precision specified above.

    The final value from either the positive or negative case
   is then rounded to the destination precision.

Parameters:

n -> power to raise this BigDecimal to.

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_pow_$_$ILjava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_precision_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the precision of this BigDecimal.  (The
 precision is the number of digits in the unscaled value.)

 The precision of a zero value is 1.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_precision_$_$$I"));
throw ex;
//must have a return of type int
};
var java_math_BigDecimal_$_remainder_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this % divisor).
 
 The remainder is given by
 this.subtract(this.divideToIntegralValue(divisor).multiply(divisor)).
 Note that this is not the modulo operation (the result can be
 negative).

Parameters:

divisor -> value by which this BigDecimal is to be divided.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_remainder_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_remainder_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this %
 divisor), with rounding according to the context settings.
 The MathContext settings affect the implicit divide
 used to compute the remainder.  The remainder computation
 itself is by definition exact.  Therefore, the remainder may
 contain more than mc.getPrecision() digits.
 
 The remainder is given by
 this.subtract(this.divideToIntegralValue(divisor,
 mc).multiply(divisor)).  Note that this is not the modulo
 operation (the result can be negative).

Parameters:

divisor -> value by which this BigDecimal is to be divided.

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_remainder_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_round_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal rounded according to the
 MathContext settings.  If the precision setting is 0 then
 no rounding takes place.
 
 The effect of this method is identical to that of the
 plus(MathContext) method.

Parameters:

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_round_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_scaleByPowerOfTen_$_$I$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose numerical value is equal to
 (this * 10n).  The scale of
 the result is (this.scale() - n).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_scaleByPowerOfTen_$_$I$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_scale_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the scale of this BigDecimal.  If zero
 or positive, the scale is the number of digits to the right of
 the decimal point.  If negative, the unscaled value of the
 number is multiplied by ten to the power of the negation of the
 scale.  For example, a scale of -3 means the unscaled
 value is multiplied by 1000.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_scale_$_$$I"));
throw ex;
//must have a return of type int
};
var java_math_BigDecimal_$_setScale_$_$I$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose scale is the specified
 value, and whose value is numerically equal to this
 BigDecimal's.  Throws an ArithmeticException
 if this is not possible.
 
 This call is typically used to increase the scale, in which
 case it is guaranteed that there exists a BigDecimal
 of the specified scale and the correct value.  The call can
 also be used to reduce the scale if the caller knows that the
 BigDecimal has sufficiently many zeros at the end of
 its fractional part (i.e., factors of ten in its integer value)
 to allow for the rescaling without changing its value.
 
 This method returns the same result as the two-argument
 versions of setScale, but saves the caller the trouble
 of specifying a rounding mode in cases where it is irrelevant.
 
 Note that since BigDecimal objects are immutable,
 calls of this method do not result in the original
 object being modified, contrary to the usual convention of
 having methods named setX mutate field
 X.  Instead, setScale returns an
 object with the proper scale; the returned object may or may
 not be newly allocated.

Parameters:

newScale -> scale of the BigDecimal value to be returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_setScale_$_$I$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_setScale_$_$II$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a BigDecimal whose scale is the specified
 value, and whose unscaled value is determined by multiplying or
 dividing this BigDecimal's unscaled value by the
 appropriate power of ten to maintain its overall value.  If the
 scale is reduced by the operation, the unscaled value must be
 divided (rather than multiplied), and the value may be changed;
 in this case, the specified rounding mode is applied to the
 division.
 
 Note that since BigDecimal objects are immutable, calls of
 this method do not result in the original object being
 modified, contrary to the usual convention of having methods
 named setX mutate field X.
 Instead, setScale returns an object with the proper
 scale; the returned object may or may not be newly allocated.
 
 The new setScale(int, RoundingMode) method should
 be used in preference to this legacy method.

Parameters:

newScale -> scale of the BigDecimal value to be returned.

roundingMode -> The rounding mode to apply.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_setScale_$_$II$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_setScale_$_$ILjava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a BigDecimal whose scale is the specified
 value, and whose unscaled value is determined by multiplying or
 dividing this BigDecimal's unscaled value by the
 appropriate power of ten to maintain its overall value.  If the
 scale is reduced by the operation, the unscaled value must be
 divided (rather than multiplied), and the value may be changed;
 in this case, the specified rounding mode is applied to the
 division.

 Note that since BigDecimal objects are immutable, calls of
 this method do not result in the original object being
 modified, contrary to the usual convention of having methods
 named setX mutate field X.
 Instead, setScale returns an object with the proper
 scale; the returned object may or may not be newly allocated.

Parameters:

newScale -> scale of the BigDecimal value to be returned.

roundingMode -> The rounding mode to apply.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_setScale_$_$ILjava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_shortValueExact_$_$$S = function (arg0) {
/*implement method!*/
/*
Converts this BigDecimal to a short, checking
 for lost information.  If this BigDecimal has a
 nonzero fractional part or is out of the possible range for a
 short result then an ArithmeticException is
 thrown.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_shortValueExact_$_$$S"));
throw ex;
//must have a return of type short
};
var java_math_BigDecimal_$_signum_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the signum function of this BigDecimal.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_signum_$_$$I"));
throw ex;
//must have a return of type int
};
var java_math_BigDecimal_$_stripTrailingZeros_$_$$Ljava_math_BigDecimal$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a BigDecimal which is numerically equal to
 this one but with any trailing zeros removed from the
 representation.  For example, stripping the trailing zeros from
 the BigDecimal value 600.0, which has
 [BigInteger, scale] components equals to
 [6000, 1], yields 6E2 with [BigInteger,
 scale] components equals to [6, -2]

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_stripTrailingZeros_$_$$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_subtract_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this -
 subtrahend), and whose scale is max(this.scale(),
 subtrahend.scale()).

Parameters:

subtrahend -> value to be subtracted from this BigDecimal.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_subtract_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_subtract_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a BigDecimal whose value is (this - subtrahend),
 with rounding according to the context settings.

 If subtrahend is zero then this, rounded if necessary, is used as the
 result.  If this is zero then the result is subtrahend.negate(mc).

Parameters:

subtrahend -> value to be subtracted from this BigDecimal.

mc -> the context to use.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_subtract_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_toBigIntegerExact_$_$$Ljava_math_BigInteger$$$_ = function (arg0) {
/*implement method!*/
/*
Converts this BigDecimal to a BigInteger,
 checking for lost information.  An exception is thrown if this
 BigDecimal has a nonzero fractional part.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_toBigIntegerExact_$_$$Ljava_math_BigInteger$$$_"));
throw ex;
//must have a return of type BigInteger
};
var java_math_BigDecimal_$_toBigInteger_$_$$Ljava_math_BigInteger$$$_ = function (arg0) {
/*implement method!*/
/*
Converts this BigDecimal to a BigInteger.
 This conversion is analogous to a narrowing
 primitive conversion from double to
 long as defined in the Java Language
 Specification: any fractional part of this
 BigDecimal will be discarded.  Note that this
 conversion can lose information about the precision of the
 BigDecimal value.
 
 To have an exception thrown if the conversion is inexact (in
 other words if a nonzero fractional part is discarded), use the
 toBigIntegerExact() method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_toBigInteger_$_$$Ljava_math_BigInteger$$$_"));
throw ex;
//must have a return of type BigInteger
};
var java_math_BigDecimal_$_toEngineeringString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of this BigDecimal,
 using engineering notation if an exponent is needed.
 
 Returns a string that represents the BigDecimal as
 described in the toString() method, except that if
 exponential notation is used, the power of ten is adjusted to
 be a multiple of three (engineering notation) such that the
 integer part of nonzero values will be in the range 1 through
 999.  If exponential notation is used for zero values, a
 decimal point and one or two fractional zero digits are used so
 that the scale of the zero value is preserved.  Note that
 unlike the output of toString(), the output of this
 method is not guaranteed to recover the same [integer,
 scale] pair of this BigDecimal if the output string is
 converting back to a BigDecimal using the string constructor.  The result of this method meets
 the weaker constraint of always producing a numerically equal
 result from applying the string constructor to the method's output.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_toEngineeringString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_math_BigDecimal_$_toPlainString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of this BigDecimal
 without an exponent field.  For values with a positive scale,
 the number of digits to the right of the decimal point is used
 to indicate scale.  For values with a zero or negative scale,
 the resulting string is generated as if the value were
 converted to a numerically equal value with zero scale and as
 if all the trailing zeros of the zero scale value were present
 in the result.

 The entire string is prefixed by a minus sign character '-'
 ('\u002D') if the unscaled value is less than
 zero. No sign character is prefixed if the unscaled value is
 zero or positive.

 Note that if the result of this method is passed to the
 string constructor, only the
 numerical value of this BigDecimal will necessarily be
 recovered; the representation of the new BigDecimal
 may have a different scale.  In particular, if this
 BigDecimal has a negative scale, the string resulting
 from this method will have a scale of zero when processed by
 the string constructor.

 (This method behaves analogously to the toString
 method in 1.4 and earlier releases.)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_toPlainString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_math_BigDecimal_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the string representation of this BigDecimal,
 using scientific notation if an exponent is needed.
 
 A standard canonical string form of the BigDecimal
 is created as though by the following steps: first, the
 absolute value of the unscaled value of the BigDecimal
 is converted to a string in base ten using the characters
 '0' through '9' with no leading zeros (except
 if its value is zero, in which case a single '0'
 character is used).
 
 Next, an adjusted exponent is calculated; this is the
 negated scale, plus the number of characters in the converted
 unscaled value, less one.  That is,
 -scale+(ulength-1), where ulength is the
 length of the absolute value of the unscaled value in decimal
 digits (its precision).
 
 If the scale is greater than or equal to zero and the
 adjusted exponent is greater than or equal to -6, the
 number will be converted to a character form without using
 exponential notation.  In this case, if the scale is zero then
 no decimal point is added and if the scale is positive a
 decimal point will be inserted with the scale specifying the
 number of characters to the right of the decimal point.
 '0' characters are added to the left of the converted
 unscaled value as necessary.  If no character precedes the
 decimal point after this insertion then a conventional
 '0' character is prefixed.
 
 Otherwise (that is, if the scale is negative, or the
 adjusted exponent is less than -6), the number will be
 converted to a character form using exponential notation.  In
 this case, if the converted BigInteger has more than
 one digit a decimal point is inserted after the first digit.
 An exponent in character form is then suffixed to the converted
 unscaled value (perhaps with inserted decimal point); this
 comprises the letter 'E' followed immediately by the
 adjusted exponent converted to a character form.  The latter is
 in base ten, using the characters '0' through
 '9' with no leading zeros, and is always prefixed by a
 sign character '-' ('\u002D') if the
 adjusted exponent is negative, '+'
 ('\u002B') otherwise).
 
 Finally, the entire string is prefixed by a minus sign
 character '-' ('\u002D') if the unscaled
 value is less than zero.  No sign character is prefixed if the
 unscaled value is zero or positive.
 
 Examples:
 For each representation [unscaled value, scale]
 on the left, the resulting string is shown on the right.
  [123,0]      "123"
 [-123,0]     "-123"
 [123,-1]     "1.23E+3"
 [123,-3]     "1.23E+5"
 [123,1]      "12.3"
 [123,5]      "0.00123"
 [123,10]     "1.23E-8"
 [-123,12]    "-1.23E-10"
 

 Notes:
 

 There is a one-to-one mapping between the distinguishable
 BigDecimal values and the result of this conversion.
 That is, every distinguishable BigDecimal value
 (unscaled value and scale) has a unique string representation
 as a result of using toString.  If that string
 representation is converted back to a BigDecimal using
 the BigDecimal(String) constructor, then the original
 value will be recovered.
 
 The string produced for a given number is always the same;
 it is not affected by locale.  This means that it can be used
 as a canonical string representation for exchanging decimal
 data, or as a key for a Hashtable, etc.  Locale-sensitive
 number formatting and parsing is handled by the NumberFormat class and its subclasses.
 
 The toEngineeringString() method may be used for
 presenting numbers with exponents in engineering notation, and the
 setScale method may be used for
 rounding a BigDecimal so it has a known number of digits after
 the decimal point.
 
 The digit-to-character mapping provided by
 Character.forDigit is used.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_toString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_math_BigDecimal_$_ulp_$_$$Ljava_math_BigDecimal$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the size of an ulp, a unit in the last place, of this
 BigDecimal.  An ulp of a nonzero BigDecimal
 value is the positive distance between this value and the
 BigDecimal value next larger in magnitude with the
 same number of digits.  An ulp of a zero value is numerically
 equal to 1 with the scale of this.  The result is
 stored with the same scale as this so the result
 for zero and nonzero values is equal to [1,
 this.scale()].

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_ulp_$_$$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_unscaledValue_$_$$Ljava_math_BigInteger$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a BigInteger whose value is the unscaled
 value of this BigDecimal.  (Computes (this *
 10this.scale()).)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_unscaledValue_$_$$Ljava_math_BigInteger$$$_"));
throw ex;
//must have a return of type BigInteger
};
var java_math_BigDecimal_$_valueOf_$_$D$Ljava_math_BigDecimal$$$_ = function (arg0) {
/*implement method!*/
/*
Translates a double into a BigDecimal, using
 the double's canonical string representation provided
 by the Double.toString(double) method.
 
 Note: This is generally the preferred way to convert
 a double (or float) into a
 BigDecimal, as the value returned is equal to that
 resulting from constructing a BigDecimal from the
 result of using Double.toString(double).

Parameters:

val -> double to convert to a BigDecimal.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_valueOf_$_$D$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_valueOf_$_$J$Ljava_math_BigDecimal$$$_ = function (arg0) {
/*implement method!*/
/*
Translates a long value into a BigDecimal
 with a scale of zero.  This "static factory method"
 is provided in preference to a (long) constructor
 because it allows for reuse of frequently used
 BigDecimal values.

Parameters:

val -> value of the BigDecimal.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_valueOf_$_$J$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_valueOf_$_$JI$Ljava_math_BigDecimal$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Translates a long unscaled value and an
 int scale into a BigDecimal.  This
 "static factory method" is provided in preference to
 a (long, int) constructor because it
 allows for reuse of frequently used BigDecimal values..

Parameters:

unscaledVal -> unscaled value of the BigDecimal.

scale -> scale of the BigDecimal.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_math_BigDecimal_$_valueOf_$_$JI$Ljava_math_BigDecimal$$$_"));
throw ex;
//must have a return of type BigDecimal
};
var java_math_BigDecimal_$_byteValue_$_$$B = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_byteValue_$_$$B(arg0);};
var java_math_BigDecimal_$_shortValue_$_$$S = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_shortValue_$_$$S(arg0);};
var java_math_BigDecimal_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$J$V(arg0, arg1);};
var java_math_BigDecimal_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_math_BigDecimal_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$$V(arg0);};
var java_math_BigDecimal_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_math_BigDecimal_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notify_$_$$V(arg0);};
var java_math_BigDecimal_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notifyAll_$_$$V(arg0);};
java_math_BigDecimal.prototype._$$$init$$$__$_$D$V = java_math_BigDecimal_$__$$$init$$$__$_$D$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$DLjava_math_MathContext$$$_$V = java_math_BigDecimal_$__$$$init$$$__$_$DLjava_math_MathContext$$$_$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$I$V = java_math_BigDecimal_$__$$$init$$$__$_$I$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$ILjava_math_MathContext$$$_$V = java_math_BigDecimal_$__$$$init$$$__$_$ILjava_math_MathContext$$$_$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$J$V = java_math_BigDecimal_$__$$$init$$$__$_$J$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$JLjava_math_MathContext$$$_$V = java_math_BigDecimal_$__$$$init$$$__$_$JLjava_math_MathContext$$$_$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_math_BigDecimal_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$Ljava_lang_String$$$_Ljava_math_MathContext$$$_$V = java_math_BigDecimal_$__$$$init$$$__$_$Ljava_lang_String$$$_Ljava_math_MathContext$$$_$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$Ljava_math_BigInteger$$$_$V = java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$Ljava_math_BigInteger$$$_I$V = java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_I$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$Ljava_math_BigInteger$$$_ILjava_math_MathContext$$$_$V = java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_ILjava_math_MathContext$$$_$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$Ljava_math_BigInteger$$$_Ljava_math_MathContext$$$_$V = java_math_BigDecimal_$__$$$init$$$__$_$Ljava_math_BigInteger$$$_Ljava_math_MathContext$$$_$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$_$$$$$_C$V = java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_C$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$_$$$$$_CII$V = java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_CII$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$_$$$$$_CIILjava_math_MathContext$$$_$V = java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_CIILjava_math_MathContext$$$_$V;
java_math_BigDecimal.prototype._$$$init$$$__$_$_$$$$$_CLjava_math_MathContext$$$_$V = java_math_BigDecimal_$__$$$init$$$__$_$_$$$$$_CLjava_math_MathContext$$$_$V;
java_math_BigDecimal.prototype.abs_$_$$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_abs_$_$$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.abs_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_abs_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.access$000_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigInteger$$$_ = java_math_BigDecimal_$_access$000_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigInteger$$$_;
java_math_BigDecimal.access$100_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigInteger$$$_ = java_math_BigDecimal_$_access$100_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigInteger$$$_;
java_math_BigDecimal.prototype.add_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_add_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.add_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_add_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.byteValueExact_$_$$B = java_math_BigDecimal_$_byteValueExact_$_$$B;
java_math_BigDecimal.prototype.compareTo_$_$Ljava_lang_Object$$$_$I = java_math_BigDecimal_$_compareTo_$_$Ljava_lang_Object$$$_$I;
java_math_BigDecimal.prototype.compareTo_$_$Ljava_math_BigDecimal$$$_$I = java_math_BigDecimal_$_compareTo_$_$Ljava_math_BigDecimal$$$_$I;
java_math_BigDecimal.prototype.divideAndRemainder_$_$Ljava_math_BigDecimal$$$_$_$$$$$_Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_divideAndRemainder_$_$Ljava_math_BigDecimal$$$_$_$$$$$_Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.divideAndRemainder_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$_$$$$$_Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_divideAndRemainder_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$_$$$$$_Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.divideToIntegralValue_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_divideToIntegralValue_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.divideToIntegralValue_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_divideToIntegralValue_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.divide_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.divide_$_$Ljava_math_BigDecimal$$$_I$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_I$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.divide_$_$Ljava_math_BigDecimal$$$_II$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_II$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.divide_$_$Ljava_math_BigDecimal$$$_ILjava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_ILjava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.divide_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.divide_$_$Ljava_math_BigDecimal$$$_Ljava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_divide_$_$Ljava_math_BigDecimal$$$_Ljava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.doubleValue_$_$$D = java_math_BigDecimal_$_doubleValue_$_$$D;
java_math_BigDecimal.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_math_BigDecimal_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_math_BigDecimal.prototype.floatValue_$_$$F = java_math_BigDecimal_$_floatValue_$_$$F;
java_math_BigDecimal.prototype.hashCode_$_$$I = java_math_BigDecimal_$_hashCode_$_$$I;
java_math_BigDecimal.prototype.intValueExact_$_$$I = java_math_BigDecimal_$_intValueExact_$_$$I;
java_math_BigDecimal.prototype.intValue_$_$$I = java_math_BigDecimal_$_intValue_$_$$I;
java_math_BigDecimal.prototype.longValueExact_$_$$J = java_math_BigDecimal_$_longValueExact_$_$$J;
java_math_BigDecimal.prototype.longValue_$_$$J = java_math_BigDecimal_$_longValue_$_$$J;
java_math_BigDecimal.prototype.max_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_max_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.min_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_min_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.movePointLeft_$_$I$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_movePointLeft_$_$I$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.movePointRight_$_$I$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_movePointRight_$_$I$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.multiply_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_multiply_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.multiply_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_multiply_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.negate_$_$$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_negate_$_$$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.negate_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_negate_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.plus_$_$$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_plus_$_$$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.plus_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_plus_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.pow_$_$I$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_pow_$_$I$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.pow_$_$ILjava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_pow_$_$ILjava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.precision_$_$$I = java_math_BigDecimal_$_precision_$_$$I;
java_math_BigDecimal.prototype.remainder_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_remainder_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.remainder_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_remainder_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.round_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_round_$_$Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.scaleByPowerOfTen_$_$I$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_scaleByPowerOfTen_$_$I$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.scale_$_$$I = java_math_BigDecimal_$_scale_$_$$I;
java_math_BigDecimal.prototype.setScale_$_$I$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_setScale_$_$I$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.setScale_$_$II$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_setScale_$_$II$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.setScale_$_$ILjava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_setScale_$_$ILjava_math_RoundingMode$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.shortValueExact_$_$$S = java_math_BigDecimal_$_shortValueExact_$_$$S;
java_math_BigDecimal.prototype.signum_$_$$I = java_math_BigDecimal_$_signum_$_$$I;
java_math_BigDecimal.prototype.stripTrailingZeros_$_$$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_stripTrailingZeros_$_$$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.subtract_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_subtract_$_$Ljava_math_BigDecimal$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.subtract_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_subtract_$_$Ljava_math_BigDecimal$$$_Ljava_math_MathContext$$$_$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.toBigIntegerExact_$_$$Ljava_math_BigInteger$$$_ = java_math_BigDecimal_$_toBigIntegerExact_$_$$Ljava_math_BigInteger$$$_;
java_math_BigDecimal.prototype.toBigInteger_$_$$Ljava_math_BigInteger$$$_ = java_math_BigDecimal_$_toBigInteger_$_$$Ljava_math_BigInteger$$$_;
java_math_BigDecimal.prototype.toEngineeringString_$_$$Ljava_lang_String$$$_ = java_math_BigDecimal_$_toEngineeringString_$_$$Ljava_lang_String$$$_;
java_math_BigDecimal.prototype.toPlainString_$_$$Ljava_lang_String$$$_ = java_math_BigDecimal_$_toPlainString_$_$$Ljava_lang_String$$$_;
java_math_BigDecimal.prototype.toString_$_$$Ljava_lang_String$$$_ = java_math_BigDecimal_$_toString_$_$$Ljava_lang_String$$$_;
java_math_BigDecimal.prototype.ulp_$_$$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_ulp_$_$$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.unscaledValue_$_$$Ljava_math_BigInteger$$$_ = java_math_BigDecimal_$_unscaledValue_$_$$Ljava_math_BigInteger$$$_;
java_math_BigDecimal.valueOf_$_$D$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_valueOf_$_$D$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.valueOf_$_$J$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_valueOf_$_$J$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.valueOf_$_$JI$Ljava_math_BigDecimal$$$_ = java_math_BigDecimal_$_valueOf_$_$JI$Ljava_math_BigDecimal$$$_;
java_math_BigDecimal.prototype.byteValue_$_$$B = java_math_BigDecimal_$_byteValue_$_$$B;
java_math_BigDecimal.prototype.shortValue_$_$$S = java_math_BigDecimal_$_shortValue_$_$$S;
java_math_BigDecimal.prototype.wait_$_$J$V = java_math_BigDecimal_$_wait_$_$J$V;
java_math_BigDecimal.prototype.wait_$_$JI$V = java_math_BigDecimal_$_wait_$_$JI$V;
java_math_BigDecimal.prototype.wait_$_$$V = java_math_BigDecimal_$_wait_$_$$V;
java_math_BigDecimal.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_math_BigDecimal_$_getClass_$_$$Ljava_lang_Class$$$_;
java_math_BigDecimal.prototype.notify_$_$$V = java_math_BigDecimal_$_notify_$_$$V;
java_math_BigDecimal.prototype.notifyAll_$_$$V = java_math_BigDecimal_$_notifyAll_$_$$V;
