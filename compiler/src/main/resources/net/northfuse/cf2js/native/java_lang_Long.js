/** @constructor */
function java_lang_Long() {}
var java_lang_Long_$__$$$init$$$__$_$J$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Long object that
 represents the specified long argument.


Parameters:value - the value to be represented by the 
          Long object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$__$$$init$$$__$_$J$V"));
throw ex;
};
var java_lang_Long_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Long object that
 represents the long value indicated by the
 String parameter. The string is converted to a
 long value in exactly the manner used by the
 parseLong method for radix 10.


Parameters:s - the String to be converted to a 
                   Long.
Throws:
NumberFormatException - if the String does not
               contain a parsable long.See Also:parseLong(java.lang.String, int)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$__$$$init$$$__$_$Ljava_lang_String$$$_$V"));
throw ex;
};
var java_lang_Long_$_bitCount_$_$J$I = function (arg0) {
/*implement method!*/
/*
Returns the number of one-bits in the two's complement binary
 representation of the specified long value.  This function is
 sometimes referred to as the population count.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_bitCount_$_$J$I"));
throw ex;
//must have a return of type int
};
var java_lang_Long_$_byteValue_$_$$B = function (arg0) {
/*implement method!*/
/*
Returns the value of this Long as a
 byte.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_byteValue_$_$$B"));
throw ex;
//must have a return of type byte
};
var java_lang_Long_$_compareTo_$_$Ljava_lang_Long$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Compares two Long objects numerically.

Parameters:

anotherLong -> the Long to be compared.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_compareTo_$_$Ljava_lang_Long$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Long_$_compareTo_$_$Ljava_lang_Object$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_compareTo_$_$Ljava_lang_Object$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Long_$_decode_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_ = function (arg0) {
/*implement method!*/
/*
Decodes a String into a Long.
 Accepts decimal, hexadecimal, and octal numbers given by the
 following grammar:

 
 
 DecodableString:
 Signopt DecimalNumeral
 Signopt 0x HexDigits
 Signopt 0X HexDigits
 Signopt # HexDigits
 Signopt 0 OctalDigits
 
 Sign:
 -
 
 

 DecimalNumeral, HexDigits, and OctalDigits
 are defined in §3.10.1 
 of the Java 
 Language Specification.
 
 The sequence of characters following an (optional) negative
 sign and/or radix specifier ("0x",
 "0X", "#", or
 leading zero) is parsed as by the Long.parseLong
 method with the indicated radix (10, 16, or 8).  This sequence
 of characters must represent a positive value or a NumberFormatException will be thrown.  The result is negated
 if first character of the specified String is the
 minus sign.  No whitespace characters are permitted in the
 String.

Parameters:

nm -> the String to decode.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_decode_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_"));
throw ex;
//must have a return of type Long
};
var java_lang_Long_$_doubleValue_$_$$D = function (arg0) {
/*implement method!*/
/*
Returns the value of this Long as a
 double.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_doubleValue_$_$$D"));
throw ex;
//must have a return of type double
};
var java_lang_Long_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Compares this object to the specified object.  The result is
 true if and only if the argument is not
 null and is a Long object that
 contains the same long value as this object.

Parameters:

obj -> the object to compare with.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_equals_$_$Ljava_lang_Object$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Long_$_floatValue_$_$$F = function (arg0) {
/*implement method!*/
/*
Returns the value of this Long as a
 float.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_floatValue_$_$$F"));
throw ex;
//must have a return of type float
};
var java_lang_Long_$_getChars_$_$JI_$$$$$_C$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_getChars_$_$JI_$$$$$_C$V"));
throw ex;
//must have a return of type void
};
var java_lang_Long_$_getLong_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_ = function (arg0) {
/*implement method!*/
/*
Determines the long value of the system property
 with the specified name.
 
 The first argument is treated as the name of a system property.
 System properties are accessible through the System.getProperty(java.lang.String) method. The
 string value of this property is then interpreted as a
 long value and a Long object
 representing this value is returned.  Details of possible
 numeric formats can be found with the definition of
 getProperty.
 
 If there is no property with the specified name, if the
 specified name is empty or null, or if the
 property does not have the correct numeric format, then
 null is returned.
 
 In other words, this method returns a Long object equal to 
 the value of:
 
 getLong(nm, null)

Parameters:

nm -> property name.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_getLong_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_"));
throw ex;
//must have a return of type Long
};
var java_lang_Long_$_getLong_$_$Ljava_lang_String$$$_J$Ljava_lang_Long$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Determines the long value of the system property
 with the specified name.
 
 The first argument is treated as the name of a system property.
 System properties are accessible through the System.getProperty(java.lang.String) method. The
 string value of this property is then interpreted as a
 long value and a Long object
 representing this value is returned.  Details of possible
 numeric formats can be found with the definition of
 getProperty.
 
 The second argument is the default value. A Long object
 that represents the value of the second argument is returned if there
 is no property of the specified name, if the property does not have
 the correct numeric format, or if the specified name is empty or null.
 
 In other words, this method returns a Long object equal 
 to the value of:
 
 getLong(nm, new Long(val))
 
 but in practice it may be implemented in a manner such as: 
  Long result = getLong(nm, null);
 return (result == null) ? new Long(val) : result;
 
 to avoid the unnecessary allocation of a Long object when 
 the default value is not needed.

Parameters:

nm -> property name.

val -> default value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_getLong_$_$Ljava_lang_String$$$_J$Ljava_lang_Long$$$_"));
throw ex;
//must have a return of type Long
};
var java_lang_Long_$_getLong_$_$Ljava_lang_String$$$_Ljava_lang_Long$$$_$Ljava_lang_Long$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns the long value of the system property with
 the specified name.  The first argument is treated as the name
 of a system property.  System properties are accessible through
 the System.getProperty(java.lang.String)
 method. The string value of this property is then interpreted
 as a long value, as per the
 Long.decode method, and a Long object
 representing this value is returned.
 
 If the property value begins with the two ASCII characters
 0x or the ASCII character #, not followed by 
 a minus sign, then the rest of it is parsed as a hexadecimal integer
 exactly as for the method valueOf(java.lang.String, int) 
 with radix 16. 
 If the property value begins with the ASCII character
 0 followed by another character, it is parsed as
 an octal integer exactly as by the method valueOf(java.lang.String, int) with radix 8.
 Otherwise the property value is parsed as a decimal
 integer exactly as by the method 
 valueOf(java.lang.String, int) with radix 10.
 
 
 Note that, in every case, neither L
 ('\u004C') nor l
 ('\u006C') is permitted to appear at the end
 of the property value as a type indicator, as would be
 permitted in Java programming language source code.
 
 The second argument is the default value. The default value is
 returned if there is no property of the specified name, if the
 property does not have the correct numeric format, or if the
 specified name is empty or null.

Parameters:

nm -> property name.

val -> default value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_getLong_$_$Ljava_lang_String$$$_Ljava_lang_Long$$$_$Ljava_lang_Long$$$_"));
throw ex;
//must have a return of type Long
};
var java_lang_Long_$_hashCode_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns a hash code for this Long. The result is
 the exclusive OR of the two halves of the primitive
 long value held by this Long
 object. That is, the hashcode is the value of the expression:
  (int)(this.longValue()^(this.longValue()>>>32))

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_hashCode_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Long_$_highestOneBit_$_$J$J = function (arg0) {
/*implement method!*/
/*
Returns a long value with at most a single one-bit, in the
 position of the highest-order ("leftmost") one-bit in the specified
 long value.  Returns zero if the specified value has no
 one-bits in its two's complement binary representation, that is, if it
 is equal to zero.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_highestOneBit_$_$J$J"));
throw ex;
//must have a return of type long
};
var java_lang_Long_$_intValue_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the value of this Long as an
 int.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_intValue_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Long_$_longValue_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the value of this Long as a
 long value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_longValue_$_$$J"));
throw ex;
//must have a return of type long
};
var java_lang_Long_$_lowestOneBit_$_$J$J = function (arg0) {
/*implement method!*/
/*
Returns a long value with at most a single one-bit, in the
 position of the lowest-order ("rightmost") one-bit in the specified
 long value.  Returns zero if the specified value has no
 one-bits in its two's complement binary representation, that is, if it
 is equal to zero.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_lowestOneBit_$_$J$J"));
throw ex;
//must have a return of type long
};
var java_lang_Long_$_numberOfLeadingZeros_$_$J$I = function (arg0) {
/*implement method!*/
/*
Returns the number of zero bits preceding the highest-order
 ("leftmost") one-bit in the two's complement binary representation
 of the specified long value.  Returns 64 if the
 specified value has no one-bits in its two's complement representation,
 in other words if it is equal to zero.

 Note that this method is closely related to the logarithm base 2.
 For all positive long values x:
 
 floor(log2(x)) = 63 - numberOfLeadingZeros(x)
 ceil(log2(x)) = 64 - numberOfLeadingZeros(x - 1)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_numberOfLeadingZeros_$_$J$I"));
throw ex;
//must have a return of type int
};
var java_lang_Long_$_numberOfTrailingZeros_$_$J$I = function (arg0) {
/*implement method!*/
/*
Returns the number of zero bits following the lowest-order ("rightmost")
 one-bit in the two's complement binary representation of the specified
 long value.  Returns 64 if the specified value has no
 one-bits in its two's complement representation, in other words if it is
 equal to zero.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_numberOfTrailingZeros_$_$J$I"));
throw ex;
//must have a return of type int
};
var java_lang_Long_$_parseLong_$_$Ljava_lang_String$$$_$J = function (arg0) {
/*implement method!*/
/*
Parses the string argument as a signed decimal
 long.  The characters in the string must all be
 decimal digits, except that the first character may be an ASCII
 minus sign '-' (\u002D') to
 indicate a negative value. The resulting long
 value is returned, exactly as if the argument and the radix
 10 were given as arguments to the parseLong(java.lang.String, int) method.
 
 Note that neither the character L
 ('\u004C') nor l
 ('\u006C') is permitted to appear at the end
 of the string as a type indicator, as would be permitted in
 Java programming language source code.

Parameters:

s -> a String containing the long
             representation to be parsed

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_parseLong_$_$Ljava_lang_String$$$_$J"));
throw ex;
//must have a return of type long
};
var java_lang_Long_$_parseLong_$_$Ljava_lang_String$$$_I$J = function (arg0, arg1) {
/*implement method!*/
/*
Parses the string argument as a signed long in the
 radix specified by the second argument. The characters in the
 string must all be digits of the specified radix (as determined
 by whether Character.digit(char, int) returns
 a nonnegative value), except that the first character may be an
 ASCII minus sign '-' ('\u002D') to
 indicate a negative value. The resulting long
 value is returned.
 
 Note that neither the character L
 ('\u004C') nor l
 ('\u006C') is permitted to appear at the end
 of the string as a type indicator, as would be permitted in
 Java programming language source code - except that either
 L or l may appear as a digit for a
 radix greater than 22.
 
 An exception of type NumberFormatException is
 thrown if any of the following situations occurs:
 
 The first argument is null or is a string of
 length zero.
 The radix is either smaller than Character.MIN_RADIX or larger than Character.MAX_RADIX.
 Any character of the string is not a digit of the specified
 radix, except that the first character may be a minus sign
 '-' ('\u002d') provided that the
 string is longer than length 1.
 The value represented by the string is not a value of type
      long. 
 
 Examples:
  parseLong("0", 10) returns 0L
 parseLong("473", 10) returns 473L
 parseLong("-0", 10) returns 0L
 parseLong("-FF", 16) returns -255L
 parseLong("1100110", 2) returns 102L
 parseLong("99", 8) throws a NumberFormatException
 parseLong("Hazelnut", 10) throws a NumberFormatException
 parseLong("Hazelnut", 36) returns 1356099454469L

Parameters:

s -> the String containing the
                     long representation to be parsed.

radix -> the radix to be used while parsing s.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_parseLong_$_$Ljava_lang_String$$$_I$J"));
throw ex;
//must have a return of type long
};
var java_lang_Long_$_reverseBytes_$_$J$J = function (arg0) {
/*implement method!*/
/*
Returns the value obtained by reversing the order of the bytes in the
 two's complement representation of the specified long value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_reverseBytes_$_$J$J"));
throw ex;
//must have a return of type long
};
var java_lang_Long_$_reverse_$_$J$J = function (arg0) {
/*implement method!*/
/*
Returns the value obtained by reversing the order of the bits in the
 two's complement binary representation of the specified long
 value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_reverse_$_$J$J"));
throw ex;
//must have a return of type long
};
var java_lang_Long_$_rotateLeft_$_$JI$J = function (arg0, arg1) {
/*implement method!*/
/*
Returns the value obtained by rotating the two's complement binary
 representation of the specified long value left by the
 specified number of bits.  (Bits shifted out of the left hand, or
 high-order, side reenter on the right, or low-order.)

 Note that left rotation with a negative distance is equivalent to
 right rotation: rotateLeft(val, -distance) == rotateRight(val,
 distance).  Note also that rotation by any multiple of 64 is a
 no-op, so all but the last six bits of the rotation distance can be
 ignored, even if the distance is negative: rotateLeft(val,
 distance) == rotateLeft(val, distance & 0x3F).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_rotateLeft_$_$JI$J"));
throw ex;
//must have a return of type long
};
var java_lang_Long_$_rotateRight_$_$JI$J = function (arg0, arg1) {
/*implement method!*/
/*
Returns the value obtained by rotating the two's complement binary
 representation of the specified long value right by the
 specified number of bits.  (Bits shifted out of the right hand, or
 low-order, side reenter on the left, or high-order.)

 Note that right rotation with a negative distance is equivalent to
 left rotation: rotateRight(val, -distance) == rotateLeft(val,
 distance).  Note also that rotation by any multiple of 64 is a
 no-op, so all but the last six bits of the rotation distance can be
 ignored, even if the distance is negative: rotateRight(val,
 distance) == rotateRight(val, distance & 0x3F).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_rotateRight_$_$JI$J"));
throw ex;
//must have a return of type long
};
var java_lang_Long_$_shortValue_$_$$S = function (arg0) {
/*implement method!*/
/*
Returns the value of this Long as a
 short.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_shortValue_$_$$S"));
throw ex;
//must have a return of type short
};
var java_lang_Long_$_signum_$_$J$I = function (arg0) {
/*implement method!*/
/*
Returns the signum function of the specified long value.  (The
 return value is -1 if the specified value is negative; 0 if the
 specified value is zero; and 1 if the specified value is positive.)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_signum_$_$J$I"));
throw ex;
//must have a return of type int
};
var java_lang_Long_$_stringSize_$_$J$I = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_stringSize_$_$J$I"));
throw ex;
//must have a return of type int
};
var java_lang_Long_$_toBinaryString_$_$J$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of the long
 argument as an unsigned integer in base 2.
 
 The unsigned long value is the argument plus
 264 if the argument is negative; otherwise, it is
 equal to the argument.  This value is converted to a string of
 ASCII digits in binary (base 2) with no extra leading
 0s.  If the unsigned magnitude is zero, it is
 represented by a single zero character '0'
 ('\u0030'); otherwise, the first character of
 the representation of the unsigned magnitude will not be the
 zero character. The characters '0'
 ('\u0030') and '1'
 ('\u0031') are used as binary digits.

Parameters:

i -> a long to be converted to a string.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_toBinaryString_$_$J$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Long_$_toHexString_$_$J$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of the long
 argument as an unsigned integer in base 16.
 
 The unsigned long value is the argument plus
 264 if the argument is negative; otherwise, it is
 equal to the argument.  This value is converted to a string of
 ASCII digits in hexadecimal (base 16) with no extra
 leading 0s.  If the unsigned magnitude is zero, it
 is represented by a single zero character '0'
 ('\u0030'); otherwise, the first character of
 the representation of the unsigned magnitude will not be the
 zero character. The following characters are used as
 hexadecimal digits:
  0123456789abcdef
 
 These are the characters '\u0030' through
 '\u0039' and  '\u0061' through
 '\u0066'.  If uppercase letters are desired,
 the String.toUpperCase() method may be called
 on the result:
  Long.toHexString(n).toUpperCase()

Parameters:

i -> a long to be converted to a string.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_toHexString_$_$J$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Long_$_toOctalString_$_$J$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of the long
 argument as an unsigned integer in base 8.
 
 The unsigned long value is the argument plus
 264 if the argument is negative; otherwise, it is
 equal to the argument.  This value is converted to a string of
 ASCII digits in octal (base 8) with no extra leading
 0s.
 
 If the unsigned magnitude is zero, it is represented by a
 single zero character '0'
 ('\u0030'); otherwise, the first character of
 the representation of the unsigned magnitude will not be the
 zero character. The following characters are used as octal
 digits:
  01234567
 
 These are the characters '\u0030' through 
 '\u0037'.

Parameters:

i -> a long to be converted to a string.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_toOctalString_$_$J$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Long_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a String object representing this
 Long's value.  The value is converted to signed
 decimal representation and returned as a string, exactly as if
 the long value were given as an argument to the
 toString(long) method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_toString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Long_$_toString_$_$J$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a String object representing the specified
 long.  The argument is converted to signed decimal
 representation and returned as a string, exactly as if the
 argument and the radix 10 were given as arguments to the toString(long, int) method.

Parameters:

i -> a long to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_toString_$_$J$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Long_$_toString_$_$JI$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a string representation of the first argument in the
 radix specified by the second argument.
 
 If the radix is smaller than Character.MIN_RADIX
 or larger than Character.MAX_RADIX, then the radix
 10 is used instead.
 
 If the first argument is negative, the first element of the
 result is the ASCII minus sign '-'
 ('\u002d'). If the first argument is not
 negative, no sign character appears in the result.
 
 The remaining characters of the result represent the magnitude
 of the first argument. If the magnitude is zero, it is
 represented by a single zero character '0'
 ('\u0030'); otherwise, the first character of
 the representation of the magnitude will not be the zero
 character.  The following ASCII characters are used as digits:
    0123456789abcdefghijklmnopqrstuvwxyz
 
 These are '\u0030' through
 '\u0039' and '\u0061' through
 '\u007a'. If radix is
 N, then the first N of these characters
 are used as radix-N digits in the order shown. Thus,
 the digits for hexadecimal (radix 16) are
 0123456789abcdef. If uppercase letters are
 desired, the String.toUpperCase() method may
 be called on the result:
  Long.toString(n, 16).toUpperCase()

Parameters:

i -> a longto be converted to a string.

radix -> the radix to use in the string representation.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_toString_$_$JI$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Long_$_valueOf_$_$J$Ljava_lang_Long$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a Long instance representing the specified
 long value.
 If a new Long instance is not required, this method
 should generally be used in preference to the constructor
 Long(long), as this method is likely to yield
 significantly better space and time performance by caching
 frequently requested values.

Parameters:

l -> a long value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_valueOf_$_$J$Ljava_lang_Long$$$_"));
throw ex;
//must have a return of type Long
};
var java_lang_Long_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a Long object holding the value
 of the specified String. The argument is
 interpreted as representing a signed decimal long,
 exactly as if the argument were given to the parseLong(java.lang.String) method. The result is a
 Long object that represents the integer value
 specified by the string.
 
 In other words, this method returns a Long object
 equal to the value of:

  new Long(Long.parseLong(s))

Parameters:

s -> the string to be parsed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_"));
throw ex;
//must have a return of type Long
};
var java_lang_Long_$_valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Long$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a Long object holding the value
 extracted from the specified String when parsed
 with the radix given by the second argument.  The first
 argument is interpreted as representing a signed
 long in the radix specified by the second
 argument, exactly as if the arguments were given to the parseLong(java.lang.String, int) method. The result is a
 Long object that represents the long
 value specified by the string.
 
 In other words, this method returns a Long object equal 
 to the value of:

 
 new Long(Long.parseLong(s, radix))

Parameters:

s -> the string to be parsed

radix -> the radix to be used in interpreting s

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Long_$_valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Long$$$_"));
throw ex;
//must have a return of type Long
};
var java_lang_Long_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$J$V(arg0, arg1);};
var java_lang_Long_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_Long_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$$V(arg0);};
var java_lang_Long_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_Long_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notify_$_$$V(arg0);};
var java_lang_Long_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notifyAll_$_$$V(arg0);};
java_lang_Long.prototype._$$$init$$$__$_$J$V = java_lang_Long_$__$$$init$$$__$_$J$V;
java_lang_Long.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_lang_Long_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_lang_Long.bitCount_$_$J$I = java_lang_Long_$_bitCount_$_$J$I;
java_lang_Long.prototype.byteValue_$_$$B = java_lang_Long_$_byteValue_$_$$B;
java_lang_Long.prototype.compareTo_$_$Ljava_lang_Long$$$_$I = java_lang_Long_$_compareTo_$_$Ljava_lang_Long$$$_$I;
java_lang_Long.prototype.compareTo_$_$Ljava_lang_Object$$$_$I = java_lang_Long_$_compareTo_$_$Ljava_lang_Object$$$_$I;
java_lang_Long.decode_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_ = java_lang_Long_$_decode_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_;
java_lang_Long.prototype.doubleValue_$_$$D = java_lang_Long_$_doubleValue_$_$$D;
java_lang_Long.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_Long_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_Long.prototype.floatValue_$_$$F = java_lang_Long_$_floatValue_$_$$F;
java_lang_Long.getChars_$_$JI_$$$$$_C$V = java_lang_Long_$_getChars_$_$JI_$$$$$_C$V;
java_lang_Long.getLong_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_ = java_lang_Long_$_getLong_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_;
java_lang_Long.getLong_$_$Ljava_lang_String$$$_J$Ljava_lang_Long$$$_ = java_lang_Long_$_getLong_$_$Ljava_lang_String$$$_J$Ljava_lang_Long$$$_;
java_lang_Long.getLong_$_$Ljava_lang_String$$$_Ljava_lang_Long$$$_$Ljava_lang_Long$$$_ = java_lang_Long_$_getLong_$_$Ljava_lang_String$$$_Ljava_lang_Long$$$_$Ljava_lang_Long$$$_;
java_lang_Long.prototype.hashCode_$_$$I = java_lang_Long_$_hashCode_$_$$I;
java_lang_Long.highestOneBit_$_$J$J = java_lang_Long_$_highestOneBit_$_$J$J;
java_lang_Long.prototype.intValue_$_$$I = java_lang_Long_$_intValue_$_$$I;
java_lang_Long.prototype.longValue_$_$$J = java_lang_Long_$_longValue_$_$$J;
java_lang_Long.lowestOneBit_$_$J$J = java_lang_Long_$_lowestOneBit_$_$J$J;
java_lang_Long.numberOfLeadingZeros_$_$J$I = java_lang_Long_$_numberOfLeadingZeros_$_$J$I;
java_lang_Long.numberOfTrailingZeros_$_$J$I = java_lang_Long_$_numberOfTrailingZeros_$_$J$I;
java_lang_Long.parseLong_$_$Ljava_lang_String$$$_$J = java_lang_Long_$_parseLong_$_$Ljava_lang_String$$$_$J;
java_lang_Long.parseLong_$_$Ljava_lang_String$$$_I$J = java_lang_Long_$_parseLong_$_$Ljava_lang_String$$$_I$J;
java_lang_Long.reverseBytes_$_$J$J = java_lang_Long_$_reverseBytes_$_$J$J;
java_lang_Long.reverse_$_$J$J = java_lang_Long_$_reverse_$_$J$J;
java_lang_Long.rotateLeft_$_$JI$J = java_lang_Long_$_rotateLeft_$_$JI$J;
java_lang_Long.rotateRight_$_$JI$J = java_lang_Long_$_rotateRight_$_$JI$J;
java_lang_Long.prototype.shortValue_$_$$S = java_lang_Long_$_shortValue_$_$$S;
java_lang_Long.signum_$_$J$I = java_lang_Long_$_signum_$_$J$I;
java_lang_Long.stringSize_$_$J$I = java_lang_Long_$_stringSize_$_$J$I;
java_lang_Long.toBinaryString_$_$J$Ljava_lang_String$$$_ = java_lang_Long_$_toBinaryString_$_$J$Ljava_lang_String$$$_;
java_lang_Long.toHexString_$_$J$Ljava_lang_String$$$_ = java_lang_Long_$_toHexString_$_$J$Ljava_lang_String$$$_;
java_lang_Long.toOctalString_$_$J$Ljava_lang_String$$$_ = java_lang_Long_$_toOctalString_$_$J$Ljava_lang_String$$$_;
java_lang_Long.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_Long_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_Long.toString_$_$J$Ljava_lang_String$$$_ = java_lang_Long_$_toString_$_$J$Ljava_lang_String$$$_;
java_lang_Long.toString_$_$JI$Ljava_lang_String$$$_ = java_lang_Long_$_toString_$_$JI$Ljava_lang_String$$$_;
java_lang_Long.valueOf_$_$J$Ljava_lang_Long$$$_ = java_lang_Long_$_valueOf_$_$J$Ljava_lang_Long$$$_;
java_lang_Long.valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_ = java_lang_Long_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Long$$$_;
java_lang_Long.valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Long$$$_ = java_lang_Long_$_valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Long$$$_;
java_lang_Long.prototype.wait_$_$J$V = java_lang_Long_$_wait_$_$J$V;
java_lang_Long.prototype.wait_$_$JI$V = java_lang_Long_$_wait_$_$JI$V;
java_lang_Long.prototype.wait_$_$$V = java_lang_Long_$_wait_$_$$V;
java_lang_Long.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_Long_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_Long.prototype.notify_$_$$V = java_lang_Long_$_notify_$_$$V;
java_lang_Long.prototype.notifyAll_$_$$V = java_lang_Long_$_notifyAll_$_$$V;
