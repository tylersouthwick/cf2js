/** @constructor */
function java_util_concurrent_ThreadPoolExecutor() {}
var java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_$V = function (arg0, arg1, arg2, arg3, arg4, arg5) {
/*implement constructor!*/
/*
Creates a new ThreadPoolExecutor with the given initial
 parameters and default thread factory and rejected execution handler.
 It may be more convenient to use one of the Executors factory
 methods instead of this general purpose constructor.


Parameters:corePoolSize - the number of threads to keep in the
 pool, even if they are idle.maximumPoolSize - the maximum number of threads to allow in the
 pool.keepAliveTime - when the number of threads is greater than
 the core, this is the maximum time that excess idle threads
 will wait for new tasks before terminating.unit - the time unit for the keepAliveTime
 argument.workQueue - the queue to use for holding tasks before they
 are executed. This queue will hold only the Runnable
 tasks submitted by the execute method.
Throws:
IllegalArgumentException - if corePoolSize or
 keepAliveTime less than zero, or if maximumPoolSize less than or
 equal to zero, or if corePoolSize greater than maximumPoolSize.
NullPointerException - if workQueue is null

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_$V"));
throw ex;
};
var java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_RejectedExecutionHandler$$$_$V = function (arg0, arg1, arg2, arg3, arg4, arg5, arg6) {
/*implement constructor!*/
/*
Creates a new ThreadPoolExecutor with the given initial
 parameters and default thread factory.


Parameters:corePoolSize - the number of threads to keep in the
 pool, even if they are idle.maximumPoolSize - the maximum number of threads to allow in the
 pool.keepAliveTime - when the number of threads is greater than
 the core, this is the maximum time that excess idle threads
 will wait for new tasks before terminating.unit - the time unit for the keepAliveTime
 argument.workQueue - the queue to use for holding tasks before they
 are executed. This queue will hold only the Runnable
 tasks submitted by the execute method.handler - the handler to use when execution is blocked
 because the thread bounds and queue capacities are reached.
Throws:
IllegalArgumentException - if corePoolSize or
 keepAliveTime less than zero, or if maximumPoolSize less than or
 equal to zero, or if corePoolSize greater than maximumPoolSize.
NullPointerException - if workQueue
 or handler are null.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_RejectedExecutionHandler$$$_$V"));
throw ex;
};
var java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_ThreadFactory$$$_$V = function (arg0, arg1, arg2, arg3, arg4, arg5, arg6) {
/*implement constructor!*/
/*
Creates a new ThreadPoolExecutor with the given initial
 parameters and default rejected execution handler.


Parameters:corePoolSize - the number of threads to keep in the
 pool, even if they are idle.maximumPoolSize - the maximum number of threads to allow in the
 pool.keepAliveTime - when the number of threads is greater than
 the core, this is the maximum time that excess idle threads
 will wait for new tasks before terminating.unit - the time unit for the keepAliveTime
 argument.workQueue - the queue to use for holding tasks before they
 are executed. This queue will hold only the Runnable
 tasks submitted by the execute method.threadFactory - the factory to use when the executor
 creates a new thread.
Throws:
IllegalArgumentException - if corePoolSize or
 keepAliveTime less than zero, or if maximumPoolSize less than or
 equal to zero, or if corePoolSize greater than maximumPoolSize.
NullPointerException - if workQueue
 or threadFactory are null.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_ThreadFactory$$$_$V"));
throw ex;
};
var java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_ThreadFactory$$$_Ljava_util_concurrent_RejectedExecutionHandler$$$_$V = function (arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7) {
/*implement constructor!*/
/*
Creates a new ThreadPoolExecutor with the given initial
 parameters.


Parameters:corePoolSize - the number of threads to keep in the
 pool, even if they are idle.maximumPoolSize - the maximum number of threads to allow in the
 pool.keepAliveTime - when the number of threads is greater than
 the core, this is the maximum time that excess idle threads
 will wait for new tasks before terminating.unit - the time unit for the keepAliveTime
 argument.workQueue - the queue to use for holding tasks before they
 are executed. This queue will hold only the Runnable
 tasks submitted by the execute method.threadFactory - the factory to use when the executor
 creates a new thread.handler - the handler to use when execution is blocked
 because the thread bounds and queue capacities are reached.
Throws:
IllegalArgumentException - if corePoolSize or
 keepAliveTime less than zero, or if maximumPoolSize less than or
 equal to zero, or if corePoolSize greater than maximumPoolSize.
NullPointerException - if workQueue
 or threadFactory or handler are null.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_ThreadFactory$$$_Ljava_util_concurrent_RejectedExecutionHandler$$$_$V"));
throw ex;
};
var java_util_concurrent_ThreadPoolExecutor_$_afterExecute_$_$Ljava_lang_Runnable$$$_Ljava_lang_Throwable$$$_$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Method invoked upon completion of execution of the given Runnable.
 This method is invoked by the thread that executed the task. If
 non-null, the Throwable is the uncaught RuntimeException
 or Error that caused execution to terminate abruptly.

 Note: When actions are enclosed in tasks (such as
 FutureTask) either explicitly or via methods such as
 submit, these task objects catch and maintain
 computational exceptions, and so they do not cause abrupt
 termination, and the internal exceptions are not
 passed to this method.

 This implementation does nothing, but may be customized in
 subclasses. Note: To properly nest multiple overridings, subclasses
 should generally invoke super.afterExecute at the
 beginning of this method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_afterExecute_$_$Ljava_lang_Runnable$$$_Ljava_lang_Throwable$$$_$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_allowCoreThreadTimeOut_$_$Z$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the policy governing whether core threads may time out and
 terminate if no tasks arrive within the keep-alive time, being
 replaced if needed when new tasks arrive. When false, core
 threads are never terminated due to lack of incoming
 tasks. When true, the same keep-alive policy applying to
 non-core threads applies also to core threads. To avoid
 continual thread replacement, the keep-alive time must be
 greater than zero when setting true. This method
 should in general be called before the pool is actively used.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_allowCoreThreadTimeOut_$_$Z$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_allowsCoreThreadTimeOut_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if this pool allows core threads to time out and
 terminate if no tasks arrive within the keepAlive time, being
 replaced if needed when new tasks arrive. When true, the same
 keep-alive policy applying to non-core threads applies also to
 core threads. When false (the default), core threads are never
 terminated due to lack of incoming tasks.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_allowsCoreThreadTimeOut_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_util_concurrent_ThreadPoolExecutor_$_awaitTermination_$_$JLjava_util_concurrent_TimeUnit$$$_$Z = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Description copied from interface: ExecutorService

Parameters:

timeout -> the maximum time to wait

unit -> the time unit of the timeout argument

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_awaitTermination_$_$JLjava_util_concurrent_TimeUnit$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_util_concurrent_ThreadPoolExecutor_$_beforeExecute_$_$Ljava_lang_Thread$$$_Ljava_lang_Runnable$$$_$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Method invoked prior to executing the given Runnable in the
 given thread.  This method is invoked by thread t that
 will execute task r, and may be used to re-initialize
 ThreadLocals, or to perform logging.

 This implementation does nothing, but may be customized in
 subclasses. Note: To properly nest multiple overridings, subclasses
 should generally invoke super.beforeExecute at the end of
 this method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_beforeExecute_$_$Ljava_lang_Thread$$$_Ljava_lang_Runnable$$$_$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_execute_$_$Ljava_lang_Runnable$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Executes the given task sometime in the future.  The task
 may execute in a new thread or in an existing pooled thread.

 If the task cannot be submitted for execution, either because this
 executor has been shutdown or because its capacity has been reached,
 the task is handled by the current RejectedExecutionHandler.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_execute_$_$Ljava_lang_Runnable$$$_$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_finalize_$_$$V = function (arg0) {
/*implement method!*/
/*
Invokes shutdown when this executor is no longer
 referenced.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_finalize_$_$$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_getActiveCount_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the approximate number of threads that are actively
 executing tasks.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getActiveCount_$_$$I"));
throw ex;
//must have a return of type int
};
var java_util_concurrent_ThreadPoolExecutor_$_getCompletedTaskCount_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the approximate total number of tasks that have
 completed execution. Because the states of tasks and threads
 may change dynamically during computation, the returned value
 is only an approximation, but one that does not ever decrease
 across successive calls.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getCompletedTaskCount_$_$$J"));
throw ex;
//must have a return of type long
};
var java_util_concurrent_ThreadPoolExecutor_$_getCorePoolSize_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the core number of threads.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getCorePoolSize_$_$$I"));
throw ex;
//must have a return of type int
};
var java_util_concurrent_ThreadPoolExecutor_$_getKeepAliveTime_$_$Ljava_util_concurrent_TimeUnit$$$_$J = function (arg0, arg1) {
/*implement method!*/
/*
Returns the thread keep-alive time, which is the amount of time
 that threads in excess of the core pool size may remain
 idle before being terminated.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getKeepAliveTime_$_$Ljava_util_concurrent_TimeUnit$$$_$J"));
throw ex;
//must have a return of type long
};
var java_util_concurrent_ThreadPoolExecutor_$_getLargestPoolSize_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the largest number of threads that have ever
 simultaneously been in the pool.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getLargestPoolSize_$_$$I"));
throw ex;
//must have a return of type int
};
var java_util_concurrent_ThreadPoolExecutor_$_getMaximumPoolSize_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the maximum allowed number of threads.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getMaximumPoolSize_$_$$I"));
throw ex;
//must have a return of type int
};
var java_util_concurrent_ThreadPoolExecutor_$_getPoolSize_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the current number of threads in the pool.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getPoolSize_$_$$I"));
throw ex;
//must have a return of type int
};
var java_util_concurrent_ThreadPoolExecutor_$_getQueue_$_$$Ljava_util_concurrent_BlockingQueue$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the task queue used by this executor. Access to the
 task queue is intended primarily for debugging and monitoring.
 This queue may be in active use.  Retrieving the task queue
 does not prevent queued tasks from executing.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getQueue_$_$$Ljava_util_concurrent_BlockingQueue$$$_"));
throw ex;
//must have a return of type BlockingQueue
};
var java_util_concurrent_ThreadPoolExecutor_$_getRejectedExecutionHandler_$_$$Ljava_util_concurrent_RejectedExecutionHandler$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the current handler for unexecutable tasks.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getRejectedExecutionHandler_$_$$Ljava_util_concurrent_RejectedExecutionHandler$$$_"));
throw ex;
//must have a return of type RejectedExecutionHandler
};
var java_util_concurrent_ThreadPoolExecutor_$_getTaskCount_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the approximate total number of tasks that have ever been
 scheduled for execution.  Because the states of tasks and
 threads may change dynamically during computation, the returned
 value is only an approximation.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getTaskCount_$_$$J"));
throw ex;
//must have a return of type long
};
var java_util_concurrent_ThreadPoolExecutor_$_getTask_$_$$Ljava_lang_Runnable$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getTask_$_$$Ljava_lang_Runnable$$$_"));
throw ex;
//must have a return of type Runnable
};
var java_util_concurrent_ThreadPoolExecutor_$_getThreadFactory_$_$$Ljava_util_concurrent_ThreadFactory$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the thread factory used to create new threads.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_getThreadFactory_$_$$Ljava_util_concurrent_ThreadFactory$$$_"));
throw ex;
//must have a return of type ThreadFactory
};
var java_util_concurrent_ThreadPoolExecutor_$_interruptIdleWorkers_$_$$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_interruptIdleWorkers_$_$$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_isShutdown_$_$$Z = function (arg0) {
/*implement method!*/
/*
Description copied from interface: ExecutorService

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_isShutdown_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_util_concurrent_ThreadPoolExecutor_$_isStopped_$_$$Z = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_isStopped_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_util_concurrent_ThreadPoolExecutor_$_isTerminated_$_$$Z = function (arg0) {
/*implement method!*/
/*
Description copied from interface: ExecutorService

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_isTerminated_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_util_concurrent_ThreadPoolExecutor_$_isTerminating_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if this executor is in the process of terminating
 after shutdown or shutdownNow but has not
 completely terminated.  This method may be useful for
 debugging. A return of true reported a sufficient
 period after shutdown may indicate that submitted tasks have
 ignored or suppressed interruption, causing this executor not
 to properly terminate.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_isTerminating_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_util_concurrent_ThreadPoolExecutor_$_prestartAllCoreThreads_$_$$I = function (arg0) {
/*implement method!*/
/*
Starts all core threads, causing them to idly wait for work. This
 overrides the default policy of starting core threads only when
 new tasks are executed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_prestartAllCoreThreads_$_$$I"));
throw ex;
//must have a return of type int
};
var java_util_concurrent_ThreadPoolExecutor_$_prestartCoreThread_$_$$Z = function (arg0) {
/*implement method!*/
/*
Starts a core thread, causing it to idly wait for work. This
 overrides the default policy of starting core threads only when
 new tasks are executed. This method will return false
 if all core threads have already been started.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_prestartCoreThread_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_util_concurrent_ThreadPoolExecutor_$_purge_$_$$V = function (arg0) {
/*implement method!*/
/*
Tries to remove from the work queue all Future
 tasks that have been cancelled. This method can be useful as a
 storage reclamation operation, that has no other impact on
 functionality. Cancelled tasks are never executed, but may
 accumulate in work queues until worker threads can actively
 remove them. Invoking this method instead tries to remove them now.
 However, this method may fail to remove tasks in
 the presence of interference by other threads.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_purge_$_$$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_reject_$_$Ljava_lang_Runnable$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_reject_$_$Ljava_lang_Runnable$$$_$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_remove_$_$Ljava_lang_Runnable$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Removes this task from the executor's internal queue if it is
 present, thus causing it not to be run if it has not already
 started.

  This method may be useful as one part of a cancellation
 scheme.  It may fail to remove tasks that have been converted
 into other forms before being placed on the internal queue. For
 example, a task entered using submit might be
 converted into a form that maintains Future status.
 However, in such cases, method purge()
 may be used to remove those Futures that have been cancelled.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_remove_$_$Ljava_lang_Runnable$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_util_concurrent_ThreadPoolExecutor_$_setCorePoolSize_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the core number of threads.  This overrides any value set
 in the constructor.  If the new value is smaller than the
 current value, excess existing threads will be terminated when
 they next become idle. If larger, new threads will, if needed,
 be started to execute any queued tasks.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_setCorePoolSize_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_setKeepAliveTime_$_$JLjava_util_concurrent_TimeUnit$$$_$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Sets the time limit for which threads may remain idle before
 being terminated.  If there are more than the core number of
 threads currently in the pool, after waiting this amount of
 time without processing a task, excess threads will be
 terminated.  This overrides any value set in the constructor.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_setKeepAliveTime_$_$JLjava_util_concurrent_TimeUnit$$$_$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_setMaximumPoolSize_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the maximum allowed number of threads. This overrides any
 value set in the constructor. If the new value is smaller than
 the current value, excess existing threads will be
 terminated when they next become idle.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_setMaximumPoolSize_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_setRejectedExecutionHandler_$_$Ljava_util_concurrent_RejectedExecutionHandler$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets a new handler for unexecutable tasks.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_setRejectedExecutionHandler_$_$Ljava_util_concurrent_RejectedExecutionHandler$$$_$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_setThreadFactory_$_$Ljava_util_concurrent_ThreadFactory$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the thread factory used to create new threads.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_setThreadFactory_$_$Ljava_util_concurrent_ThreadFactory$$$_$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_shutdownNow_$_$$Ljava_util_List$$$_ = function (arg0) {
/*implement method!*/
/*
Attempts to stop all actively executing tasks, halts the
 processing of waiting tasks, and returns a list of the tasks
 that were awaiting execution. These tasks are drained (removed)
 from the task queue upon return from this method.

 There are no guarantees beyond best-effort attempts to stop
 processing actively executing tasks.  This implementation
 cancels tasks via Thread.interrupt(), so any task that
 fails to respond to interrupts may never terminate.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_shutdownNow_$_$$Ljava_util_List$$$_"));
throw ex;
//must have a return of type List
};
var java_util_concurrent_ThreadPoolExecutor_$_shutdown_$_$$V = function (arg0) {
/*implement method!*/
/*
Initiates an orderly shutdown in which previously submitted
 tasks are executed, but no new tasks will be
 accepted. Invocation has no additional effect if already shut
 down.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_shutdown_$_$$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_terminated_$_$$V = function (arg0) {
/*implement method!*/
/*
Method invoked when the Executor has terminated.  Default
 implementation does nothing. Note: To properly nest multiple
 overridings, subclasses should generally invoke
 super.terminated within this method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_terminated_$_$$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_workerDone_$_$Ljava_util_concurrent_ThreadPoolExecutor$Worker$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_concurrent_ThreadPoolExecutor_$_workerDone_$_$Ljava_util_concurrent_ThreadPoolExecutor$Worker$$$_$V"));
throw ex;
//must have a return of type void
};
var java_util_concurrent_ThreadPoolExecutor_$_submit_$_$Ljava_lang_Runnable$$$_$Ljava_util_concurrent_Future$$$_ = function (arg0, arg1) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_submit_$_$Ljava_lang_Runnable$$$_$Ljava_util_concurrent_Future$$$_(arg0, arg1);};
var java_util_concurrent_ThreadPoolExecutor_$_submit_$_$Ljava_lang_Runnable$$$_Ljava_lang_Object$$$_$Ljava_util_concurrent_Future$$$_ = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_submit_$_$Ljava_lang_Runnable$$$_Ljava_lang_Object$$$_$Ljava_util_concurrent_Future$$$_(arg0, arg1, arg2);};
var java_util_concurrent_ThreadPoolExecutor_$_submit_$_$Ljava_util_concurrent_Callable$$$_$Ljava_util_concurrent_Future$$$_ = function (arg0, arg1) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_submit_$_$Ljava_util_concurrent_Callable$$$_$Ljava_util_concurrent_Future$$$_(arg0, arg1);};
var java_util_concurrent_ThreadPoolExecutor_$_invokeAll_$_$Ljava_util_Collection$$$_$Ljava_util_List$$$_ = function (arg0, arg1) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_invokeAll_$_$Ljava_util_Collection$$$_$Ljava_util_List$$$_(arg0, arg1);};
var java_util_concurrent_ThreadPoolExecutor_$_invokeAll_$_$Ljava_util_Collection$$$_JLjava_util_concurrent_TimeUnit$$$_$Ljava_util_List$$$_ = function (arg0, arg1, arg2, arg3) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_invokeAll_$_$Ljava_util_Collection$$$_JLjava_util_concurrent_TimeUnit$$$_$Ljava_util_List$$$_(arg0, arg1, arg2, arg3);};
var java_util_concurrent_ThreadPoolExecutor_$_invokeAny_$_$Ljava_util_Collection$$$_$Ljava_lang_Object$$$_ = function (arg0, arg1) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_invokeAny_$_$Ljava_util_Collection$$$_$Ljava_lang_Object$$$_(arg0, arg1);};
var java_util_concurrent_ThreadPoolExecutor_$_invokeAny_$_$Ljava_util_Collection$$$_JLjava_util_concurrent_TimeUnit$$$_$Ljava_lang_Object$$$_ = function (arg0, arg1, arg2, arg3) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_invokeAny_$_$Ljava_util_Collection$$$_JLjava_util_concurrent_TimeUnit$$$_$Ljava_lang_Object$$$_(arg0, arg1, arg2, arg3);};
var java_util_concurrent_ThreadPoolExecutor_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_wait_$_$J$V(arg0, arg1);};
var java_util_concurrent_ThreadPoolExecutor_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_util_concurrent_ThreadPoolExecutor_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_wait_$_$$V(arg0);};
var java_util_concurrent_ThreadPoolExecutor_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_util_concurrent_ThreadPoolExecutor_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_util_concurrent_ThreadPoolExecutor_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_hashCode_$_$$I(arg0);};
var java_util_concurrent_ThreadPoolExecutor_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_util_concurrent_ThreadPoolExecutor_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_notify_$_$$V(arg0);};
var java_util_concurrent_ThreadPoolExecutor_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_util_concurrent_AbstractExecutorService_$_notifyAll_$_$$V(arg0);};
java_util_concurrent_ThreadPoolExecutor.prototype._$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_$V = java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype._$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_RejectedExecutionHandler$$$_$V = java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_RejectedExecutionHandler$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype._$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_ThreadFactory$$$_$V = java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_ThreadFactory$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype._$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_ThreadFactory$$$_Ljava_util_concurrent_RejectedExecutionHandler$$$_$V = java_util_concurrent_ThreadPoolExecutor_$__$$$init$$$__$_$IIJLjava_util_concurrent_TimeUnit$$$_Ljava_util_concurrent_BlockingQueue$$$_Ljava_util_concurrent_ThreadFactory$$$_Ljava_util_concurrent_RejectedExecutionHandler$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype.afterExecute_$_$Ljava_lang_Runnable$$$_Ljava_lang_Throwable$$$_$V = java_util_concurrent_ThreadPoolExecutor_$_afterExecute_$_$Ljava_lang_Runnable$$$_Ljava_lang_Throwable$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype.allowCoreThreadTimeOut_$_$Z$V = java_util_concurrent_ThreadPoolExecutor_$_allowCoreThreadTimeOut_$_$Z$V;
java_util_concurrent_ThreadPoolExecutor.prototype.allowsCoreThreadTimeOut_$_$$Z = java_util_concurrent_ThreadPoolExecutor_$_allowsCoreThreadTimeOut_$_$$Z;
java_util_concurrent_ThreadPoolExecutor.prototype.awaitTermination_$_$JLjava_util_concurrent_TimeUnit$$$_$Z = java_util_concurrent_ThreadPoolExecutor_$_awaitTermination_$_$JLjava_util_concurrent_TimeUnit$$$_$Z;
java_util_concurrent_ThreadPoolExecutor.prototype.beforeExecute_$_$Ljava_lang_Thread$$$_Ljava_lang_Runnable$$$_$V = java_util_concurrent_ThreadPoolExecutor_$_beforeExecute_$_$Ljava_lang_Thread$$$_Ljava_lang_Runnable$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype.execute_$_$Ljava_lang_Runnable$$$_$V = java_util_concurrent_ThreadPoolExecutor_$_execute_$_$Ljava_lang_Runnable$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype.finalize_$_$$V = java_util_concurrent_ThreadPoolExecutor_$_finalize_$_$$V;
java_util_concurrent_ThreadPoolExecutor.prototype.getActiveCount_$_$$I = java_util_concurrent_ThreadPoolExecutor_$_getActiveCount_$_$$I;
java_util_concurrent_ThreadPoolExecutor.prototype.getCompletedTaskCount_$_$$J = java_util_concurrent_ThreadPoolExecutor_$_getCompletedTaskCount_$_$$J;
java_util_concurrent_ThreadPoolExecutor.prototype.getCorePoolSize_$_$$I = java_util_concurrent_ThreadPoolExecutor_$_getCorePoolSize_$_$$I;
java_util_concurrent_ThreadPoolExecutor.prototype.getKeepAliveTime_$_$Ljava_util_concurrent_TimeUnit$$$_$J = java_util_concurrent_ThreadPoolExecutor_$_getKeepAliveTime_$_$Ljava_util_concurrent_TimeUnit$$$_$J;
java_util_concurrent_ThreadPoolExecutor.prototype.getLargestPoolSize_$_$$I = java_util_concurrent_ThreadPoolExecutor_$_getLargestPoolSize_$_$$I;
java_util_concurrent_ThreadPoolExecutor.prototype.getMaximumPoolSize_$_$$I = java_util_concurrent_ThreadPoolExecutor_$_getMaximumPoolSize_$_$$I;
java_util_concurrent_ThreadPoolExecutor.prototype.getPoolSize_$_$$I = java_util_concurrent_ThreadPoolExecutor_$_getPoolSize_$_$$I;
java_util_concurrent_ThreadPoolExecutor.prototype.getQueue_$_$$Ljava_util_concurrent_BlockingQueue$$$_ = java_util_concurrent_ThreadPoolExecutor_$_getQueue_$_$$Ljava_util_concurrent_BlockingQueue$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.getRejectedExecutionHandler_$_$$Ljava_util_concurrent_RejectedExecutionHandler$$$_ = java_util_concurrent_ThreadPoolExecutor_$_getRejectedExecutionHandler_$_$$Ljava_util_concurrent_RejectedExecutionHandler$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.getTaskCount_$_$$J = java_util_concurrent_ThreadPoolExecutor_$_getTaskCount_$_$$J;
java_util_concurrent_ThreadPoolExecutor.prototype.getTask_$_$$Ljava_lang_Runnable$$$_ = java_util_concurrent_ThreadPoolExecutor_$_getTask_$_$$Ljava_lang_Runnable$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.getThreadFactory_$_$$Ljava_util_concurrent_ThreadFactory$$$_ = java_util_concurrent_ThreadPoolExecutor_$_getThreadFactory_$_$$Ljava_util_concurrent_ThreadFactory$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.interruptIdleWorkers_$_$$V = java_util_concurrent_ThreadPoolExecutor_$_interruptIdleWorkers_$_$$V;
java_util_concurrent_ThreadPoolExecutor.prototype.isShutdown_$_$$Z = java_util_concurrent_ThreadPoolExecutor_$_isShutdown_$_$$Z;
java_util_concurrent_ThreadPoolExecutor.prototype.isStopped_$_$$Z = java_util_concurrent_ThreadPoolExecutor_$_isStopped_$_$$Z;
java_util_concurrent_ThreadPoolExecutor.prototype.isTerminated_$_$$Z = java_util_concurrent_ThreadPoolExecutor_$_isTerminated_$_$$Z;
java_util_concurrent_ThreadPoolExecutor.prototype.isTerminating_$_$$Z = java_util_concurrent_ThreadPoolExecutor_$_isTerminating_$_$$Z;
java_util_concurrent_ThreadPoolExecutor.prototype.prestartAllCoreThreads_$_$$I = java_util_concurrent_ThreadPoolExecutor_$_prestartAllCoreThreads_$_$$I;
java_util_concurrent_ThreadPoolExecutor.prototype.prestartCoreThread_$_$$Z = java_util_concurrent_ThreadPoolExecutor_$_prestartCoreThread_$_$$Z;
java_util_concurrent_ThreadPoolExecutor.prototype.purge_$_$$V = java_util_concurrent_ThreadPoolExecutor_$_purge_$_$$V;
java_util_concurrent_ThreadPoolExecutor.prototype.reject_$_$Ljava_lang_Runnable$$$_$V = java_util_concurrent_ThreadPoolExecutor_$_reject_$_$Ljava_lang_Runnable$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype.remove_$_$Ljava_lang_Runnable$$$_$Z = java_util_concurrent_ThreadPoolExecutor_$_remove_$_$Ljava_lang_Runnable$$$_$Z;
java_util_concurrent_ThreadPoolExecutor.prototype.setCorePoolSize_$_$I$V = java_util_concurrent_ThreadPoolExecutor_$_setCorePoolSize_$_$I$V;
java_util_concurrent_ThreadPoolExecutor.prototype.setKeepAliveTime_$_$JLjava_util_concurrent_TimeUnit$$$_$V = java_util_concurrent_ThreadPoolExecutor_$_setKeepAliveTime_$_$JLjava_util_concurrent_TimeUnit$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype.setMaximumPoolSize_$_$I$V = java_util_concurrent_ThreadPoolExecutor_$_setMaximumPoolSize_$_$I$V;
java_util_concurrent_ThreadPoolExecutor.prototype.setRejectedExecutionHandler_$_$Ljava_util_concurrent_RejectedExecutionHandler$$$_$V = java_util_concurrent_ThreadPoolExecutor_$_setRejectedExecutionHandler_$_$Ljava_util_concurrent_RejectedExecutionHandler$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype.setThreadFactory_$_$Ljava_util_concurrent_ThreadFactory$$$_$V = java_util_concurrent_ThreadPoolExecutor_$_setThreadFactory_$_$Ljava_util_concurrent_ThreadFactory$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype.shutdownNow_$_$$Ljava_util_List$$$_ = java_util_concurrent_ThreadPoolExecutor_$_shutdownNow_$_$$Ljava_util_List$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.shutdown_$_$$V = java_util_concurrent_ThreadPoolExecutor_$_shutdown_$_$$V;
java_util_concurrent_ThreadPoolExecutor.prototype.terminated_$_$$V = java_util_concurrent_ThreadPoolExecutor_$_terminated_$_$$V;
java_util_concurrent_ThreadPoolExecutor.prototype.workerDone_$_$Ljava_util_concurrent_ThreadPoolExecutor$Worker$$$_$V = java_util_concurrent_ThreadPoolExecutor_$_workerDone_$_$Ljava_util_concurrent_ThreadPoolExecutor$Worker$$$_$V;
java_util_concurrent_ThreadPoolExecutor.prototype.submit_$_$Ljava_lang_Runnable$$$_$Ljava_util_concurrent_Future$$$_ = java_util_concurrent_ThreadPoolExecutor_$_submit_$_$Ljava_lang_Runnable$$$_$Ljava_util_concurrent_Future$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.submit_$_$Ljava_lang_Runnable$$$_Ljava_lang_Object$$$_$Ljava_util_concurrent_Future$$$_ = java_util_concurrent_ThreadPoolExecutor_$_submit_$_$Ljava_lang_Runnable$$$_Ljava_lang_Object$$$_$Ljava_util_concurrent_Future$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.submit_$_$Ljava_util_concurrent_Callable$$$_$Ljava_util_concurrent_Future$$$_ = java_util_concurrent_ThreadPoolExecutor_$_submit_$_$Ljava_util_concurrent_Callable$$$_$Ljava_util_concurrent_Future$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.invokeAll_$_$Ljava_util_Collection$$$_$Ljava_util_List$$$_ = java_util_concurrent_ThreadPoolExecutor_$_invokeAll_$_$Ljava_util_Collection$$$_$Ljava_util_List$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.invokeAll_$_$Ljava_util_Collection$$$_JLjava_util_concurrent_TimeUnit$$$_$Ljava_util_List$$$_ = java_util_concurrent_ThreadPoolExecutor_$_invokeAll_$_$Ljava_util_Collection$$$_JLjava_util_concurrent_TimeUnit$$$_$Ljava_util_List$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.invokeAny_$_$Ljava_util_Collection$$$_$Ljava_lang_Object$$$_ = java_util_concurrent_ThreadPoolExecutor_$_invokeAny_$_$Ljava_util_Collection$$$_$Ljava_lang_Object$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.invokeAny_$_$Ljava_util_Collection$$$_JLjava_util_concurrent_TimeUnit$$$_$Ljava_lang_Object$$$_ = java_util_concurrent_ThreadPoolExecutor_$_invokeAny_$_$Ljava_util_Collection$$$_JLjava_util_concurrent_TimeUnit$$$_$Ljava_lang_Object$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.wait_$_$J$V = java_util_concurrent_ThreadPoolExecutor_$_wait_$_$J$V;
java_util_concurrent_ThreadPoolExecutor.prototype.wait_$_$JI$V = java_util_concurrent_ThreadPoolExecutor_$_wait_$_$JI$V;
java_util_concurrent_ThreadPoolExecutor.prototype.wait_$_$$V = java_util_concurrent_ThreadPoolExecutor_$_wait_$_$$V;
java_util_concurrent_ThreadPoolExecutor.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_util_concurrent_ThreadPoolExecutor_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_util_concurrent_ThreadPoolExecutor.prototype.toString_$_$$Ljava_lang_String$$$_ = java_util_concurrent_ThreadPoolExecutor_$_toString_$_$$Ljava_lang_String$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.hashCode_$_$$I = java_util_concurrent_ThreadPoolExecutor_$_hashCode_$_$$I;
java_util_concurrent_ThreadPoolExecutor.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_util_concurrent_ThreadPoolExecutor_$_getClass_$_$$Ljava_lang_Class$$$_;
java_util_concurrent_ThreadPoolExecutor.prototype.notify_$_$$V = java_util_concurrent_ThreadPoolExecutor_$_notify_$_$$V;
java_util_concurrent_ThreadPoolExecutor.prototype.notifyAll_$_$$V = java_util_concurrent_ThreadPoolExecutor_$_notifyAll_$_$$V;
