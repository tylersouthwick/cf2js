/** @constructor */
function java_lang_Class() {}
var java_lang_Class_$_access$100_$_$_$$$$$_Ljava_lang_Object$$$__$$$$$_Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_access$100_$_$_$$$$$_Ljava_lang_Object$$$__$$$$$_Ljava_lang_Object$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_access$202_$_$Z$Z = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_access$202_$_$Z$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_access$302_$_$Z$Z = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_access$302_$_$Z$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_asSubclass_$_$Ljava_lang_Class$$$_$Ljava_lang_Class$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Casts this Class object to represent a subclass of the class
 represented by the specified class object.  Checks that that the cast
 is valid, and throws a ClassCastException if it is not.  If
 this method succeeds, it always returns a reference to this class object.

 This method is useful when a client needs to "narrow" the type of
 a Class object to pass it to an API that restricts the
 Class objects that it is willing to accept.  A cast would
 generate a compile-time warning, as the correctness of the cast
 could not be checked at runtime (because generic types are implemented
 by erasure).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_asSubclass_$_$Ljava_lang_Class$$$_$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_lang_Class_$_cast_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Casts an object to the class or interface represented
 by this Class object.

Parameters:

obj -> the object to be cast

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_cast_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object
};
var java_lang_Class_$_desiredAssertionStatus_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns the assertion status that would be assigned to this
 class if it were to be initialized at the time this method is invoked.
 If this class has had its assertion status set, the most recent
 setting will be returned; otherwise, if any package default assertion
 status pertains to this class, the most recent setting for the most
 specific pertinent package default assertion status is returned;
 otherwise, if this class is not a system class (i.e., it has a
 class loader) its class loader's default assertion status is returned;
 otherwise, the system class default assertion status is returned.
 
 Few programmers will have any need for this method; it is provided
 for the benefit of the JRE itself.  (It allows a class to determine at
 the time that it is initialized whether assertions should be enabled.)
 Note that this method is not guaranteed to return the actual
 assertion status that was (or will be) associated with the specified
 class when it was (or will be) initialized.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_desiredAssertionStatus_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_enumConstantDirectory_$_$$Ljava_util_Map$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_enumConstantDirectory_$_$$Ljava_util_Map$$$_"));
throw ex;
//must have a return of type Map
};
var java_lang_Class_$_forName_$_$Ljava_lang_String$$$_$Ljava_lang_Class$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the Class object associated with the class or
 interface with the given string name.  Invoking this method is
 equivalent to:

   Class.forName(className, true, currentLoader)
 

 where currentLoader denotes the defining class loader of
 the current class.

  For example, the following code fragment returns the
 runtime Class descriptor for the class named
 java.lang.Thread:

    Class t = Class.forName("java.lang.Thread")
 
 
 A call to forName("X") causes the class named 
 X to be initialized.

Parameters:

className -> the fully qualified name of the desired class.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_forName_$_$Ljava_lang_String$$$_$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_lang_Class_$_forName_$_$Ljava_lang_String$$$_ZLjava_lang_ClassLoader$$$_$Ljava_lang_Class$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the Class object associated with the class or
 interface with the given string name, using the given class loader.
 Given the fully qualified name for a class or interface (in the same
 format returned by getName) this method attempts to
 locate, load, and link the class or interface.  The specified class
 loader is used to load the class or interface.  If the parameter
 loader is null, the class is loaded through the bootstrap
 class loader.  The class is initialized only if the
 initialize parameter is true and if it has
 not been initialized earlier.

  If name denotes a primitive type or void, an attempt
 will be made to locate a user-defined class in the unnamed package whose
 name is name. Therefore, this method cannot be used to
 obtain any of the Class objects representing primitive
 types or void.

  If name denotes an array class, the component type of
 the array class is loaded but not initialized.

  For example, in an instance method the expression:

   Class.forName("Foo")
 

 is equivalent to:

   Class.forName("Foo", true, this.getClass().getClassLoader())
 

 Note that this method throws errors related to loading, linking or
 initializing as specified in Sections 12.2, 12.3 and 12.4 of The
 Java Language Specification.
 Note that this method does not check whether the requested class 
 is accessible to its caller.

  If the loader is null, and a security
 manager is present, and the caller's class loader is not null, then this
 method calls the security manager's checkPermission method
 with a RuntimePermission("getClassLoader") permission to
 ensure it's ok to access the bootstrap class loader.

Parameters:

name -> fully qualified name of the desired class

initialize -> whether the class must be initialized

loader -> class loader from which the class must be loaded

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_forName_$_$Ljava_lang_String$$$_ZLjava_lang_ClassLoader$$$_$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_lang_Class_$_getAnnotationType_$_$$Lsun_reflect_annotation_AnnotationType$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getAnnotationType_$_$$Lsun_reflect_annotation_AnnotationType$$$_"));
throw ex;
//must have a return of type AnnotationType
};
var java_lang_Class_$_getAnnotation_$_$Ljava_lang_Class$$$_$Ljava_lang_annotation_Annotation$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Description copied from interface: AnnotatedElement

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getAnnotation_$_$Ljava_lang_Class$$$_$Ljava_lang_annotation_Annotation$$$_"));
throw ex;
//must have a return of type Annotation
};
var java_lang_Class_$_getAnnotations_$_$$_$$$$$_Ljava_lang_annotation_Annotation$$$_ = function (arg0) {
/*implement method!*/
/*
Description copied from interface: AnnotatedElement

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getAnnotations_$_$$_$$$$$_Ljava_lang_annotation_Annotation$$$_"));
throw ex;
//must have a return of type Annotation[]
};
var java_lang_Class_$_getCanonicalName_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the canonical name of the underlying class as
 defined by the Java Language Specification.  Returns null if
 the underlying class does not have a canonical name (i.e., if
 it is a local or anonymous class or an array whose component
 type does not have a canonical name).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getCanonicalName_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Class_$_getClassLoader0_$_$$Ljava_lang_ClassLoader$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getClassLoader0_$_$$Ljava_lang_ClassLoader$$$_"));
throw ex;
//must have a return of type ClassLoader
};
var java_lang_Class_$_getClassLoader_$_$$Ljava_lang_ClassLoader$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the class loader for the class.  Some implementations may use
 null to represent the bootstrap class loader. This method will return
 null in such implementations if this class was loaded by the bootstrap
 class loader.

  If a security manager is present, and the caller's class loader is
 not null and the caller's class loader is not the same as or an ancestor of
 the class loader for the class whose class loader is requested, then
 this method calls the security manager's checkPermission 
 method with a RuntimePermission("getClassLoader") 
 permission to ensure it's ok to access the class loader for the class.
 
 If this object
 represents a primitive type or void, null is returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getClassLoader_$_$$Ljava_lang_ClassLoader$$$_"));
throw ex;
//must have a return of type ClassLoader
};
var java_lang_Class_$_getClasses_$_$$_$$$$$_Ljava_lang_Class$$$_ = function (arg0) {
/*implement method!*/
/*
Returns an array containing Class objects representing all
 the public classes and interfaces that are members of the class
 represented by this Class object.  This includes public
 class and interface members inherited from superclasses and public class
 and interface members declared by the class.  This method returns an
 array of length 0 if this Class object has no public member
 classes or interfaces.  This method also returns an array of length 0 if
 this Class object represents a primitive type, an array
 class, or void.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getClasses_$_$$_$$$$$_Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class[]
};
var java_lang_Class_$_getComponentType_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the Class representing the component type of an
 array.  If this class does not represent an array class this method
 returns null.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getComponentType_$_$$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_lang_Class_$_getConstantPool_$_$$Lsun_reflect_ConstantPool$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getConstantPool_$_$$Lsun_reflect_ConstantPool$$$_"));
throw ex;
//must have a return of type ConstantPool
};
var java_lang_Class_$_getConstructor_$_$_$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Constructor$$$_ = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getConstructor_$_$_$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Constructor$$$_"));
throw ex;
//must have a return of type Constructor
};
var java_lang_Class_$_getConstructors_$_$$_$$$$$_Ljava_lang_reflect_Constructor$$$_ = function (arg0) {
/*implement method!*/
/*
Returns an array containing Constructor objects reflecting
 all the public constructors of the class represented by this
 Class object.  An array of length 0 is returned if the
 class has no public constructors, or if the class is an array class, or
 if the class reflects a primitive type or void.

 Note that while this method returns an array of Constructor<T> objects (that is an array of constructors from
 this class), the return type of this method is Constructor<?>[] and not Constructor<T>[] as
 might be expected.  This less informative return type is
 necessary since after being returned from this method, the
 array could be modified to hold Constructor objects for
 different classes, which would violate the type guarantees of
 Constructor<T>[].

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getConstructors_$_$$_$$$$$_Ljava_lang_reflect_Constructor$$$_"));
throw ex;
//must have a return of type Constructor[]
};
var java_lang_Class_$_getDeclaredAnnotations_$_$$_$$$$$_Ljava_lang_annotation_Annotation$$$_ = function (arg0) {
/*implement method!*/
/*
Description copied from interface: AnnotatedElement

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getDeclaredAnnotations_$_$$_$$$$$_Ljava_lang_annotation_Annotation$$$_"));
throw ex;
//must have a return of type Annotation[]
};
var java_lang_Class_$_getDeclaredClasses_$_$$_$$$$$_Ljava_lang_Class$$$_ = function (arg0) {
/*implement method!*/
/*
Returns an array of Class objects reflecting all the
 classes and interfaces declared as members of the class represented by
 this Class object. This includes public, protected, default
 (package) access, and private classes and interfaces declared by the
 class, but excludes inherited classes and interfaces.  This method
 returns an array of length 0 if the class declares no classes or
 interfaces as members, or if this Class object represents a
 primitive type, an array class, or void.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getDeclaredClasses_$_$$_$$$$$_Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class[]
};
var java_lang_Class_$_getDeclaredConstructor_$_$_$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Constructor$$$_ = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getDeclaredConstructor_$_$_$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Constructor$$$_"));
throw ex;
//must have a return of type Constructor
};
var java_lang_Class_$_getDeclaredConstructors_$_$$_$$$$$_Ljava_lang_reflect_Constructor$$$_ = function (arg0) {
/*implement method!*/
/*
Returns an array of Constructor objects reflecting all the
 constructors declared by the class represented by this
 Class object. These are public, protected, default
 (package) access, and private constructors.  The elements in the array
 returned are not sorted and are not in any particular order.  If the
 class has a default constructor, it is included in the returned array.
 This method returns an array of length 0 if this Class
 object represents an interface, a primitive type, an array class, or
 void.

  See The Java Language Specification, section 8.2.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getDeclaredConstructors_$_$$_$$$$$_Ljava_lang_reflect_Constructor$$$_"));
throw ex;
//must have a return of type Constructor[]
};
var java_lang_Class_$_getDeclaredField_$_$Ljava_lang_String$$$_$Ljava_lang_reflect_Field$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a Field object that reflects the specified declared
 field of the class or interface represented by this Class
 object. The name parameter is a String that
 specifies the simple name of the desired field.  Note that this method
 will not reflect the length field of an array class.

Parameters:

name -> the name of the field

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getDeclaredField_$_$Ljava_lang_String$$$_$Ljava_lang_reflect_Field$$$_"));
throw ex;
//must have a return of type Field
};
var java_lang_Class_$_getDeclaredFields_$_$$_$$$$$_Ljava_lang_reflect_Field$$$_ = function (arg0) {
/*implement method!*/
/*
Returns an array of Field objects reflecting all the fields
 declared by the class or interface represented by this
 Class object. This includes public, protected, default
 (package) access, and private fields, but excludes inherited fields.
 The elements in the array returned are not sorted and are not in any
 particular order.  This method returns an array of length 0 if the class
 or interface declares no fields, or if this Class object
 represents a primitive type, an array class, or void.

  See The Java Language Specification, sections 8.2 and 8.3.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getDeclaredFields_$_$$_$$$$$_Ljava_lang_reflect_Field$$$_"));
throw ex;
//must have a return of type Field[]
};
var java_lang_Class_$_getDeclaredMethod_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Method$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getDeclaredMethod_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Method$$$_"));
throw ex;
//must have a return of type Method
};
var java_lang_Class_$_getDeclaredMethods_$_$$_$$$$$_Ljava_lang_reflect_Method$$$_ = function (arg0) {
/*implement method!*/
/*
Returns an array of Method objects reflecting all the
 methods declared by the class or interface represented by this
 Class object. This includes public, protected, default
 (package) access, and private methods, but excludes inherited methods.
 The elements in the array returned are not sorted and are not in any
 particular order.  This method returns an array of length 0 if the class
 or interface declares no methods, or if this Class object
 represents a primitive type, an array class, or void.  The class
 initialization method <clinit> is not included in the
 returned array. If the class declares multiple public member methods
 with the same parameter types, they are all included in the returned
 array.

  See The Java Language Specification, section 8.2.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getDeclaredMethods_$_$$_$$$$$_Ljava_lang_reflect_Method$$$_"));
throw ex;
//must have a return of type Method[]
};
var java_lang_Class_$_getDeclaringClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*implement method!*/
/*
If the class or interface represented by this Class object
 is a member of another class, returns the Class object
 representing the class in which it was declared.  This method returns
 null if this class or interface is not a member of any other class.  If
 this Class object represents an array class, a primitive
 type, or void,then this method returns null.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getDeclaringClass_$_$$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_lang_Class_$_getEnclosingClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the immediately enclosing class of the underlying
 class.  If the underlying class is a top level class this
 method returns null.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getEnclosingClass_$_$$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_lang_Class_$_getEnclosingConstructor_$_$$Ljava_lang_reflect_Constructor$$$_ = function (arg0) {
/*implement method!*/
/*
If this Class object represents a local or anonymous
 class within a constructor, returns a Constructor object representing
 the immediately enclosing constructor of the underlying
 class. Returns null otherwise.  In particular, this
 method returns null if the underlying class is a local
 or anonymous class immediately enclosed by a type declaration,
 instance initializer or static initializer.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getEnclosingConstructor_$_$$Ljava_lang_reflect_Constructor$$$_"));
throw ex;
//must have a return of type Constructor
};
var java_lang_Class_$_getEnclosingMethod_$_$$Ljava_lang_reflect_Method$$$_ = function (arg0) {
/*implement method!*/
/*
If this Class object represents a local or anonymous
 class within a method, returns a Method object representing the
 immediately enclosing method of the underlying class. Returns
 null otherwise.

 In particular, this method returns null if the underlying
 class is a local or anonymous class immediately enclosed by a type
 declaration, instance initializer or static initializer.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getEnclosingMethod_$_$$Ljava_lang_reflect_Method$$$_"));
throw ex;
//must have a return of type Method
};
var java_lang_Class_$_getEnumConstantsShared_$_$$_$$$$$_Ljava_lang_Object$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getEnumConstantsShared_$_$$_$$$$$_Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object[]
};
var java_lang_Class_$_getEnumConstants_$_$$_$$$$$_Ljava_lang_Object$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the elements of this enum class or null if this
 Class object does not represent an enum type.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getEnumConstants_$_$$_$$$$$_Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object[]
};
var java_lang_Class_$_getField_$_$Ljava_lang_String$$$_$Ljava_lang_reflect_Field$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a Field object that reflects the specified public
 member field of the class or interface represented by this
 Class object. The name parameter is a
 String specifying the simple name of the desired field.

  The field to be reflected is determined by the algorithm that
 follows.  Let C be the class represented by this object:
 
  If C declares a public field with the name specified, that is the
      field to be reflected.
  If no field was found in step 1 above, this algorithm is applied
            recursively to each direct superinterface of C. The direct
            superinterfaces are searched in the order they were declared.
  If no field was found in steps 1 and 2 above, and C has a
      superclass S, then this algorithm is invoked recursively upon S.
      If C has no superclass, then a NoSuchFieldException
      is thrown.
 

  See The Java Language Specification, sections 8.2 and 8.3.

Parameters:

name -> the field name

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getField_$_$Ljava_lang_String$$$_$Ljava_lang_reflect_Field$$$_"));
throw ex;
//must have a return of type Field
};
var java_lang_Class_$_getFields_$_$$_$$$$$_Ljava_lang_reflect_Field$$$_ = function (arg0) {
/*implement method!*/
/*
Returns an array containing Field objects reflecting all
 the accessible public fields of the class or interface represented by
 this Class object.  The elements in the array returned are
 not sorted and are not in any particular order.  This method returns an
 array of length 0 if the class or interface has no accessible public
 fields, or if it represents an array class, a primitive type, or void.

  Specifically, if this Class object represents a class,
 this method returns the public fields of this class and of all its
 superclasses.  If this Class object represents an
 interface, this method returns the fields of this interface and of all
 its superinterfaces.

  The implicit length field for array class is not reflected by this
 method. User code should use the methods of class Array to
 manipulate arrays.

  See The Java Language Specification, sections 8.2 and 8.3.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getFields_$_$$_$$$$$_Ljava_lang_reflect_Field$$$_"));
throw ex;
//must have a return of type Field[]
};
var java_lang_Class_$_getGenericInterfaces_$_$$_$$$$$_Ljava_lang_reflect_Type$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the Types representing the interfaces 
 directly implemented by the class or interface represented by
 this object.

 If a superinterface is a parameterized type, the
 Type object returned for it must accurately reflect
 the actual type parameters used in the source code. The
 parameterized type representing each superinterface is created
 if it had not been created before. See the declaration of
 ParameterizedType
 for the semantics of the creation process for parameterized
 types.

  If this object represents a class, the return value is an
 array containing objects representing all interfaces
 implemented by the class. The order of the interface objects in
 the array corresponds to the order of the interface names in
 the implements clause of the declaration of the class
 represented by this object.  In the case of an array class, the
 interfaces Cloneable and Serializable are
 returned in that order.

 If this object represents an interface, the array contains
 objects representing all interfaces directly extended by the
 interface.  The order of the interface objects in the array
 corresponds to the order of the interface names in the
 extends clause of the declaration of the interface
 represented by this object.

 If this object represents a class or interface that
 implements no interfaces, the method returns an array of length
 0.

 If this object represents a primitive type or void, the
 method returns an array of length 0.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getGenericInterfaces_$_$$_$$$$$_Ljava_lang_reflect_Type$$$_"));
throw ex;
//must have a return of type Type[]
};
var java_lang_Class_$_getGenericSuperclass_$_$$Ljava_lang_reflect_Type$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the Type representing the direct superclass of
 the entity (class, interface, primitive type or void) represented by
 this Class.
 
 If the superclass is a parameterized type, the Type
 object returned must accurately reflect the actual type
 parameters used in the source code. The parameterized type
 representing the superclass is created if it had not been
 created before. See the declaration of ParameterizedType for the
 semantics of the creation process for parameterized types.  If
 this Class represents either the Object
 class, an interface, a primitive type, or void, then null is
 returned.  If this object represents an array class then the
 Class object representing the Object class is
 returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getGenericSuperclass_$_$$Ljava_lang_reflect_Type$$$_"));
throw ex;
//must have a return of type Type
};
var java_lang_Class_$_getInterfaces_$_$$_$$$$$_Ljava_lang_Class$$$_ = function (arg0) {
/*implement method!*/
/*
Determines the interfaces implemented by the class or interface
 represented by this object.

  If this object represents a class, the return value is an array
 containing objects representing all interfaces implemented by the
 class. The order of the interface objects in the array corresponds to
 the order of the interface names in the implements clause
 of the declaration of the class represented by this object. For 
 example, given the declaration:
  class Shimmer implements FloorWax, DessertTopping { ... }
 
 suppose the value of s is an instance of 
 Shimmer; the value of the expression:
  s.getClass().getInterfaces()[0]
 
 is the Class object that represents interface 
 FloorWax; and the value of:
  s.getClass().getInterfaces()[1]
 
 is the Class object that represents interface 
 DessertTopping.

  If this object represents an interface, the array contains objects
 representing all interfaces extended by the interface. The order of the
 interface objects in the array corresponds to the order of the interface
 names in the extends clause of the declaration of the
 interface represented by this object.

  If this object represents a class or interface that implements no
 interfaces, the method returns an array of length 0.

  If this object represents a primitive type or void, the method
 returns an array of length 0.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getInterfaces_$_$$_$$$$$_Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class[]
};
var java_lang_Class_$_getMethod_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Method$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getMethod_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Method$$$_"));
throw ex;
//must have a return of type Method
};
var java_lang_Class_$_getMethods_$_$$_$$$$$_Ljava_lang_reflect_Method$$$_ = function (arg0) {
/*implement method!*/
/*
Returns an array containing Method objects reflecting all
 the public member methods of the class or interface represented
 by this Class object, including those declared by the class
 or interface and those inherited from superclasses and
 superinterfaces.  Array classes return all the (public) member methods 
 inherited from the Object class.  The elements in the array 
 returned are not sorted and are not in any particular order.  This 
 method returns an array of length 0 if this Class object
 represents a class or interface that has no public member methods, or if
 this Class object represents a primitive type or void.

  The class initialization method <clinit> is not
 included in the returned array. If the class declares multiple public
 member methods with the same parameter types, they are all included in
 the returned array.

  See The Java Language Specification, sections 8.2 and 8.4.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getMethods_$_$$_$$$$$_Ljava_lang_reflect_Method$$$_"));
throw ex;
//must have a return of type Method[]
};
var java_lang_Class_$_getModifiers_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the Java language modifiers for this class or interface, encoded
 in an integer. The modifiers consist of the Java Virtual Machine's
 constants for public, protected,
 private, final, static,
 abstract and interface; they should be decoded
 using the methods of class Modifier.

  If the underlying class is an array class, then its
 public, private and protected
 modifiers are the same as those of its component type.  If this
 Class represents a primitive type or void, its
 public modifier is always true, and its
 protected and private modifiers are always
 false. If this object represents an array class, a
 primitive type or void, then its final modifier is always
 true and its interface modifier is always
 false. The values of its other modifiers are not determined
 by this specification.

  The modifier encodings are defined in The Java Virtual Machine
 Specification, table 4.1.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getModifiers_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Class_$_getName_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the  name of the entity (class, interface, array class,
 primitive type, or void) represented by this Class object,
 as a String.
 
  If this class object represents a reference type that is not an
 array type then the binary name of the class is returned, as specified
 by the Java Language Specification, Second Edition.

  If this class object represents a primitive type or void, then the
 name returned is a String equal to the Java language
 keyword corresponding to the primitive type or void.
 
  If this class object represents a class of arrays, then the internal
 form of the name consists of the name of the element type preceded by
 one or more '[' characters representing the depth of the array
 nesting.  The encoding of element type names is as follows:

 
  Element Type       Encoding
  boolean            Z
  byte               B
  char               C
  class or interface  
                             Lclassname;
  double             D
  float              F
  int                I
  long               J
  short              S
 

  The class or interface name classname is the binary name of
 the class specified above.

  Examples:
  String.class.getName()
     returns "java.lang.String"
 byte.class.getName()
     returns "byte"
 (new Object[3]).getClass().getName()
     returns "[Ljava.lang.Object;"
 (new int[3][4][5][6][7][8][9]).getClass().getName()
     returns "[[[[[[[I"

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getName_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Class_$_getPackage_$_$$Ljava_lang_Package$$$_ = function (arg0) {
/*implement method!*/
/*
Gets the package for this class.  The class loader of this class is used
 to find the package.  If the class was loaded by the bootstrap class
 loader the set of packages loaded from CLASSPATH is searched to find the
 package of the class. Null is returned if no package object was created
 by the class loader of this class.

  Packages have attributes for versions and specifications only if the
 information was defined in the manifests that accompany the classes, and
 if the class loader created the package instance with the attributes
 from the manifest.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getPackage_$_$$Ljava_lang_Package$$$_"));
throw ex;
//must have a return of type Package
};
var java_lang_Class_$_getPrimitiveClass_$_$Ljava_lang_String$$$_$Ljava_lang_Class$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getPrimitiveClass_$_$Ljava_lang_String$$$_$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_lang_Class_$_getProtectionDomain_$_$$Ljava_security_ProtectionDomain$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the ProtectionDomain of this class.  If there is a
 security manager installed, this method first calls the security
 manager's checkPermission method with a
 RuntimePermission("getProtectionDomain") permission to
 ensure it's ok to get the
 ProtectionDomain.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getProtectionDomain_$_$$Ljava_security_ProtectionDomain$$$_"));
throw ex;
//must have a return of type ProtectionDomain
};
var java_lang_Class_$_getResourceAsStream_$_$Ljava_lang_String$$$_$Ljava_io_InputStream$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Finds a resource with a given name.  The rules for searching resources
 associated with a given class are implemented by the defining
 class loader of the class.  This method
 delegates to this object's class loader.  If this object was loaded by
 the bootstrap class loader, the method delegates to ClassLoader.getSystemResourceAsStream(java.lang.String).

  Before delegation, an absolute resource name is constructed from the
 given resource name using this algorithm:

 

  If the name begins with a '/'
 ('\u002f'), then the absolute name of the resource is the
 portion of the name following the '/'. 

  Otherwise, the absolute name is of the following form:

    modified_package_name/name
 

  Where the modified_package_name is the package name of this
 object with '/' substituted for '.'
 ('\u002e').

Parameters:

name -> name of the desired resource

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getResourceAsStream_$_$Ljava_lang_String$$$_$Ljava_io_InputStream$$$_"));
throw ex;
//must have a return of type InputStream
};
var java_lang_Class_$_getResource_$_$Ljava_lang_String$$$_$Ljava_net_URL$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Finds a resource with a given name.  The rules for searching resources
 associated with a given class are implemented by the defining
 class loader of the class.  This method
 delegates to this object's class loader.  If this object was loaded by
 the bootstrap class loader, the method delegates to ClassLoader.getSystemResource(java.lang.String).

  Before delegation, an absolute resource name is constructed from the
 given resource name using this algorithm:

 

  If the name begins with a '/'
 ('\u002f'), then the absolute name of the resource is the
 portion of the name following the '/'. 

  Otherwise, the absolute name is of the following form:

    modified_package_name/name
 

  Where the modified_package_name is the package name of this
 object with '/' substituted for '.'
 ('\u002e').

Parameters:

name -> name of the desired resource

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getResource_$_$Ljava_lang_String$$$_$Ljava_net_URL$$$_"));
throw ex;
//must have a return of type URL
};
var java_lang_Class_$_getSigners_$_$$_$$$$$_Ljava_lang_Object$$$_ = function (arg0) {
/*implement method!*/
/*
Gets the signers of this class.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getSigners_$_$$_$$$$$_Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object[]
};
var java_lang_Class_$_getSimpleName_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the simple name of the underlying class as given in the
 source code. Returns an empty string if the underlying class is
 anonymous.

 The simple name of an array is the simple name of the
 component type with "[]" appended.  In particular the simple
 name of an array whose component type is anonymous is "[]".

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getSimpleName_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Class_$_getSuperclass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the Class representing the superclass of the entity
 (class, interface, primitive type or void) represented by this
 Class.  If this Class represents either the
 Object class, an interface, a primitive type, or void, then
 null is returned.  If this object represents an array class then the
 Class object representing the Object class is
 returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getSuperclass_$_$$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_lang_Class_$_getTypeParameters_$_$$_$$$$$_Ljava_lang_reflect_TypeVariable$$$_ = function (arg0) {
/*implement method!*/
/*
Returns an array of TypeVariable objects that represent the
 type variables declared by the generic declaration represented by this
 GenericDeclaration object, in declaration order.  Returns an
 array of length 0 if the underlying generic declaration declares no type
 variables.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_getTypeParameters_$_$$_$$$$$_Ljava_lang_reflect_TypeVariable$$$_"));
throw ex;
//must have a return of type TypeVariable[]
};
var java_lang_Class_$_isAnnotationPresent_$_$Ljava_lang_Class$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Description copied from interface: AnnotatedElement

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isAnnotationPresent_$_$Ljava_lang_Class$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_isAnnotation_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if this Class object represents an annotation
 type.  Note that if this method returns true, isInterface()
 would also return true, as all annotation types are also interfaces.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isAnnotation_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_isAnonymousClass_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if and only if the underlying class
 is an anonymous class.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isAnonymousClass_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_isArray_$_$$Z = function (arg0) {
/*implement method!*/
/*
Determines if this Class object represents an array class.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isArray_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_isAssignableFrom_$_$Ljava_lang_Class$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Determines if the class or interface represented by this
 Class object is either the same as, or is a superclass or
 superinterface of, the class or interface represented by the specified
 Class parameter. It returns true if so;
 otherwise it returns false. If this Class
 object represents a primitive type, this method returns
 true if the specified Class parameter is
 exactly this Class object; otherwise it returns
 false.

  Specifically, this method tests whether the type represented by the
 specified Class parameter can be converted to the type
 represented by this Class object via an identity conversion
 or via a widening reference conversion. See The Java Language
 Specification, sections 5.1.1 and 5.1.4 , for details.

Parameters:

cls -> the Class object to be checked

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isAssignableFrom_$_$Ljava_lang_Class$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_isEnum_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if and only if this class was declared as an enum in the
 source code.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isEnum_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_isInstance_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Determines if the specified Object is assignment-compatible
 with the object represented by this Class.  This method is
 the dynamic equivalent of the Java language instanceof
 operator. The method returns true if the specified
 Object argument is non-null and can be cast to the
 reference type represented by this Class object without
 raising a ClassCastException. It returns false
 otherwise.

  Specifically, if this Class object represents a
 declared class, this method returns true if the specified
 Object argument is an instance of the represented class (or
 of any of its subclasses); it returns false otherwise. If
 this Class object represents an array class, this method
 returns true if the specified Object argument
 can be converted to an object of the array class by an identity
 conversion or by a widening reference conversion; it returns
 false otherwise. If this Class object
 represents an interface, this method returns true if the
 class or any superclass of the specified Object argument
 implements this interface; it returns false otherwise. If
 this Class object represents a primitive type, this method
 returns false.

Parameters:

obj -> the object to check

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isInstance_$_$Ljava_lang_Object$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_isInterface_$_$$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified Class object represents an
 interface type.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isInterface_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_isLocalClass_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if and only if the underlying class
 is a local class.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isLocalClass_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_isMemberClass_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if and only if the underlying class
 is a member class.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isMemberClass_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_isPrimitive_$_$$Z = function (arg0) {
/*implement method!*/
/*
Determines if the specified Class object represents a
 primitive type.

  There are nine predefined Class objects to represent
 the eight primitive types and void.  These are created by the Java
 Virtual Machine, and have the same names as the primitive types that
 they represent, namely boolean, byte,
 char, short, int,
 long, float, and double.

  These objects may only be accessed via the following public static
 final variables, and are the only Class objects for which
 this method returns true.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isPrimitive_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_isSynthetic_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if this class is a synthetic class;
 returns false otherwise.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_isSynthetic_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Class_$_newInstance_$_$$Ljava_lang_Object$$$_ = function (arg0) {
/*implement method!*/
/*
Creates a new instance of the class represented by this Class
 object.  The class is instantiated as if by a new
 expression with an empty argument list.  The class is initialized if it
 has not already been initialized.

 Note that this method propagates any exception thrown by the
 nullary constructor, including a checked exception.  Use of
 this method effectively bypasses the compile-time exception
 checking that would otherwise be performed by the compiler.
 The Constructor.newInstance method avoids this problem by wrapping
 any exception thrown by the constructor in a (checked) InvocationTargetException.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_newInstance_$_$$Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object
};
var java_lang_Class_$_setAnnotationType_$_$Lsun_reflect_annotation_AnnotationType$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_setAnnotationType_$_$Lsun_reflect_annotation_AnnotationType$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Class_$_setProtectionDomain0_$_$Ljava_security_ProtectionDomain$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_setProtectionDomain0_$_$Ljava_security_ProtectionDomain$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Class_$_setSigners_$_$_$$$$$_Ljava_lang_Object$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_setSigners_$_$_$$$$$_Ljava_lang_Object$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Class_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Converts the object to a string. The string representation is the
 string "class" or "interface", followed by a space, and then by the
 fully qualified name of the class in the format returned by
 getName.  If this Class object represents a
 primitive type, this method returns the name of the primitive type.  If
 this Class object represents void this method returns
 "void".

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Class_$_toString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Class_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_lang_Class_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_Class_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_lang_Class_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_lang_Class_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var java_lang_Class_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_Class_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_lang_Class_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_lang_Class.access$100_$_$_$$$$$_Ljava_lang_Object$$$__$$$$$_Ljava_lang_Object$$$_$Z = java_lang_Class_$_access$100_$_$_$$$$$_Ljava_lang_Object$$$__$$$$$_Ljava_lang_Object$$$_$Z;
java_lang_Class.access$202_$_$Z$Z = java_lang_Class_$_access$202_$_$Z$Z;
java_lang_Class.access$302_$_$Z$Z = java_lang_Class_$_access$302_$_$Z$Z;
java_lang_Class.prototype.asSubclass_$_$Ljava_lang_Class$$$_$Ljava_lang_Class$$$_ = java_lang_Class_$_asSubclass_$_$Ljava_lang_Class$$$_$Ljava_lang_Class$$$_;
java_lang_Class.prototype.cast_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_ = java_lang_Class_$_cast_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_;
java_lang_Class.prototype.desiredAssertionStatus_$_$$Z = java_lang_Class_$_desiredAssertionStatus_$_$$Z;
java_lang_Class.prototype.enumConstantDirectory_$_$$Ljava_util_Map$$$_ = java_lang_Class_$_enumConstantDirectory_$_$$Ljava_util_Map$$$_;
java_lang_Class.forName_$_$Ljava_lang_String$$$_$Ljava_lang_Class$$$_ = java_lang_Class_$_forName_$_$Ljava_lang_String$$$_$Ljava_lang_Class$$$_;
java_lang_Class.forName_$_$Ljava_lang_String$$$_ZLjava_lang_ClassLoader$$$_$Ljava_lang_Class$$$_ = java_lang_Class_$_forName_$_$Ljava_lang_String$$$_ZLjava_lang_ClassLoader$$$_$Ljava_lang_Class$$$_;
java_lang_Class.prototype.getAnnotationType_$_$$Lsun_reflect_annotation_AnnotationType$$$_ = java_lang_Class_$_getAnnotationType_$_$$Lsun_reflect_annotation_AnnotationType$$$_;
java_lang_Class.prototype.getAnnotation_$_$Ljava_lang_Class$$$_$Ljava_lang_annotation_Annotation$$$_ = java_lang_Class_$_getAnnotation_$_$Ljava_lang_Class$$$_$Ljava_lang_annotation_Annotation$$$_;
java_lang_Class.prototype.getAnnotations_$_$$_$$$$$_Ljava_lang_annotation_Annotation$$$_ = java_lang_Class_$_getAnnotations_$_$$_$$$$$_Ljava_lang_annotation_Annotation$$$_;
java_lang_Class.prototype.getCanonicalName_$_$$Ljava_lang_String$$$_ = java_lang_Class_$_getCanonicalName_$_$$Ljava_lang_String$$$_;
java_lang_Class.prototype.getClassLoader0_$_$$Ljava_lang_ClassLoader$$$_ = java_lang_Class_$_getClassLoader0_$_$$Ljava_lang_ClassLoader$$$_;
java_lang_Class.prototype.getClassLoader_$_$$Ljava_lang_ClassLoader$$$_ = java_lang_Class_$_getClassLoader_$_$$Ljava_lang_ClassLoader$$$_;
java_lang_Class.prototype.getClasses_$_$$_$$$$$_Ljava_lang_Class$$$_ = java_lang_Class_$_getClasses_$_$$_$$$$$_Ljava_lang_Class$$$_;
java_lang_Class.prototype.getComponentType_$_$$Ljava_lang_Class$$$_ = java_lang_Class_$_getComponentType_$_$$Ljava_lang_Class$$$_;
java_lang_Class.prototype.getConstantPool_$_$$Lsun_reflect_ConstantPool$$$_ = java_lang_Class_$_getConstantPool_$_$$Lsun_reflect_ConstantPool$$$_;
java_lang_Class.prototype.getConstructor_$_$_$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Constructor$$$_ = java_lang_Class_$_getConstructor_$_$_$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Constructor$$$_;
java_lang_Class.prototype.getConstructors_$_$$_$$$$$_Ljava_lang_reflect_Constructor$$$_ = java_lang_Class_$_getConstructors_$_$$_$$$$$_Ljava_lang_reflect_Constructor$$$_;
java_lang_Class.prototype.getDeclaredAnnotations_$_$$_$$$$$_Ljava_lang_annotation_Annotation$$$_ = java_lang_Class_$_getDeclaredAnnotations_$_$$_$$$$$_Ljava_lang_annotation_Annotation$$$_;
java_lang_Class.prototype.getDeclaredClasses_$_$$_$$$$$_Ljava_lang_Class$$$_ = java_lang_Class_$_getDeclaredClasses_$_$$_$$$$$_Ljava_lang_Class$$$_;
java_lang_Class.prototype.getDeclaredConstructor_$_$_$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Constructor$$$_ = java_lang_Class_$_getDeclaredConstructor_$_$_$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Constructor$$$_;
java_lang_Class.prototype.getDeclaredConstructors_$_$$_$$$$$_Ljava_lang_reflect_Constructor$$$_ = java_lang_Class_$_getDeclaredConstructors_$_$$_$$$$$_Ljava_lang_reflect_Constructor$$$_;
java_lang_Class.prototype.getDeclaredField_$_$Ljava_lang_String$$$_$Ljava_lang_reflect_Field$$$_ = java_lang_Class_$_getDeclaredField_$_$Ljava_lang_String$$$_$Ljava_lang_reflect_Field$$$_;
java_lang_Class.prototype.getDeclaredFields_$_$$_$$$$$_Ljava_lang_reflect_Field$$$_ = java_lang_Class_$_getDeclaredFields_$_$$_$$$$$_Ljava_lang_reflect_Field$$$_;
java_lang_Class.prototype.getDeclaredMethod_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Method$$$_ = java_lang_Class_$_getDeclaredMethod_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Method$$$_;
java_lang_Class.prototype.getDeclaredMethods_$_$$_$$$$$_Ljava_lang_reflect_Method$$$_ = java_lang_Class_$_getDeclaredMethods_$_$$_$$$$$_Ljava_lang_reflect_Method$$$_;
java_lang_Class.prototype.getDeclaringClass_$_$$Ljava_lang_Class$$$_ = java_lang_Class_$_getDeclaringClass_$_$$Ljava_lang_Class$$$_;
java_lang_Class.prototype.getEnclosingClass_$_$$Ljava_lang_Class$$$_ = java_lang_Class_$_getEnclosingClass_$_$$Ljava_lang_Class$$$_;
java_lang_Class.prototype.getEnclosingConstructor_$_$$Ljava_lang_reflect_Constructor$$$_ = java_lang_Class_$_getEnclosingConstructor_$_$$Ljava_lang_reflect_Constructor$$$_;
java_lang_Class.prototype.getEnclosingMethod_$_$$Ljava_lang_reflect_Method$$$_ = java_lang_Class_$_getEnclosingMethod_$_$$Ljava_lang_reflect_Method$$$_;
java_lang_Class.prototype.getEnumConstantsShared_$_$$_$$$$$_Ljava_lang_Object$$$_ = java_lang_Class_$_getEnumConstantsShared_$_$$_$$$$$_Ljava_lang_Object$$$_;
java_lang_Class.prototype.getEnumConstants_$_$$_$$$$$_Ljava_lang_Object$$$_ = java_lang_Class_$_getEnumConstants_$_$$_$$$$$_Ljava_lang_Object$$$_;
java_lang_Class.prototype.getField_$_$Ljava_lang_String$$$_$Ljava_lang_reflect_Field$$$_ = java_lang_Class_$_getField_$_$Ljava_lang_String$$$_$Ljava_lang_reflect_Field$$$_;
java_lang_Class.prototype.getFields_$_$$_$$$$$_Ljava_lang_reflect_Field$$$_ = java_lang_Class_$_getFields_$_$$_$$$$$_Ljava_lang_reflect_Field$$$_;
java_lang_Class.prototype.getGenericInterfaces_$_$$_$$$$$_Ljava_lang_reflect_Type$$$_ = java_lang_Class_$_getGenericInterfaces_$_$$_$$$$$_Ljava_lang_reflect_Type$$$_;
java_lang_Class.prototype.getGenericSuperclass_$_$$Ljava_lang_reflect_Type$$$_ = java_lang_Class_$_getGenericSuperclass_$_$$Ljava_lang_reflect_Type$$$_;
java_lang_Class.prototype.getInterfaces_$_$$_$$$$$_Ljava_lang_Class$$$_ = java_lang_Class_$_getInterfaces_$_$$_$$$$$_Ljava_lang_Class$$$_;
java_lang_Class.prototype.getMethod_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Method$$$_ = java_lang_Class_$_getMethod_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_Class$$$_$Ljava_lang_reflect_Method$$$_;
java_lang_Class.prototype.getMethods_$_$$_$$$$$_Ljava_lang_reflect_Method$$$_ = java_lang_Class_$_getMethods_$_$$_$$$$$_Ljava_lang_reflect_Method$$$_;
java_lang_Class.prototype.getModifiers_$_$$I = java_lang_Class_$_getModifiers_$_$$I;
java_lang_Class.prototype.getName_$_$$Ljava_lang_String$$$_ = java_lang_Class_$_getName_$_$$Ljava_lang_String$$$_;
java_lang_Class.prototype.getPackage_$_$$Ljava_lang_Package$$$_ = java_lang_Class_$_getPackage_$_$$Ljava_lang_Package$$$_;
java_lang_Class.getPrimitiveClass_$_$Ljava_lang_String$$$_$Ljava_lang_Class$$$_ = java_lang_Class_$_getPrimitiveClass_$_$Ljava_lang_String$$$_$Ljava_lang_Class$$$_;
java_lang_Class.prototype.getProtectionDomain_$_$$Ljava_security_ProtectionDomain$$$_ = java_lang_Class_$_getProtectionDomain_$_$$Ljava_security_ProtectionDomain$$$_;
java_lang_Class.prototype.getResourceAsStream_$_$Ljava_lang_String$$$_$Ljava_io_InputStream$$$_ = java_lang_Class_$_getResourceAsStream_$_$Ljava_lang_String$$$_$Ljava_io_InputStream$$$_;
java_lang_Class.prototype.getResource_$_$Ljava_lang_String$$$_$Ljava_net_URL$$$_ = java_lang_Class_$_getResource_$_$Ljava_lang_String$$$_$Ljava_net_URL$$$_;
java_lang_Class.prototype.getSigners_$_$$_$$$$$_Ljava_lang_Object$$$_ = java_lang_Class_$_getSigners_$_$$_$$$$$_Ljava_lang_Object$$$_;
java_lang_Class.prototype.getSimpleName_$_$$Ljava_lang_String$$$_ = java_lang_Class_$_getSimpleName_$_$$Ljava_lang_String$$$_;
java_lang_Class.prototype.getSuperclass_$_$$Ljava_lang_Class$$$_ = java_lang_Class_$_getSuperclass_$_$$Ljava_lang_Class$$$_;
java_lang_Class.prototype.getTypeParameters_$_$$_$$$$$_Ljava_lang_reflect_TypeVariable$$$_ = java_lang_Class_$_getTypeParameters_$_$$_$$$$$_Ljava_lang_reflect_TypeVariable$$$_;
java_lang_Class.prototype.isAnnotationPresent_$_$Ljava_lang_Class$$$_$Z = java_lang_Class_$_isAnnotationPresent_$_$Ljava_lang_Class$$$_$Z;
java_lang_Class.prototype.isAnnotation_$_$$Z = java_lang_Class_$_isAnnotation_$_$$Z;
java_lang_Class.prototype.isAnonymousClass_$_$$Z = java_lang_Class_$_isAnonymousClass_$_$$Z;
java_lang_Class.prototype.isArray_$_$$Z = java_lang_Class_$_isArray_$_$$Z;
java_lang_Class.prototype.isAssignableFrom_$_$Ljava_lang_Class$$$_$Z = java_lang_Class_$_isAssignableFrom_$_$Ljava_lang_Class$$$_$Z;
java_lang_Class.prototype.isEnum_$_$$Z = java_lang_Class_$_isEnum_$_$$Z;
java_lang_Class.prototype.isInstance_$_$Ljava_lang_Object$$$_$Z = java_lang_Class_$_isInstance_$_$Ljava_lang_Object$$$_$Z;
java_lang_Class.prototype.isInterface_$_$$Z = java_lang_Class_$_isInterface_$_$$Z;
java_lang_Class.prototype.isLocalClass_$_$$Z = java_lang_Class_$_isLocalClass_$_$$Z;
java_lang_Class.prototype.isMemberClass_$_$$Z = java_lang_Class_$_isMemberClass_$_$$Z;
java_lang_Class.prototype.isPrimitive_$_$$Z = java_lang_Class_$_isPrimitive_$_$$Z;
java_lang_Class.prototype.isSynthetic_$_$$Z = java_lang_Class_$_isSynthetic_$_$$Z;
java_lang_Class.prototype.newInstance_$_$$Ljava_lang_Object$$$_ = java_lang_Class_$_newInstance_$_$$Ljava_lang_Object$$$_;
java_lang_Class.prototype.setAnnotationType_$_$Lsun_reflect_annotation_AnnotationType$$$_$V = java_lang_Class_$_setAnnotationType_$_$Lsun_reflect_annotation_AnnotationType$$$_$V;
java_lang_Class.prototype.setProtectionDomain0_$_$Ljava_security_ProtectionDomain$$$_$V = java_lang_Class_$_setProtectionDomain0_$_$Ljava_security_ProtectionDomain$$$_$V;
java_lang_Class.prototype.setSigners_$_$_$$$$$_Ljava_lang_Object$$$_$V = java_lang_Class_$_setSigners_$_$_$$$$$_Ljava_lang_Object$$$_$V;
java_lang_Class.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_Class_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_Class.prototype.wait_$_$J$V = java_lang_Class_$_wait_$_$J$V;
java_lang_Class.prototype.wait_$_$JI$V = java_lang_Class_$_wait_$_$JI$V;
java_lang_Class.prototype.wait_$_$$V = java_lang_Class_$_wait_$_$$V;
java_lang_Class.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_Class_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_Class.prototype.hashCode_$_$$I = java_lang_Class_$_hashCode_$_$$I;
java_lang_Class.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_Class_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_Class.prototype.notify_$_$$V = java_lang_Class_$_notify_$_$$V;
java_lang_Class.prototype.notifyAll_$_$$V = java_lang_Class_$_notifyAll_$_$$V;
