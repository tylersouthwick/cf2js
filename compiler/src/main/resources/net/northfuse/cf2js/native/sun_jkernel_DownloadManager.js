/** @constructor */
function sun_jkernel_DownloadManager() {}
var sun_jkernel_DownloadManager_$__$$$init$$$__$_$$V = function (arg0) {
/*implement constructor!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$__$$$init$$$__$_$$V"));
throw ex;
};
var sun_jkernel_DownloadManager_$_access$000_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_access$000_$_$$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_access$100_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_access$100_$_$$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_access$200_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_access$200_$_$$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_access$300_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_access$300_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var sun_jkernel_DownloadManager_$_access$400_$_$Ljava_lang_String$$$_$Z = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_access$400_$_$Ljava_lang_String$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var sun_jkernel_DownloadManager_$_addToTotalDownloadSize_$_$I$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_addToTotalDownloadSize_$_$I$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_appendTransactionId_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_appendTransactionId_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var sun_jkernel_DownloadManager_$_askUserToRetryDownloadOrQuit_$_$I$Z = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_askUserToRetryDownloadOrQuit_$_$I$Z"));
throw ex;
//must have a return of type boolean
};
var sun_jkernel_DownloadManager_$_bundleInstallComplete_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_bundleInstallComplete_$_$$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_bundleInstallStart_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_bundleInstallStart_$_$$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_copyReceiptFile_$_$Ljava_io_File$$$_Ljava_io_File$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_copyReceiptFile_$_$Ljava_io_File$$$_Ljava_io_File$$$_$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_decrementDownloadCount_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_decrementDownloadCount_$_$$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_displayError_$_$ILjava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_displayError_$_$ILjava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_doBackgroundDownloads_$_$Z$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_doBackgroundDownloads_$_$Z$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_downloadFile_$_$Ljava_lang_String$$$_$Z = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_downloadFile_$_$Ljava_lang_String$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var sun_jkernel_DownloadManager_$_downloadFromURL_$_$Ljava_lang_String$$$_Ljava_io_File$$$_Ljava_lang_String$$$_Z$V = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_downloadFromURL_$_$Ljava_lang_String$$$_Ljava_io_File$$$_Ljava_lang_String$$$_Z$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_dumpOutput_$_$Ljava_lang_Process$$$_$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_dumpOutput_$_$Ljava_lang_Process$$$_$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_fatalError_$_$I$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_fatalError_$_$I$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_fatalError_$_$ILjava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_fatalError_$_$ILjava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_flushBundleURLs_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_flushBundleURLs_$_$$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_getAdditionalBootStrapPaths_$_$$_$$$$$_Ljava_io_File$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getAdditionalBootStrapPaths_$_$$_$$$$$_Ljava_io_File$$$_"));
throw ex;
//must have a return of type File[]
};
var sun_jkernel_DownloadManager_$_getBaseDownloadURL_$_$$Ljava_lang_String$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getBaseDownloadURL_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var sun_jkernel_DownloadManager_$_getBootClassPathEntryForClass_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getBootClassPathEntryForClass_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var sun_jkernel_DownloadManager_$_getBootClassPathEntryForResource_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getBootClassPathEntryForResource_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var sun_jkernel_DownloadManager_$_getBundleForResource_$_$Ljava_lang_String$$$_$Lsun_jkernel_Bundle$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getBundleForResource_$_$Ljava_lang_String$$$_$Lsun_jkernel_Bundle$$$_"));
throw ex;
//must have a return of type Bundle
};
var sun_jkernel_DownloadManager_$_getBundleNames_$_$$_$$$$$_Ljava_lang_String$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getBundleNames_$_$$_$$$$$_Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String[]
};
var sun_jkernel_DownloadManager_$_getBundlePath_$_$$Ljava_io_File$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getBundlePath_$_$$Ljava_io_File$$$_"));
throw ex;
//must have a return of type File
};
var sun_jkernel_DownloadManager_$_getBundleProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getBundleProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var sun_jkernel_DownloadManager_$_getBundleURLs_$_$Z$Ljava_util_Properties$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getBundleURLs_$_$Z$Ljava_util_Properties$$$_"));
throw ex;
//must have a return of type Properties
};
var sun_jkernel_DownloadManager_$_getCurrentProcessId_$_$$I = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getCurrentProcessId_$_$$I"));
throw ex;
//must have a return of type int
};
var sun_jkernel_DownloadManager_$_getDebugKey_$_$$Z = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getDebugKey_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var sun_jkernel_DownloadManager_$_getDebugProperty_$_$$Z = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getDebugProperty_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var sun_jkernel_DownloadManager_$_getFileMap_$_$$Ljava_util_Map$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getFileMap_$_$$Ljava_util_Map$$$_"));
throw ex;
//must have a return of type Map
};
var sun_jkernel_DownloadManager_$_getKernelJREDir_$_$$Ljava_lang_String$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getKernelJREDir_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var sun_jkernel_DownloadManager_$_getLocalLowKernelJava_$_$$Ljava_lang_String$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getLocalLowKernelJava_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var sun_jkernel_DownloadManager_$_getLocalLowTempBundlePath_$_$$Ljava_io_File$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getLocalLowTempBundlePath_$_$$Ljava_io_File$$$_"));
throw ex;
//must have a return of type File
};
var sun_jkernel_DownloadManager_$_getResourceMap_$_$$Ljava_util_Map$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getResourceMap_$_$$Ljava_util_Map$$$_"));
throw ex;
//must have a return of type Map
};
var sun_jkernel_DownloadManager_$_getUrlFromRegistry_$_$$Ljava_lang_String$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getUrlFromRegistry_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var sun_jkernel_DownloadManager_$_getVisitorId0_$_$$Ljava_lang_String$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getVisitorId0_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var sun_jkernel_DownloadManager_$_getVisitorId_$_$$Ljava_lang_String$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_getVisitorId_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var sun_jkernel_DownloadManager_$_handleException_$_$Ljava_lang_Throwable$$$_$Z = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_handleException_$_$Ljava_lang_Throwable$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var sun_jkernel_DownloadManager_$_incrementDownloadCount_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_incrementDownloadCount_$_$$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_isCurrentThreadDownloading_$_$$Z = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_isCurrentThreadDownloading_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var sun_jkernel_DownloadManager_$_isJREComplete_$_$$Z = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_isJREComplete_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var sun_jkernel_DownloadManager_$_isWindowsVista_$_$$Z = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_isWindowsVista_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var sun_jkernel_DownloadManager_$_loadJKernelLibrary_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_loadJKernelLibrary_$_$$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_log_$_$Ljava_lang_String$$$_$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_log_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_log_$_$Ljava_lang_Throwable$$$_$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_log_$_$Ljava_lang_Throwable$$$_$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_main_$_$_$$$$$_Ljava_lang_String$$$_$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_main_$_$_$$$$$_Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_moveDirWithBroker_$_$Ljava_lang_String$$$_$Z = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_moveDirWithBroker_$_$Ljava_lang_String$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var sun_jkernel_DownloadManager_$_moveFileWithBroker_$_$Ljava_lang_String$$$_$Z = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_moveFileWithBroker_$_$Ljava_lang_String$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var sun_jkernel_DownloadManager_$_performCompletionIfNeeded_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_performCompletionIfNeeded_$_$$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_postDownloadComplete_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_postDownloadComplete_$_$$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_postDownloadError_$_$I$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_postDownloadError_$_$I$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_println_$_$Ljava_lang_String$$$_$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_println_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_readTreeMap_$_$Ljava_io_InputStream$$$_$Ljava_util_Map$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_readTreeMap_$_$Ljava_io_InputStream$$$_$Ljava_util_Map$$$_"));
throw ex;
//must have a return of type Map
};
var sun_jkernel_DownloadManager_$_sendErrorPing_$_$I$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_sendErrorPing_$_$I$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_send_$_$Ljava_io_InputStream$$$_Ljava_io_OutputStream$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("sun_jkernel_DownloadManager_$_send_$_$Ljava_io_InputStream$$$_Ljava_io_OutputStream$$$_$V"));
throw ex;
//must have a return of type void
};
var sun_jkernel_DownloadManager_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var sun_jkernel_DownloadManager_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var sun_jkernel_DownloadManager_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var sun_jkernel_DownloadManager_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var sun_jkernel_DownloadManager_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var sun_jkernel_DownloadManager_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var sun_jkernel_DownloadManager_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var sun_jkernel_DownloadManager_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var sun_jkernel_DownloadManager_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
sun_jkernel_DownloadManager.prototype._$$$init$$$__$_$$V = sun_jkernel_DownloadManager_$__$$$init$$$__$_$$V;
sun_jkernel_DownloadManager.access$000_$_$$V = sun_jkernel_DownloadManager_$_access$000_$_$$V;
sun_jkernel_DownloadManager.access$100_$_$$V = sun_jkernel_DownloadManager_$_access$100_$_$$V;
sun_jkernel_DownloadManager.access$200_$_$$V = sun_jkernel_DownloadManager_$_access$200_$_$$V;
sun_jkernel_DownloadManager.access$300_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_access$300_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.access$400_$_$Ljava_lang_String$$$_$Z = sun_jkernel_DownloadManager_$_access$400_$_$Ljava_lang_String$$$_$Z;
sun_jkernel_DownloadManager.addToTotalDownloadSize_$_$I$V = sun_jkernel_DownloadManager_$_addToTotalDownloadSize_$_$I$V;
sun_jkernel_DownloadManager.appendTransactionId_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_appendTransactionId_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.askUserToRetryDownloadOrQuit_$_$I$Z = sun_jkernel_DownloadManager_$_askUserToRetryDownloadOrQuit_$_$I$Z;
sun_jkernel_DownloadManager.bundleInstallComplete_$_$$V = sun_jkernel_DownloadManager_$_bundleInstallComplete_$_$$V;
sun_jkernel_DownloadManager.bundleInstallStart_$_$$V = sun_jkernel_DownloadManager_$_bundleInstallStart_$_$$V;
sun_jkernel_DownloadManager.copyReceiptFile_$_$Ljava_io_File$$$_Ljava_io_File$$$_$V = sun_jkernel_DownloadManager_$_copyReceiptFile_$_$Ljava_io_File$$$_Ljava_io_File$$$_$V;
sun_jkernel_DownloadManager.decrementDownloadCount_$_$$V = sun_jkernel_DownloadManager_$_decrementDownloadCount_$_$$V;
sun_jkernel_DownloadManager.displayError_$_$ILjava_lang_String$$$_$V = sun_jkernel_DownloadManager_$_displayError_$_$ILjava_lang_String$$$_$V;
sun_jkernel_DownloadManager.doBackgroundDownloads_$_$Z$V = sun_jkernel_DownloadManager_$_doBackgroundDownloads_$_$Z$V;
sun_jkernel_DownloadManager.downloadFile_$_$Ljava_lang_String$$$_$Z = sun_jkernel_DownloadManager_$_downloadFile_$_$Ljava_lang_String$$$_$Z;
sun_jkernel_DownloadManager.downloadFromURL_$_$Ljava_lang_String$$$_Ljava_io_File$$$_Ljava_lang_String$$$_Z$V = sun_jkernel_DownloadManager_$_downloadFromURL_$_$Ljava_lang_String$$$_Ljava_io_File$$$_Ljava_lang_String$$$_Z$V;
sun_jkernel_DownloadManager.dumpOutput_$_$Ljava_lang_Process$$$_$V = sun_jkernel_DownloadManager_$_dumpOutput_$_$Ljava_lang_Process$$$_$V;
sun_jkernel_DownloadManager.fatalError_$_$I$V = sun_jkernel_DownloadManager_$_fatalError_$_$I$V;
sun_jkernel_DownloadManager.fatalError_$_$ILjava_lang_String$$$_$V = sun_jkernel_DownloadManager_$_fatalError_$_$ILjava_lang_String$$$_$V;
sun_jkernel_DownloadManager.flushBundleURLs_$_$$V = sun_jkernel_DownloadManager_$_flushBundleURLs_$_$$V;
sun_jkernel_DownloadManager.getAdditionalBootStrapPaths_$_$$_$$$$$_Ljava_io_File$$$_ = sun_jkernel_DownloadManager_$_getAdditionalBootStrapPaths_$_$$_$$$$$_Ljava_io_File$$$_;
sun_jkernel_DownloadManager.getBaseDownloadURL_$_$$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_getBaseDownloadURL_$_$$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.getBootClassPathEntryForClass_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_getBootClassPathEntryForClass_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.getBootClassPathEntryForResource_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_getBootClassPathEntryForResource_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.getBundleForResource_$_$Ljava_lang_String$$$_$Lsun_jkernel_Bundle$$$_ = sun_jkernel_DownloadManager_$_getBundleForResource_$_$Ljava_lang_String$$$_$Lsun_jkernel_Bundle$$$_;
sun_jkernel_DownloadManager.getBundleNames_$_$$_$$$$$_Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_getBundleNames_$_$$_$$$$$_Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.getBundlePath_$_$$Ljava_io_File$$$_ = sun_jkernel_DownloadManager_$_getBundlePath_$_$$Ljava_io_File$$$_;
sun_jkernel_DownloadManager.getBundleProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_getBundleProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.getBundleURLs_$_$Z$Ljava_util_Properties$$$_ = sun_jkernel_DownloadManager_$_getBundleURLs_$_$Z$Ljava_util_Properties$$$_;
sun_jkernel_DownloadManager.getCurrentProcessId_$_$$I = sun_jkernel_DownloadManager_$_getCurrentProcessId_$_$$I;
sun_jkernel_DownloadManager.getDebugKey_$_$$Z = sun_jkernel_DownloadManager_$_getDebugKey_$_$$Z;
sun_jkernel_DownloadManager.getDebugProperty_$_$$Z = sun_jkernel_DownloadManager_$_getDebugProperty_$_$$Z;
sun_jkernel_DownloadManager.getFileMap_$_$$Ljava_util_Map$$$_ = sun_jkernel_DownloadManager_$_getFileMap_$_$$Ljava_util_Map$$$_;
sun_jkernel_DownloadManager.getKernelJREDir_$_$$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_getKernelJREDir_$_$$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.getLocalLowKernelJava_$_$$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_getLocalLowKernelJava_$_$$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.getLocalLowTempBundlePath_$_$$Ljava_io_File$$$_ = sun_jkernel_DownloadManager_$_getLocalLowTempBundlePath_$_$$Ljava_io_File$$$_;
sun_jkernel_DownloadManager.getResourceMap_$_$$Ljava_util_Map$$$_ = sun_jkernel_DownloadManager_$_getResourceMap_$_$$Ljava_util_Map$$$_;
sun_jkernel_DownloadManager.getUrlFromRegistry_$_$$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_getUrlFromRegistry_$_$$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.getVisitorId0_$_$$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_getVisitorId0_$_$$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.getVisitorId_$_$$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_getVisitorId_$_$$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.handleException_$_$Ljava_lang_Throwable$$$_$Z = sun_jkernel_DownloadManager_$_handleException_$_$Ljava_lang_Throwable$$$_$Z;
sun_jkernel_DownloadManager.incrementDownloadCount_$_$$V = sun_jkernel_DownloadManager_$_incrementDownloadCount_$_$$V;
sun_jkernel_DownloadManager.isCurrentThreadDownloading_$_$$Z = sun_jkernel_DownloadManager_$_isCurrentThreadDownloading_$_$$Z;
sun_jkernel_DownloadManager.isJREComplete_$_$$Z = sun_jkernel_DownloadManager_$_isJREComplete_$_$$Z;
sun_jkernel_DownloadManager.isWindowsVista_$_$$Z = sun_jkernel_DownloadManager_$_isWindowsVista_$_$$Z;
sun_jkernel_DownloadManager.loadJKernelLibrary_$_$$V = sun_jkernel_DownloadManager_$_loadJKernelLibrary_$_$$V;
sun_jkernel_DownloadManager.log_$_$Ljava_lang_String$$$_$V = sun_jkernel_DownloadManager_$_log_$_$Ljava_lang_String$$$_$V;
sun_jkernel_DownloadManager.log_$_$Ljava_lang_Throwable$$$_$V = sun_jkernel_DownloadManager_$_log_$_$Ljava_lang_Throwable$$$_$V;
sun_jkernel_DownloadManager.main_$_$_$$$$$_Ljava_lang_String$$$_$V = sun_jkernel_DownloadManager_$_main_$_$_$$$$$_Ljava_lang_String$$$_$V;
sun_jkernel_DownloadManager.moveDirWithBroker_$_$Ljava_lang_String$$$_$Z = sun_jkernel_DownloadManager_$_moveDirWithBroker_$_$Ljava_lang_String$$$_$Z;
sun_jkernel_DownloadManager.moveFileWithBroker_$_$Ljava_lang_String$$$_$Z = sun_jkernel_DownloadManager_$_moveFileWithBroker_$_$Ljava_lang_String$$$_$Z;
sun_jkernel_DownloadManager.performCompletionIfNeeded_$_$$V = sun_jkernel_DownloadManager_$_performCompletionIfNeeded_$_$$V;
sun_jkernel_DownloadManager.postDownloadComplete_$_$$V = sun_jkernel_DownloadManager_$_postDownloadComplete_$_$$V;
sun_jkernel_DownloadManager.postDownloadError_$_$I$V = sun_jkernel_DownloadManager_$_postDownloadError_$_$I$V;
sun_jkernel_DownloadManager.println_$_$Ljava_lang_String$$$_$V = sun_jkernel_DownloadManager_$_println_$_$Ljava_lang_String$$$_$V;
sun_jkernel_DownloadManager.readTreeMap_$_$Ljava_io_InputStream$$$_$Ljava_util_Map$$$_ = sun_jkernel_DownloadManager_$_readTreeMap_$_$Ljava_io_InputStream$$$_$Ljava_util_Map$$$_;
sun_jkernel_DownloadManager.sendErrorPing_$_$I$V = sun_jkernel_DownloadManager_$_sendErrorPing_$_$I$V;
sun_jkernel_DownloadManager.send_$_$Ljava_io_InputStream$$$_Ljava_io_OutputStream$$$_$V = sun_jkernel_DownloadManager_$_send_$_$Ljava_io_InputStream$$$_Ljava_io_OutputStream$$$_$V;
sun_jkernel_DownloadManager.prototype.wait_$_$J$V = sun_jkernel_DownloadManager_$_wait_$_$J$V;
sun_jkernel_DownloadManager.prototype.wait_$_$JI$V = sun_jkernel_DownloadManager_$_wait_$_$JI$V;
sun_jkernel_DownloadManager.prototype.wait_$_$$V = sun_jkernel_DownloadManager_$_wait_$_$$V;
sun_jkernel_DownloadManager.prototype.equals_$_$Ljava_lang_Object$$$_$Z = sun_jkernel_DownloadManager_$_equals_$_$Ljava_lang_Object$$$_$Z;
sun_jkernel_DownloadManager.prototype.toString_$_$$Ljava_lang_String$$$_ = sun_jkernel_DownloadManager_$_toString_$_$$Ljava_lang_String$$$_;
sun_jkernel_DownloadManager.prototype.hashCode_$_$$I = sun_jkernel_DownloadManager_$_hashCode_$_$$I;
sun_jkernel_DownloadManager.prototype.getClass_$_$$Ljava_lang_Class$$$_ = sun_jkernel_DownloadManager_$_getClass_$_$$Ljava_lang_Class$$$_;
sun_jkernel_DownloadManager.prototype.notify_$_$$V = sun_jkernel_DownloadManager_$_notify_$_$$V;
sun_jkernel_DownloadManager.prototype.notifyAll_$_$$V = sun_jkernel_DownloadManager_$_notifyAll_$_$$V;
