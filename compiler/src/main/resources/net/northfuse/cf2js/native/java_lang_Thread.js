/** @constructor */
function java_lang_Thread() {}
var java_lang_Thread_$__$$$init$$$__$_$$V = function (arg0) {
/*implement constructor!*/
/*
Allocates a new Thread object. This constructor has 
 the same effect as Thread(null, null,
 gname), where gname is 
 a newly generated name. Automatically generated names are of the 
 form "Thread-"+n, where n is an integer.


See Also:Thread(ThreadGroup, Runnable, String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$__$$$init$$$__$_$$V"));
throw ex;
};
var java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_Runnable$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Allocates a new Thread object. This constructor has 
 the same effect as Thread(null, target,
 gname), where gname is 
 a newly generated name. Automatically generated names are of the 
 form "Thread-"+n, where n is an integer.


Parameters:target - the object whose run method is called.See Also:Thread(ThreadGroup, Runnable, String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_Runnable$$$_$V"));
throw ex;
};
var java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_Runnable$$$_Ljava_lang_String$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Allocates a new Thread object. This constructor has 
 the same effect as Thread(null, target, name).


Parameters:target - the object whose run method is called.name - the name of the new thread.See Also:Thread(ThreadGroup, Runnable, String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_Runnable$$$_Ljava_lang_String$$$_$V"));
throw ex;
};
var java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Allocates a new Thread object. This constructor has 
 the same effect as Thread(null, null, name).


Parameters:name - the name of the new thread.See Also:Thread(ThreadGroup, Runnable, String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_String$$$_$V"));
throw ex;
};
var java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Allocates a new Thread object. This constructor has 
 the same effect as Thread(group, target,
 gname), where gname is 
 a newly generated name. Automatically generated names are of the 
 form "Thread-"+n, where n is an integer.


Parameters:group - the thread group.target - the object whose run method is called.
Throws:
SecurityException - if the current thread cannot create a
             thread in the specified thread group.See Also:Thread(ThreadGroup, Runnable, String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_$V"));
throw ex;
};
var java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_Ljava_lang_String$$$_$V = function (arg0, arg1, arg2, arg3) {
/*implement constructor!*/
/*
Allocates a new Thread object so that it has 
 target as its run object, has the specified 
 name as its name, and belongs to the thread group 
 referred to by group.
 
 If group is null and there is a 
 security manager, the group is determined by the security manager's 
 getThreadGroup method. If group is 
 null and there is not a security manager, or the
 security manager's getThreadGroup method returns 
 null, the group is set to be the same ThreadGroup 
 as the thread that is creating the new thread.
 
 If there is a security manager, its checkAccess 
 method is called with the ThreadGroup as its argument.
 In addition, its checkPermission
 method is called with the
 RuntimePermission("enableContextClassLoaderOverride")
 permission when invoked directly or indirectly by the constructor
 of a subclass which overrides the getContextClassLoader
 or setContextClassLoader methods.
 This may result in a SecurityException.

 
 If the target argument is not null, the 
 run method of the target is called when 
 this thread is started. If the target argument is 
 null, this thread's run method is called 
 when this thread is started. 
 
 The priority of the newly created thread is set equal to the 
 priority of the thread creating it, that is, the currently running 
 thread. The method setPriority may be used to 
 change the priority to a new value. 
 
 The newly created thread is initially marked as being a daemon 
 thread if and only if the thread creating it is currently marked 
 as a daemon thread. The method setDaemon  may be used 
 to change whether or not a thread is a daemon.


Parameters:group - the thread group.target - the object whose run method is called.name - the name of the new thread.
Throws:
SecurityException - if the current thread cannot create a
               thread in the specified thread group or cannot
               override the context class loader methods.See Also:Runnable.run(), 
run(), 
setDaemon(boolean), 
setPriority(int), 
ThreadGroup.checkAccess(), 
SecurityManager.checkAccess(java.lang.Thread)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_Ljava_lang_String$$$_$V"));
throw ex;
};
var java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_Ljava_lang_String$$$_J$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement constructor!*/
/*
Allocates a new Thread object so that it has
 target as its run object, has the specified
 name as its name, belongs to the thread group referred to
 by group, and has the specified stack size.

 This constructor is identical to Thread(ThreadGroup,Runnable,String) with the exception of the fact
 that it allows the thread stack size to be specified.  The stack size
 is the approximate number of bytes of address space that the virtual
 machine is to allocate for this thread's stack.  The effect of the
 stackSize parameter, if any, is highly platform dependent.

 On some platforms, specifying a higher value for the
 stackSize parameter may allow a thread to achieve greater
 recursion depth before throwing a StackOverflowError.
 Similarly, specifying a lower value may allow a greater number of
 threads to exist concurrently without throwing an OutOfMemoryError (or other internal error).  The details of
 the relationship between the value of the stackSize parameter
 and the maximum recursion depth and concurrency level are
 platform-dependent.  On some platforms, the value of the
 stackSize parameter may have no effect whatsoever.
 
 The virtual machine is free to treat the stackSize
 parameter as a suggestion.  If the specified value is unreasonably low
 for the platform, the virtual machine may instead use some
 platform-specific minimum value; if the specified value is unreasonably
 high, the virtual machine may instead use some platform-specific
 maximum.  Likewise, the virtual machine is free to round the specified
 value up or down as it sees fit (or to ignore it completely).

 Specifying a value of zero for the stackSize parameter will
 cause this constructor to behave exactly like the
 Thread(ThreadGroup, Runnable, String) constructor.

 Due to the platform-dependent nature of the behavior of this
 constructor, extreme care should be exercised in its use.
 The thread stack size necessary to perform a given computation will
 likely vary from one JRE implementation to another.  In light of this
 variation, careful tuning of the stack size parameter may be required,
 and the tuning may need to be repeated for each JRE implementation on
 which an application is to run.

 Implementation note: Java platform implementers are encouraged to
 document their implementation's behavior with respect to the
 stackSize parameter.


Parameters:group - the thread group.target - the object whose run method is called.name - the name of the new thread.stackSize - the desired stack size for the new thread, or
             zero to indicate that this parameter is to be ignored.
Throws:
SecurityException - if the current thread cannot create a
               thread in the specified thread group.Since:
  1.4

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_Ljava_lang_String$$$_J$V"));
throw ex;
};
var java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_String$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Allocates a new Thread object. This constructor has 
 the same effect as Thread(group, null, name)


Parameters:group - the thread group.name - the name of the new thread.
Throws:
SecurityException - if the current thread cannot create a
               thread in the specified thread group.See Also:Thread(ThreadGroup, Runnable, String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_String$$$_$V"));
throw ex;
};
var java_lang_Thread_$_activeCount_$_$$I = function () {
/*implement method!*/
/*
Returns the number of active threads in the current thread's thread
 group.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_activeCount_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Thread_$_blockedOn_$_$Lsun_nio_ch_Interruptible$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_blockedOn_$_$Lsun_nio_ch_Interruptible$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_checkAccess_$_$$V = function (arg0) {
/*implement method!*/
/*
Determines if the currently running thread has permission to 
 modify this thread. 
 
 If there is a security manager, its checkAccess method 
 is called with this thread as its argument. This may result in 
 throwing a SecurityException.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_checkAccess_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_clone_$_$$Ljava_lang_Object$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a clone if the class of this object is Cloneable.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_clone_$_$$Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object
};
var java_lang_Thread_$_countStackFrames_$_$$I = function (arg0) {
/*implement method!*/
/*
Deprecated. The definition of this call depends on suspend(),
                   which is deprecated.  Further, the results of this call
                   were never well-defined.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_countStackFrames_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Thread_$_currentThread_$_$$Ljava_lang_Thread$$$_ = function () {
/*implement method!*/
/*
Returns a reference to the currently executing thread object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_currentThread_$_$$Ljava_lang_Thread$$$_"));
throw ex;
//must have a return of type Thread
};
var java_lang_Thread_$_destroy_$_$$V = function (arg0) {
/*implement method!*/
/*
Deprecated. This method was originally designed to destroy this
     thread without any cleanup. Any monitors it held would have
     remained locked. However, the method was never implemented.
     If if were to be implemented, it would be deadlock-prone in
     much the manner of suspend(). If the target thread held
     a lock protecting a critical system resource when it was
     destroyed, no thread could ever access this resource again.
     If another thread ever attempted to lock this resource, deadlock
     would result. Such deadlocks typically manifest themselves as
     "frozen" processes. For more information, see
     
     Why are Thread.stop, Thread.suspend and Thread.resume Deprecated?.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_destroy_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_dumpStack_$_$$V = function () {
/*implement method!*/
/*
Prints a stack trace of the current thread to the standard error stream.
 This method is used only for debugging.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_dumpStack_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_enumerate_$_$_$$$$$_Ljava_lang_Thread$$$_$I = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_enumerate_$_$_$$$$$_Ljava_lang_Thread$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Thread_$_getAllStackTraces_$_$$Ljava_util_Map$$$_ = function () {
/*implement method!*/
/*
Returns a map of stack traces for all live threads.
 The map keys are threads and each map value is an array of
 StackTraceElement that represents the stack dump
 of the corresponding Thread.
 The returned stack traces are in the format specified for
 the getStackTrace method.

 The threads may be executing while this method is called.
 The stack trace of each thread only represents a snapshot and
 each stack trace may be obtained at different time.  A zero-length
 array will be returned in the map value if the virtual machine has 
 no stack trace information about a thread.

 If there is a security manager, then the security manager's 
 checkPermission method is called with a 
 RuntimePermission("getStackTrace") permission as well as
 RuntimePermission("modifyThreadGroup") permission
 to see if it is ok to get the stack trace of all threads.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_getAllStackTraces_$_$$Ljava_util_Map$$$_"));
throw ex;
//must have a return of type Map
};
var java_lang_Thread_$_getContextClassLoader_$_$$Ljava_lang_ClassLoader$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the context ClassLoader for this Thread. The context
 ClassLoader is provided by the creator of the thread for use
 by code running in this thread when loading classes and resources.
 If not set, the default is the ClassLoader context of the parent
 Thread. The context ClassLoader of the primordial thread is
 typically set to the class loader used to load the application.

 First, if there is a security manager, and the caller's class
 loader is not null and the caller's class loader is not the same as or
 an ancestor of the context class loader for the thread whose
 context class loader is being requested, then the security manager's
 checkPermission 
 method is called with a 
 RuntimePermission("getClassLoader") permission
  to see if it's ok to get the context ClassLoader..

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_getContextClassLoader_$_$$Ljava_lang_ClassLoader$$$_"));
throw ex;
//must have a return of type ClassLoader
};
var java_lang_Thread_$_getDefaultUncaughtExceptionHandler_$_$$Ljava_lang_Thread$UncaughtExceptionHandler$$$_ = function () {
/*implement method!*/
/*
Returns the default handler invoked when a thread abruptly terminates
 due to an uncaught exception. If the returned value is null,
 there is no default.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_getDefaultUncaughtExceptionHandler_$_$$Ljava_lang_Thread$UncaughtExceptionHandler$$$_"));
throw ex;
//must have a return of type UncaughtExceptionHandler
};
var java_lang_Thread_$_getId_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the identifier of this Thread.  The thread ID is a positive
 long number generated when this thread was created.  
 The thread ID is unique and remains unchanged during its lifetime.  
 When a thread is terminated, this thread ID may be reused.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_getId_$_$$J"));
throw ex;
//must have a return of type long
};
var java_lang_Thread_$_getName_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns this thread's name.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_getName_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Thread_$_getPriority_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns this thread's priority.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_getPriority_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Thread_$_getStackTrace_$_$$_$$$$$_Ljava_lang_StackTraceElement$$$_ = function (arg0) {
/*implement method!*/
/*
Returns an array of stack trace elements representing the stack dump
 of this thread.  This method will return a zero-length array if
 this thread has not started or has terminated. 
 If the returned array is of non-zero length then the first element of 
 the array represents the top of the stack, which is the most recent
 method invocation in the sequence.  The last element of the array
 represents the bottom of the stack, which is the least recent method
 invocation in the sequence.

 If there is a security manager, and this thread is not 
 the current thread, then the security manager's 
 checkPermission method is called with a 
 RuntimePermission("getStackTrace") permission
 to see if it's ok to get the stack trace. 

 Some virtual machines may, under some circumstances, omit one
 or more stack frames from the stack trace.  In the extreme case,
 a virtual machine that has no stack trace information concerning
 this thread is permitted to return a zero-length array from this
 method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_getStackTrace_$_$$_$$$$$_Ljava_lang_StackTraceElement$$$_"));
throw ex;
//must have a return of type StackTraceElement[]
};
var java_lang_Thread_$_getState_$_$$Ljava_lang_Thread$State$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the state of this thread.
 This method is designed for use in monitoring of the system state,
 not for synchronization control.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_getState_$_$$Ljava_lang_Thread$State$$$_"));
throw ex;
//must have a return of type State
};
var java_lang_Thread_$_getThreadGroup_$_$$Ljava_lang_ThreadGroup$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the thread group to which this thread belongs. 
 This method returns null if this thread has died
 (been stopped).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_getThreadGroup_$_$$Ljava_lang_ThreadGroup$$$_"));
throw ex;
//must have a return of type ThreadGroup
};
var java_lang_Thread_$_getUncaughtExceptionHandler_$_$$Ljava_lang_Thread$UncaughtExceptionHandler$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the handler invoked when this thread abruptly terminates
 due to an uncaught exception. If this thread has not had an
 uncaught exception handler explicitly set then this thread's
 ThreadGroup object is returned, unless this thread
 has terminated, in which case null is returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_getUncaughtExceptionHandler_$_$$Ljava_lang_Thread$UncaughtExceptionHandler$$$_"));
throw ex;
//must have a return of type UncaughtExceptionHandler
};
var java_lang_Thread_$_holdsLock_$_$Ljava_lang_Object$$$_$Z = function (arg0) {
/*implement method!*/
/*
Returns true if and only if the current thread holds the
 monitor lock on the specified object.

 This method is designed to allow a program to assert that
 the current thread already holds a specified lock:
      assert Thread.holdsLock(obj);

Parameters:

obj -> the object on which to test lock ownership

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_holdsLock_$_$Ljava_lang_Object$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Thread_$_interrupt_$_$$V = function (arg0) {
/*implement method!*/
/*
Interrupts this thread.
 
  Unless the current thread is interrupting itself, which is
 always permitted, the checkAccess method
 of this thread is invoked, which may cause a SecurityException to be thrown.

  If this thread is blocked in an invocation of the wait(), wait(long), or wait(long, int) methods of the Object
 class, or of the join(), join(long), join(long, int), sleep(long), or sleep(long, int),
 methods of this class, then its interrupt status will be cleared and it
 will receive an InterruptedException.

  If this thread is blocked in an I/O operation upon an interruptible
 channel then the channel will be closed, the thread's interrupt
 status will be set, and the thread will receive a ClosedByInterruptException.

  If this thread is blocked in a Selector
 then the thread's interrupt status will be set and it will return
 immediately from the selection operation, possibly with a non-zero
 value, just as if the selector's wakeup method were invoked.

  If none of the previous conditions hold then this thread's interrupt
 status will be set. 

  Interrupting a thread that is not alive need not have any effect.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_interrupt_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_interrupted_$_$$Z = function () {
/*implement method!*/
/*
Tests whether the current thread has been interrupted.  The
 interrupted status of the thread is cleared by this method.  In
 other words, if this method were to be called twice in succession, the
 second call would return false (unless the current thread were
 interrupted again, after the first call had cleared its interrupted
 status and before the second call had examined it).

 A thread interruption ignored because a thread was not alive 
 at the time of the interrupt will be reflected by this method 
 returning false.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_interrupted_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Thread_$_isAlive_$_$$Z = function (arg0) {
/*implement method!*/
/*
Tests if this thread is alive. A thread is alive if it has 
 been started and has not yet died.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_isAlive_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Thread_$_isDaemon_$_$$Z = function (arg0) {
/*implement method!*/
/*
Tests if this thread is a daemon thread.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_isDaemon_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Thread_$_isInterrupted_$_$$Z = function (arg0) {
/*implement method!*/
/*
Tests whether this thread has been interrupted.  The interrupted
 status of the thread is unaffected by this method.

 A thread interruption ignored because a thread was not alive 
 at the time of the interrupt will be reflected by this method 
 returning false.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_isInterrupted_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Thread_$_join_$_$$V = function (arg0) {
/*implement method!*/
/*
Waits for this thread to die.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_join_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_join_$_$J$V = function (arg0, arg1) {
/*implement method!*/
/*
Waits at most millis milliseconds for this thread to 
 die. A timeout of 0 means to wait forever.

Parameters:

millis -> the time to wait in milliseconds.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_join_$_$J$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_join_$_$JI$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Waits at most millis milliseconds plus 
 nanos nanoseconds for this thread to die.

Parameters:

millis -> the time to wait in milliseconds.

nanos -> 0-999999 additional nanoseconds to wait.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_join_$_$JI$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_resume_$_$$V = function (arg0) {
/*implement method!*/
/*
Deprecated. This method exists solely for use with suspend(),
     which has been deprecated because it is deadlock-prone.
     For more information, see 
     Why 
     are Thread.stop, Thread.suspend and Thread.resume Deprecated?.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_resume_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_run_$_$$V = function (arg0) {
/*implement method!*/
/*
If this thread was constructed using a separate 
 Runnable run object, then that 
 Runnable object's run method is called; 
 otherwise, this method does nothing and returns. 
 
 Subclasses of Thread should override this method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_run_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_setContextClassLoader_$_$Ljava_lang_ClassLoader$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the context ClassLoader for this Thread. The context
 ClassLoader can be set when a thread is created, and allows
 the creator of the thread to provide the appropriate class loader
 to code running in the thread when loading classes and resources.

 First, if there is a security manager, its checkPermission 
 method is called with a 
 RuntimePermission("setContextClassLoader") permission
  to see if it's ok to set the context ClassLoader..

Parameters:

cl -> the context ClassLoader for this Thread

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_setContextClassLoader_$_$Ljava_lang_ClassLoader$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_setDaemon_$_$Z$V = function (arg0, arg1) {
/*implement method!*/
/*
Marks this thread as either a daemon thread or a user thread. The 
 Java Virtual Machine exits when the only threads running are all 
 daemon threads. 
 
 This method must be called before the thread is started. 
 
 This method first calls the checkAccess method 
 of this thread 
 with no arguments. This may result in throwing a 
 SecurityException (in the current thread).

Parameters:

on -> if true, marks this thread as a
                  daemon thread.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_setDaemon_$_$Z$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_setDefaultUncaughtExceptionHandler_$_$Ljava_lang_Thread$UncaughtExceptionHandler$$$_$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_setDefaultUncaughtExceptionHandler_$_$Ljava_lang_Thread$UncaughtExceptionHandler$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_setName_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Changes the name of this thread to be equal to the argument 
 name. 
 
 First the checkAccess method of this thread is called 
 with no arguments. This may result in throwing a 
 SecurityException.

Parameters:

name -> the new name for this thread.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_setName_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_setPriority_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Changes the priority of this thread. 
 
 First the checkAccess method of this thread is called 
 with no arguments. This may result in throwing a 
 SecurityException. 
 
 Otherwise, the priority of this thread is set to the smaller of 
 the specified newPriority and the maximum permitted 
 priority of the thread's thread group.

Parameters:

newPriority -> priority to set this thread to

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_setPriority_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_setUncaughtExceptionHandler_$_$Ljava_lang_Thread$UncaughtExceptionHandler$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_setUncaughtExceptionHandler_$_$Ljava_lang_Thread$UncaughtExceptionHandler$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_sleep_$_$J$V = function (arg0) {
/*implement method!*/
/*
Causes the currently executing thread to sleep (temporarily cease 
 execution) for the specified number of milliseconds, subject to 
 the precision and accuracy of system timers and schedulers. The thread 
 does not lose ownership of any monitors.

Parameters:

millis -> the length of time to sleep in milliseconds.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_sleep_$_$J$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_sleep_$_$JI$V = function (arg0, arg1) {
/*implement method!*/
/*
Causes the currently executing thread to sleep (cease execution) 
 for the specified number of milliseconds plus the specified number 
 of nanoseconds, subject to the precision and accuracy of system 
 timers and schedulers. The thread does not lose ownership of any 
 monitors.

Parameters:

millis -> the length of time to sleep in milliseconds.

nanos -> 0-999999 additional nanoseconds to sleep.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_sleep_$_$JI$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_start_$_$$V = function (arg0) {
/*implement method!*/
/*
Causes this thread to begin execution; the Java Virtual Machine 
 calls the run method of this thread. 
 
 The result is that two threads are running concurrently: the 
 current thread (which returns from the call to the 
 start method) and the other thread (which executes its 
 run method). 
 
 It is never legal to start a thread more than once.
 In particular, a thread may not be restarted once it has completed
 execution.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_start_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_stop_$_$$V = function (arg0) {
/*implement method!*/
/*
Deprecated. This method is inherently unsafe.  Stopping a thread with
             Thread.stop causes it to unlock all of the monitors that it
             has locked (as a natural consequence of the unchecked
             ThreadDeath exception propagating up the stack).  If
       any of the objects previously protected by these monitors were in
       an inconsistent state, the damaged objects become visible to
       other threads, potentially resulting in arbitrary behavior.  Many
       uses of stop should be replaced by code that simply
       modifies some variable to indicate that the target thread should
       stop running.  The target thread should check this variable  
       regularly, and return from its run method in an orderly fashion
       if the variable indicates that it is to stop running.  If the
       target thread waits for long periods (on a condition variable,
       for example), the interrupt method should be used to
       interrupt the wait. 
       For more information, see 
       Why 
       are Thread.stop, Thread.suspend and Thread.resume Deprecated?.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_stop_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_stop_$_$Ljava_lang_Throwable$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Deprecated. This method is inherently unsafe.  See stop()
        for details.  An additional danger of this
        method is that it may be used to generate exceptions that the
        target thread is unprepared to handle (including checked
        exceptions that the thread could not possibly throw, were it
        not for this method).
        For more information, see 
        Why 
        are Thread.stop, Thread.suspend and Thread.resume Deprecated?.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_stop_$_$Ljava_lang_Throwable$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_suspend_$_$$V = function (arg0) {
/*implement method!*/
/*
Deprecated. This method has been deprecated, as it is
   inherently deadlock-prone.  If the target thread holds a lock on the
   monitor protecting a critical system resource when it is suspended, no
   thread can access this resource until the target thread is resumed. If
   the thread that would resume the target thread attempts to lock this
   monitor prior to calling resume, deadlock results.  Such
   deadlocks typically manifest themselves as "frozen" processes.
   For more information, see 
   Why 
   are Thread.stop, Thread.suspend and Thread.resume Deprecated?.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_suspend_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of this thread, including the 
 thread's name, priority, and thread group.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_toString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Thread_$_yield_$_$$V = function () {
/*implement method!*/
/*
Causes the currently executing thread object to temporarily pause 
 and allow other threads to execute.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Thread_$_yield_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Thread_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_lang_Thread_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_Thread_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_lang_Thread_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_lang_Thread_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var java_lang_Thread_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_Thread_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_lang_Thread_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_lang_Thread.prototype._$$$init$$$__$_$$V = java_lang_Thread_$__$$$init$$$__$_$$V;
java_lang_Thread.prototype._$$$init$$$__$_$Ljava_lang_Runnable$$$_$V = java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_Runnable$$$_$V;
java_lang_Thread.prototype._$$$init$$$__$_$Ljava_lang_Runnable$$$_Ljava_lang_String$$$_$V = java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_Runnable$$$_Ljava_lang_String$$$_$V;
java_lang_Thread.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_lang_Thread.prototype._$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_$V = java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_$V;
java_lang_Thread.prototype._$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_Ljava_lang_String$$$_$V = java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_Ljava_lang_String$$$_$V;
java_lang_Thread.prototype._$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_Ljava_lang_String$$$_J$V = java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_Runnable$$$_Ljava_lang_String$$$_J$V;
java_lang_Thread.prototype._$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_String$$$_$V = java_lang_Thread_$__$$$init$$$__$_$Ljava_lang_ThreadGroup$$$_Ljava_lang_String$$$_$V;
java_lang_Thread.activeCount_$_$$I = java_lang_Thread_$_activeCount_$_$$I;
java_lang_Thread.prototype.blockedOn_$_$Lsun_nio_ch_Interruptible$$$_$V = java_lang_Thread_$_blockedOn_$_$Lsun_nio_ch_Interruptible$$$_$V;
java_lang_Thread.prototype.checkAccess_$_$$V = java_lang_Thread_$_checkAccess_$_$$V;
java_lang_Thread.prototype.clone_$_$$Ljava_lang_Object$$$_ = java_lang_Thread_$_clone_$_$$Ljava_lang_Object$$$_;
java_lang_Thread.prototype.countStackFrames_$_$$I = java_lang_Thread_$_countStackFrames_$_$$I;
java_lang_Thread.currentThread_$_$$Ljava_lang_Thread$$$_ = java_lang_Thread_$_currentThread_$_$$Ljava_lang_Thread$$$_;
java_lang_Thread.prototype.destroy_$_$$V = java_lang_Thread_$_destroy_$_$$V;
java_lang_Thread.dumpStack_$_$$V = java_lang_Thread_$_dumpStack_$_$$V;
java_lang_Thread.enumerate_$_$_$$$$$_Ljava_lang_Thread$$$_$I = java_lang_Thread_$_enumerate_$_$_$$$$$_Ljava_lang_Thread$$$_$I;
java_lang_Thread.getAllStackTraces_$_$$Ljava_util_Map$$$_ = java_lang_Thread_$_getAllStackTraces_$_$$Ljava_util_Map$$$_;
java_lang_Thread.prototype.getContextClassLoader_$_$$Ljava_lang_ClassLoader$$$_ = java_lang_Thread_$_getContextClassLoader_$_$$Ljava_lang_ClassLoader$$$_;
java_lang_Thread.getDefaultUncaughtExceptionHandler_$_$$Ljava_lang_Thread$UncaughtExceptionHandler$$$_ = java_lang_Thread_$_getDefaultUncaughtExceptionHandler_$_$$Ljava_lang_Thread$UncaughtExceptionHandler$$$_;
java_lang_Thread.prototype.getId_$_$$J = java_lang_Thread_$_getId_$_$$J;
java_lang_Thread.prototype.getName_$_$$Ljava_lang_String$$$_ = java_lang_Thread_$_getName_$_$$Ljava_lang_String$$$_;
java_lang_Thread.prototype.getPriority_$_$$I = java_lang_Thread_$_getPriority_$_$$I;
java_lang_Thread.prototype.getStackTrace_$_$$_$$$$$_Ljava_lang_StackTraceElement$$$_ = java_lang_Thread_$_getStackTrace_$_$$_$$$$$_Ljava_lang_StackTraceElement$$$_;
java_lang_Thread.prototype.getState_$_$$Ljava_lang_Thread$State$$$_ = java_lang_Thread_$_getState_$_$$Ljava_lang_Thread$State$$$_;
java_lang_Thread.prototype.getThreadGroup_$_$$Ljava_lang_ThreadGroup$$$_ = java_lang_Thread_$_getThreadGroup_$_$$Ljava_lang_ThreadGroup$$$_;
java_lang_Thread.prototype.getUncaughtExceptionHandler_$_$$Ljava_lang_Thread$UncaughtExceptionHandler$$$_ = java_lang_Thread_$_getUncaughtExceptionHandler_$_$$Ljava_lang_Thread$UncaughtExceptionHandler$$$_;
java_lang_Thread.holdsLock_$_$Ljava_lang_Object$$$_$Z = java_lang_Thread_$_holdsLock_$_$Ljava_lang_Object$$$_$Z;
java_lang_Thread.prototype.interrupt_$_$$V = java_lang_Thread_$_interrupt_$_$$V;
java_lang_Thread.interrupted_$_$$Z = java_lang_Thread_$_interrupted_$_$$Z;
java_lang_Thread.prototype.isAlive_$_$$Z = java_lang_Thread_$_isAlive_$_$$Z;
java_lang_Thread.prototype.isDaemon_$_$$Z = java_lang_Thread_$_isDaemon_$_$$Z;
java_lang_Thread.prototype.isInterrupted_$_$$Z = java_lang_Thread_$_isInterrupted_$_$$Z;
java_lang_Thread.prototype.join_$_$$V = java_lang_Thread_$_join_$_$$V;
java_lang_Thread.prototype.join_$_$J$V = java_lang_Thread_$_join_$_$J$V;
java_lang_Thread.prototype.join_$_$JI$V = java_lang_Thread_$_join_$_$JI$V;
java_lang_Thread.prototype.resume_$_$$V = java_lang_Thread_$_resume_$_$$V;
java_lang_Thread.prototype.run_$_$$V = java_lang_Thread_$_run_$_$$V;
java_lang_Thread.prototype.setContextClassLoader_$_$Ljava_lang_ClassLoader$$$_$V = java_lang_Thread_$_setContextClassLoader_$_$Ljava_lang_ClassLoader$$$_$V;
java_lang_Thread.prototype.setDaemon_$_$Z$V = java_lang_Thread_$_setDaemon_$_$Z$V;
java_lang_Thread.setDefaultUncaughtExceptionHandler_$_$Ljava_lang_Thread$UncaughtExceptionHandler$$$_$V = java_lang_Thread_$_setDefaultUncaughtExceptionHandler_$_$Ljava_lang_Thread$UncaughtExceptionHandler$$$_$V;
java_lang_Thread.prototype.setName_$_$Ljava_lang_String$$$_$V = java_lang_Thread_$_setName_$_$Ljava_lang_String$$$_$V;
java_lang_Thread.prototype.setPriority_$_$I$V = java_lang_Thread_$_setPriority_$_$I$V;
java_lang_Thread.prototype.setUncaughtExceptionHandler_$_$Ljava_lang_Thread$UncaughtExceptionHandler$$$_$V = java_lang_Thread_$_setUncaughtExceptionHandler_$_$Ljava_lang_Thread$UncaughtExceptionHandler$$$_$V;
java_lang_Thread.sleep_$_$J$V = java_lang_Thread_$_sleep_$_$J$V;
java_lang_Thread.sleep_$_$JI$V = java_lang_Thread_$_sleep_$_$JI$V;
java_lang_Thread.prototype.start_$_$$V = java_lang_Thread_$_start_$_$$V;
java_lang_Thread.prototype.stop_$_$$V = java_lang_Thread_$_stop_$_$$V;
java_lang_Thread.prototype.stop_$_$Ljava_lang_Throwable$$$_$V = java_lang_Thread_$_stop_$_$Ljava_lang_Throwable$$$_$V;
java_lang_Thread.prototype.suspend_$_$$V = java_lang_Thread_$_suspend_$_$$V;
java_lang_Thread.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_Thread_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_Thread.yield_$_$$V = java_lang_Thread_$_yield_$_$$V;
java_lang_Thread.prototype.wait_$_$J$V = java_lang_Thread_$_wait_$_$J$V;
java_lang_Thread.prototype.wait_$_$JI$V = java_lang_Thread_$_wait_$_$JI$V;
java_lang_Thread.prototype.wait_$_$$V = java_lang_Thread_$_wait_$_$$V;
java_lang_Thread.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_Thread_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_Thread.prototype.hashCode_$_$$I = java_lang_Thread_$_hashCode_$_$$I;
java_lang_Thread.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_Thread_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_Thread.prototype.notify_$_$$V = java_lang_Thread_$_notify_$_$$V;
java_lang_Thread.prototype.notifyAll_$_$$V = java_lang_Thread_$_notifyAll_$_$$V;
