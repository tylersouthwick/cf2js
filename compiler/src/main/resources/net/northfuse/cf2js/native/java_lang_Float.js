/** @constructor */
function java_lang_Float() {}
var java_lang_Float_$__$$$init$$$__$_$D$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Float object that
 represents the argument converted to type float.


Parameters:value - the value to be represented by the Float.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$__$$$init$$$__$_$D$V"));
throw ex;
};
var java_lang_Float_$__$$$init$$$__$_$F$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Float object that
 represents the primitive float argument.


Parameters:value - the value to be represented by the Float.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$__$$$init$$$__$_$F$V"));
throw ex;
};
var java_lang_Float_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Float object that 
 represents the floating-point value of type float 
 represented by the string. The string is converted to a 
 float value as if by the valueOf method.


Parameters:s - a string to be converted to a Float.
Throws:
NumberFormatException - if the string does not contain a
               parsable number.See Also:valueOf(java.lang.String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$__$$$init$$$__$_$Ljava_lang_String$$$_$V"));
throw ex;
};
var java_lang_Float_$_byteValue_$_$$B = function (arg0) {
/*implement method!*/
/*
Returns the value of this Float as a
 byte (by casting to a byte).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_byteValue_$_$$B"));
throw ex;
//must have a return of type byte
};
var java_lang_Float_$_compareTo_$_$Ljava_lang_Float$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Compares two Float objects numerically.  There are
 two ways in which comparisons performed by this method differ
 from those performed by the Java language numerical comparison
 operators (<, <=, ==, >= >) when
 applied to primitive float values:
 
                Float.NaN is considered by this method to
                be equal to itself and greater than all other
                float values
                (including Float.POSITIVE_INFINITY).
 
                0.0f is considered by this method to be greater
                than -0.0f.
 
 This ensures that the natural ordering of Float
 objects imposed by this method is consistent with equals.

Parameters:

anotherFloat -> the Float to be compared.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_compareTo_$_$Ljava_lang_Float$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Float_$_compareTo_$_$Ljava_lang_Object$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_compareTo_$_$Ljava_lang_Object$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Float_$_compare_$_$FF$I = function (arg0, arg1) {
/*implement method!*/
/*
Compares the two specified float values. The sign
 of the integer value returned is the same as that of the
 integer that would be returned by the call:
     new Float(f1).compareTo(new Float(f2))

Parameters:

f1 -> the first float to compare.

f2 -> the second float to compare.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_compare_$_$FF$I"));
throw ex;
//must have a return of type int
};
var java_lang_Float_$_doubleValue_$_$$D = function (arg0) {
/*implement method!*/
/*
Returns the double value of this
 Float object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_doubleValue_$_$$D"));
throw ex;
//must have a return of type double
};
var java_lang_Float_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Compares this object against the specified object.  The result
 is true if and only if the argument is not
 null and is a Float object that
 represents a float with the same value as the
 float represented by this object. For this
 purpose, two float values are considered to be the
 same if and only if the method floatToIntBits(float)
 returns the identical int value when applied to
 each.
 
 Note that in most cases, for two instances of class
 Float, f1 and f2, the value
 of f1.equals(f2) is true if and only if
    f1.floatValue() == f2.floatValue()
 
 
 also has the value true. However, there are two exceptions:
 
 If f1 and f2 both represent
     Float.NaN, then the equals method returns
     true, even though Float.NaN==Float.NaN
     has the value false.
 If f1 represents +0.0f while
     f2 represents -0.0f, or vice
     versa, the equal test has the value
     false, even though 0.0f==-0.0f
     has the value true.
 
 This definition allows hash tables to operate properly.

Parameters:

obj -> the object to be compared

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_equals_$_$Ljava_lang_Object$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Float_$_floatToIntBits_$_$F$I = function (arg0) {
/*implement method!*/
/*
Returns a representation of the specified floating-point value
 according to the IEEE 754 floating-point "single format" bit
 layout.
 
 Bit 31 (the bit that is selected by the mask 
 0x80000000) represents the sign of the floating-point 
 number. 
 Bits 30-23 (the bits that are selected by the mask 
 0x7f800000) represent the exponent. 
 Bits 22-0 (the bits that are selected by the mask 
 0x007fffff) represent the significand (sometimes called 
 the mantissa) of the floating-point number. 
 If the argument is positive infinity, the result is 
 0x7f800000. 
 If the argument is negative infinity, the result is 
 0xff800000. 
 If the argument is NaN, the result is 0x7fc00000. 
 
 In all cases, the result is an integer that, when given to the 
 intBitsToFloat(int) method, will produce a floating-point 
 value the same as the argument to floatToIntBits
 (except all NaN values are collapsed to a single
 "canonical" NaN value).

Parameters:

value -> a floating-point number.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_floatToIntBits_$_$F$I"));
throw ex;
//must have a return of type int
};
var java_lang_Float_$_floatToRawIntBits_$_$F$I = function (arg0) {
/*implement method!*/
/*
Returns a representation of the specified floating-point value
 according to the IEEE 754 floating-point "single format" bit
 layout, preserving Not-a-Number (NaN) values.
 
 Bit 31 (the bit that is selected by the mask 
 0x80000000) represents the sign of the floating-point 
 number. 
 Bits 30-23 (the bits that are selected by the mask 
 0x7f800000) represent the exponent. 
 Bits 22-0 (the bits that are selected by the mask 
 0x007fffff) represent the significand (sometimes called 
 the mantissa) of the floating-point number. 
 If the argument is positive infinity, the result is 
 0x7f800000. 
 If the argument is negative infinity, the result is 
 0xff800000.
 
 If the argument is NaN, the result is the integer representing
 the actual NaN value.  Unlike the floatToIntBits
 method, floatToRawIntBits does not collapse all the
 bit patterns encoding a NaN to a single "canonical"
 NaN value.
 
 In all cases, the result is an integer that, when given to the
 intBitsToFloat(int) method, will produce a
 floating-point value the same as the argument to
 floatToRawIntBits.

Parameters:

value -> a floating-point number.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_floatToRawIntBits_$_$F$I"));
throw ex;
//must have a return of type int
};
var java_lang_Float_$_floatValue_$_$$F = function (arg0) {
/*implement method!*/
/*
Returns the float value of this Float
 object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_floatValue_$_$$F"));
throw ex;
//must have a return of type float
};
var java_lang_Float_$_hashCode_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns a hash code for this Float object. The
 result is the integer bit representation, exactly as produced
 by the method floatToIntBits(float), of the primitive
 float value represented by this Float
 object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_hashCode_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Float_$_intBitsToFloat_$_$I$F = function (arg0) {
/*implement method!*/
/*
Returns the float value corresponding to a given
 bit representation.
 The argument is considered to be a representation of a
 floating-point value according to the IEEE 754 floating-point
 "single format" bit layout.
 
 If the argument is 0x7f800000, the result is positive
 infinity.
 
 If the argument is 0xff800000, the result is negative
 infinity.
 
 If the argument is any value in the range
 0x7f800001 through 0x7fffffff or in
 the range 0xff800001 through
 0xffffffff, the result is a NaN.  No IEEE 754
 floating-point operation provided by Java can distinguish
 between two NaN values of the same type with different bit
 patterns.  Distinct values of NaN are only distinguishable by
 use of the Float.floatToRawIntBits method.
 
 In all other cases, let s, e, and m be three 
 values that can be computed from the argument: 
  int s = ((bits >> 31) == 0) ? 1 : -1;
 int e = ((bits >> 23) & 0xff);
 int m = (e == 0) ?
                 (bits & 0x7fffff) << 1 :
                 (bits & 0x7fffff) | 0x800000;
 
 Then the floating-point result equals the value of the mathematical 
 expression s·m·2e-150.

 Note that this method may not be able to return a
 float NaN with exactly same bit pattern as the
 int argument.  IEEE 754 distinguishes between two
 kinds of NaNs, quiet NaNs and signaling NaNs.  The
 differences between the two kinds of NaN are generally not
 visible in Java.  Arithmetic operations on signaling NaNs turn
 them into quiet NaNs with a different, but often similar, bit
 pattern.  However, on some processors merely copying a
 signaling NaN also performs that conversion.  In particular,
 copying a signaling NaN to return it to the calling method may
 perform this conversion.  So intBitsToFloat may
 not be able to return a float with a signaling NaN
 bit pattern.  Consequently, for some int values,
 floatToRawIntBits(intBitsToFloat(start)) may
 not equal start.  Moreover, which
 particular bit patterns represent signaling NaNs is platform
 dependent; although all NaN bit patterns, quiet or signaling,
 must be in the NaN range identified above.

Parameters:

bits -> an integer.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_intBitsToFloat_$_$I$F"));
throw ex;
//must have a return of type float
};
var java_lang_Float_$_intValue_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the value of this Float as an
 int (by casting to type int).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_intValue_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Float_$_isInfinite_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if this Float value is
 infinitely large in magnitude, false otherwise.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_isInfinite_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Float_$_isInfinite_$_$F$Z = function (arg0) {
/*implement method!*/
/*
Returns true if the specified number is infinitely
 large in magnitude, false otherwise.

Parameters:

v -> the value to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_isInfinite_$_$F$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Float_$_isNaN_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if this Float value is a
 Not-a-Number (NaN), false otherwise.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_isNaN_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Float_$_isNaN_$_$F$Z = function (arg0) {
/*implement method!*/
/*
Returns true if the specified number is a
 Not-a-Number (NaN) value, false otherwise.

Parameters:

v -> the value to be tested.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_isNaN_$_$F$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Float_$_longValue_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns value of this Float as a long
 (by casting to type long).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_longValue_$_$$J"));
throw ex;
//must have a return of type long
};
var java_lang_Float_$_parseFloat_$_$Ljava_lang_String$$$_$F = function (arg0) {
/*implement method!*/
/*
Returns a new float initialized to the value
 represented by the specified String, as performed
 by the valueOf method of class Float.

Parameters:

s -> the string to be parsed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_parseFloat_$_$Ljava_lang_String$$$_$F"));
throw ex;
//must have a return of type float
};
var java_lang_Float_$_shortValue_$_$$S = function (arg0) {
/*implement method!*/
/*
Returns the value of this Float as a
 short (by casting to a short).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_shortValue_$_$$S"));
throw ex;
//must have a return of type short
};
var java_lang_Float_$_toHexString_$_$F$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a hexadecimal string representation of the
 float argument. All characters mentioned below are
 ASCII characters.

 
 If the argument is NaN, the result is the string
     "NaN".
 Otherwise, the result is a string that represents the sign and 
 magnitude (absolute value) of the argument. If the sign is negative, 
 the first character of the result is '-' 
 ('\u002D'); if the sign is positive, no sign character 
 appears in the result. As for the magnitude m:

  
 If m is infinity, it is represented by the string
 "Infinity"; thus, positive infinity produces the
 result "Infinity" and negative infinity produces
 the result "-Infinity".

 If m is zero, it is represented by the string
 "0x0.0p0"; thus, negative zero produces the result
 "-0x0.0p0" and positive zero produces the result
 "0x0.0p0".

 If m is a float value with a
 normalized representation, substrings are used to represent the
 significand and exponent fields.  The significand is
 represented by the characters "0x1."
 followed by a lowercase hexadecimal representation of the rest
 of the significand as a fraction.  Trailing zeros in the
 hexadecimal representation are removed unless all the digits
 are zero, in which case a single zero is used. Next, the
 exponent is represented by "p" followed
 by a decimal string of the unbiased exponent as if produced by
 a call to Integer.toString on the
 exponent value.

 If m is a float value with a subnormal
 representation, the significand is represented by the
 characters "0x0." followed by a
 hexadecimal representation of the rest of the significand as a
 fraction.  Trailing zeros in the hexadecimal representation are
 removed. Next, the exponent is represented by
 "p-126".  Note that there must be at
 least one nonzero digit in a subnormal significand.

 
 
 

 
 Examples
 Floating-point ValueHexadecimal String
 1.0  0x1.0p0
 -1.0 -0x1.0p0
 2.0  0x1.0p1
 3.0  0x1.8p1
 0.5  0x1.0p-1
 0.25 0x1.0p-2
 Float.MAX_VALUE
     0x1.fffffep127
 Minimum Normal Value
     0x1.0p-126
 Maximum Subnormal Value
     0x0.fffffep-126
 Float.MIN_VALUE
     0x0.000002p-126

Parameters:

f -> the float to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_toHexString_$_$F$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Float_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of this Float object.
 The primitive float value represented by this object
 is converted to a String exactly as if by the method
 toString of one argument.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_toString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Float_$_toString_$_$F$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of the float
 argument. All characters mentioned below are ASCII characters.
 
 If the argument is NaN, the result is the string
 "NaN".
 Otherwise, the result is a string that represents the sign and 
     magnitude (absolute value) of the argument. If the sign is
     negative, the first character of the result is
     '-' ('\u002D'); if the sign is
     positive, no sign character appears in the result. As for
     the magnitude m:
 
 If m is infinity, it is represented by the characters 
     "Infinity"; thus, positive infinity produces
     the result "Infinity" and negative infinity
     produces the result "-Infinity".
 If m is zero, it is represented by the characters 
     "0.0"; thus, negative zero produces the result
     "-0.0" and positive zero produces the result
     "0.0".
  If m is greater than or equal to 10-3 but 
      less than 107, then it is represented as the
      integer part of m, in decimal form with no leading
      zeroes, followed by '.'
      ('\u002E'), followed by one or more
      decimal digits representing the fractional part of
      m.
  If m is less than 10-3 or greater than or
      equal to 107, then it is represented in
      so-called "computerized scientific notation." Let n
      be the unique integer such that 10n <= 
      m < 10n+1; then let a 
      be the mathematically exact quotient of m and 
      10n so that 1 <= a < 10.
      The magnitude is then represented as the integer part of
      a, as a single decimal digit, followed by
      '.' ('\u002E'), followed by
      decimal digits representing the fractional part of
      a, followed by the letter 'E'
      ('\u0045'), followed by a representation
      of n as a decimal integer, as produced by the
      method Integer.toString(int).
 
 
 How many digits must be printed for the fractional part of
 m or a? There must be at least one digit
 to represent the fractional part, and beyond that as many, but
 only as many, more digits as are needed to uniquely distinguish
 the argument value from adjacent values of type
 float. That is, suppose that x is the
 exact mathematical value represented by the decimal
 representation produced by this method for a finite nonzero
 argument f. Then f must be the float
 value nearest to x; or, if two float values are
 equally close to x, then f must be one of
 them and the least significant bit of the significand of
 f must be 0.
 
 To create localized string representations of a floating-point
 value, use subclasses of NumberFormat.

Parameters:

f -> the float to be converted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_toString_$_$F$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Float_$_valueOf_$_$F$Ljava_lang_Float$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a Float instance representing the specified
 float value.
 If a new Float instance is not required, this method
 should generally be used in preference to the constructor
 Float(float), as this method is likely to yield
 significantly better space and time performance by caching
 frequently requested values.

Parameters:

f -> a float value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_valueOf_$_$F$Ljava_lang_Float$$$_"));
throw ex;
//must have a return of type Float
};
var java_lang_Float_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Float$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a Float object holding the
 float value represented by the argument string
 s.
 
 If s is null, then a
 NullPointerException is thrown.
 
 Leading and trailing whitespace characters in s
 are ignored.  Whitespace is removed as if by the String.trim() method; that is, both ASCII space and control
 characters are removed. The rest of s should
 constitute a FloatValue as described by the lexical
 syntax rules:

 
 
 FloatValue:
 Signopt NaN
 Signopt Infinity
 Signopt FloatingPointLiteral
 Signopt HexFloatingPointLiteral
 SignedInteger
 

 

 
 HexFloatingPointLiteral:
  HexSignificand BinaryExponent FloatTypeSuffixopt
 

 

 
 HexSignificand:
 HexNumeral
 HexNumeral .
 0x HexDigitsopt 
     . HexDigits
 0X HexDigitsopt 
     . HexDigits
 

 

 
 BinaryExponent:
 BinaryExponentIndicator SignedInteger
 

 

 
 BinaryExponentIndicator:
 p
 P
 

 

 where Sign, FloatingPointLiteral,
 HexNumeral, HexDigits, SignedInteger and
 FloatTypeSuffix are as defined in the lexical structure
 sections of the of the Java Language
 Specification. If s does not have the form of
 a FloatValue, then a NumberFormatException
 is thrown. Otherwise, s is regarded as
 representing an exact decimal value in the usual
 "computerized scientific notation" or as an exact
 hexadecimal value; this exact numerical value is then
 conceptually converted to an "infinitely precise"
 binary value that is then rounded to type float
 by the usual round-to-nearest rule of IEEE 754 floating-point
 arithmetic, which includes preserving the sign of a zero
 value. Finally, a Float object representing this
 float value is returned.
 
 To interpret localized string representations of a
 floating-point value, use subclasses of NumberFormat.

 Note that trailing format specifiers, specifiers that
 determine the type of a floating-point literal
 (1.0f is a float value;
 1.0d is a double value), do
 not influence the results of this method.  In other
 words, the numerical value of the input string is converted
 directly to the target floating-point type.  In general, the
 two-step sequence of conversions, string to double
 followed by double to float, is
 not equivalent to converting a string directly to
 float.  For example, if first converted to an
 intermediate double and then to
 float, the string
 "1.00000017881393421514957253748434595763683319091796875001d"
 results in the float value
 1.0000002f; if the string is converted directly to
 float, 1.0000001f results.

 To avoid calling this method on an invalid string and having
 a NumberFormatException be thrown, the documentation
 for Double.valueOf lists a regular
 expression which can be used to screen the input.

Parameters:

s -> the string to be parsed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Float_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Float$$$_"));
throw ex;
//must have a return of type Float
};
var java_lang_Float_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$J$V(arg0, arg1);};
var java_lang_Float_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_Float_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$$V(arg0);};
var java_lang_Float_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_Float_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notify_$_$$V(arg0);};
var java_lang_Float_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notifyAll_$_$$V(arg0);};
java_lang_Float.prototype._$$$init$$$__$_$D$V = java_lang_Float_$__$$$init$$$__$_$D$V;
java_lang_Float.prototype._$$$init$$$__$_$F$V = java_lang_Float_$__$$$init$$$__$_$F$V;
java_lang_Float.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_lang_Float_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_lang_Float.prototype.byteValue_$_$$B = java_lang_Float_$_byteValue_$_$$B;
java_lang_Float.prototype.compareTo_$_$Ljava_lang_Float$$$_$I = java_lang_Float_$_compareTo_$_$Ljava_lang_Float$$$_$I;
java_lang_Float.prototype.compareTo_$_$Ljava_lang_Object$$$_$I = java_lang_Float_$_compareTo_$_$Ljava_lang_Object$$$_$I;
java_lang_Float.compare_$_$FF$I = java_lang_Float_$_compare_$_$FF$I;
java_lang_Float.prototype.doubleValue_$_$$D = java_lang_Float_$_doubleValue_$_$$D;
java_lang_Float.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_Float_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_Float.floatToIntBits_$_$F$I = java_lang_Float_$_floatToIntBits_$_$F$I;
java_lang_Float.floatToRawIntBits_$_$F$I = java_lang_Float_$_floatToRawIntBits_$_$F$I;
java_lang_Float.prototype.floatValue_$_$$F = java_lang_Float_$_floatValue_$_$$F;
java_lang_Float.prototype.hashCode_$_$$I = java_lang_Float_$_hashCode_$_$$I;
java_lang_Float.intBitsToFloat_$_$I$F = java_lang_Float_$_intBitsToFloat_$_$I$F;
java_lang_Float.prototype.intValue_$_$$I = java_lang_Float_$_intValue_$_$$I;
java_lang_Float.prototype.isInfinite_$_$$Z = java_lang_Float_$_isInfinite_$_$$Z;
java_lang_Float.isInfinite_$_$F$Z = java_lang_Float_$_isInfinite_$_$F$Z;
java_lang_Float.prototype.isNaN_$_$$Z = java_lang_Float_$_isNaN_$_$$Z;
java_lang_Float.isNaN_$_$F$Z = java_lang_Float_$_isNaN_$_$F$Z;
java_lang_Float.prototype.longValue_$_$$J = java_lang_Float_$_longValue_$_$$J;
java_lang_Float.parseFloat_$_$Ljava_lang_String$$$_$F = java_lang_Float_$_parseFloat_$_$Ljava_lang_String$$$_$F;
java_lang_Float.prototype.shortValue_$_$$S = java_lang_Float_$_shortValue_$_$$S;
java_lang_Float.toHexString_$_$F$Ljava_lang_String$$$_ = java_lang_Float_$_toHexString_$_$F$Ljava_lang_String$$$_;
java_lang_Float.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_Float_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_Float.toString_$_$F$Ljava_lang_String$$$_ = java_lang_Float_$_toString_$_$F$Ljava_lang_String$$$_;
java_lang_Float.valueOf_$_$F$Ljava_lang_Float$$$_ = java_lang_Float_$_valueOf_$_$F$Ljava_lang_Float$$$_;
java_lang_Float.valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Float$$$_ = java_lang_Float_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Float$$$_;
java_lang_Float.prototype.wait_$_$J$V = java_lang_Float_$_wait_$_$J$V;
java_lang_Float.prototype.wait_$_$JI$V = java_lang_Float_$_wait_$_$JI$V;
java_lang_Float.prototype.wait_$_$$V = java_lang_Float_$_wait_$_$$V;
java_lang_Float.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_Float_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_Float.prototype.notify_$_$$V = java_lang_Float_$_notify_$_$$V;
java_lang_Float.prototype.notifyAll_$_$$V = java_lang_Float_$_notifyAll_$_$$V;
