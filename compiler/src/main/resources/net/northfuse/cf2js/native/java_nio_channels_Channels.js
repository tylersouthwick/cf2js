/** @constructor */
function java_nio_channels_Channels() {}
var java_nio_channels_Channels_$_access$000_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_nio_ByteBuffer$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_nio_channels_Channels_$_access$000_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_nio_ByteBuffer$$$_$V"));
throw ex;
//must have a return of type void
};
var java_nio_channels_Channels_$_newChannel_$_$Ljava_io_InputStream$$$_$Ljava_nio_channels_ReadableByteChannel$$$_ = function (arg0) {
/*implement method!*/
/*
Constructs a channel that reads bytes from the given stream.

  The resulting channel will not be buffered; it will simply redirect
 its I/O operations to the given stream.  Closing the channel will in
 turn cause the stream to be closed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_nio_channels_Channels_$_newChannel_$_$Ljava_io_InputStream$$$_$Ljava_nio_channels_ReadableByteChannel$$$_"));
throw ex;
//must have a return of type ReadableByteChannel
};
var java_nio_channels_Channels_$_newChannel_$_$Ljava_io_OutputStream$$$_$Ljava_nio_channels_WritableByteChannel$$$_ = function (arg0) {
/*implement method!*/
/*
Constructs a channel that writes bytes to the given stream.

  The resulting channel will not be buffered; it will simply redirect
 its I/O operations to the given stream.  Closing the channel will in
 turn cause the stream to be closed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_nio_channels_Channels_$_newChannel_$_$Ljava_io_OutputStream$$$_$Ljava_nio_channels_WritableByteChannel$$$_"));
throw ex;
//must have a return of type WritableByteChannel
};
var java_nio_channels_Channels_$_newInputStream_$_$Ljava_nio_channels_ReadableByteChannel$$$_$Ljava_io_InputStream$$$_ = function (arg0) {
/*implement method!*/
/*
Constructs a stream that reads bytes from the given channel.

  The read methods of the resulting stream will throw an
 IllegalBlockingModeException if invoked while the underlying
 channel is in non-blocking mode.  The stream will not be buffered, and
 it will not support the mark or reset methods.  The stream will be safe for access by
 multiple concurrent threads.  Closing the stream will in turn cause the
 channel to be closed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_nio_channels_Channels_$_newInputStream_$_$Ljava_nio_channels_ReadableByteChannel$$$_$Ljava_io_InputStream$$$_"));
throw ex;
//must have a return of type InputStream
};
var java_nio_channels_Channels_$_newOutputStream_$_$Ljava_nio_channels_WritableByteChannel$$$_$Ljava_io_OutputStream$$$_ = function (arg0) {
/*implement method!*/
/*
Constructs a stream that writes bytes to the given channel.

  The write methods of the resulting stream will throw an
 IllegalBlockingModeException if invoked while the underlying
 channel is in non-blocking mode.  The stream will not be buffered.  The
 stream will be safe for access by multiple concurrent threads.  Closing
 the stream will in turn cause the channel to be closed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_nio_channels_Channels_$_newOutputStream_$_$Ljava_nio_channels_WritableByteChannel$$$_$Ljava_io_OutputStream$$$_"));
throw ex;
//must have a return of type OutputStream
};
var java_nio_channels_Channels_$_newReader_$_$Ljava_nio_channels_ReadableByteChannel$$$_Ljava_lang_String$$$_$Ljava_io_Reader$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Constructs a reader that decodes bytes from the given channel according
 to the named charset.

  An invocation of this method of the form

  Channels.newReader(ch, csname)

 behaves in exactly the same way as the expression

  Channels.newReader(ch,
                    Charset.forName(csName)
                        .newDecoder(),
                    -1);

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_nio_channels_Channels_$_newReader_$_$Ljava_nio_channels_ReadableByteChannel$$$_Ljava_lang_String$$$_$Ljava_io_Reader$$$_"));
throw ex;
//must have a return of type Reader
};
var java_nio_channels_Channels_$_newReader_$_$Ljava_nio_channels_ReadableByteChannel$$$_Ljava_nio_charset_CharsetDecoder$$$_I$Ljava_io_Reader$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Constructs a reader that decodes bytes from the given channel using the
 given decoder.

  The resulting stream will contain an internal input buffer of at
 least minBufferCap bytes.  The stream's read methods
 will, as needed, fill the buffer by reading bytes from the underlying
 channel; if the channel is in non-blocking mode when bytes are to be
 read then an IllegalBlockingModeException will be thrown.  The
 resulting stream will not otherwise be buffered, and it will not support
 the mark or reset methods.
 Closing the stream will in turn cause the channel to be closed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_nio_channels_Channels_$_newReader_$_$Ljava_nio_channels_ReadableByteChannel$$$_Ljava_nio_charset_CharsetDecoder$$$_I$Ljava_io_Reader$$$_"));
throw ex;
//must have a return of type Reader
};
var java_nio_channels_Channels_$_newWriter_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_lang_String$$$_$Ljava_io_Writer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Constructs a writer that encodes characters according to the named
 charset and writes the resulting bytes to the given channel.

  An invocation of this method of the form

  Channels.newWriter(ch, csname)

 behaves in exactly the same way as the expression

  Channels.newWriter(ch,
                    Charset.forName(csName)
                        .newEncoder(),
                    -1);

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_nio_channels_Channels_$_newWriter_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_lang_String$$$_$Ljava_io_Writer$$$_"));
throw ex;
//must have a return of type Writer
};
var java_nio_channels_Channels_$_newWriter_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_nio_charset_CharsetEncoder$$$_I$Ljava_io_Writer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Constructs a writer that encodes characters using the given encoder and
 writes the resulting bytes to the given channel.

  The resulting stream will contain an internal output buffer of at
 least minBufferCap bytes.  The stream's write methods
 will, as needed, flush the buffer by writing bytes to the underlying
 channel; if the channel is in non-blocking mode when bytes are to be
 written then an IllegalBlockingModeException will be thrown.
 The resulting stream will not otherwise be buffered.  Closing the stream
 will in turn cause the channel to be closed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_nio_channels_Channels_$_newWriter_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_nio_charset_CharsetEncoder$$$_I$Ljava_io_Writer$$$_"));
throw ex;
//must have a return of type Writer
};
var java_nio_channels_Channels_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_nio_channels_Channels_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_nio_channels_Channels_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_nio_channels_Channels_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_nio_channels_Channels_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_nio_channels_Channels_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var java_nio_channels_Channels_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_nio_channels_Channels_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_nio_channels_Channels_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_nio_channels_Channels.access$000_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_nio_ByteBuffer$$$_$V = java_nio_channels_Channels_$_access$000_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_nio_ByteBuffer$$$_$V;
java_nio_channels_Channels.newChannel_$_$Ljava_io_InputStream$$$_$Ljava_nio_channels_ReadableByteChannel$$$_ = java_nio_channels_Channels_$_newChannel_$_$Ljava_io_InputStream$$$_$Ljava_nio_channels_ReadableByteChannel$$$_;
java_nio_channels_Channels.newChannel_$_$Ljava_io_OutputStream$$$_$Ljava_nio_channels_WritableByteChannel$$$_ = java_nio_channels_Channels_$_newChannel_$_$Ljava_io_OutputStream$$$_$Ljava_nio_channels_WritableByteChannel$$$_;
java_nio_channels_Channels.newInputStream_$_$Ljava_nio_channels_ReadableByteChannel$$$_$Ljava_io_InputStream$$$_ = java_nio_channels_Channels_$_newInputStream_$_$Ljava_nio_channels_ReadableByteChannel$$$_$Ljava_io_InputStream$$$_;
java_nio_channels_Channels.newOutputStream_$_$Ljava_nio_channels_WritableByteChannel$$$_$Ljava_io_OutputStream$$$_ = java_nio_channels_Channels_$_newOutputStream_$_$Ljava_nio_channels_WritableByteChannel$$$_$Ljava_io_OutputStream$$$_;
java_nio_channels_Channels.newReader_$_$Ljava_nio_channels_ReadableByteChannel$$$_Ljava_lang_String$$$_$Ljava_io_Reader$$$_ = java_nio_channels_Channels_$_newReader_$_$Ljava_nio_channels_ReadableByteChannel$$$_Ljava_lang_String$$$_$Ljava_io_Reader$$$_;
java_nio_channels_Channels.newReader_$_$Ljava_nio_channels_ReadableByteChannel$$$_Ljava_nio_charset_CharsetDecoder$$$_I$Ljava_io_Reader$$$_ = java_nio_channels_Channels_$_newReader_$_$Ljava_nio_channels_ReadableByteChannel$$$_Ljava_nio_charset_CharsetDecoder$$$_I$Ljava_io_Reader$$$_;
java_nio_channels_Channels.newWriter_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_lang_String$$$_$Ljava_io_Writer$$$_ = java_nio_channels_Channels_$_newWriter_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_lang_String$$$_$Ljava_io_Writer$$$_;
java_nio_channels_Channels.newWriter_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_nio_charset_CharsetEncoder$$$_I$Ljava_io_Writer$$$_ = java_nio_channels_Channels_$_newWriter_$_$Ljava_nio_channels_WritableByteChannel$$$_Ljava_nio_charset_CharsetEncoder$$$_I$Ljava_io_Writer$$$_;
java_nio_channels_Channels.prototype.wait_$_$J$V = java_nio_channels_Channels_$_wait_$_$J$V;
java_nio_channels_Channels.prototype.wait_$_$JI$V = java_nio_channels_Channels_$_wait_$_$JI$V;
java_nio_channels_Channels.prototype.wait_$_$$V = java_nio_channels_Channels_$_wait_$_$$V;
java_nio_channels_Channels.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_nio_channels_Channels_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_nio_channels_Channels.prototype.toString_$_$$Ljava_lang_String$$$_ = java_nio_channels_Channels_$_toString_$_$$Ljava_lang_String$$$_;
java_nio_channels_Channels.prototype.hashCode_$_$$I = java_nio_channels_Channels_$_hashCode_$_$$I;
java_nio_channels_Channels.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_nio_channels_Channels_$_getClass_$_$$Ljava_lang_Class$$$_;
java_nio_channels_Channels.prototype.notify_$_$$V = java_nio_channels_Channels_$_notify_$_$$V;
java_nio_channels_Channels.prototype.notifyAll_$_$$V = java_nio_channels_Channels_$_notifyAll_$_$$V;
