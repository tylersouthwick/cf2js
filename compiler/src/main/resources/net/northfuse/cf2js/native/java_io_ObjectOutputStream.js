/** @constructor */
function java_io_ObjectOutputStream() {}
var java_io_ObjectOutputStream_$__$$$init$$$__$_$Ljava_io_OutputStream$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Creates an ObjectOutputStream that writes to the specified OutputStream.
 This constructor writes the serialization stream header to the
 underlying stream; callers may wish to flush the stream immediately to
 ensure that constructors for receiving ObjectInputStreams will not block
 when reading the header.

 If a security manager is installed, this constructor will check for
 the "enableSubclassImplementation" SerializablePermission when invoked
 directly or indirectly by the constructor of a subclass which overrides
 the ObjectOutputStream.putFields or ObjectOutputStream.writeUnshared
 methods.


Parameters:out - output stream to write to
Throws:
IOException - if an I/O error occurs while writing stream header
SecurityException - if untrusted subclass illegally overrides
                security-sensitive methods
NullPointerException - if out is nullSince:
  1.4
See Also:ObjectOutputStream(), 
putFields(), 
ObjectInputStream.ObjectInputStream(InputStream)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$__$$$init$$$__$_$Ljava_io_OutputStream$$$_$V"));
throw ex;
};
var java_io_ObjectOutputStream_$_access$000_$_$Ljava_io_ObjectOutputStream$$$_$Ljava_io_ObjectOutputStream$BlockDataOutputStream$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_access$000_$_$Ljava_io_ObjectOutputStream$$$_$Ljava_io_ObjectOutputStream$BlockDataOutputStream$$$_"));
throw ex;
//must have a return of type BlockDataOutputStream
};
var java_io_ObjectOutputStream_$_access$100_$_$$Z = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_access$100_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_io_ObjectOutputStream_$_access$200_$_$Ljava_io_ObjectOutputStream$$$_$Ljava_io_ObjectOutputStream$DebugTraceInfoStack$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_access$200_$_$Ljava_io_ObjectOutputStream$$$_$Ljava_io_ObjectOutputStream$DebugTraceInfoStack$$$_"));
throw ex;
//must have a return of type DebugTraceInfoStack
};
var java_io_ObjectOutputStream_$_access$300_$_$Ljava_io_ObjectOutputStream$$$_Ljava_lang_Object$$$_Z$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_access$300_$_$Ljava_io_ObjectOutputStream$$$_Ljava_lang_Object$$$_Z$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_access$400_$_$_$$$$$_FI_$$$$$_BII$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_access$400_$_$_$$$$$_FI_$$$$$_BII$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_access$500_$_$_$$$$$_DI_$$$$$_BII$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_access$500_$_$_$$$$$_DI_$$$$$_BII$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_annotateClass_$_$Ljava_lang_Class$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Subclasses may implement this method to allow class data to be stored in
 the stream. By default this method does nothing.  The corresponding
 method in ObjectInputStream is resolveClass.  This method is called
 exactly once for each unique class in the stream.  The class name and
 signature will have already been written to the stream.  This method may
 make free use of the ObjectOutputStream to save any representation of
 the class it deems suitable (for example, the bytes of the class file).
 The resolveClass method in the corresponding subclass of
 ObjectInputStream must read and use any data or objects written by
 annotateClass.

Parameters:

cl -> the class to annotate custom data for

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_annotateClass_$_$Ljava_lang_Class$$$_$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_annotateProxyClass_$_$Ljava_lang_Class$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Subclasses may implement this method to store custom data in the stream
 along with descriptors for dynamic proxy classes.

 This method is called exactly once for each unique proxy class
 descriptor in the stream.  The default implementation of this method in
 ObjectOutputStream does nothing.

 The corresponding method in ObjectInputStream is
 resolveProxyClass.  For a given subclass of
 ObjectOutputStream that overrides this method, the
 resolveProxyClass method in the corresponding subclass of
 ObjectInputStream must read any data or objects written by
 annotateProxyClass.

Parameters:

cl -> the proxy class to annotate custom data for

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_annotateProxyClass_$_$Ljava_lang_Class$$$_$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_close_$_$$V = function (arg0) {
/*implement method!*/
/*
Closes the stream. This method must be called to release any resources
 associated with the stream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_close_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_defaultWriteObject_$_$$V = function (arg0) {
/*implement method!*/
/*
Write the non-static and non-transient fields of the current class to
 this stream.  This may only be called from the writeObject method of the
 class being serialized. It will throw the NotActiveException if it is
 called otherwise.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_defaultWriteObject_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_drain_$_$$V = function (arg0) {
/*implement method!*/
/*
Drain any buffered data in ObjectOutputStream.  Similar to flush but
 does not propagate the flush to the underlying stream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_drain_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_enableReplaceObject_$_$Z$Z = function (arg0, arg1) {
/*implement method!*/
/*
Enable the stream to do replacement of objects in the stream.  When
 enabled, the replaceObject method is called for every object being
 serialized.

 If enable is true, and there is a security manager
 installed, this method first calls the security manager's
 checkPermission method with a
 SerializablePermission("enableSubstitution") permission to
 ensure it's ok to enable the stream to do replacement of objects in the
 stream.

Parameters:

enable -> boolean parameter to enable replacement of objects

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_enableReplaceObject_$_$Z$Z"));
throw ex;
//must have a return of type boolean
};
var java_io_ObjectOutputStream_$_flush_$_$$V = function (arg0) {
/*implement method!*/
/*
Flushes the stream. This will write any buffered output bytes and flush
 through to the underlying stream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_flush_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_getProtocolVersion_$_$$I = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_getProtocolVersion_$_$$I"));
throw ex;
//must have a return of type int
};
var java_io_ObjectOutputStream_$_putFields_$_$$Ljava_io_ObjectOutputStream$PutField$$$_ = function (arg0) {
/*implement method!*/
/*
Retrieve the object used to buffer persistent fields to be written to
 the stream.  The fields will be written to the stream when writeFields
 method is called.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_putFields_$_$$Ljava_io_ObjectOutputStream$PutField$$$_"));
throw ex;
//must have a return of type PutField
};
var java_io_ObjectOutputStream_$_replaceObject_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
This method will allow trusted subclasses of ObjectOutputStream to
 substitute one object for another during serialization. Replacing
 objects is disabled until enableReplaceObject is called. The
 enableReplaceObject method checks that the stream requesting to do
 replacement can be trusted.  The first occurrence of each object written
 into the serialization stream is passed to replaceObject.  Subsequent
 references to the object are replaced by the object returned by the
 original call to replaceObject.  To ensure that the private state of
 objects is not unintentionally exposed, only trusted streams may use
 replaceObject.
 
 The ObjectOutputStream.writeObject method takes a parameter of type
 Object (as opposed to type Serializable) to allow for cases where
 non-serializable objects are replaced by serializable ones.
 
 When a subclass is replacing objects it must insure that either a
 complementary substitution must be made during deserialization or that
 the substituted object is compatible with every field where the
 reference will be stored.  Objects whose type is not a subclass of the
 type of the field or array element abort the serialization by raising an
 exception and the object is not be stored.

 This method is called only once when each object is first
 encountered.  All subsequent references to the object will be redirected
 to the new object. This method should return the object to be
 substituted or the original object.

 Null can be returned as the object to be substituted, but may cause
 NullReferenceException in classes that contain references to the
 original object since they may be expecting an object instead of
 null.

Parameters:

obj -> the object to be replaced

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_replaceObject_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object
};
var java_io_ObjectOutputStream_$_reset_$_$$V = function (arg0) {
/*implement method!*/
/*
Reset will disregard the state of any objects already written to the
 stream.  The state is reset to be the same as a new ObjectOutputStream.
 The current point in the stream is marked as reset so the corresponding
 ObjectInputStream will be reset at the same point.  Objects previously
 written to the stream will not be refered to as already being in the
 stream.  They will be written to the stream again.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_reset_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_useProtocolVersion_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Specify stream protocol version to use when writing the stream.

 This routine provides a hook to enable the current version of
 Serialization to write in a format that is backwards compatible to a
 previous version of the stream format.

 Every effort will be made to avoid introducing additional
 backwards incompatibilities; however, sometimes there is no
 other alternative.

Parameters:

version -> use ProtocolVersion from java.io.ObjectStreamConstants.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_useProtocolVersion_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeBoolean_$_$Z$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes a boolean.

Parameters:

val -> the boolean to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeBoolean_$_$Z$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeByte_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes an 8 bit byte.

Parameters:

val -> the byte value to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeByte_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeBytes_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes a String as a sequence of bytes.

Parameters:

str -> the String of bytes to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeBytes_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeChar_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes a 16 bit char.

Parameters:

val -> the char value to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeChar_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeChars_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes a String as a sequence of chars.

Parameters:

str -> the String of chars to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeChars_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeClassDescriptor_$_$Ljava_io_ObjectStreamClass$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Write the specified class descriptor to the ObjectOutputStream.  Class
 descriptors are used to identify the classes of objects written to the
 stream.  Subclasses of ObjectOutputStream may override this method to
 customize the way in which class descriptors are written to the
 serialization stream.  The corresponding method in ObjectInputStream,
 readClassDescriptor, should then be overridden to
 reconstitute the class descriptor from its custom stream representation.
 By default, this method writes class descriptors according to the format
 defined in the Object Serialization specification.
 
 Note that this method will only be called if the ObjectOutputStream
 is not using the old serialization stream format (set by calling
 ObjectOutputStream's useProtocolVersion method).  If this
 serialization stream is using the old format
 (PROTOCOL_VERSION_1), the class descriptor will be written
 internally in a manner that cannot be overridden or customized.

Parameters:

desc -> class descriptor to write to the stream

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeClassDescriptor_$_$Ljava_io_ObjectStreamClass$$$_$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeDouble_$_$D$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes a 64 bit double.

Parameters:

val -> the double value to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeDouble_$_$D$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeFields_$_$$V = function (arg0) {
/*implement method!*/
/*
Write the buffered fields to the stream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeFields_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeFloat_$_$F$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes a 32 bit float.

Parameters:

val -> the float value to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeFloat_$_$F$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeInt_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes a 32 bit int.

Parameters:

val -> the integer value to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeInt_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeLong_$_$J$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes a 64 bit long.

Parameters:

val -> the long value to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeLong_$_$J$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeObjectOverride_$_$Ljava_lang_Object$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Method used by subclasses to override the default writeObject method.
 This method is called by trusted subclasses of ObjectInputStream that
 constructed ObjectInputStream using the protected no-arg constructor.
 The subclass is expected to provide an override method with the modifier
 "final".

Parameters:

obj -> object to be written to the underlying stream

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeObjectOverride_$_$Ljava_lang_Object$$$_$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeObject_$_$Ljava_lang_Object$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Write the specified object to the ObjectOutputStream.  The class of the
 object, the signature of the class, and the values of the non-transient
 and non-static fields of the class and all of its supertypes are
 written.  Default serialization for a class can be overridden using the
 writeObject and the readObject methods.  Objects referenced by this
 object are written transitively so that a complete equivalent graph of
 objects can be reconstructed by an ObjectInputStream.

 Exceptions are thrown for problems with the OutputStream and for
 classes that should not be serialized.  All exceptions are fatal to the
 OutputStream, which is left in an indeterminate state, and it is up to
 the caller to ignore or recover the stream state.

Parameters:

obj -> the object to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeObject_$_$Ljava_lang_Object$$$_$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeShort_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes a 16 bit short.

Parameters:

val -> the short value to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeShort_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeStreamHeader_$_$$V = function (arg0) {
/*implement method!*/
/*
The writeStreamHeader method is provided so subclasses can append or
 prepend their own header to the stream.  It writes the magic number and
 version to the stream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeStreamHeader_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeTypeString_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeTypeString_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeUTF_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Primitive data write of this String in 
 modified UTF-8
 format.  Note that there is a
 significant difference between writing a String into the stream as
 primitive data or as an Object. A String instance written by writeObject
 is written into the stream as a String initially. Future writeObject()
 calls write references to the string into the stream.

Parameters:

str -> the String to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeUTF_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_writeUnshared_$_$Ljava_lang_Object$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes an "unshared" object to the ObjectOutputStream.  This method is
 identical to writeObject, except that it always writes the given object
 as a new, unique object in the stream (as opposed to a back-reference
 pointing to a previously serialized instance).  Specifically:
 
   An object written via writeUnshared is always serialized in the
       same manner as a newly appearing object (an object that has not
       been written to the stream yet), regardless of whether or not the
       object has been written previously.

   If writeObject is used to write an object that has been previously
       written with writeUnshared, the previous writeUnshared operation
       is treated as if it were a write of a separate object.  In other
       words, ObjectOutputStream will never generate back-references to
       object data written by calls to writeUnshared.
 
 While writing an object via writeUnshared does not in itself guarantee a
 unique reference to the object when it is deserialized, it allows a
 single object to be defined multiple times in a stream, so that multiple
 calls to readUnshared by the receiver will not conflict.  Note that the
 rules described above only apply to the base-level object written with
 writeUnshared, and not to any transitively referenced sub-objects in the
 object graph to be serialized.

 ObjectOutputStream subclasses which override this method can only be
 constructed in security contexts possessing the
 "enableSubclassImplementation" SerializablePermission; any attempt to
 instantiate such a subclass without this permission will cause a
 SecurityException to be thrown.

Parameters:

obj -> object to write to stream

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_writeUnshared_$_$Ljava_lang_Object$$$_$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_write_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes a byte. This method will block until the byte is actually
 written.

Parameters:

val -> the byte to be written to the stream

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_write_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_write_$_$_$$$$$_B$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes an array of bytes. This method will block until the bytes are
 actually written.

Parameters:

buf -> the data to be written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_write_$_$_$$$$$_B$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_write_$_$_$$$$$_BII$V = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Writes a sub array of bytes.

Parameters:

buf -> the data to be written

off -> the start offset in the data

len -> the number of bytes that are written

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectOutputStream_$_write_$_$_$$$$$_BII$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectOutputStream_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_io_OutputStream_$_wait_$_$J$V(arg0, arg1);};
var java_io_ObjectOutputStream_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_io_OutputStream_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_io_ObjectOutputStream_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_wait_$_$$V(arg0);};
var java_io_ObjectOutputStream_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_io_OutputStream_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_io_ObjectOutputStream_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_io_ObjectOutputStream_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_hashCode_$_$$I(arg0);};
var java_io_ObjectOutputStream_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_io_ObjectOutputStream_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_notify_$_$$V(arg0);};
var java_io_ObjectOutputStream_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_notifyAll_$_$$V(arg0);};
java_io_ObjectOutputStream.prototype._$$$init$$$__$_$Ljava_io_OutputStream$$$_$V = java_io_ObjectOutputStream_$__$$$init$$$__$_$Ljava_io_OutputStream$$$_$V;
java_io_ObjectOutputStream.access$000_$_$Ljava_io_ObjectOutputStream$$$_$Ljava_io_ObjectOutputStream$BlockDataOutputStream$$$_ = java_io_ObjectOutputStream_$_access$000_$_$Ljava_io_ObjectOutputStream$$$_$Ljava_io_ObjectOutputStream$BlockDataOutputStream$$$_;
java_io_ObjectOutputStream.access$100_$_$$Z = java_io_ObjectOutputStream_$_access$100_$_$$Z;
java_io_ObjectOutputStream.access$200_$_$Ljava_io_ObjectOutputStream$$$_$Ljava_io_ObjectOutputStream$DebugTraceInfoStack$$$_ = java_io_ObjectOutputStream_$_access$200_$_$Ljava_io_ObjectOutputStream$$$_$Ljava_io_ObjectOutputStream$DebugTraceInfoStack$$$_;
java_io_ObjectOutputStream.access$300_$_$Ljava_io_ObjectOutputStream$$$_Ljava_lang_Object$$$_Z$V = java_io_ObjectOutputStream_$_access$300_$_$Ljava_io_ObjectOutputStream$$$_Ljava_lang_Object$$$_Z$V;
java_io_ObjectOutputStream.access$400_$_$_$$$$$_FI_$$$$$_BII$V = java_io_ObjectOutputStream_$_access$400_$_$_$$$$$_FI_$$$$$_BII$V;
java_io_ObjectOutputStream.access$500_$_$_$$$$$_DI_$$$$$_BII$V = java_io_ObjectOutputStream_$_access$500_$_$_$$$$$_DI_$$$$$_BII$V;
java_io_ObjectOutputStream.prototype.annotateClass_$_$Ljava_lang_Class$$$_$V = java_io_ObjectOutputStream_$_annotateClass_$_$Ljava_lang_Class$$$_$V;
java_io_ObjectOutputStream.prototype.annotateProxyClass_$_$Ljava_lang_Class$$$_$V = java_io_ObjectOutputStream_$_annotateProxyClass_$_$Ljava_lang_Class$$$_$V;
java_io_ObjectOutputStream.prototype.close_$_$$V = java_io_ObjectOutputStream_$_close_$_$$V;
java_io_ObjectOutputStream.prototype.defaultWriteObject_$_$$V = java_io_ObjectOutputStream_$_defaultWriteObject_$_$$V;
java_io_ObjectOutputStream.prototype.drain_$_$$V = java_io_ObjectOutputStream_$_drain_$_$$V;
java_io_ObjectOutputStream.prototype.enableReplaceObject_$_$Z$Z = java_io_ObjectOutputStream_$_enableReplaceObject_$_$Z$Z;
java_io_ObjectOutputStream.prototype.flush_$_$$V = java_io_ObjectOutputStream_$_flush_$_$$V;
java_io_ObjectOutputStream.prototype.getProtocolVersion_$_$$I = java_io_ObjectOutputStream_$_getProtocolVersion_$_$$I;
java_io_ObjectOutputStream.prototype.putFields_$_$$Ljava_io_ObjectOutputStream$PutField$$$_ = java_io_ObjectOutputStream_$_putFields_$_$$Ljava_io_ObjectOutputStream$PutField$$$_;
java_io_ObjectOutputStream.prototype.replaceObject_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_ = java_io_ObjectOutputStream_$_replaceObject_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_;
java_io_ObjectOutputStream.prototype.reset_$_$$V = java_io_ObjectOutputStream_$_reset_$_$$V;
java_io_ObjectOutputStream.prototype.useProtocolVersion_$_$I$V = java_io_ObjectOutputStream_$_useProtocolVersion_$_$I$V;
java_io_ObjectOutputStream.prototype.writeBoolean_$_$Z$V = java_io_ObjectOutputStream_$_writeBoolean_$_$Z$V;
java_io_ObjectOutputStream.prototype.writeByte_$_$I$V = java_io_ObjectOutputStream_$_writeByte_$_$I$V;
java_io_ObjectOutputStream.prototype.writeBytes_$_$Ljava_lang_String$$$_$V = java_io_ObjectOutputStream_$_writeBytes_$_$Ljava_lang_String$$$_$V;
java_io_ObjectOutputStream.prototype.writeChar_$_$I$V = java_io_ObjectOutputStream_$_writeChar_$_$I$V;
java_io_ObjectOutputStream.prototype.writeChars_$_$Ljava_lang_String$$$_$V = java_io_ObjectOutputStream_$_writeChars_$_$Ljava_lang_String$$$_$V;
java_io_ObjectOutputStream.prototype.writeClassDescriptor_$_$Ljava_io_ObjectStreamClass$$$_$V = java_io_ObjectOutputStream_$_writeClassDescriptor_$_$Ljava_io_ObjectStreamClass$$$_$V;
java_io_ObjectOutputStream.prototype.writeDouble_$_$D$V = java_io_ObjectOutputStream_$_writeDouble_$_$D$V;
java_io_ObjectOutputStream.prototype.writeFields_$_$$V = java_io_ObjectOutputStream_$_writeFields_$_$$V;
java_io_ObjectOutputStream.prototype.writeFloat_$_$F$V = java_io_ObjectOutputStream_$_writeFloat_$_$F$V;
java_io_ObjectOutputStream.prototype.writeInt_$_$I$V = java_io_ObjectOutputStream_$_writeInt_$_$I$V;
java_io_ObjectOutputStream.prototype.writeLong_$_$J$V = java_io_ObjectOutputStream_$_writeLong_$_$J$V;
java_io_ObjectOutputStream.prototype.writeObjectOverride_$_$Ljava_lang_Object$$$_$V = java_io_ObjectOutputStream_$_writeObjectOverride_$_$Ljava_lang_Object$$$_$V;
java_io_ObjectOutputStream.prototype.writeObject_$_$Ljava_lang_Object$$$_$V = java_io_ObjectOutputStream_$_writeObject_$_$Ljava_lang_Object$$$_$V;
java_io_ObjectOutputStream.prototype.writeShort_$_$I$V = java_io_ObjectOutputStream_$_writeShort_$_$I$V;
java_io_ObjectOutputStream.prototype.writeStreamHeader_$_$$V = java_io_ObjectOutputStream_$_writeStreamHeader_$_$$V;
java_io_ObjectOutputStream.prototype.writeTypeString_$_$Ljava_lang_String$$$_$V = java_io_ObjectOutputStream_$_writeTypeString_$_$Ljava_lang_String$$$_$V;
java_io_ObjectOutputStream.prototype.writeUTF_$_$Ljava_lang_String$$$_$V = java_io_ObjectOutputStream_$_writeUTF_$_$Ljava_lang_String$$$_$V;
java_io_ObjectOutputStream.prototype.writeUnshared_$_$Ljava_lang_Object$$$_$V = java_io_ObjectOutputStream_$_writeUnshared_$_$Ljava_lang_Object$$$_$V;
java_io_ObjectOutputStream.prototype.write_$_$I$V = java_io_ObjectOutputStream_$_write_$_$I$V;
java_io_ObjectOutputStream.prototype.write_$_$_$$$$$_B$V = java_io_ObjectOutputStream_$_write_$_$_$$$$$_B$V;
java_io_ObjectOutputStream.prototype.write_$_$_$$$$$_BII$V = java_io_ObjectOutputStream_$_write_$_$_$$$$$_BII$V;
java_io_ObjectOutputStream.prototype.wait_$_$J$V = java_io_ObjectOutputStream_$_wait_$_$J$V;
java_io_ObjectOutputStream.prototype.wait_$_$JI$V = java_io_ObjectOutputStream_$_wait_$_$JI$V;
java_io_ObjectOutputStream.prototype.wait_$_$$V = java_io_ObjectOutputStream_$_wait_$_$$V;
java_io_ObjectOutputStream.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_io_ObjectOutputStream_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_io_ObjectOutputStream.prototype.toString_$_$$Ljava_lang_String$$$_ = java_io_ObjectOutputStream_$_toString_$_$$Ljava_lang_String$$$_;
java_io_ObjectOutputStream.prototype.hashCode_$_$$I = java_io_ObjectOutputStream_$_hashCode_$_$$I;
java_io_ObjectOutputStream.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_io_ObjectOutputStream_$_getClass_$_$$Ljava_lang_Class$$$_;
java_io_ObjectOutputStream.prototype.notify_$_$$V = java_io_ObjectOutputStream_$_notify_$_$$V;
java_io_ObjectOutputStream.prototype.notifyAll_$_$$V = java_io_ObjectOutputStream_$_notifyAll_$_$$V;
