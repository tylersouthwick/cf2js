/** @constructor */
function java_lang_StringBuffer() {}
var java_lang_StringBuffer_$__$$$init$$$__$_$$V = function (arg0) {
/*implement constructor!*/
/*
Constructs a string buffer with no characters in it and an 
 initial capacity of 16 characters.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$__$$$init$$$__$_$$V"));
throw ex;
};
var java_lang_StringBuffer_$__$$$init$$$__$_$I$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a string buffer with no characters in it and 
 the specified initial capacity.


Parameters:capacity - the initial capacity.
Throws:
NegativeArraySizeException - if the capacity
               argument is less than 0.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$__$$$init$$$__$_$I$V"));
throw ex;
};
var java_lang_StringBuffer_$__$$$init$$$__$_$Ljava_lang_CharSequence$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a string buffer that contains the same characters
 as the specified CharSequence. The initial capacity of
 the string buffer is 16 plus the length of the
 CharSequence argument.
 
 If the length of the specified CharSequence is
 less than or equal to zero, then an empty buffer of capacity
 16 is returned.


Parameters:seq - the sequence to copy.
Throws:
NullPointerException - if seq is nullSince:
  1.5

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$__$$$init$$$__$_$Ljava_lang_CharSequence$$$_$V"));
throw ex;
};
var java_lang_StringBuffer_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a string buffer initialized to the contents of the 
 specified string. The initial capacity of the string buffer is 
 16 plus the length of the string argument.


Parameters:str - the initial contents of the buffer.
Throws:
NullPointerException - if str is null

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$__$$$init$$$__$_$Ljava_lang_String$$$_$V"));
throw ex;
};
var java_lang_StringBuffer_$_appendCodePoint_$_$I$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the codePoint
 argument to this sequence.

  The argument is appended to the contents of this sequence.
 The length of this sequence increases by
 Character.charCount(codePoint).

  The overall effect is exactly as if the argument were
 converted to a char array by the method Character.toChars(int) and the character in that array were
 then appended to this character
 sequence.

Parameters:

codePoint -> a Unicode code point

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_appendCodePoint_$_$I$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_appendCodePoint_$_$I$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the codePoint
 argument to this sequence.

  The argument is appended to the contents of this sequence.
 The length of this sequence increases by
 Character.charCount(codePoint).

  The overall effect is exactly as if the argument were
 converted to a char array by the method Character.toChars(int) and the character in that array were
 then appended to this character
 sequence.

Parameters:

codePoint -> a Unicode code point

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_appendCodePoint_$_$I$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$C$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the char 
 argument to this sequence. 
 
 The argument is appended to the contents of this sequence. 
 The length of this sequence increases by 1. 
 
 The overall effect is exactly as if the argument were converted to 
 a string by the method String.valueOf(char) and the character 
 in that string were then appended to this 
 character sequence.

Parameters:

c -> a char.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$C$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$C$Ljava_lang_Appendable$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the char 
 argument to this sequence. 
 
 The argument is appended to the contents of this sequence. 
 The length of this sequence increases by 1. 
 
 The overall effect is exactly as if the argument were converted to 
 a string by the method String.valueOf(char) and the character 
 in that string were then appended to this 
 character sequence.

Parameters:

c -> a char.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$C$Ljava_lang_Appendable$$$_"));
throw ex;
//must have a return of type Appendable
};
var java_lang_StringBuffer_$_append_$_$C$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the char 
 argument to this sequence. 
 
 The argument is appended to the contents of this sequence. 
 The length of this sequence increases by 1. 
 
 The overall effect is exactly as if the argument were converted to 
 a string by the method String.valueOf(char) and the character 
 in that string were then appended to this 
 character sequence.

Parameters:

c -> a char.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$C$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$D$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the double 
 argument to this sequence.
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this sequence.

Parameters:

d -> a double.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$D$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$D$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the double 
 argument to this sequence.
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this sequence.

Parameters:

d -> a double.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$D$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$F$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the float 
 argument to this sequence.
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this string sequence.

Parameters:

f -> a float.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$F$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$F$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the float 
 argument to this sequence.
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this string sequence.

Parameters:

f -> a float.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$F$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$I$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the int 
 argument to this sequence. 
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this sequence.

Parameters:

i -> an int.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$I$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$I$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the int 
 argument to this sequence. 
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this sequence.

Parameters:

i -> an int.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$I$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$J$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the long 
 argument to this sequence.
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this sequence.

Parameters:

lng -> a long.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$J$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$J$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the long 
 argument to this sequence.
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this sequence.

Parameters:

lng -> a long.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$J$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the specified CharSequence to this
 sequence.
 
 The characters of the CharSequence argument are appended, 
 in order, increasing the length of this sequence by the length of the 
 argument.

 The result of this method is exactly the same as if it were an
 invocation of this.append(s, 0, s.length());

 This method synchronizes on this (the destination) 
 object but does not synchronize on the source (s).

 If s is null, then the four characters 
 "null" are appended.

Parameters:

s -> the CharSequence to append.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_Appendable$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the specified CharSequence to this
 sequence.
 
 The characters of the CharSequence argument are appended, 
 in order, increasing the length of this sequence by the length of the 
 argument.

 The result of this method is exactly the same as if it were an
 invocation of this.append(s, 0, s.length());

 This method synchronizes on this (the destination) 
 object but does not synchronize on the source (s).

 If s is null, then the four characters 
 "null" are appended.

Parameters:

s -> the CharSequence to append.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_Appendable$$$_"));
throw ex;
//must have a return of type Appendable
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the specified CharSequence to this
 sequence.
 
 The characters of the CharSequence argument are appended, 
 in order, increasing the length of this sequence by the length of the 
 argument.

 The result of this method is exactly the same as if it were an
 invocation of this.append(s, 0, s.length());

 This method synchronizes on this (the destination) 
 object but does not synchronize on the source (s).

 If s is null, then the four characters 
 "null" are appended.

Parameters:

s -> the CharSequence to append.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Appends a subsequence of the specified CharSequence to this
 sequence.
 
 Characters of the argument s, starting at
 index start, are appended, in order, to the contents of
 this sequence up to the (exclusive) index end. The length
 of this sequence is increased by the value of end - start.
 
 Let n be the length of this character sequence just prior to
 execution of the append method. Then the character at
 index k in this character sequence becomes equal to the
 character at index k in this sequence, if k is less than
 n; otherwise, it is equal to the character at index 
 k+start-n in the argument s.
 
 If s is null, then this method appends
 characters as if the s parameter was a sequence containing the four
 characters "null".

Parameters:

s -> the sequence to append.

start -> the starting index of the subsequence to be appended.

end -> the end index of the subsequence to be appended.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_Appendable$$$_ = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Appends a subsequence of the specified CharSequence to this
 sequence.
 
 Characters of the argument s, starting at
 index start, are appended, in order, to the contents of
 this sequence up to the (exclusive) index end. The length
 of this sequence is increased by the value of end - start.
 
 Let n be the length of this character sequence just prior to
 execution of the append method. Then the character at
 index k in this character sequence becomes equal to the
 character at index k in this sequence, if k is less than
 n; otherwise, it is equal to the character at index 
 k+start-n in the argument s.
 
 If s is null, then this method appends
 characters as if the s parameter was a sequence containing the four
 characters "null".

Parameters:

s -> the sequence to append.

start -> the starting index of the subsequence to be appended.

end -> the end index of the subsequence to be appended.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_Appendable$$$_"));
throw ex;
//must have a return of type Appendable
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Appends a subsequence of the specified CharSequence to this
 sequence.
 
 Characters of the argument s, starting at
 index start, are appended, in order, to the contents of
 this sequence up to the (exclusive) index end. The length
 of this sequence is increased by the value of end - start.
 
 Let n be the length of this character sequence just prior to
 execution of the append method. Then the character at
 index k in this character sequence becomes equal to the
 character at index k in this sequence, if k is less than
 n; otherwise, it is equal to the character at index 
 k+start-n in the argument s.
 
 If s is null, then this method appends
 characters as if the s parameter was a sequence containing the four
 characters "null".

Parameters:

s -> the sequence to append.

start -> the starting index of the subsequence to be appended.

end -> the end index of the subsequence to be appended.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_Object$$$_$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the Object 
 argument.
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this sequence.

Parameters:

obj -> an Object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_Object$$$_$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_Object$$$_$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the Object 
 argument.
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this sequence.

Parameters:

obj -> an Object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_Object$$$_$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the specified string to this character sequence.
 
 The characters of the String argument are appended, in 
 order, increasing the length of this sequence by the length of the 
 argument. If str is null, then the four 
 characters "null" are appended.
 
 Let n be the length of this character sequence just prior to 
 execution of the append method. Then the character at 
 index k in the new character sequence is equal to the character 
 at index k in the old character sequence, if k is less 
 than n; otherwise, it is equal to the character at index 
 k-n in the argument str.

Parameters:

str -> a string.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_String$$$_$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the specified string to this character sequence.
 
 The characters of the String argument are appended, in 
 order, increasing the length of this sequence by the length of the 
 argument. If str is null, then the four 
 characters "null" are appended.
 
 Let n be the length of this character sequence just prior to 
 execution of the append method. Then the character at 
 index k in the new character sequence is equal to the character 
 at index k in the old character sequence, if k is less 
 than n; otherwise, it is equal to the character at index 
 k-n in the argument str.

Parameters:

str -> a string.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_String$$$_$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_StringBuffer$$$_$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the specified StringBuffer to this sequence.
 
 The characters of the StringBuffer argument are appended, 
 in order, to the contents of this StringBuffer, increasing the 
 length of this StringBuffer by the length of the argument. 
 If sb is null, then the four characters 
 "null" are appended to this StringBuffer.
 
 Let n be the length of the old character sequence, the one 
 contained in the StringBuffer just prior to execution of the 
 append method. Then the character at index k in 
 the new character sequence is equal to the character at index k 
 in the old character sequence, if k is less than n; 
 otherwise, it is equal to the character at index k-n in the 
 argument sb.
 
 This method synchronizes on this (the destination) 
 object but does not synchronize on the source (sb).

Parameters:

sb -> the StringBuffer to append.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_StringBuffer$$$_$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$Ljava_lang_StringBuffer$$$_$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the specified StringBuffer to this sequence.
 
 The characters of the StringBuffer argument are appended, 
 in order, to the contents of this StringBuffer, increasing the 
 length of this StringBuffer by the length of the argument. 
 If sb is null, then the four characters 
 "null" are appended to this StringBuffer.
 
 Let n be the length of the old character sequence, the one 
 contained in the StringBuffer just prior to execution of the 
 append method. Then the character at index k in 
 the new character sequence is equal to the character at index k 
 in the old character sequence, if k is less than n; 
 otherwise, it is equal to the character at index k-n in the 
 argument sb.
 
 This method synchronizes on this (the destination) 
 object but does not synchronize on the source (sb).

Parameters:

sb -> the StringBuffer to append.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Ljava_lang_StringBuffer$$$_$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$Z$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the boolean 
 argument to the sequence.
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this sequence.

Parameters:

b -> a boolean.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Z$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$Z$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the boolean 
 argument to the sequence.
 
 The argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then appended to this sequence.

Parameters:

b -> a boolean.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$Z$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$_$$$$$_C$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the char array 
 argument to this sequence. 
 
 The characters of the array argument are appended, in order, to 
 the contents of this sequence. The length of this sequence
 increases by the length of the argument. 
 
 The overall effect is exactly as if the argument were converted to 
 a string by the method String.valueOf(char[]) and the 
 characters of that string were then appended 
 to this character sequence.

Parameters:

str -> the characters to be appended.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$_$$$$$_C$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$_$$$$$_C$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Appends the string representation of the char array 
 argument to this sequence. 
 
 The characters of the array argument are appended, in order, to 
 the contents of this sequence. The length of this sequence
 increases by the length of the argument. 
 
 The overall effect is exactly as if the argument were converted to 
 a string by the method String.valueOf(char[]) and the 
 characters of that string were then appended 
 to this character sequence.

Parameters:

str -> the characters to be appended.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$_$$$$$_C$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_append_$_$_$$$$$_CII$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Appends the string representation of a subarray of the
 char array argument to this sequence.
 
 Characters of the char array str, starting at
 index offset, are appended, in order, to the contents
 of this sequence. The length of this sequence increases
 by the value of len.
 
 The overall effect is exactly as if the arguments were converted to
 a string by the method String.valueOf(char[],int,int) and the
 characters of that string were then appended
 to this character sequence.

Parameters:

str -> the characters to be appended.

offset -> the index of the first char to append.

len -> the number of chars to append.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$_$$$$$_CII$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_append_$_$_$$$$$_CII$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Appends the string representation of a subarray of the
 char array argument to this sequence.
 
 Characters of the char array str, starting at
 index offset, are appended, in order, to the contents
 of this sequence. The length of this sequence increases
 by the value of len.
 
 The overall effect is exactly as if the arguments were converted to
 a string by the method String.valueOf(char[],int,int) and the
 characters of that string were then appended
 to this character sequence.

Parameters:

str -> the characters to be appended.

offset -> the index of the first char to append.

len -> the number of chars to append.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_append_$_$_$$$$$_CII$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_capacity_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the current capacity. The capacity is the amount of storage 
 available for newly inserted characters, beyond which an allocation 
 will occur.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_capacity_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_StringBuffer_$_charAt_$_$I$C = function (arg0, arg1) {
/*implement method!*/
/*
Returns the char value in this sequence at the specified index.
 The first char value is at index 0, the next at index
 1, and so on, as in array indexing.
 
 The index argument must be greater than or equal to
 0, and less than the length of this sequence.

 If the char value specified by the index is a
 surrogate, the surrogate
 value is returned.

Parameters:

index -> the index of the desired char value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_charAt_$_$I$C"));
throw ex;
//must have a return of type char
};
var java_lang_StringBuffer_$_codePointAt_$_$I$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the character (Unicode code point) at the specified
 index. The index refers to char values
 (Unicode code units) and ranges from 0 to
 length() - 1.

  If the char value specified at the given index
 is in the high-surrogate range, the following index is less
 than the length of this sequence, and the
 char value at the following index is in the
 low-surrogate range, then the supplementary code point
 corresponding to this surrogate pair is returned. Otherwise,
 the char value at the given index is returned.

Parameters:

index -> the index to the char values

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_codePointAt_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_StringBuffer_$_codePointBefore_$_$I$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the character (Unicode code point) before the specified
 index. The index refers to char values
 (Unicode code units) and ranges from 1 to length().

  If the char value at (index - 1)
 is in the low-surrogate range, (index - 2) is not
 negative, and the char value at (index -
 2) is in the high-surrogate range, then the
 supplementary code point value of the surrogate pair is
 returned. If the char value at index -
 1 is an unpaired low-surrogate or a high-surrogate, the
 surrogate value is returned.

Parameters:

index -> the index following the code point that should be returned

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_codePointBefore_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_StringBuffer_$_codePointCount_$_$II$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the number of Unicode code points in the specified text
 range of this sequence. The text range begins at the specified
 beginIndex and extends to the char at
 index endIndex - 1. Thus the length (in
 chars) of the text range is
 endIndex-beginIndex. Unpaired surrogates within
 this sequence count as one code point each.

Parameters:

beginIndex -> the index to the first char of
 the text range.

endIndex -> the index after the last char of
 the text range.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_codePointCount_$_$II$I"));
throw ex;
//must have a return of type int
};
var java_lang_StringBuffer_$_deleteCharAt_$_$I$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Removes the char at the specified position in this
 sequence. This sequence is shortened by one char.

 Note: If the character at the given index is a supplementary
 character, this method does not remove the entire character. If
 correct handling of supplementary characters is required,
 determine the number of chars to remove by calling
 Character.charCount(thisSequence.codePointAt(index)),
 where thisSequence is this sequence.

Parameters:

index -> Index of char to remove

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_deleteCharAt_$_$I$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_deleteCharAt_$_$I$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Removes the char at the specified position in this
 sequence. This sequence is shortened by one char.

 Note: If the character at the given index is a supplementary
 character, this method does not remove the entire character. If
 correct handling of supplementary characters is required,
 determine the number of chars to remove by calling
 Character.charCount(thisSequence.codePointAt(index)),
 where thisSequence is this sequence.

Parameters:

index -> Index of char to remove

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_deleteCharAt_$_$I$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_delete_$_$II$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Removes the characters in a substring of this sequence.
 The substring begins at the specified start and extends to
 the character at index end - 1 or to the end of the
 sequence if no such character exists. If
 start is equal to end, no changes are made.

Parameters:

start -> The beginning index, inclusive.

end -> The ending index, exclusive.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_delete_$_$II$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_delete_$_$II$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Removes the characters in a substring of this sequence.
 The substring begins at the specified start and extends to
 the character at index end - 1 or to the end of the
 sequence if no such character exists. If
 start is equal to end, no changes are made.

Parameters:

start -> The beginning index, inclusive.

end -> The ending index, exclusive.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_delete_$_$II$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_ensureCapacity_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Ensures that the capacity is at least equal to the specified minimum.
 If the current capacity is less than the argument, then a new internal
 array is allocated with greater capacity. The new capacity is the 
 larger of: 
 
 The minimumCapacity argument. 
 Twice the old capacity, plus 2. 
 
 If the minimumCapacity argument is nonpositive, this
 method takes no action and simply returns.

Parameters:

minimumCapacity -> the minimum desired capacity.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_ensureCapacity_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_lang_StringBuffer_$_getChars_$_$II_$$$$$_CI$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*
Characters are copied from this sequence into the 
 destination character array dst. The first character to 
 be copied is at index srcBegin; the last character to 
 be copied is at index srcEnd-1. The total number of 
 characters to be copied is srcEnd-srcBegin. The 
 characters are copied into the subarray of dst starting 
 at index dstBegin and ending at index:
  dstbegin + (srcEnd-srcBegin) - 1

Parameters:

srcBegin -> start copying at this offset.

srcEnd -> stop copying at this offset.

dst -> the array to copy the data into.

dstBegin -> offset into dst.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_getChars_$_$II_$$$$$_CI$V"));
throw ex;
//must have a return of type void
};
var java_lang_StringBuffer_$_indexOf_$_$Ljava_lang_String$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the index within this string of the first occurrence of the
 specified substring. The integer returned is the smallest value 
 k such that:
  this.toString().startsWith(str, k)
 
 is true.

Parameters:

str -> any string.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_indexOf_$_$Ljava_lang_String$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_StringBuffer_$_indexOf_$_$Ljava_lang_String$$$_I$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the index within this string of the first occurrence of the
 specified substring, starting at the specified index.  The integer
 returned is the smallest value k for which:
      k >= Math.min(fromIndex, str.length()) &&
                   this.toString().startsWith(str, k)
 
 If no such value of k exists, then -1 is returned.

Parameters:

str -> the substring for which to search.

fromIndex -> the index from which to start the search.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_indexOf_$_$Ljava_lang_String$$$_I$I"));
throw ex;
//must have a return of type int
};
var java_lang_StringBuffer_$_insert_$_$IC$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the char 
 argument into this sequence. 
 
 The second argument is inserted into the contents of this sequence
 at the position indicated by offset. The length 
 of this sequence increases by one. 
 
 The overall effect is exactly as if the argument were converted to 
 a string by the method String.valueOf(char) and the character 
 in that string were then inserted into 
 this character sequence at the position indicated by
 offset.
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

c -> a char.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$IC$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$IC$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the char 
 argument into this sequence. 
 
 The second argument is inserted into the contents of this sequence
 at the position indicated by offset. The length 
 of this sequence increases by one. 
 
 The overall effect is exactly as if the argument were converted to 
 a string by the method String.valueOf(char) and the character 
 in that string were then inserted into 
 this character sequence at the position indicated by
 offset.
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

c -> a char.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$IC$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_insert_$_$ID$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the double 
 argument into this sequence.
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the indicated 
 offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

d -> a double.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$ID$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$ID$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the double 
 argument into this sequence.
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the indicated 
 offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

d -> a double.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$ID$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_insert_$_$IF$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the float 
 argument into this sequence. 
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the indicated 
 offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

f -> a float.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$IF$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$IF$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the float 
 argument into this sequence. 
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the indicated 
 offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

f -> a float.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$IF$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_insert_$_$II$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the second int 
 argument into this sequence. 
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the indicated 
 offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

i -> an int.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$II$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$II$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the second int 
 argument into this sequence. 
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the indicated 
 offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

i -> an int.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$II$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_insert_$_$IJ$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the long 
 argument into this sequence. 
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the position 
 indicated by offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

l -> a long.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$IJ$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$IJ$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the long 
 argument into this sequence. 
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the position 
 indicated by offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

l -> a long.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$IJ$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the specified CharSequence into this sequence.
 
 The characters of the CharSequence argument are inserted,
 in order, into this sequence at the indicated offset, moving up
 any characters originally above that position and increasing the length 
 of this sequence by the length of the argument s.
 
 The result of this method is exactly the same as if it were an
 invocation of this object's insert(dstOffset, s, 0, s.length()) method.

 If s is null, then the four characters 
 "null" are inserted into this sequence.

Parameters:

dstOffset -> the offset.

s -> the sequence to be inserted

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the specified CharSequence into this sequence.
 
 The characters of the CharSequence argument are inserted,
 in order, into this sequence at the indicated offset, moving up
 any characters originally above that position and increasing the length 
 of this sequence by the length of the argument s.
 
 The result of this method is exactly the same as if it were an
 invocation of this object's insert(dstOffset, s, 0, s.length()) method.

 If s is null, then the four characters 
 "null" are inserted into this sequence.

Parameters:

dstOffset -> the offset.

s -> the sequence to be inserted

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_II$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*
Inserts a subsequence of the specified CharSequence into
 this sequence.
 
 The subsequence of the argument s specified by
 start and end are inserted,
 in order, into this sequence at the specified destination offset, moving
 up any characters originally above that position. The length of this
 sequence is increased by end - start.
 
 The character at index k in this sequence becomes equal to:
 
 the character at index k in this sequence, if
 k is less than dstOffset
 the character at index k+start-dstOffset in
 the argument s, if k is greater than or equal to
 dstOffset but is less than dstOffset+end-start
 the character at index k-(end-start) in this
 sequence, if k is greater than or equal to
 dstOffset+end-start
 
 The dstOffset argument must be greater than or equal to
 0, and less than or equal to the length of this
 sequence.
 The start argument must be nonnegative, and not greater than
 end.
 The end argument must be greater than or equal to
 start, and less than or equal to the length of s.

 If s is null, then this method inserts
 characters as if the s parameter was a sequence containing the four
 characters "null".

Parameters:

dstOffset -> the offset in this sequence.

s -> the sequence to be inserted.

start -> the starting index of the subsequence to be inserted.

end -> the end index of the subsequence to be inserted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_II$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_II$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*
Inserts a subsequence of the specified CharSequence into
 this sequence.
 
 The subsequence of the argument s specified by
 start and end are inserted,
 in order, into this sequence at the specified destination offset, moving
 up any characters originally above that position. The length of this
 sequence is increased by end - start.
 
 The character at index k in this sequence becomes equal to:
 
 the character at index k in this sequence, if
 k is less than dstOffset
 the character at index k+start-dstOffset in
 the argument s, if k is greater than or equal to
 dstOffset but is less than dstOffset+end-start
 the character at index k-(end-start) in this
 sequence, if k is greater than or equal to
 dstOffset+end-start
 
 The dstOffset argument must be greater than or equal to
 0, and less than or equal to the length of this
 sequence.
 The start argument must be nonnegative, and not greater than
 end.
 The end argument must be greater than or equal to
 start, and less than or equal to the length of s.

 If s is null, then this method inserts
 characters as if the s parameter was a sequence containing the four
 characters "null".

Parameters:

dstOffset -> the offset in this sequence.

s -> the sequence to be inserted.

start -> the starting index of the subsequence to be inserted.

end -> the end index of the subsequence to be inserted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_II$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_insert_$_$ILjava_lang_Object$$$_$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the Object 
 argument into this character sequence.
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the indicated 
 offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

obj -> an Object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$ILjava_lang_Object$$$_$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$ILjava_lang_Object$$$_$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the Object 
 argument into this character sequence.
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the indicated 
 offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

obj -> an Object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$ILjava_lang_Object$$$_$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_insert_$_$ILjava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string into this character sequence.
 
 The characters of the String argument are inserted, in 
 order, into this sequence at the indicated offset, moving up any 
 characters originally above that position and increasing the length 
 of this sequence by the length of the argument. If 
 str is null, then the four characters 
 "null" are inserted into this sequence.
 
 The character at index k in the new character sequence is 
 equal to:
 
 the character at index k in the old character sequence, if 
 k is less than offset 
 the character at index k-offset in the 
 argument str, if k is not less than 
 offset but is less than offset+str.length() 
 the character at index k-str.length() in the 
 old character sequence, if k is not less than 
 offset+str.length()
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

str -> a string.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$ILjava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$ILjava_lang_String$$$_$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string into this character sequence.
 
 The characters of the String argument are inserted, in 
 order, into this sequence at the indicated offset, moving up any 
 characters originally above that position and increasing the length 
 of this sequence by the length of the argument. If 
 str is null, then the four characters 
 "null" are inserted into this sequence.
 
 The character at index k in the new character sequence is 
 equal to:
 
 the character at index k in the old character sequence, if 
 k is less than offset 
 the character at index k-offset in the 
 argument str, if k is not less than 
 offset but is less than offset+str.length() 
 the character at index k-str.length() in the 
 old character sequence, if k is not less than 
 offset+str.length()
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

str -> a string.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$ILjava_lang_String$$$_$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_insert_$_$IZ$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the boolean 
 argument into this sequence. 
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the indicated 
 offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

b -> a boolean.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$IZ$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$IZ$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the boolean 
 argument into this sequence. 
 
 The second argument is converted to a string as if by the method 
 String.valueOf, and the characters of that 
 string are then inserted into this sequence at the indicated 
 offset. 
 
 The offset argument must be greater than or equal to 
 0, and less than or equal to the length of this 
 sequence.

Parameters:

offset -> the offset.

b -> a boolean.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$IZ$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_insert_$_$I_$$$$$_C$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the char array 
 argument into this sequence.
 
 The characters of the array argument are inserted into the 
 contents of this sequence at the position indicated by 
 offset. The length of this sequence increases by 
 the length of the argument. 
 
 The overall effect is exactly as if the argument were converted to 
 a string by the method String.valueOf(char[]) and the 
 characters of that string were then 
 inserted into this 
 character sequence at the position indicated by
 offset.

Parameters:

offset -> the offset.

str -> a character array.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$I_$$$$$_C$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$I_$$$$$_C$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Inserts the string representation of the char array 
 argument into this sequence.
 
 The characters of the array argument are inserted into the 
 contents of this sequence at the position indicated by 
 offset. The length of this sequence increases by 
 the length of the argument. 
 
 The overall effect is exactly as if the argument were converted to 
 a string by the method String.valueOf(char[]) and the 
 characters of that string were then 
 inserted into this 
 character sequence at the position indicated by
 offset.

Parameters:

offset -> the offset.

str -> a character array.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$I_$$$$$_C$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_insert_$_$I_$$$$$_CII$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*
Inserts the string representation of a subarray of the str
 array argument into this sequence. The subarray begins at the
 specified offset and extends len chars.
 The characters of the subarray are inserted into this sequence at
 the position indicated by index. The length of this
 sequence increases by len chars.

Parameters:

index -> position at which to insert subarray.

str -> A char array.

offset -> the index of the first char in subarray to
             be inserted.

len -> the number of chars in the subarray to
             be inserted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$I_$$$$$_CII$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_insert_$_$I_$$$$$_CII$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*
Inserts the string representation of a subarray of the str
 array argument into this sequence. The subarray begins at the
 specified offset and extends len chars.
 The characters of the subarray are inserted into this sequence at
 the position indicated by index. The length of this
 sequence increases by len chars.

Parameters:

index -> position at which to insert subarray.

str -> A char array.

offset -> the index of the first char in subarray to
             be inserted.

len -> the number of chars in the subarray to
             be inserted.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_insert_$_$I_$$$$$_CII$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_lastIndexOf_$_$Ljava_lang_String$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the index within this string of the rightmost occurrence
 of the specified substring.  The rightmost empty string "" is
 considered to occur at the index value this.length(). 
 The returned index is the largest value k such that 
  this.toString().startsWith(str, k)
 
 is true.

Parameters:

str -> the substring to search for.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_lastIndexOf_$_$Ljava_lang_String$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_StringBuffer_$_lastIndexOf_$_$Ljava_lang_String$$$_I$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the index within this string of the last occurrence of the
 specified substring. The integer returned is the largest value k
 such that:
      k <= Math.min(fromIndex, str.length()) &&
                   this.toString().startsWith(str, k)
 
 If no such value of k exists, then -1 is returned.

Parameters:

str -> the substring to search for.

fromIndex -> the index to start the search from.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_lastIndexOf_$_$Ljava_lang_String$$$_I$I"));
throw ex;
//must have a return of type int
};
var java_lang_StringBuffer_$_length_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the length (character count).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_length_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_StringBuffer_$_offsetByCodePoints_$_$II$I = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns the index within this sequence that is offset from the
 given index by codePointOffset code
 points. Unpaired surrogates within the text range given by
 index and codePointOffset count as
 one code point each.

Parameters:

index -> the index to be offset

codePointOffset -> the offset in code points

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_offsetByCodePoints_$_$II$I"));
throw ex;
//must have a return of type int
};
var java_lang_StringBuffer_$_replace_$_$IILjava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Replaces the characters in a substring of this sequence
 with characters in the specified String. The substring
 begins at the specified start and extends to the character
 at index end - 1 or to the end of the
 sequence if no such character exists. First the
 characters in the substring are removed and then the specified
 String is inserted at start. (This 
 sequence will be lengthened to accommodate the
 specified String if necessary.)

Parameters:

start -> The beginning index, inclusive.

end -> The ending index, exclusive.

str -> String that will replace previous contents.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_replace_$_$IILjava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_replace_$_$IILjava_lang_String$$$_$Ljava_lang_StringBuffer$$$_ = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Replaces the characters in a substring of this sequence
 with characters in the specified String. The substring
 begins at the specified start and extends to the character
 at index end - 1 or to the end of the
 sequence if no such character exists. First the
 characters in the substring are removed and then the specified
 String is inserted at start. (This 
 sequence will be lengthened to accommodate the
 specified String if necessary.)

Parameters:

start -> The beginning index, inclusive.

end -> The ending index, exclusive.

str -> String that will replace previous contents.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_replace_$_$IILjava_lang_String$$$_$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_reverse_$_$$Ljava_lang_AbstractStringBuilder$$$_ = function (arg0) {
/*implement method!*/
/*
Causes this character sequence to be replaced by the reverse of
 the sequence. If there are any surrogate pairs included in the
 sequence, these are treated as single characters for the
 reverse operation. Thus, the order of the high-low surrogates
 is never reversed.

 Let n be the character length of this character sequence
 (not the length in char values) just prior to
 execution of the reverse method. Then the
 character at index k in the new character sequence is
 equal to the character at index n-k-1 in the old
 character sequence.

 Note that the reverse operation may result in producing
 surrogate pairs that were unpaired low-surrogates and
 high-surrogates before the operation. For example, reversing
 "\uDC00\uD800" produces "\uD800\uDC00" which is
 a valid surrogate pair.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_reverse_$_$$Ljava_lang_AbstractStringBuilder$$$_"));
throw ex;
//must have a return of type AbstractStringBuilder
};
var java_lang_StringBuffer_$_reverse_$_$$Ljava_lang_StringBuffer$$$_ = function (arg0) {
/*implement method!*/
/*
Causes this character sequence to be replaced by the reverse of
 the sequence. If there are any surrogate pairs included in the
 sequence, these are treated as single characters for the
 reverse operation. Thus, the order of the high-low surrogates
 is never reversed.

 Let n be the character length of this character sequence
 (not the length in char values) just prior to
 execution of the reverse method. Then the
 character at index k in the new character sequence is
 equal to the character at index n-k-1 in the old
 character sequence.

 Note that the reverse operation may result in producing
 surrogate pairs that were unpaired low-surrogates and
 high-surrogates before the operation. For example, reversing
 "\uDC00\uD800" produces "\uD800\uDC00" which is
 a valid surrogate pair.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_reverse_$_$$Ljava_lang_StringBuffer$$$_"));
throw ex;
//must have a return of type StringBuffer
};
var java_lang_StringBuffer_$_setCharAt_$_$IC$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
The character at the specified index is set to ch. This 
 sequence is altered to represent a new character sequence that is 
 identical to the old character sequence, except that it contains the 
 character ch at position index. 
 
 The index argument must be greater than or equal to 
 0, and less than the length of this sequence.

Parameters:

index -> the index of the character to modify.

ch -> the new character.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_setCharAt_$_$IC$V"));
throw ex;
//must have a return of type void
};
var java_lang_StringBuffer_$_setLength_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the length of the character sequence.
 The sequence is changed to a new character sequence 
 whose length is specified by the argument. For every nonnegative 
 index k less than newLength, the character at 
 index k in the new character sequence is the same as the 
 character at index k in the old sequence if k is less 
 than the length of the old character sequence; otherwise, it is the 
 null character '\u0000'. 
  
 In other words, if the newLength argument is less than 
 the current length, the length is changed to the specified length.
 
 If the newLength argument is greater than or equal 
 to the current length, sufficient null characters 
 ('\u0000') are appended so that 
 length becomes the newLength argument. 
 
 The newLength argument must be greater than or equal 
 to 0.

Parameters:

newLength -> the new length

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_setLength_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_lang_StringBuffer_$_subSequence_$_$II$Ljava_lang_CharSequence$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a new character sequence that is a subsequence of this sequence.

  An invocation of this method of the form

  sb.subSequence(begin, end)

 behaves in exactly the same way as the invocation

  sb.substring(begin, end)

 This method is provided so that this class can
 implement the CharSequence interface.

Parameters:

start -> the start index, inclusive.

end -> the end index, exclusive.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_subSequence_$_$II$Ljava_lang_CharSequence$$$_"));
throw ex;
//must have a return of type CharSequence
};
var java_lang_StringBuffer_$_substring_$_$I$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a new String that contains a subsequence of
 characters currently contained in this character sequence. The 
 substring begins at the specified index and extends to the end of
 this sequence.

Parameters:

start -> The beginning index, inclusive.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_substring_$_$I$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_StringBuffer_$_substring_$_$II$Ljava_lang_String$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Returns a new String that contains a subsequence of
 characters currently contained in this sequence. The 
 substring begins at the specified start and 
 extends to the character at index end - 1.

Parameters:

start -> The beginning index, inclusive.

end -> The ending index, exclusive.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_substring_$_$II$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_StringBuffer_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representing the data in this sequence.
 A new String object is allocated and initialized to 
 contain the character sequence currently represented by this 
 object. This String is then returned. Subsequent 
 changes to this sequence do not affect the contents of the 
 String.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_toString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_StringBuffer_$_trimToSize_$_$$V = function (arg0) {
/*implement method!*/
/*
Attempts to reduce storage used for the character sequence.
 If the buffer is larger than necessary to hold its current sequence of 
 characters, then it may be resized to become more space efficient. 
 Calling this method may, but is not required to, affect the value 
 returned by a subsequent call to the capacity() method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_StringBuffer_$_trimToSize_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_StringBuffer_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_AbstractStringBuilder_$_wait_$_$J$V(arg0, arg1);};
var java_lang_StringBuffer_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_AbstractStringBuilder_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_StringBuffer_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_AbstractStringBuilder_$_wait_$_$$V(arg0);};
var java_lang_StringBuffer_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_AbstractStringBuilder_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_lang_StringBuffer_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_AbstractStringBuilder_$_hashCode_$_$$I(arg0);};
var java_lang_StringBuffer_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_AbstractStringBuilder_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_StringBuffer_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_AbstractStringBuilder_$_notify_$_$$V(arg0);};
var java_lang_StringBuffer_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_AbstractStringBuilder_$_notifyAll_$_$$V(arg0);};
java_lang_StringBuffer.prototype._$$$init$$$__$_$$V = java_lang_StringBuffer_$__$$$init$$$__$_$$V;
java_lang_StringBuffer.prototype._$$$init$$$__$_$I$V = java_lang_StringBuffer_$__$$$init$$$__$_$I$V;
java_lang_StringBuffer.prototype._$$$init$$$__$_$Ljava_lang_CharSequence$$$_$V = java_lang_StringBuffer_$__$$$init$$$__$_$Ljava_lang_CharSequence$$$_$V;
java_lang_StringBuffer.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_lang_StringBuffer_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_lang_StringBuffer.prototype.appendCodePoint_$_$I$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_appendCodePoint_$_$I$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.appendCodePoint_$_$I$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_appendCodePoint_$_$I$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$C$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$C$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$C$Ljava_lang_Appendable$$$_ = java_lang_StringBuffer_$_append_$_$C$Ljava_lang_Appendable$$$_;
java_lang_StringBuffer.prototype.append_$_$C$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$C$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$D$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$D$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$D$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$D$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$F$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$F$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$F$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$F$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$I$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$I$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$I$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$I$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$J$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$J$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$J$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$J$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_Appendable$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_Appendable$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_Appendable$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_Appendable$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_CharSequence$$$_II$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_Object$$$_$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_Object$$$_$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_Object$$$_$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_Object$$$_$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_String$$$_$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_String$$$_$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_StringBuffer$$$_$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_StringBuffer$$$_$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$Ljava_lang_StringBuffer$$$_$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$Ljava_lang_StringBuffer$$$_$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$Z$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$Z$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$Z$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$Z$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$_$$$$$_C$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$_$$$$$_C$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$_$$$$$_C$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$_$$$$$_C$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.append_$_$_$$$$$_CII$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_append_$_$_$$$$$_CII$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.append_$_$_$$$$$_CII$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_append_$_$_$$$$$_CII$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.capacity_$_$$I = java_lang_StringBuffer_$_capacity_$_$$I;
java_lang_StringBuffer.prototype.charAt_$_$I$C = java_lang_StringBuffer_$_charAt_$_$I$C;
java_lang_StringBuffer.prototype.codePointAt_$_$I$I = java_lang_StringBuffer_$_codePointAt_$_$I$I;
java_lang_StringBuffer.prototype.codePointBefore_$_$I$I = java_lang_StringBuffer_$_codePointBefore_$_$I$I;
java_lang_StringBuffer.prototype.codePointCount_$_$II$I = java_lang_StringBuffer_$_codePointCount_$_$II$I;
java_lang_StringBuffer.prototype.deleteCharAt_$_$I$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_deleteCharAt_$_$I$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.deleteCharAt_$_$I$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_deleteCharAt_$_$I$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.delete_$_$II$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_delete_$_$II$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.delete_$_$II$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_delete_$_$II$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.ensureCapacity_$_$I$V = java_lang_StringBuffer_$_ensureCapacity_$_$I$V;
java_lang_StringBuffer.prototype.getChars_$_$II_$$$$$_CI$V = java_lang_StringBuffer_$_getChars_$_$II_$$$$$_CI$V;
java_lang_StringBuffer.prototype.indexOf_$_$Ljava_lang_String$$$_$I = java_lang_StringBuffer_$_indexOf_$_$Ljava_lang_String$$$_$I;
java_lang_StringBuffer.prototype.indexOf_$_$Ljava_lang_String$$$_I$I = java_lang_StringBuffer_$_indexOf_$_$Ljava_lang_String$$$_I$I;
java_lang_StringBuffer.prototype.insert_$_$IC$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$IC$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$IC$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$IC$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.insert_$_$ID$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$ID$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$ID$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$ID$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.insert_$_$IF$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$IF$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$IF$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$IF$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.insert_$_$II$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$II$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$II$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$II$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.insert_$_$IJ$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$IJ$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$IJ$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$IJ$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.insert_$_$ILjava_lang_CharSequence$$$_$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$ILjava_lang_CharSequence$$$_$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.insert_$_$ILjava_lang_CharSequence$$$_II$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_II$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$ILjava_lang_CharSequence$$$_II$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$ILjava_lang_CharSequence$$$_II$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.insert_$_$ILjava_lang_Object$$$_$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$ILjava_lang_Object$$$_$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$ILjava_lang_Object$$$_$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$ILjava_lang_Object$$$_$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.insert_$_$ILjava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$ILjava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$ILjava_lang_String$$$_$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$ILjava_lang_String$$$_$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.insert_$_$IZ$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$IZ$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$IZ$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$IZ$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.insert_$_$I_$$$$$_C$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$I_$$$$$_C$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$I_$$$$$_C$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$I_$$$$$_C$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.insert_$_$I_$$$$$_CII$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_insert_$_$I_$$$$$_CII$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.insert_$_$I_$$$$$_CII$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_insert_$_$I_$$$$$_CII$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.lastIndexOf_$_$Ljava_lang_String$$$_$I = java_lang_StringBuffer_$_lastIndexOf_$_$Ljava_lang_String$$$_$I;
java_lang_StringBuffer.prototype.lastIndexOf_$_$Ljava_lang_String$$$_I$I = java_lang_StringBuffer_$_lastIndexOf_$_$Ljava_lang_String$$$_I$I;
java_lang_StringBuffer.prototype.length_$_$$I = java_lang_StringBuffer_$_length_$_$$I;
java_lang_StringBuffer.prototype.offsetByCodePoints_$_$II$I = java_lang_StringBuffer_$_offsetByCodePoints_$_$II$I;
java_lang_StringBuffer.prototype.replace_$_$IILjava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_replace_$_$IILjava_lang_String$$$_$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.replace_$_$IILjava_lang_String$$$_$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_replace_$_$IILjava_lang_String$$$_$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.reverse_$_$$Ljava_lang_AbstractStringBuilder$$$_ = java_lang_StringBuffer_$_reverse_$_$$Ljava_lang_AbstractStringBuilder$$$_;
java_lang_StringBuffer.prototype.reverse_$_$$Ljava_lang_StringBuffer$$$_ = java_lang_StringBuffer_$_reverse_$_$$Ljava_lang_StringBuffer$$$_;
java_lang_StringBuffer.prototype.setCharAt_$_$IC$V = java_lang_StringBuffer_$_setCharAt_$_$IC$V;
java_lang_StringBuffer.prototype.setLength_$_$I$V = java_lang_StringBuffer_$_setLength_$_$I$V;
java_lang_StringBuffer.prototype.subSequence_$_$II$Ljava_lang_CharSequence$$$_ = java_lang_StringBuffer_$_subSequence_$_$II$Ljava_lang_CharSequence$$$_;
java_lang_StringBuffer.prototype.substring_$_$I$Ljava_lang_String$$$_ = java_lang_StringBuffer_$_substring_$_$I$Ljava_lang_String$$$_;
java_lang_StringBuffer.prototype.substring_$_$II$Ljava_lang_String$$$_ = java_lang_StringBuffer_$_substring_$_$II$Ljava_lang_String$$$_;
java_lang_StringBuffer.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_StringBuffer_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_StringBuffer.prototype.trimToSize_$_$$V = java_lang_StringBuffer_$_trimToSize_$_$$V;
java_lang_StringBuffer.prototype.wait_$_$J$V = java_lang_StringBuffer_$_wait_$_$J$V;
java_lang_StringBuffer.prototype.wait_$_$JI$V = java_lang_StringBuffer_$_wait_$_$JI$V;
java_lang_StringBuffer.prototype.wait_$_$$V = java_lang_StringBuffer_$_wait_$_$$V;
java_lang_StringBuffer.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_StringBuffer_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_StringBuffer.prototype.hashCode_$_$$I = java_lang_StringBuffer_$_hashCode_$_$$I;
java_lang_StringBuffer.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_StringBuffer_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_StringBuffer.prototype.notify_$_$$V = java_lang_StringBuffer_$_notify_$_$$V;
java_lang_StringBuffer.prototype.notifyAll_$_$$V = java_lang_StringBuffer_$_notifyAll_$_$$V;
