/** @constructor */
function java_lang_SecurityManager() {}
var java_lang_SecurityManager_$__$$$init$$$__$_$$V = function (arg0) {
/*implement constructor!*/
/*
Constructs a new SecurityManager.

  If there is a security manager already installed, this method first
 calls the security manager's checkPermission method
 with the RuntimePermission("createSecurityManager")
 permission to ensure the calling thread has permission to create a new 
 security manager.
 This may result in throwing a SecurityException.



Throws:
SecurityException - if a security manager already 
             exists and its checkPermission method 
             doesn't allow creation of a new security manager.See Also:System.getSecurityManager(), 
checkPermission, 
RuntimePermission

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$__$$$init$$$__$_$$V"));
throw ex;
};
var java_lang_SecurityManager_$_checkAccept_$_$Ljava_lang_String$$$_I$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not permitted to accept a socket connection from 
 the specified host and port number. 
 
 This method is invoked for the current security manager by the 
 accept method of class ServerSocket. 
 
 This method calls checkPermission with the
 SocketPermission(host+":"+port,"accept") permission.
 
 If you override this method, then you should make a call to 
 super.checkAccept
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkAccept_$_$Ljava_lang_String$$$_I$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkAccess_$_$Ljava_lang_Thread$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to modify the thread argument. 
 
 This method is invoked for the current security manager by the 
 stop, suspend, resume, 
 setPriority, setName, and 
 setDaemon methods of class Thread. 
 
 If the thread argument is a system thread (belongs to
 the thread group with a null parent) then 
 this method calls checkPermission with the
 RuntimePermission("modifyThread") permission.
 If the thread argument is not a system thread,
 this method just returns silently.
 
 Applications that want a stricter policy should override this
 method. If this method is overridden, the method that overrides
 it should additionally check to see if the calling thread has the
 RuntimePermission("modifyThread") permission, and
 if so, return silently. This is to ensure that code granted
 that permission (such as the JDK itself) is allowed to
 manipulate any thread.
 
 If this method is overridden, then 
 super.checkAccess should
 be called by the first statement in the overridden method, or the 
 equivalent security check should be placed in the overridden method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkAccess_$_$Ljava_lang_Thread$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkAccess_$_$Ljava_lang_ThreadGroup$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to modify the thread group argument. 
 
 This method is invoked for the current security manager when a 
 new child thread or child thread group is created, and by the 
 setDaemon, setMaxPriority, 
 stop, suspend, resume, and 
 destroy methods of class ThreadGroup. 
 
 If the thread group argument is the system thread group (
 has a null parent) then 
 this method calls checkPermission with the
 RuntimePermission("modifyThreadGroup") permission.
 If the thread group argument is not the system thread group,
 this method just returns silently.
 
 Applications that want a stricter policy should override this
 method. If this method is overridden, the method that overrides
 it should additionally check to see if the calling thread has the
 RuntimePermission("modifyThreadGroup") permission, and
 if so, return silently. This is to ensure that code granted
 that permission (such as the JDK itself) is allowed to
 manipulate any thread.
 
 If this method is overridden, then 
 super.checkAccess should
 be called by the first statement in the overridden method, or the 
 equivalent security check should be placed in the overridden method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkAccess_$_$Ljava_lang_ThreadGroup$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkAwtEventQueueAccess_$_$$V = function (arg0) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to access the AWT event queue.
 
 This method calls checkPermission with the
 AWTPermission("accessEventQueue") permission.
 
 If you override this method, then you should make a call to 
 super.checkAwtEventQueueAccess
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkAwtEventQueueAccess_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkConnect_$_$Ljava_lang_String$$$_I$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to open a socket connection to the 
 specified host and port number. 
 
 A port number of -1 indicates that the calling 
 method is attempting to determine the IP address of the specified 
 host name. 
 
 This method calls checkPermission with the
 SocketPermission(host+":"+port,"connect") permission if
 the port is not equal to -1. If the port is equal to -1, then
 it calls checkPermission with the
 SocketPermission(host,"resolve") permission.
 
 If you override this method, then you should make a call to 
 super.checkConnect
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkConnect_$_$Ljava_lang_String$$$_I$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkConnect_$_$Ljava_lang_String$$$_ILjava_lang_Object$$$_$V = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Throws a SecurityException if the 
 specified security context is not allowed to open a socket 
 connection to the specified host and port number. 
 
 A port number of -1 indicates that the calling 
 method is attempting to determine the IP address of the specified 
 host name. 
  If context is not an instance of 
 AccessControlContext then a
 SecurityException is thrown.
 
 Otherwise, the port number is checked. If it is not equal
 to -1, the context's checkPermission
 method is called with a 
 SocketPermission(host+":"+port,"connect") permission.
 If the port is equal to -1, then
 the context's checkPermission method 
 is called with a
 SocketPermission(host,"resolve") permission.
 
 If you override this method, then you should make a call to 
 super.checkConnect
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkConnect_$_$Ljava_lang_String$$$_ILjava_lang_Object$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkCreateClassLoader_$_$$V = function (arg0) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to create a new class loader. 
 
 This method calls checkPermission with the
 RuntimePermission("createClassLoader")
 permission.
 
 If you override this method, then you should make a call to 
 super.checkCreateClassLoader
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkCreateClassLoader_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkDelete_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to delete the specified file. 
 
 This method is invoked for the current security manager by the 
 delete method of class File.
 
 This method calls checkPermission with the
 FilePermission(file,"delete") permission.
 
 If you override this method, then you should make a call to 
 super.checkDelete
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkDelete_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkExec_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to create a subprocess. 
 
 This method is invoked for the current security manager by the 
 exec methods of class Runtime.
 
 This method calls checkPermission with the
 FilePermission(cmd,"execute") permission
 if cmd is an absolute path, otherwise it calls 
 checkPermission with 
 FilePermission("<<ALL FILES>>","execute").
 
 If you override this method, then you should make a call to 
 super.checkExec
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkExec_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkExit_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to cause the Java Virtual Machine to 
 halt with the specified status code. 
 
 This method is invoked for the current security manager by the 
 exit method of class Runtime. A status 
 of 0 indicates success; other values indicate various 
 errors. 
 
 This method calls checkPermission with the
 RuntimePermission("exitVM."+status) permission.
 
 If you override this method, then you should make a call to 
 super.checkExit
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkExit_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkLink_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to dynamic link the library code 
 specified by the string argument file. The argument is either a 
 simple library name or a complete filename. 
 
 This method is invoked for the current security manager by 
 methods load and loadLibrary of class 
 Runtime. 
 
 This method calls checkPermission with the
 RuntimePermission("loadLibrary."+lib) permission.
 
 If you override this method, then you should make a call to 
 super.checkLink
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkLink_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkListen_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to wait for a connection request on 
 the specified local port number. 
 
 If port is not 0, this method calls
 checkPermission with the
 SocketPermission("localhost:"+port,"listen").
 If port is zero, this method calls checkPermission
 with SocketPermission("localhost:1024-","listen").
 
 If you override this method, then you should make a call to 
 super.checkListen
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkListen_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkMemberAccess_$_$Ljava_lang_Class$$$_I$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to access members. 
 
 The default policy is to allow access to PUBLIC members, as well
 as access to classes that have the same class loader as the caller.
 In all other cases, this method calls checkPermission 
 with the RuntimePermission("accessDeclaredMembers")
  permission.
 
 If this method is overridden, then a call to 
 super.checkMemberAccess cannot be made,
 as the default implementation of checkMemberAccess
 relies on the code being checked being at a stack depth of
 4.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkMemberAccess_$_$Ljava_lang_Class$$$_I$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkMulticast_$_$Ljava_net_InetAddress$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to use
 (join/leave/send/receive) IP multicast.
 
 This method calls checkPermission with the
 java.net.SocketPermission(maddr.getHostAddress(), 
 "accept,connect") permission.
 
 If you override this method, then you should make a call to 
 super.checkMulticast
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkMulticast_$_$Ljava_net_InetAddress$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkMulticast_$_$Ljava_net_InetAddress$$$_B$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Deprecated. Use #checkPermission(java.security.Permission) instead

Parameters:

maddr -> Internet group address to be used.

ttl -> value in use, if it is multicast send.
 Note: this particular implementation does not use the ttl
 parameter.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkMulticast_$_$Ljava_net_InetAddress$$$_B$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkPackageAccess_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the
 calling thread is not allowed to access the package specified by
 the argument.
 
 This method is used by the loadClass method of class
 loaders.
 
 This method first gets a list of
 restricted packages by obtaining a comma-separated list from
 a call to
 java.security.Security.getProperty("package.access"),
 and checks to see if pkg starts with or equals
 any of the restricted packages. If it does, then
 checkPermission gets called with the
 RuntimePermission("accessClassInPackage."+pkg)
 permission.
 
 If this method is overridden, then
 super.checkPackageAccess should be called
 as the first line in the overridden method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkPackageAccess_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkPackageDefinition_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the
 calling thread is not allowed to define classes in the package
 specified by the argument.
 
 This method is used by the loadClass method of some
 class loaders.
 
 This method first gets a list of restricted packages by
 obtaining a comma-separated list from a call to
 java.security.Security.getProperty("package.definition"),
 and checks to see if pkg starts with or equals
 any of the restricted packages. If it does, then
 checkPermission gets called with the
 RuntimePermission("defineClassInPackage."+pkg)
 permission.
 
 If this method is overridden, then
 super.checkPackageDefinition should be called
 as the first line in the overridden method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkPackageDefinition_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkPermission_$_$Ljava_security_Permission$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the requested
 access, specified by the given permission, is not permitted based
 on the security policy currently in effect.
 
 This method calls AccessController.checkPermission 
 with the given permission.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkPermission_$_$Ljava_security_Permission$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkPermission_$_$Ljava_security_Permission$$$_Ljava_lang_Object$$$_$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Throws a SecurityException if the
 specified security context is denied access to the resource
 specified by the given permission.
 The context must be a security 
 context returned by a previous call to 
 getSecurityContext and the access control
 decision is based upon the configured security policy for
 that security context.
 
 If context is an instance of 
 AccessControlContext then the
 AccessControlContext.checkPermission method is
 invoked with the specified permission.
 
 If context is not an instance of 
 AccessControlContext then a
 SecurityException is thrown.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkPermission_$_$Ljava_security_Permission$$$_Ljava_lang_Object$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkPrintJobAccess_$_$$V = function (arg0) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to initiate a print job request.
 
 This method calls
 checkPermission with the
 RuntimePermission("queuePrintJob") permission.
 
 If you override this method, then you should make a call to 
 super.checkPrintJobAccess
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkPrintJobAccess_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkPropertiesAccess_$_$$V = function (arg0) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to access or modify the system 
 properties. 
 
 This method is used by the getProperties and 
 setProperties methods of class System. 
 
 This method calls checkPermission with the
 PropertyPermission("*", "read,write") permission.
 
 If you override this method, then you should make a call to 
 super.checkPropertiesAccess
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkPropertiesAccess_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkPropertyAccess_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to access the system property with 
 the specified key name. 
 
 This method is used by the getProperty method of 
 class System. 
 
 This method calls checkPermission with the
 PropertyPermission(key, "read") permission.
 
 
 If you override this method, then you should make a call to 
 super.checkPropertyAccess
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkPropertyAccess_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkRead_$_$Ljava_io_FileDescriptor$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to read from the specified file 
 descriptor. 
 
 This method calls checkPermission with the
 RuntimePermission("readFileDescriptor")
 permission.
 
 If you override this method, then you should make a call to 
 super.checkRead
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkRead_$_$Ljava_io_FileDescriptor$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkRead_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to read the file specified by the 
 string argument. 
 
 This method calls checkPermission with the
 FilePermission(file,"read") permission.
 
 If you override this method, then you should make a call to 
 super.checkRead
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkRead_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkRead_$_$Ljava_lang_String$$$_Ljava_lang_Object$$$_$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Throws a SecurityException if the 
 specified security context is not allowed to read the file 
 specified by the string argument. The context must be a security 
 context returned by a previous call to 
 getSecurityContext. 
  If context is an instance of 
 AccessControlContext then the
 AccessControlContext.checkPermission method will
 be invoked with the FilePermission(file,"read") permission.
  If context is not an instance of 
 AccessControlContext then a
 SecurityException is thrown. 
 
 If you override this method, then you should make a call to 
 super.checkRead
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkRead_$_$Ljava_lang_String$$$_Ljava_lang_Object$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkSecurityAccess_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Determines whether the permission with the specified permission target
 name should be granted or denied.

  If the requested permission is allowed, this method returns
 quietly. If denied, a SecurityException is raised. 

  This method creates a SecurityPermission object for
 the given permission target name and calls checkPermission
 with it.

  See the documentation for
 SecurityPermission for
 a list of possible permission target names.
 
  If you override this method, then you should make a call to 
 super.checkSecurityAccess
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkSecurityAccess_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkSetFactory_$_$$V = function (arg0) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to set the socket factory used by 
 ServerSocket or Socket, or the stream 
 handler factory used by URL. 
 
 This method calls checkPermission with the
 RuntimePermission("setFactory") permission.
 
 If you override this method, then you should make a call to 
 super.checkSetFactory
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkSetFactory_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkSystemClipboardAccess_$_$$V = function (arg0) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to access the system clipboard.
 
 This method calls checkPermission with the
 AWTPermission("accessClipboard") 
 permission.
 
 If you override this method, then you should make a call to 
 super.checkSystemClipboardAccess
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkSystemClipboardAccess_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkTopLevelWindow_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Returns false if the calling 
 thread is not trusted to bring up the top-level window indicated 
 by the window argument. In this case, the caller can 
 still decide to show the window, but the window should include 
 some sort of visual warning. If the method returns 
 true, then the window can be shown without any 
 special restrictions. 
 
 See class Window for more information on trusted and 
 untrusted windows. 
 
 This method calls
 checkPermission with the
 AWTPermission("showWindowWithoutWarningBanner") permission,
 and returns true if a SecurityException is not thrown,
 otherwise it returns false.
 
 If you override this method, then you should make a call to 
 super.checkTopLevelWindow
 at the point the overridden method would normally return 
 false, and the value of 
 super.checkTopLevelWindow should
 be returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkTopLevelWindow_$_$Ljava_lang_Object$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_SecurityManager_$_checkWrite_$_$Ljava_io_FileDescriptor$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to write to the specified file 
 descriptor. 
 
 This method calls checkPermission with the
 RuntimePermission("writeFileDescriptor")
 permission.
 
 If you override this method, then you should make a call to 
 super.checkWrite
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkWrite_$_$Ljava_io_FileDescriptor$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_checkWrite_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Throws a SecurityException if the 
 calling thread is not allowed to write to the file specified by 
 the string argument. 
 
 This method calls checkPermission with the
 FilePermission(file,"write") permission.
 
 If you override this method, then you should make a call to 
 super.checkWrite
 at the point the overridden method would normally throw an
 exception.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_checkWrite_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_SecurityManager_$_classDepth_$_$Ljava_lang_String$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Deprecated. This type of security checking is not recommended.
  It is recommended that the checkPermission
  call be used instead.

Parameters:

name -> the fully qualified name of the class to search for.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_classDepth_$_$Ljava_lang_String$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_SecurityManager_$_classLoaderDepth_$_$$I = function (arg0) {
/*implement method!*/
/*
Deprecated. This type of security checking is not recommended.
  It is recommended that the checkPermission
  call be used instead.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_classLoaderDepth_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_SecurityManager_$_currentClassLoader_$_$$Ljava_lang_ClassLoader$$$_ = function (arg0) {
/*implement method!*/
/*
Deprecated. This type of security checking is not recommended.
  It is recommended that the checkPermission
  call be used instead.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_currentClassLoader_$_$$Ljava_lang_ClassLoader$$$_"));
throw ex;
//must have a return of type ClassLoader
};
var java_lang_SecurityManager_$_currentLoadedClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*implement method!*/
/*
Deprecated. This type of security checking is not recommended.
  It is recommended that the checkPermission
  call be used instead.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_currentLoadedClass_$_$$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_lang_SecurityManager_$_getClassContext_$_$$_$$$$$_Ljava_lang_Class$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the current execution stack as an array of classes. 
 
 The length of the array is the number of methods on the execution 
 stack. The element at index 0 is the class of the 
 currently executing method, the element at index 1 is 
 the class of that method's caller, and so on.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_getClassContext_$_$$_$$$$$_Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class[]
};
var java_lang_SecurityManager_$_getInCheck_$_$$Z = function (arg0) {
/*implement method!*/
/*
Deprecated. This type of security checking is not recommended.
  It is recommended that the checkPermission
  call be used instead.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_getInCheck_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_SecurityManager_$_getSecurityContext_$_$$Ljava_lang_Object$$$_ = function (arg0) {
/*implement method!*/
/*
Creates an object that encapsulates the current execution 
 environment. The result of this method is used, for example, by the 
 three-argument checkConnect method and by the 
 two-argument checkRead method. 
 These methods are needed because a trusted method may be called 
 on to read a file or open a socket on behalf of another method. 
 The trusted method needs to determine if the other (possibly 
 untrusted) method would be allowed to perform the operation on its 
 own. 
  The default implementation of this method is to return 
 an AccessControlContext object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_getSecurityContext_$_$$Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object
};
var java_lang_SecurityManager_$_getThreadGroup_$_$$Ljava_lang_ThreadGroup$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the thread group into which to instantiate any new
 thread being created at the time this is being called.
 By default, it returns the thread group of the current
 thread. This should be overridden by a specific security
 manager to return the appropriate thread group.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_getThreadGroup_$_$$Ljava_lang_ThreadGroup$$$_"));
throw ex;
//must have a return of type ThreadGroup
};
var java_lang_SecurityManager_$_inClassLoader_$_$$Z = function (arg0) {
/*implement method!*/
/*
Deprecated. This type of security checking is not recommended.
  It is recommended that the checkPermission
  call be used instead.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_inClassLoader_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_SecurityManager_$_inClass_$_$Ljava_lang_String$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Deprecated. This type of security checking is not recommended.
  It is recommended that the checkPermission
  call be used instead.

Parameters:

name -> the fully qualified name of the class.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_SecurityManager_$_inClass_$_$Ljava_lang_String$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_SecurityManager_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_lang_SecurityManager_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_SecurityManager_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_lang_SecurityManager_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_lang_SecurityManager_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_lang_SecurityManager_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var java_lang_SecurityManager_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_SecurityManager_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_lang_SecurityManager_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_lang_SecurityManager.prototype._$$$init$$$__$_$$V = java_lang_SecurityManager_$__$$$init$$$__$_$$V;
java_lang_SecurityManager.prototype.checkAccept_$_$Ljava_lang_String$$$_I$V = java_lang_SecurityManager_$_checkAccept_$_$Ljava_lang_String$$$_I$V;
java_lang_SecurityManager.prototype.checkAccess_$_$Ljava_lang_Thread$$$_$V = java_lang_SecurityManager_$_checkAccess_$_$Ljava_lang_Thread$$$_$V;
java_lang_SecurityManager.prototype.checkAccess_$_$Ljava_lang_ThreadGroup$$$_$V = java_lang_SecurityManager_$_checkAccess_$_$Ljava_lang_ThreadGroup$$$_$V;
java_lang_SecurityManager.prototype.checkAwtEventQueueAccess_$_$$V = java_lang_SecurityManager_$_checkAwtEventQueueAccess_$_$$V;
java_lang_SecurityManager.prototype.checkConnect_$_$Ljava_lang_String$$$_I$V = java_lang_SecurityManager_$_checkConnect_$_$Ljava_lang_String$$$_I$V;
java_lang_SecurityManager.prototype.checkConnect_$_$Ljava_lang_String$$$_ILjava_lang_Object$$$_$V = java_lang_SecurityManager_$_checkConnect_$_$Ljava_lang_String$$$_ILjava_lang_Object$$$_$V;
java_lang_SecurityManager.prototype.checkCreateClassLoader_$_$$V = java_lang_SecurityManager_$_checkCreateClassLoader_$_$$V;
java_lang_SecurityManager.prototype.checkDelete_$_$Ljava_lang_String$$$_$V = java_lang_SecurityManager_$_checkDelete_$_$Ljava_lang_String$$$_$V;
java_lang_SecurityManager.prototype.checkExec_$_$Ljava_lang_String$$$_$V = java_lang_SecurityManager_$_checkExec_$_$Ljava_lang_String$$$_$V;
java_lang_SecurityManager.prototype.checkExit_$_$I$V = java_lang_SecurityManager_$_checkExit_$_$I$V;
java_lang_SecurityManager.prototype.checkLink_$_$Ljava_lang_String$$$_$V = java_lang_SecurityManager_$_checkLink_$_$Ljava_lang_String$$$_$V;
java_lang_SecurityManager.prototype.checkListen_$_$I$V = java_lang_SecurityManager_$_checkListen_$_$I$V;
java_lang_SecurityManager.prototype.checkMemberAccess_$_$Ljava_lang_Class$$$_I$V = java_lang_SecurityManager_$_checkMemberAccess_$_$Ljava_lang_Class$$$_I$V;
java_lang_SecurityManager.prototype.checkMulticast_$_$Ljava_net_InetAddress$$$_$V = java_lang_SecurityManager_$_checkMulticast_$_$Ljava_net_InetAddress$$$_$V;
java_lang_SecurityManager.prototype.checkMulticast_$_$Ljava_net_InetAddress$$$_B$V = java_lang_SecurityManager_$_checkMulticast_$_$Ljava_net_InetAddress$$$_B$V;
java_lang_SecurityManager.prototype.checkPackageAccess_$_$Ljava_lang_String$$$_$V = java_lang_SecurityManager_$_checkPackageAccess_$_$Ljava_lang_String$$$_$V;
java_lang_SecurityManager.prototype.checkPackageDefinition_$_$Ljava_lang_String$$$_$V = java_lang_SecurityManager_$_checkPackageDefinition_$_$Ljava_lang_String$$$_$V;
java_lang_SecurityManager.prototype.checkPermission_$_$Ljava_security_Permission$$$_$V = java_lang_SecurityManager_$_checkPermission_$_$Ljava_security_Permission$$$_$V;
java_lang_SecurityManager.prototype.checkPermission_$_$Ljava_security_Permission$$$_Ljava_lang_Object$$$_$V = java_lang_SecurityManager_$_checkPermission_$_$Ljava_security_Permission$$$_Ljava_lang_Object$$$_$V;
java_lang_SecurityManager.prototype.checkPrintJobAccess_$_$$V = java_lang_SecurityManager_$_checkPrintJobAccess_$_$$V;
java_lang_SecurityManager.prototype.checkPropertiesAccess_$_$$V = java_lang_SecurityManager_$_checkPropertiesAccess_$_$$V;
java_lang_SecurityManager.prototype.checkPropertyAccess_$_$Ljava_lang_String$$$_$V = java_lang_SecurityManager_$_checkPropertyAccess_$_$Ljava_lang_String$$$_$V;
java_lang_SecurityManager.prototype.checkRead_$_$Ljava_io_FileDescriptor$$$_$V = java_lang_SecurityManager_$_checkRead_$_$Ljava_io_FileDescriptor$$$_$V;
java_lang_SecurityManager.prototype.checkRead_$_$Ljava_lang_String$$$_$V = java_lang_SecurityManager_$_checkRead_$_$Ljava_lang_String$$$_$V;
java_lang_SecurityManager.prototype.checkRead_$_$Ljava_lang_String$$$_Ljava_lang_Object$$$_$V = java_lang_SecurityManager_$_checkRead_$_$Ljava_lang_String$$$_Ljava_lang_Object$$$_$V;
java_lang_SecurityManager.prototype.checkSecurityAccess_$_$Ljava_lang_String$$$_$V = java_lang_SecurityManager_$_checkSecurityAccess_$_$Ljava_lang_String$$$_$V;
java_lang_SecurityManager.prototype.checkSetFactory_$_$$V = java_lang_SecurityManager_$_checkSetFactory_$_$$V;
java_lang_SecurityManager.prototype.checkSystemClipboardAccess_$_$$V = java_lang_SecurityManager_$_checkSystemClipboardAccess_$_$$V;
java_lang_SecurityManager.prototype.checkTopLevelWindow_$_$Ljava_lang_Object$$$_$Z = java_lang_SecurityManager_$_checkTopLevelWindow_$_$Ljava_lang_Object$$$_$Z;
java_lang_SecurityManager.prototype.checkWrite_$_$Ljava_io_FileDescriptor$$$_$V = java_lang_SecurityManager_$_checkWrite_$_$Ljava_io_FileDescriptor$$$_$V;
java_lang_SecurityManager.prototype.checkWrite_$_$Ljava_lang_String$$$_$V = java_lang_SecurityManager_$_checkWrite_$_$Ljava_lang_String$$$_$V;
java_lang_SecurityManager.prototype.classDepth_$_$Ljava_lang_String$$$_$I = java_lang_SecurityManager_$_classDepth_$_$Ljava_lang_String$$$_$I;
java_lang_SecurityManager.prototype.classLoaderDepth_$_$$I = java_lang_SecurityManager_$_classLoaderDepth_$_$$I;
java_lang_SecurityManager.prototype.currentClassLoader_$_$$Ljava_lang_ClassLoader$$$_ = java_lang_SecurityManager_$_currentClassLoader_$_$$Ljava_lang_ClassLoader$$$_;
java_lang_SecurityManager.prototype.currentLoadedClass_$_$$Ljava_lang_Class$$$_ = java_lang_SecurityManager_$_currentLoadedClass_$_$$Ljava_lang_Class$$$_;
java_lang_SecurityManager.prototype.getClassContext_$_$$_$$$$$_Ljava_lang_Class$$$_ = java_lang_SecurityManager_$_getClassContext_$_$$_$$$$$_Ljava_lang_Class$$$_;
java_lang_SecurityManager.prototype.getInCheck_$_$$Z = java_lang_SecurityManager_$_getInCheck_$_$$Z;
java_lang_SecurityManager.prototype.getSecurityContext_$_$$Ljava_lang_Object$$$_ = java_lang_SecurityManager_$_getSecurityContext_$_$$Ljava_lang_Object$$$_;
java_lang_SecurityManager.prototype.getThreadGroup_$_$$Ljava_lang_ThreadGroup$$$_ = java_lang_SecurityManager_$_getThreadGroup_$_$$Ljava_lang_ThreadGroup$$$_;
java_lang_SecurityManager.prototype.inClassLoader_$_$$Z = java_lang_SecurityManager_$_inClassLoader_$_$$Z;
java_lang_SecurityManager.prototype.inClass_$_$Ljava_lang_String$$$_$Z = java_lang_SecurityManager_$_inClass_$_$Ljava_lang_String$$$_$Z;
java_lang_SecurityManager.prototype.wait_$_$J$V = java_lang_SecurityManager_$_wait_$_$J$V;
java_lang_SecurityManager.prototype.wait_$_$JI$V = java_lang_SecurityManager_$_wait_$_$JI$V;
java_lang_SecurityManager.prototype.wait_$_$$V = java_lang_SecurityManager_$_wait_$_$$V;
java_lang_SecurityManager.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_SecurityManager_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_SecurityManager.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_SecurityManager_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_SecurityManager.prototype.hashCode_$_$$I = java_lang_SecurityManager_$_hashCode_$_$$I;
java_lang_SecurityManager.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_SecurityManager_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_SecurityManager.prototype.notify_$_$$V = java_lang_SecurityManager_$_notify_$_$$V;
java_lang_SecurityManager.prototype.notifyAll_$_$$V = java_lang_SecurityManager_$_notifyAll_$_$$V;
