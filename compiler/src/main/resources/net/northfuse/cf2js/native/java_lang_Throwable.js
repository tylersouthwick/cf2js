/** @constructor */
function java_lang_Throwable() {}
var java_lang_Throwable_$__$$$init$$$__$_$$V = function (arg0) {
/*implement constructor!*/
/*
Constructs a new throwable with null as its detail message.
 The cause is not initialized, and may subsequently be initialized by a
 call to initCause(java.lang.Throwable).

 The fillInStackTrace() method is called to initialize
 the stack trace data in the newly created throwable.

*/
    arg0.message = null;
    arg0.cause = null;
};
var java_lang_Throwable_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a new throwable with the specified detail message.  The
 cause is not initialized, and may subsequently be initialized by
 a call to initCause(java.lang.Throwable).

 The fillInStackTrace() method is called to initialize
 the stack trace data in the newly created throwable.


Parameters:message - the detail message. The detail message is saved for
          later retrieval by the getMessage() method.

*/
    arg0.message = arg1;
    arg0.cause = null;
};
var java_lang_Throwable_$__$$$init$$$__$_$Ljava_lang_String$$$_Ljava_lang_Throwable$$$_$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Constructs a new throwable with the specified detail message and
 cause.  Note that the detail message associated with
 cause is not automatically incorporated in
 this throwable's detail message.

 The fillInStackTrace() method is called to initialize
 the stack trace data in the newly created throwable.


Parameters:message - the detail message (which is saved for later retrieval
         by the getMessage() method).cause - the cause (which is saved for later retrieval by the
         getCause() method).  (A null value is
         permitted, and indicates that the cause is nonexistent or
         unknown.)Since:
  1.4

*/
    arg0.message = arg1;
    arg0.cause = arg2;
};
var java_lang_Throwable_$__$$$init$$$__$_$Ljava_lang_Throwable$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a new throwable with the specified cause and a detail
 message of (cause==null ? null : cause.toString()) (which
 typically contains the class and detail message of cause).
 This constructor is useful for throwables that are little more than
 wrappers for other throwables (for example, PrivilegedActionException).

 The fillInStackTrace() method is called to initialize
 the stack trace data in the newly created throwable.


Parameters:cause - the cause (which is saved for later retrieval by the
         getCause() method).  (A null value is
         permitted, and indicates that the cause is nonexistent or
         unknown.)Since:
  1.4

*/
    arg0.message = null;
    arg0.cause = arg1;
};
var java_lang_Throwable_$_fillInStackTrace_$_$$Ljava_lang_Throwable$$$_ = function (arg0) {
/*implement method!*/
/*
Fills in the execution stack trace. This method records within this
 Throwable object information about the current state of
 the stack frames for the current thread.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Throwable_$_fillInStackTrace_$_$$Ljava_lang_Throwable$$$_"));
throw ex;
//must have a return of type Throwable
};
var java_lang_Throwable_$_getCause_$_$$Ljava_lang_Throwable$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the cause of this throwable or null if the
 cause is nonexistent or unknown.  (The cause is the throwable that
 caused this throwable to get thrown.)

 This implementation returns the cause that was supplied via one of
 the constructors requiring a Throwable, or that was set after
 creation with the initCause(Throwable) method.  While it is
 typically unnecessary to override this method, a subclass can override
 it to return a cause set by some other means.  This is appropriate for
 a "legacy chained throwable" that predates the addition of chained
 exceptions to Throwable.  Note that it is not
 necessary to override any of the PrintStackTrace methods,
 all of which invoke the getCause method to determine the
 cause of a throwable.

*/
//must have a return of type Throwable
    return arg0.cause;
};
var java_lang_Throwable_$_getLocalizedMessage_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Creates a localized description of this throwable.
 Subclasses may override this method in order to produce a
 locale-specific message.  For subclasses that do not override this
 method, the default implementation returns the same result as
 getMessage().

*/
//must have a return of type String
    return arg0.message;
};
var java_lang_Throwable_$_getMessage_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the detail message string of this throwable.

*/
//must have a return of type String
    return arg0.message;
};
var java_lang_Throwable_$_getStackTrace_$_$$_$$$$$_Ljava_lang_StackTraceElement$$$_ = function (arg0) {
/*implement method!*/
/*
Provides programmatic access to the stack trace information printed by
 printStackTrace().  Returns an array of stack trace elements,
 each representing one stack frame.  The zeroth element of the array
 (assuming the array's length is non-zero) represents the top of the
 stack, which is the last method invocation in the sequence.  Typically,
 this is the point at which this throwable was created and thrown.
 The last element of the array (assuming the array's length is non-zero)
 represents the bottom of the stack, which is the first method invocation
 in the sequence.

 Some virtual machines may, under some circumstances, omit one
 or more stack frames from the stack trace.  In the extreme case,
 a virtual machine that has no stack trace information concerning
 this throwable is permitted to return a zero-length array from this
 method.  Generally speaking, the array returned by this method will
 contain one element for every frame that would be printed by
 printStackTrace.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Throwable_$_getStackTrace_$_$$_$$$$$_Ljava_lang_StackTraceElement$$$_"));
throw ex;
//must have a return of type StackTraceElement[]
};
var java_lang_Throwable_$_initCause_$_$Ljava_lang_Throwable$$$_$Ljava_lang_Throwable$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Initializes the cause of this throwable to the specified value.
 (The cause is the throwable that caused this throwable to get thrown.)

 This method can be called at most once.  It is generally called from
 within the constructor, or immediately after creating the
 throwable.  If this throwable was created
 with Throwable(Throwable) or
 Throwable(String,Throwable), this method cannot be called
 even once.

Parameters:

cause -> the cause (which is saved for later retrieval by the
         getCause() method).  (A null value is
         permitted, and indicates that the cause is nonexistent or
         unknown.)

*/
//must have a return of type Throwable
    arg0.cause = arg1;
	return arg1;
};
var java_lang_Throwable_$_printStackTrace_$_$$V = function (arg0) {
/*implement method!*/
/*
Prints this throwable and its backtrace to the
 standard error stream. This method prints a stack trace for this
 Throwable object on the error output stream that is
 the value of the field System.err. The first line of
 output contains the result of the toString() method for
 this object.  Remaining lines represent data previously recorded by
 the method fillInStackTrace(). The format of this
 information depends on the implementation, but the following
 example may be regarded as typical:
  java.lang.NullPointerException
         at MyClass.mash(MyClass.java:9)
         at MyClass.crunch(MyClass.java:6)
         at MyClass.main(MyClass.java:3)
 
 This example was produced by running the program:
  class MyClass {
     public static void main(String[] args) {
         crunch(null);
     }
     static void crunch(int[] a) {
         mash(a);
     }
     static void mash(int[] b) {
         System.out.println(b[0]);
     }
 }
 
 The backtrace for a throwable with an initialized, non-null cause
 should generally include the backtrace for the cause.  The format
 of this information depends on the implementation, but the following
 example may be regarded as typical:
  HighLevelException: MidLevelException: LowLevelException
         at Junk.a(Junk.java:13)
         at Junk.main(Junk.java:4)
 Caused by: MidLevelException: LowLevelException
         at Junk.c(Junk.java:23)
         at Junk.b(Junk.java:17)
         at Junk.a(Junk.java:11)
         ... 1 more
 Caused by: LowLevelException
         at Junk.e(Junk.java:30)
         at Junk.d(Junk.java:27)
         at Junk.c(Junk.java:21)
         ... 3 more
 
 Note the presence of lines containing the characters "...".
 These lines indicate that the remainder of the stack trace for this
 exception matches the indicated number of frames from the bottom of the
 stack trace of the exception that was caused by this exception (the
 "enclosing" exception).  This shorthand can greatly reduce the length
 of the output in the common case where a wrapped exception is thrown
 from same method as the "causative exception" is caught.  The above
 example was produced by running the program:
  public class Junk {
     public static void main(String args[]) {
         try {
             a();
         } catch(HighLevelException e) {
             e.printStackTrace();
         }
     }
     static void a() throws HighLevelException {
         try {
             b();
         } catch(MidLevelException e) {
             throw new HighLevelException(e);
         }
     }
     static void b() throws MidLevelException {
         c();
     }
     static void c() throws MidLevelException {
         try {
             d();
         } catch(LowLevelException e) {
             throw new MidLevelException(e);
         }
     }
     static void d() throws LowLevelException {
        e();
     }
     static void e() throws LowLevelException {
         throw new LowLevelException();
     }
 }

 class HighLevelException extends Exception {
     HighLevelException(Throwable cause) { super(cause); }
 }

 class MidLevelException extends Exception {
     MidLevelException(Throwable cause)  { super(cause); }
 }

 class LowLevelException extends Exception {
 }

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Throwable_$_printStackTrace_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Throwable_$_printStackTrace_$_$Ljava_io_PrintStream$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Prints this throwable and its backtrace to the specified print stream.

Parameters:

s -> PrintStream to use for output

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Throwable_$_printStackTrace_$_$Ljava_io_PrintStream$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Throwable_$_printStackTrace_$_$Ljava_io_PrintWriter$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Prints this throwable and its backtrace to the specified
 print writer.

Parameters:

s -> PrintWriter to use for output

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Throwable_$_printStackTrace_$_$Ljava_io_PrintWriter$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Throwable_$_setStackTrace_$_$_$$$$$_Ljava_lang_StackTraceElement$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Throwable_$_setStackTrace_$_$_$$$$$_Ljava_lang_StackTraceElement$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Throwable_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a short description of this throwable.
 The result is the concatenation of:
 
  the name of the class of this object
  ": " (a colon and a space)
  the result of invoking this object's getLocalizedMessage()
      method
 
 If getLocalizedMessage returns null, then just
 the class name is returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Throwable_$_toString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Throwable_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_lang_Throwable_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_Throwable_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_lang_Throwable_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_lang_Throwable_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var java_lang_Throwable_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_Throwable_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_lang_Throwable_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_lang_Throwable.prototype._$$$init$$$__$_$$V = java_lang_Throwable_$__$$$init$$$__$_$$V;
java_lang_Throwable.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_lang_Throwable_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_lang_Throwable.prototype._$$$init$$$__$_$Ljava_lang_String$$$_Ljava_lang_Throwable$$$_$V = java_lang_Throwable_$__$$$init$$$__$_$Ljava_lang_String$$$_Ljava_lang_Throwable$$$_$V;
java_lang_Throwable.prototype._$$$init$$$__$_$Ljava_lang_Throwable$$$_$V = java_lang_Throwable_$__$$$init$$$__$_$Ljava_lang_Throwable$$$_$V;
java_lang_Throwable.prototype.fillInStackTrace_$_$$Ljava_lang_Throwable$$$_ = java_lang_Throwable_$_fillInStackTrace_$_$$Ljava_lang_Throwable$$$_;
java_lang_Throwable.prototype.getCause_$_$$Ljava_lang_Throwable$$$_ = java_lang_Throwable_$_getCause_$_$$Ljava_lang_Throwable$$$_;
java_lang_Throwable.prototype.getLocalizedMessage_$_$$Ljava_lang_String$$$_ = java_lang_Throwable_$_getLocalizedMessage_$_$$Ljava_lang_String$$$_;
java_lang_Throwable.prototype.getMessage_$_$$Ljava_lang_String$$$_ = java_lang_Throwable_$_getMessage_$_$$Ljava_lang_String$$$_;
java_lang_Throwable.prototype.getStackTrace_$_$$_$$$$$_Ljava_lang_StackTraceElement$$$_ = java_lang_Throwable_$_getStackTrace_$_$$_$$$$$_Ljava_lang_StackTraceElement$$$_;
java_lang_Throwable.prototype.initCause_$_$Ljava_lang_Throwable$$$_$Ljava_lang_Throwable$$$_ = java_lang_Throwable_$_initCause_$_$Ljava_lang_Throwable$$$_$Ljava_lang_Throwable$$$_;
java_lang_Throwable.prototype.printStackTrace_$_$$V = java_lang_Throwable_$_printStackTrace_$_$$V;
java_lang_Throwable.prototype.printStackTrace_$_$Ljava_io_PrintStream$$$_$V = java_lang_Throwable_$_printStackTrace_$_$Ljava_io_PrintStream$$$_$V;
java_lang_Throwable.prototype.printStackTrace_$_$Ljava_io_PrintWriter$$$_$V = java_lang_Throwable_$_printStackTrace_$_$Ljava_io_PrintWriter$$$_$V;
java_lang_Throwable.prototype.setStackTrace_$_$_$$$$$_Ljava_lang_StackTraceElement$$$_$V = java_lang_Throwable_$_setStackTrace_$_$_$$$$$_Ljava_lang_StackTraceElement$$$_$V;
java_lang_Throwable.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_Throwable_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_Throwable.prototype.wait_$_$J$V = java_lang_Throwable_$_wait_$_$J$V;
java_lang_Throwable.prototype.wait_$_$JI$V = java_lang_Throwable_$_wait_$_$JI$V;
java_lang_Throwable.prototype.wait_$_$$V = java_lang_Throwable_$_wait_$_$$V;
java_lang_Throwable.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_Throwable_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_Throwable.prototype.hashCode_$_$$I = java_lang_Throwable_$_hashCode_$_$$I;
java_lang_Throwable.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_Throwable_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_Throwable.prototype.notify_$_$$V = java_lang_Throwable_$_notify_$_$$V;
java_lang_Throwable.prototype.notifyAll_$_$$V = java_lang_Throwable_$_notifyAll_$_$$V;
