/** @constructor */
function java_lang_System() {}
var java_lang_System_$_arraycopy_$_$Ljava_lang_Object$$$_ILjava_lang_Object$$$_II$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*
Copies an array from the specified source array, beginning at the
 specified position, to the specified position of the destination array.
 A subsequence of array components are copied from the source
 array referenced by src to the destination array
 referenced by dest. The number of components copied is
 equal to the length argument. The components at
 positions srcPos through
 srcPos+length-1 in the source array are copied into
 positions destPos through
 destPos+length-1, respectively, of the destination
 array.
 
 If the src and dest arguments refer to the
 same array object, then the copying is performed as if the
 components at positions srcPos through
 srcPos+length-1 were first copied to a temporary
 array with length components and then the contents of
 the temporary array were copied into positions
 destPos through destPos+length-1 of the
 destination array.
 
 If dest is null, then a
 NullPointerException is thrown.
 
 If src is null, then a
 NullPointerException is thrown and the destination
 array is not modified.
 
 Otherwise, if any of the following is true, an
 ArrayStoreException is thrown and the destination is
 not modified:
 
 The src argument refers to an object that is not an
     array.
 The dest argument refers to an object that is not an
     array.
 The src argument and dest argument refer
     to arrays whose component types are different primitive types.
 The src argument refers to an array with a primitive
    component type and the dest argument refers to an array
     with a reference component type.
 The src argument refers to an array with a reference
    component type and the dest argument refers to an array
     with a primitive component type.
 
 
 Otherwise, if any of the following is true, an
 IndexOutOfBoundsException is
 thrown and the destination is not modified:
 
 The srcPos argument is negative.
 The destPos argument is negative.
 The length argument is negative.
 srcPos+length is greater than
     src.length, the length of the source array.
 destPos+length is greater than
     dest.length, the length of the destination array.
 
 
 Otherwise, if any actual component of the source array from
 position srcPos through
 srcPos+length-1 cannot be converted to the component
 type of the destination array by assignment conversion, an
 ArrayStoreException is thrown. In this case, let
 k be the smallest nonnegative integer less than
 length such that src[srcPos+k]
 cannot be converted to the component type of the destination
 array; when the exception is thrown, source array components from
 positions srcPos through
 srcPos+k-1
 will already have been copied to destination array positions
 destPos through
 destPos+k-1 and no other
 positions of the destination array will have been modified.
 (Because of the restrictions already itemized, this
 paragraph effectively applies only to the situation where both
 arrays have component types that are reference types.)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_arraycopy_$_$Ljava_lang_Object$$$_ILjava_lang_Object$$$_II$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_clearProperty_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Removes the system property indicated by the specified key. 
 
 First, if a security manager exists, its 
 SecurityManager.checkPermission method
 is called with a PropertyPermission(key, "write")
 permission. This may result in a SecurityException being thrown.
 If no exception is thrown, the specified property is removed.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_clearProperty_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_System_$_console_$_$$Ljava_io_Console$$$_ = function () {
/*implement method!*/
/*
Returns the unique Console object associated
 with the current Java virtual machine, if any.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_console_$_$$Ljava_io_Console$$$_"));
throw ex;
//must have a return of type Console
};
var java_lang_System_$_currentTimeMillis_$_$$J = function () {
/*implement method!*/
/*
Returns the current time in milliseconds.  Note that
 while the unit of time of the return value is a millisecond,
 the granularity of the value depends on the underlying
 operating system and may be larger.  For example, many
 operating systems measure time in units of tens of
 milliseconds.

  See the description of the class Date for
 a discussion of slight discrepancies that may arise between
 "computer time" and coordinated universal time (UTC).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_currentTimeMillis_$_$$J"));
throw ex;
//must have a return of type long
};
var java_lang_System_$_exit_$_$I$V = function (arg0) {
/*implement method!*/
/*
Terminates the currently running Java Virtual Machine. The
 argument serves as a status code; by convention, a nonzero status
 code indicates abnormal termination.
 
 This method calls the exit method in class
 Runtime. This method never returns normally.
 
 The call System.exit(n) is effectively equivalent to
 the call:
  Runtime.getRuntime().exit(n)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_exit_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_gc_$_$$V = function () {
/*implement method!*/
/*
Runs the garbage collector.
 
 Calling the gc method suggests that the Java Virtual
 Machine expend effort toward recycling unused objects in order to
 make the memory they currently occupy available for quick reuse.
 When control returns from the method call, the Java Virtual
 Machine has made a best effort to reclaim space from all discarded
 objects.
 
 The call System.gc() is effectively equivalent to the
 call:
  Runtime.getRuntime().gc()

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_gc_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_getCallerClass_$_$$Ljava_lang_Class$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_getCallerClass_$_$$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_lang_System_$_getProperties_$_$$Ljava_util_Properties$$$_ = function () {
/*implement method!*/
/*
Determines the current system properties.
 
 First, if there is a security manager, its
 checkPropertiesAccess method is called with no
 arguments. This may result in a security exception.
 
 The current set of system properties for use by the 
 getProperty(String) method is returned as a 
 Properties object. If there is no current set of 
 system properties, a set of system properties is first created and 
 initialized. This set of system properties always includes values 
 for the following keys: 
 
 Key
     Description of Associated Value
 java.version
     Java Runtime Environment version
 java.vendor
     Java Runtime Environment vendorjava.vendor.url
     Java vendor URL
 java.home
     Java installation directory
 java.vm.specification.version
     Java Virtual Machine specification version
 java.vm.specification.vendor
     Java Virtual Machine specification vendor
 java.vm.specification.name
     Java Virtual Machine specification name
 java.vm.version
     Java Virtual Machine implementation version
 java.vm.vendor
     Java Virtual Machine implementation vendor
 java.vm.name
     Java Virtual Machine implementation name
 java.specification.version
     Java Runtime Environment specification  version
 java.specification.vendor
     Java Runtime Environment specification  vendor
 java.specification.name
     Java Runtime Environment specification  name
 java.class.version
     Java class format version number
 java.class.path
     Java class path
 java.library.path
     List of paths to search when loading libraries
 java.io.tmpdir
     Default temp file path
 java.compiler
     Name of JIT compiler to use
 java.ext.dirs
     Path of extension directory or directories
 os.name
     Operating system name
 os.arch
     Operating system architecture
 os.version
     Operating system version
 file.separator
     File separator ("/" on UNIX)
 path.separator
     Path separator (":" on UNIX)
 line.separator
     Line separator ("\n" on UNIX)
 user.name
     User's account name
 user.home
     User's home directory
 user.dir
     User's current working directory
 
 
 Multiple paths in a system property value are separated by the path
 separator character of the platform.
 
 Note that even if the security manager does not permit the
 getProperties operation, it may choose to permit the
 getProperty(String) operation.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_getProperties_$_$$Ljava_util_Properties$$$_"));
throw ex;
//must have a return of type Properties
};
var java_lang_System_$_getProperty_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Gets the system property indicated by the specified key.
 
 First, if there is a security manager, its
 checkPropertyAccess method is called with the key as
 its argument. This may result in a SecurityException.
 
 If there is no current set of system properties, a set of system
 properties is first created and initialized in the same manner as
 for the getProperties method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_getProperty_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_System_$_getProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Gets the system property indicated by the specified key.
 
 First, if there is a security manager, its
 checkPropertyAccess method is called with the
 key as its argument.
 
 If there is no current set of system properties, a set of system
 properties is first created and initialized in the same manner as
 for the getProperties method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_getProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_System_$_getSecurityManager_$_$$Ljava_lang_SecurityManager$$$_ = function () {
/*implement method!*/
/*
Gets the system security interface.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_getSecurityManager_$_$$Ljava_lang_SecurityManager$$$_"));
throw ex;
//must have a return of type SecurityManager
};
var java_lang_System_$_getenv_$_$$Ljava_util_Map$$$_ = function () {
/*implement method!*/
/*
Returns an unmodifiable string map view of the current system environment.
 The environment is a system-dependent mapping from names to
 values which is passed from parent to child processes.

 If the system does not support environment variables, an
 empty map is returned.

 The returned map will never contain null keys or values.
 Attempting to query the presence of a null key or value will
 throw a NullPointerException.  Attempting to query
 the presence of a key or value which is not of type
 String will throw a ClassCastException.

 The returned map and its collection views may not obey the
 general contract of the Object.equals(java.lang.Object) and
 Object.hashCode() methods.

 The returned map is typically case-sensitive on all platforms.

 If a security manager exists, its
 checkPermission
 method is called with a
 RuntimePermission("getenv.*")
 permission.  This may result in a SecurityException being
 thrown.

 When passing information to a Java subprocess,
 system properties
 are generally preferred over environment variables.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_getenv_$_$$Ljava_util_Map$$$_"));
throw ex;
//must have a return of type Map
};
var java_lang_System_$_getenv_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Gets the value of the specified environment variable. An
 environment variable is a system-dependent external named
 value.

 If a security manager exists, its
 checkPermission
 method is called with a
 RuntimePermission("getenv."+name)
 permission.  This may result in a SecurityException
 being thrown.  If no exception is thrown the value of the
 variable name is returned.

 System
 properties and environment variables are both
 conceptually mappings between names and values.  Both
 mechanisms can be used to pass user-defined information to a
 Java process.  Environment variables have a more global effect,
 because they are visible to all descendants of the process
 which defines them, not just the immediate Java subprocess.
 They can have subtly different semantics, such as case
 insensitivity, on different operating systems.  For these
 reasons, environment variables are more likely to have
 unintended side effects.  It is best to use system properties
 where possible.  Environment variables should be used when a
 global effect is desired, or when an external system interface
 requires an environment variable (such as PATH).

 On UNIX systems the alphabetic case of name is
 typically significant, while on Microsoft Windows systems it is
 typically not.  For example, the expression
 System.getenv("FOO").equals(System.getenv("foo"))
 is likely to be true on Microsoft Windows.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_getenv_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_System_$_identityHashCode_$_$Ljava_lang_Object$$$_$I = function (arg0) {
/*implement method!*/
/*
Returns the same hash code for the given object as
 would be returned by the default method hashCode(),
 whether or not the given object's class overrides
 hashCode().
 The hash code for the null reference is zero.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_identityHashCode_$_$Ljava_lang_Object$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_System_$_inheritedChannel_$_$$Ljava_nio_channels_Channel$$$_ = function () {
/*implement method!*/
/*
Returns the channel inherited from the entity that created this
 Java virtual machine.

  This method returns the channel obtained by invoking the
 inheritedChannel method of the system-wide default
 SelectorProvider object. 

  In addition to the network-oriented channels described in
 inheritedChannel, this method may return other kinds of
 channels in the future.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_inheritedChannel_$_$$Ljava_nio_channels_Channel$$$_"));
throw ex;
//must have a return of type Channel
};
var java_lang_System_$_loadLibrary_$_$Ljava_lang_String$$$_$V = function (arg0) {
/*implement method!*/
/*
Loads the system library specified by the libname
 argument. The manner in which a library name is mapped to the
 actual system library is system dependent.
 
 The call System.loadLibrary(name) is effectively
 equivalent to the call
  Runtime.getRuntime().loadLibrary(name)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_loadLibrary_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_load_$_$Ljava_lang_String$$$_$V = function (arg0) {
/*implement method!*/
/*
Loads a code file with the specified filename from the local file
 system as a dynamic library. The filename
 argument must be a complete path name.
 
 The call System.load(name) is effectively equivalent
 to the call:
  Runtime.getRuntime().load(name)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_load_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_mapLibraryName_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Maps a library name into a platform-specific string representing
 a native library.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_mapLibraryName_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_System_$_nanoTime_$_$$J = function () {
/*implement method!*/
/*
Returns the current value of the most precise available system
 timer, in nanoseconds.

 This method can only be used to measure elapsed time and is
 not related to any other notion of system or wall-clock time.
 The value returned represents nanoseconds since some fixed but
 arbitrary time (perhaps in the future, so values may be
 negative).  This method provides nanosecond precision, but not
 necessarily nanosecond accuracy. No guarantees are made about
 how frequently values change. Differences in successive calls
 that span greater than approximately 292 years (263
 nanoseconds) will not accurately compute elapsed time due to
 numerical overflow.

  For example, to measure how long some code takes to execute:
    long startTime = System.nanoTime();
   // ... the code being measured ...
   long estimatedTime = System.nanoTime() - startTime;

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_nanoTime_$_$$J"));
throw ex;
//must have a return of type long
};
var java_lang_System_$_runFinalization_$_$$V = function () {
/*implement method!*/
/*
Runs the finalization methods of any objects pending finalization.
 
 Calling this method suggests that the Java Virtual Machine expend
 effort toward running the finalize methods of objects
 that have been found to be discarded but whose finalize
 methods have not yet been run. When control returns from the
 method call, the Java Virtual Machine has made a best effort to
 complete all outstanding finalizations.
 
 The call System.runFinalization() is effectively
 equivalent to the call:
  Runtime.getRuntime().runFinalization()

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_runFinalization_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_runFinalizersOnExit_$_$Z$V = function (arg0) {
/*implement method!*/
/*
Deprecated. This method is inherently unsafe.  It may result in
            finalizers being called on live objects while other threads are
      concurrently manipulating those objects, resulting in erratic
            behavior or deadlock.

Parameters:

value -> indicating enabling or disabling of finalization

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_runFinalizersOnExit_$_$Z$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_setErr_$_$Ljava_io_PrintStream$$$_$V = function (arg0) {
/*implement method!*/
/*
Reassigns the "standard" error output stream.

 First, if there is a security manager, its checkPermission
 method is called with a RuntimePermission("setIO") permission
  to see if it's ok to reassign the "standard" error output stream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_setErr_$_$Ljava_io_PrintStream$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_setIn_$_$Ljava_io_InputStream$$$_$V = function (arg0) {
/*implement method!*/
/*
Reassigns the "standard" input stream.

 First, if there is a security manager, its checkPermission
 method is called with a RuntimePermission("setIO") permission
  to see if it's ok to reassign the "standard" input stream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_setIn_$_$Ljava_io_InputStream$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_setOut_$_$Ljava_io_PrintStream$$$_$V = function (arg0) {
/*implement method!*/
/*
Reassigns the "standard" output stream.

 First, if there is a security manager, its checkPermission
 method is called with a RuntimePermission("setIO") permission
  to see if it's ok to reassign the "standard" output stream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_setOut_$_$Ljava_io_PrintStream$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_setProperties_$_$Ljava_util_Properties$$$_$V = function (arg0) {
/*implement method!*/
/*
Sets the system properties to the Properties
 argument.
 
 First, if there is a security manager, its
 checkPropertiesAccess method is called with no
 arguments. This may result in a security exception.
 
 The argument becomes the current set of system properties for use
 by the getProperty(String) method. If the argument is
 null, then the current set of system properties is
 forgotten.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_setProperties_$_$Ljava_util_Properties$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_setProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Sets the system property indicated by the specified key.
 
 First, if a security manager exists, its
 SecurityManager.checkPermission method
 is called with a PropertyPermission(key, "write")
 permission. This may result in a SecurityException being thrown.
 If no exception is thrown, the specified property is set to the given
 value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_setProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_System_$_setSecurityManager_$_$Ljava_lang_SecurityManager$$$_$V = function (arg0) {
/*implement method!*/
/*
Sets the System security.

  If there is a security manager already installed, this method first
 calls the security manager's checkPermission method
 with a RuntimePermission("setSecurityManager")
 permission to ensure it's ok to replace the existing
 security manager.
 This may result in throwing a SecurityException.

  Otherwise, the argument is established as the current
 security manager. If the argument is null and no
 security manager has been established, then no action is taken and
 the method simply returns.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_System_$_setSecurityManager_$_$Ljava_lang_SecurityManager$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_System_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_lang_System_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_System_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_lang_System_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_lang_System_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_lang_System_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var java_lang_System_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_System_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_lang_System_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_lang_System.arraycopy_$_$Ljava_lang_Object$$$_ILjava_lang_Object$$$_II$V = java_lang_System_$_arraycopy_$_$Ljava_lang_Object$$$_ILjava_lang_Object$$$_II$V;
java_lang_System.clearProperty_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = java_lang_System_$_clearProperty_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_;
java_lang_System.console_$_$$Ljava_io_Console$$$_ = java_lang_System_$_console_$_$$Ljava_io_Console$$$_;
java_lang_System.currentTimeMillis_$_$$J = java_lang_System_$_currentTimeMillis_$_$$J;
java_lang_System.exit_$_$I$V = java_lang_System_$_exit_$_$I$V;
java_lang_System.gc_$_$$V = java_lang_System_$_gc_$_$$V;
java_lang_System.getCallerClass_$_$$Ljava_lang_Class$$$_ = java_lang_System_$_getCallerClass_$_$$Ljava_lang_Class$$$_;
java_lang_System.getProperties_$_$$Ljava_util_Properties$$$_ = java_lang_System_$_getProperties_$_$$Ljava_util_Properties$$$_;
java_lang_System.getProperty_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = java_lang_System_$_getProperty_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_;
java_lang_System.getProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = java_lang_System_$_getProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_;
java_lang_System.getSecurityManager_$_$$Ljava_lang_SecurityManager$$$_ = java_lang_System_$_getSecurityManager_$_$$Ljava_lang_SecurityManager$$$_;
java_lang_System.getenv_$_$$Ljava_util_Map$$$_ = java_lang_System_$_getenv_$_$$Ljava_util_Map$$$_;
java_lang_System.getenv_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = java_lang_System_$_getenv_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_;
java_lang_System.identityHashCode_$_$Ljava_lang_Object$$$_$I = java_lang_System_$_identityHashCode_$_$Ljava_lang_Object$$$_$I;
java_lang_System.inheritedChannel_$_$$Ljava_nio_channels_Channel$$$_ = java_lang_System_$_inheritedChannel_$_$$Ljava_nio_channels_Channel$$$_;
java_lang_System.loadLibrary_$_$Ljava_lang_String$$$_$V = java_lang_System_$_loadLibrary_$_$Ljava_lang_String$$$_$V;
java_lang_System.load_$_$Ljava_lang_String$$$_$V = java_lang_System_$_load_$_$Ljava_lang_String$$$_$V;
java_lang_System.mapLibraryName_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_ = java_lang_System_$_mapLibraryName_$_$Ljava_lang_String$$$_$Ljava_lang_String$$$_;
java_lang_System.nanoTime_$_$$J = java_lang_System_$_nanoTime_$_$$J;
java_lang_System.runFinalization_$_$$V = java_lang_System_$_runFinalization_$_$$V;
java_lang_System.runFinalizersOnExit_$_$Z$V = java_lang_System_$_runFinalizersOnExit_$_$Z$V;
java_lang_System.setErr_$_$Ljava_io_PrintStream$$$_$V = java_lang_System_$_setErr_$_$Ljava_io_PrintStream$$$_$V;
java_lang_System.setIn_$_$Ljava_io_InputStream$$$_$V = java_lang_System_$_setIn_$_$Ljava_io_InputStream$$$_$V;
java_lang_System.setOut_$_$Ljava_io_PrintStream$$$_$V = java_lang_System_$_setOut_$_$Ljava_io_PrintStream$$$_$V;
java_lang_System.setProperties_$_$Ljava_util_Properties$$$_$V = java_lang_System_$_setProperties_$_$Ljava_util_Properties$$$_$V;
java_lang_System.setProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_ = java_lang_System_$_setProperty_$_$Ljava_lang_String$$$_Ljava_lang_String$$$_$Ljava_lang_String$$$_;
java_lang_System.setSecurityManager_$_$Ljava_lang_SecurityManager$$$_$V = java_lang_System_$_setSecurityManager_$_$Ljava_lang_SecurityManager$$$_$V;
java_lang_System.prototype.wait_$_$J$V = java_lang_System_$_wait_$_$J$V;
java_lang_System.prototype.wait_$_$JI$V = java_lang_System_$_wait_$_$JI$V;
java_lang_System.prototype.wait_$_$$V = java_lang_System_$_wait_$_$$V;
java_lang_System.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_System_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_System.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_System_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_System.prototype.hashCode_$_$$I = java_lang_System_$_hashCode_$_$$I;
java_lang_System.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_System_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_System.prototype.notify_$_$$V = java_lang_System_$_notify_$_$$V;
java_lang_System.prototype.notifyAll_$_$$V = java_lang_System_$_notifyAll_$_$$V;
