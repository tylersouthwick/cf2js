/** @constructor */
function java_io_FileOutputStream() {}
var java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_io_File$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Creates a file output stream to write to the file represented by 
 the specified File object. A new 
 FileDescriptor object is created to represent this 
 file connection.
 
 First, if there is a security manager, its checkWrite 
 method is called with the path represented by the file 
 argument as its argument.
 
 If the file exists but is a directory rather than a regular file, does
 not exist but cannot be created, or cannot be opened for any other
 reason then a FileNotFoundException is thrown.


Parameters:file - the file to be opened for writing.
Throws:
FileNotFoundException - if the file exists but is a directory
                   rather than a regular file, does not exist but cannot
                   be created, or cannot be opened for any other reason
SecurityException - if a security manager exists and its
               checkWrite method denies write access
               to the file.See Also:File.getPath(), 
SecurityException, 
SecurityManager.checkWrite(java.lang.String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_io_File$$$_$V"));
throw ex;
};
var java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_io_File$$$_Z$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Creates a file output stream to write to the file represented by 
 the specified File object. If the second argument is
 true, then bytes will be written to the end of the file
 rather than the beginning. A new FileDescriptor object is
 created to represent this file connection.
 
 First, if there is a security manager, its checkWrite 
 method is called with the path represented by the file 
 argument as its argument.
 
 If the file exists but is a directory rather than a regular file, does
 not exist but cannot be created, or cannot be opened for any other
 reason then a FileNotFoundException is thrown.


Parameters:file - the file to be opened for writing.append - if true, then bytes will be written
                   to the end of the file rather than the beginning
Throws:
FileNotFoundException - if the file exists but is a directory
                   rather than a regular file, does not exist but cannot
                   be created, or cannot be opened for any other reason
SecurityException - if a security manager exists and its
               checkWrite method denies write access
               to the file.Since:
  1.4
See Also:File.getPath(), 
SecurityException, 
SecurityManager.checkWrite(java.lang.String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_io_File$$$_Z$V"));
throw ex;
};
var java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_io_FileDescriptor$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Creates an output file stream to write to the specified file 
 descriptor, which represents an existing connection to an actual 
 file in the file system.
 
 First, if there is a security manager, its checkWrite 
 method is called with the file descriptor fdObj 
 argument as its argument.


Parameters:fdObj - the file descriptor to be opened for writing
Throws:
SecurityException - if a security manager exists and its
               checkWrite method denies
               write access to the file descriptorSee Also:SecurityManager.checkWrite(java.io.FileDescriptor)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_io_FileDescriptor$$$_$V"));
throw ex;
};
var java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Creates an output file stream to write to the file with the 
 specified name. A new FileDescriptor object is 
 created to represent this file connection.
 
 First, if there is a security manager, its checkWrite 
 method is called with name as its argument.
 
 If the file exists but is a directory rather than a regular file, does
 not exist but cannot be created, or cannot be opened for any other
 reason then a FileNotFoundException is thrown.


Parameters:name - the system-dependent filename
Throws:
FileNotFoundException - if the file exists but is a directory
                   rather than a regular file, does not exist but cannot
                   be created, or cannot be opened for any other reason
SecurityException - if a security manager exists and its
               checkWrite method denies write access
               to the file.See Also:SecurityManager.checkWrite(java.lang.String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_lang_String$$$_$V"));
throw ex;
};
var java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_lang_String$$$_Z$V = function (arg0, arg1, arg2) {
/*implement constructor!*/
/*
Creates an output file stream to write to the file with the specified
 name.  If the second argument is true, then
 bytes will be written to the end of the file rather than the beginning.
 A new FileDescriptor object is created to represent this
 file connection.
 
 First, if there is a security manager, its checkWrite 
 method is called with name as its argument.
 
 If the file exists but is a directory rather than a regular file, does
 not exist but cannot be created, or cannot be opened for any other
 reason then a FileNotFoundException is thrown.


Parameters:name - the system-dependent file nameappend - if true, then bytes will be written
                   to the end of the file rather than the beginning
Throws:
FileNotFoundException - if the file exists but is a directory
                   rather than a regular file, does not exist but cannot
                   be created, or cannot be opened for any other reason.
SecurityException - if a security manager exists and its
               checkWrite method denies write access
               to the file.Since:
  JDK1.1
See Also:SecurityManager.checkWrite(java.lang.String)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_lang_String$$$_Z$V"));
throw ex;
};
var java_io_FileOutputStream_$_close_$_$$V = function (arg0) {
/*implement method!*/
/*
Closes this file output stream and releases any system resources 
 associated with this stream. This file output stream may no longer 
 be used for writing bytes. 

  If this stream has an associated channel then the channel is closed
 as well.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$_close_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_FileOutputStream_$_finalize_$_$$V = function (arg0) {
/*implement method!*/
/*
Cleans up the connection to the file, and ensures that the 
 close method of this file output stream is
 called when there are no more references to this stream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$_finalize_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_FileOutputStream_$_getChannel_$_$$Ljava_nio_channels_FileChannel$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the unique FileChannel
 object associated with this file output stream. 

  The initial position of the returned channel will be equal to the
 number of bytes written to the file so far unless this stream is in
 append mode, in which case it will be equal to the size of the file.
 Writing bytes to this stream will increment the channel's position
 accordingly.  Changing the channel's position, either explicitly or by
 writing, will change this stream's file position.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$_getChannel_$_$$Ljava_nio_channels_FileChannel$$$_"));
throw ex;
//must have a return of type FileChannel
};
var java_io_FileOutputStream_$_getFD_$_$$Ljava_io_FileDescriptor$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the file descriptor associated with this stream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$_getFD_$_$$Ljava_io_FileDescriptor$$$_"));
throw ex;
//must have a return of type FileDescriptor
};
var java_io_FileOutputStream_$_write_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes the specified byte to this file output stream. Implements 
 the write method of OutputStream.

Parameters:

b -> the byte to be written.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$_write_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_io_FileOutputStream_$_write_$_$_$$$$$_B$V = function (arg0, arg1) {
/*implement method!*/
/*
Writes b.length bytes from the specified byte array 
 to this file output stream.

Parameters:

b -> the data.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$_write_$_$_$$$$$_B$V"));
throw ex;
//must have a return of type void
};
var java_io_FileOutputStream_$_write_$_$_$$$$$_BII$V = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Writes len bytes from the specified byte array 
 starting at offset off to this file output stream.

Parameters:

b -> the data.

off -> the start offset in the data.

len -> the number of bytes to write.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_FileOutputStream_$_write_$_$_$$$$$_BII$V"));
throw ex;
//must have a return of type void
};
var java_io_FileOutputStream_$_flush_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_flush_$_$$V(arg0);};
var java_io_FileOutputStream_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_io_OutputStream_$_wait_$_$J$V(arg0, arg1);};
var java_io_FileOutputStream_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_io_OutputStream_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_io_FileOutputStream_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_wait_$_$$V(arg0);};
var java_io_FileOutputStream_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_io_OutputStream_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_io_FileOutputStream_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_io_FileOutputStream_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_hashCode_$_$$I(arg0);};
var java_io_FileOutputStream_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_io_FileOutputStream_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_notify_$_$$V(arg0);};
var java_io_FileOutputStream_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_OutputStream_$_notifyAll_$_$$V(arg0);};
java_io_FileOutputStream.prototype._$$$init$$$__$_$Ljava_io_File$$$_$V = java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_io_File$$$_$V;
java_io_FileOutputStream.prototype._$$$init$$$__$_$Ljava_io_File$$$_Z$V = java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_io_File$$$_Z$V;
java_io_FileOutputStream.prototype._$$$init$$$__$_$Ljava_io_FileDescriptor$$$_$V = java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_io_FileDescriptor$$$_$V;
java_io_FileOutputStream.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_io_FileOutputStream.prototype._$$$init$$$__$_$Ljava_lang_String$$$_Z$V = java_io_FileOutputStream_$__$$$init$$$__$_$Ljava_lang_String$$$_Z$V;
java_io_FileOutputStream.prototype.close_$_$$V = java_io_FileOutputStream_$_close_$_$$V;
java_io_FileOutputStream.prototype.finalize_$_$$V = java_io_FileOutputStream_$_finalize_$_$$V;
java_io_FileOutputStream.prototype.getChannel_$_$$Ljava_nio_channels_FileChannel$$$_ = java_io_FileOutputStream_$_getChannel_$_$$Ljava_nio_channels_FileChannel$$$_;
java_io_FileOutputStream.prototype.getFD_$_$$Ljava_io_FileDescriptor$$$_ = java_io_FileOutputStream_$_getFD_$_$$Ljava_io_FileDescriptor$$$_;
java_io_FileOutputStream.prototype.write_$_$I$V = java_io_FileOutputStream_$_write_$_$I$V;
java_io_FileOutputStream.prototype.write_$_$_$$$$$_B$V = java_io_FileOutputStream_$_write_$_$_$$$$$_B$V;
java_io_FileOutputStream.prototype.write_$_$_$$$$$_BII$V = java_io_FileOutputStream_$_write_$_$_$$$$$_BII$V;
java_io_FileOutputStream.prototype.flush_$_$$V = java_io_FileOutputStream_$_flush_$_$$V;
java_io_FileOutputStream.prototype.wait_$_$J$V = java_io_FileOutputStream_$_wait_$_$J$V;
java_io_FileOutputStream.prototype.wait_$_$JI$V = java_io_FileOutputStream_$_wait_$_$JI$V;
java_io_FileOutputStream.prototype.wait_$_$$V = java_io_FileOutputStream_$_wait_$_$$V;
java_io_FileOutputStream.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_io_FileOutputStream_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_io_FileOutputStream.prototype.toString_$_$$Ljava_lang_String$$$_ = java_io_FileOutputStream_$_toString_$_$$Ljava_lang_String$$$_;
java_io_FileOutputStream.prototype.hashCode_$_$$I = java_io_FileOutputStream_$_hashCode_$_$$I;
java_io_FileOutputStream.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_io_FileOutputStream_$_getClass_$_$$Ljava_lang_Class$$$_;
java_io_FileOutputStream.prototype.notify_$_$$V = java_io_FileOutputStream_$_notify_$_$$V;
java_io_FileOutputStream.prototype.notifyAll_$_$$V = java_io_FileOutputStream_$_notifyAll_$_$$V;
