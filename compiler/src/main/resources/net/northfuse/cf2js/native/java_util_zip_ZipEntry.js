/** @constructor */
function java_util_zip_ZipEntry() {}
var java_util_zip_ZipEntry_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Creates a new zip entry with the specified name.


Parameters:name - the entry name
Throws:
NullPointerException - if the entry name is null
IllegalArgumentException - if the entry name is longer than
                  0xFFFF bytes

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$__$$$init$$$__$_$Ljava_lang_String$$$_$V"));
throw ex;
};
var java_util_zip_ZipEntry_$__$$$init$$$__$_$Ljava_util_zip_ZipEntry$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Creates a new zip entry with fields taken from the specified
 zip entry.


Parameters:e - a zip Entry object

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$__$$$init$$$__$_$Ljava_util_zip_ZipEntry$$$_$V"));
throw ex;
};
var java_util_zip_ZipEntry_$_clone_$_$$Ljava_lang_Object$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a copy of this entry.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_clone_$_$$Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object
};
var java_util_zip_ZipEntry_$_getComment_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the comment string for the entry, or null if none.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_getComment_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_util_zip_ZipEntry_$_getCompressedSize_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the size of the compressed entry data, or -1 if not known.
 In the case of a stored entry, the compressed size will be the same
 as the uncompressed size of the entry.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_getCompressedSize_$_$$J"));
throw ex;
//must have a return of type long
};
var java_util_zip_ZipEntry_$_getCrc_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the CRC-32 checksum of the uncompressed entry data, or -1 if
 not known.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_getCrc_$_$$J"));
throw ex;
//must have a return of type long
};
var java_util_zip_ZipEntry_$_getExtra_$_$$_$$$$$_B = function (arg0) {
/*implement method!*/
/*
Returns the extra field data for the entry, or null if none.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_getExtra_$_$$_$$$$$_B"));
throw ex;
//must have a return of type byte[]
};
var java_util_zip_ZipEntry_$_getMethod_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the compression method of the entry, or -1 if not specified.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_getMethod_$_$$I"));
throw ex;
//must have a return of type int
};
var java_util_zip_ZipEntry_$_getName_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns the name of the entry.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_getName_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_util_zip_ZipEntry_$_getSize_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the uncompressed size of the entry data, or -1 if not known.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_getSize_$_$$J"));
throw ex;
//must have a return of type long
};
var java_util_zip_ZipEntry_$_getTime_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the modification time of the entry, or -1 if not specified.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_getTime_$_$$J"));
throw ex;
//must have a return of type long
};
var java_util_zip_ZipEntry_$_hashCode_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the hash code value for this entry.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_hashCode_$_$$I"));
throw ex;
//must have a return of type int
};
var java_util_zip_ZipEntry_$_isDirectory_$_$$Z = function (arg0) {
/*implement method!*/
/*
Returns true if this is a directory entry. A directory entry is
 defined to be one whose name ends with a '/'.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_isDirectory_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_util_zip_ZipEntry_$_setComment_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the optional comment string for the entry.

Parameters:

comment -> the comment string

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_setComment_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_util_zip_ZipEntry_$_setCompressedSize_$_$J$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the size of the compressed entry data.

Parameters:

csize -> the compressed size to set to

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_setCompressedSize_$_$J$V"));
throw ex;
//must have a return of type void
};
var java_util_zip_ZipEntry_$_setCrc_$_$J$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the CRC-32 checksum of the uncompressed entry data.

Parameters:

crc -> the CRC-32 value

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_setCrc_$_$J$V"));
throw ex;
//must have a return of type void
};
var java_util_zip_ZipEntry_$_setExtra_$_$_$$$$$_B$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the optional extra field data for the entry.

Parameters:

extra -> the extra field data bytes

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_setExtra_$_$_$$$$$_B$V"));
throw ex;
//must have a return of type void
};
var java_util_zip_ZipEntry_$_setMethod_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the compression method for the entry.

Parameters:

method -> the compression method, either STORED or DEFLATED

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_setMethod_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_util_zip_ZipEntry_$_setSize_$_$J$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the uncompressed size of the entry data.

Parameters:

size -> the uncompressed size in bytes

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_setSize_$_$J$V"));
throw ex;
//must have a return of type void
};
var java_util_zip_ZipEntry_$_setTime_$_$J$V = function (arg0, arg1) {
/*implement method!*/
/*
Sets the modification time of the entry.

Parameters:

time -> the entry modification time in number of milliseconds
                   since the epoch

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_setTime_$_$J$V"));
throw ex;
//must have a return of type void
};
var java_util_zip_ZipEntry_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of the ZIP entry.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_util_zip_ZipEntry_$_toString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_util_zip_ZipEntry_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_util_zip_ZipEntry_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_util_zip_ZipEntry_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_util_zip_ZipEntry_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_util_zip_ZipEntry_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_util_zip_ZipEntry_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_util_zip_ZipEntry_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_util_zip_ZipEntry.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_util_zip_ZipEntry_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_util_zip_ZipEntry.prototype._$$$init$$$__$_$Ljava_util_zip_ZipEntry$$$_$V = java_util_zip_ZipEntry_$__$$$init$$$__$_$Ljava_util_zip_ZipEntry$$$_$V;
java_util_zip_ZipEntry.prototype.clone_$_$$Ljava_lang_Object$$$_ = java_util_zip_ZipEntry_$_clone_$_$$Ljava_lang_Object$$$_;
java_util_zip_ZipEntry.prototype.getComment_$_$$Ljava_lang_String$$$_ = java_util_zip_ZipEntry_$_getComment_$_$$Ljava_lang_String$$$_;
java_util_zip_ZipEntry.prototype.getCompressedSize_$_$$J = java_util_zip_ZipEntry_$_getCompressedSize_$_$$J;
java_util_zip_ZipEntry.prototype.getCrc_$_$$J = java_util_zip_ZipEntry_$_getCrc_$_$$J;
java_util_zip_ZipEntry.prototype.getExtra_$_$$_$$$$$_B = java_util_zip_ZipEntry_$_getExtra_$_$$_$$$$$_B;
java_util_zip_ZipEntry.prototype.getMethod_$_$$I = java_util_zip_ZipEntry_$_getMethod_$_$$I;
java_util_zip_ZipEntry.prototype.getName_$_$$Ljava_lang_String$$$_ = java_util_zip_ZipEntry_$_getName_$_$$Ljava_lang_String$$$_;
java_util_zip_ZipEntry.prototype.getSize_$_$$J = java_util_zip_ZipEntry_$_getSize_$_$$J;
java_util_zip_ZipEntry.prototype.getTime_$_$$J = java_util_zip_ZipEntry_$_getTime_$_$$J;
java_util_zip_ZipEntry.prototype.hashCode_$_$$I = java_util_zip_ZipEntry_$_hashCode_$_$$I;
java_util_zip_ZipEntry.prototype.isDirectory_$_$$Z = java_util_zip_ZipEntry_$_isDirectory_$_$$Z;
java_util_zip_ZipEntry.prototype.setComment_$_$Ljava_lang_String$$$_$V = java_util_zip_ZipEntry_$_setComment_$_$Ljava_lang_String$$$_$V;
java_util_zip_ZipEntry.prototype.setCompressedSize_$_$J$V = java_util_zip_ZipEntry_$_setCompressedSize_$_$J$V;
java_util_zip_ZipEntry.prototype.setCrc_$_$J$V = java_util_zip_ZipEntry_$_setCrc_$_$J$V;
java_util_zip_ZipEntry.prototype.setExtra_$_$_$$$$$_B$V = java_util_zip_ZipEntry_$_setExtra_$_$_$$$$$_B$V;
java_util_zip_ZipEntry.prototype.setMethod_$_$I$V = java_util_zip_ZipEntry_$_setMethod_$_$I$V;
java_util_zip_ZipEntry.prototype.setSize_$_$J$V = java_util_zip_ZipEntry_$_setSize_$_$J$V;
java_util_zip_ZipEntry.prototype.setTime_$_$J$V = java_util_zip_ZipEntry_$_setTime_$_$J$V;
java_util_zip_ZipEntry.prototype.toString_$_$$Ljava_lang_String$$$_ = java_util_zip_ZipEntry_$_toString_$_$$Ljava_lang_String$$$_;
java_util_zip_ZipEntry.prototype.wait_$_$J$V = java_util_zip_ZipEntry_$_wait_$_$J$V;
java_util_zip_ZipEntry.prototype.wait_$_$JI$V = java_util_zip_ZipEntry_$_wait_$_$JI$V;
java_util_zip_ZipEntry.prototype.wait_$_$$V = java_util_zip_ZipEntry_$_wait_$_$$V;
java_util_zip_ZipEntry.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_util_zip_ZipEntry_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_util_zip_ZipEntry.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_util_zip_ZipEntry_$_getClass_$_$$Ljava_lang_Class$$$_;
java_util_zip_ZipEntry.prototype.notify_$_$$V = java_util_zip_ZipEntry_$_notify_$_$$V;
java_util_zip_ZipEntry.prototype.notifyAll_$_$$V = java_util_zip_ZipEntry_$_notifyAll_$_$$V;
