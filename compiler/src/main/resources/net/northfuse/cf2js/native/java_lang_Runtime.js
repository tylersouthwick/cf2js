/** @constructor */
function java_lang_Runtime() {}
var java_lang_Runtime_$_addShutdownHook_$_$Ljava_lang_Thread$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Registers a new virtual-machine shutdown hook.

  The Java virtual machine shuts down in response to two kinds
 of events:

   

     The program exits normally, when the last non-daemon
   thread exits or when the exit (equivalently,
   System.exit) method is invoked, or

     The virtual machine is terminated in response to a
   user interrupt, such as typing ^C, or a system-wide event,
   such as user logoff or system shutdown.

   

  A shutdown hook is simply an initialized but unstarted
 thread.  When the virtual machine begins its shutdown sequence it will
 start all registered shutdown hooks in some unspecified order and let
 them run concurrently.  When all the hooks have finished it will then
 run all uninvoked finalizers if finalization-on-exit has been enabled.
 Finally, the virtual machine will halt.  Note that daemon threads will
 continue to run during the shutdown sequence, as will non-daemon threads
 if shutdown was initiated by invoking the exit
 method.

  Once the shutdown sequence has begun it can be stopped only by
 invoking the halt method, which forcibly
 terminates the virtual machine.

  Once the shutdown sequence has begun it is impossible to register a
 new shutdown hook or de-register a previously-registered hook.
 Attempting either of these operations will cause an
 IllegalStateException to be thrown.

  Shutdown hooks run at a delicate time in the life cycle of a virtual
 machine and should therefore be coded defensively.  They should, in
 particular, be written to be thread-safe and to avoid deadlocks insofar
 as possible.  They should also not rely blindly upon services that may
 have registered their own shutdown hooks and therefore may themselves in
 the process of shutting down.  Attempts to use other thread-based
 services such as the AWT event-dispatch thread, for example, may lead to
 deadlocks.

  Shutdown hooks should also finish their work quickly.  When a
 program invokes exit the expectation is
 that the virtual machine will promptly shut down and exit.  When the
 virtual machine is terminated due to user logoff or system shutdown the
 underlying operating system may only allow a fixed amount of time in
 which to shut down and exit.  It is therefore inadvisable to attempt any
 user interaction or to perform a long-running computation in a shutdown
 hook.

  Uncaught exceptions are handled in shutdown hooks just as in any
 other thread, by invoking the uncaughtException method of the thread's ThreadGroup object.  The default implementation of this method
 prints the exception's stack trace to System.err and
 terminates the thread; it does not cause the virtual machine to exit or
 halt.

  In rare circumstances the virtual machine may abort, that is,
 stop running without shutting down cleanly.  This occurs when the
 virtual machine is terminated externally, for example with the
 SIGKILL signal on Unix or the TerminateProcess call on
 Microsoft Windows.  The virtual machine may also abort if a native
 method goes awry by, for example, corrupting internal data structures or
 attempting to access nonexistent memory.  If the virtual machine aborts
 then no guarantee can be made about whether or not any shutdown hooks
 will be run.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_addShutdownHook_$_$Ljava_lang_Thread$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_availableProcessors_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the number of processors available to the Java virtual machine.

  This value may change during a particular invocation of the virtual
 machine.  Applications that are sensitive to the number of available
 processors should therefore occasionally poll this property and adjust
 their resource usage appropriately.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_availableProcessors_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Runtime_$_exec_$_$Ljava_lang_String$$$_$Ljava_lang_Process$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Executes the specified string command in a separate process.

 This is a convenience method.  An invocation of the form
 exec(command)
 behaves in exactly the same way as the invocation
 exec(command, null, null).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_exec_$_$Ljava_lang_String$$$_$Ljava_lang_Process$$$_"));
throw ex;
//must have a return of type Process
};
var java_lang_Runtime_$_exec_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_exec_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_"));
throw ex;
//must have a return of type Process
};
var java_lang_Runtime_$_exec_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_Ljava_io_File$$$_$Ljava_lang_Process$$$_ = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_exec_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_Ljava_io_File$$$_$Ljava_lang_Process$$$_"));
throw ex;
//must have a return of type Process
};
var java_lang_Runtime_$_exec_$_$_$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_ = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_exec_$_$_$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_"));
throw ex;
//must have a return of type Process
};
var java_lang_Runtime_$_exec_$_$_$$$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_ = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_exec_$_$_$$$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_"));
throw ex;
//must have a return of type Process
};
var java_lang_Runtime_$_exec_$_$_$$$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_Ljava_io_File$$$_$Ljava_lang_Process$$$_ = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_exec_$_$_$$$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_Ljava_io_File$$$_$Ljava_lang_Process$$$_"));
throw ex;
//must have a return of type Process
};
var java_lang_Runtime_$_exit_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Terminates the currently running Java virtual machine by initiating its
 shutdown sequence.  This method never returns normally.  The argument
 serves as a status code; by convention, a nonzero status code indicates
 abnormal termination.

  The virtual machine's shutdown sequence consists of two phases.  In
 the first phase all registered shutdown hooks,
 if any, are started in some unspecified order and allowed to run
 concurrently until they finish.  In the second phase all uninvoked
 finalizers are run if finalization-on-exit
 has been enabled.  Once this is done the virtual machine halts.

  If this method is invoked after the virtual machine has begun its
 shutdown sequence then if shutdown hooks are being run this method will
 block indefinitely.  If shutdown hooks have already been run and on-exit
 finalization has been enabled then this method halts the virtual machine
 with the given status code if the status is nonzero; otherwise, it
 blocks indefinitely.

  The System.exit method is the
 conventional and convenient means of invoking this method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_exit_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_freeMemory_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the amount of free memory in the Java Virtual Machine.
 Calling the 
 gc method may result in increasing the value returned 
 by freeMemory.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_freeMemory_$_$$J"));
throw ex;
//must have a return of type long
};
var java_lang_Runtime_$_gc_$_$$V = function (arg0) {
/*implement method!*/
/*
Runs the garbage collector.
 Calling this method suggests that the Java virtual machine expend 
 effort toward recycling unused objects in order to make the memory 
 they currently occupy available for quick reuse. When control 
 returns from the method call, the virtual machine has made 
 its best effort to recycle all discarded objects. 
 
 The name gc stands for "garbage 
 collector". The virtual machine performs this recycling 
 process automatically as needed, in a separate thread, even if the 
 gc method is not invoked explicitly.
 
 The method System.gc() is the conventional and convenient 
 means of invoking this method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_gc_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_getLocalizedInputStream_$_$Ljava_io_InputStream$$$_$Ljava_io_InputStream$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Deprecated. As of JDK 1.1, the preferred way to translate a byte
 stream in the local encoding into a character stream in Unicode is via
 the InputStreamReader and BufferedReader
 classes.

Parameters:

in -> InputStream to localize

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_getLocalizedInputStream_$_$Ljava_io_InputStream$$$_$Ljava_io_InputStream$$$_"));
throw ex;
//must have a return of type InputStream
};
var java_lang_Runtime_$_getLocalizedOutputStream_$_$Ljava_io_OutputStream$$$_$Ljava_io_OutputStream$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Deprecated. As of JDK 1.1, the preferred way to translate a
 Unicode character stream into a byte stream in the local encoding is via
 the OutputStreamWriter, BufferedWriter, and
 PrintWriter classes.

Parameters:

out -> OutputStream to localize

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_getLocalizedOutputStream_$_$Ljava_io_OutputStream$$$_$Ljava_io_OutputStream$$$_"));
throw ex;
//must have a return of type OutputStream
};
var java_lang_Runtime_$_getRuntime_$_$$Ljava_lang_Runtime$$$_ = function () {
/*implement method!*/
/*
Returns the runtime object associated with the current Java application.
 Most of the methods of class Runtime are instance 
 methods and must be invoked with respect to the current runtime object.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_getRuntime_$_$$Ljava_lang_Runtime$$$_"));
throw ex;
//must have a return of type Runtime
};
var java_lang_Runtime_$_halt_$_$I$V = function (arg0, arg1) {
/*implement method!*/
/*
Forcibly terminates the currently running Java virtual machine.  This
 method never returns normally.

  This method should be used with extreme caution.  Unlike the
 exit method, this method does not cause shutdown
 hooks to be started and does not run uninvoked finalizers if
 finalization-on-exit has been enabled.  If the shutdown sequence has
 already been initiated then this method does not wait for any running
 shutdown hooks or finalizers to finish their work.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_halt_$_$I$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_load0_$_$Ljava_lang_Class$$$_Ljava_lang_String$$$_$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_load0_$_$Ljava_lang_Class$$$_Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_loadLibrary0_$_$Ljava_lang_Class$$$_Ljava_lang_String$$$_$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_loadLibrary0_$_$Ljava_lang_Class$$$_Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_loadLibrary_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Loads the dynamic library with the specified library name. 
 A file containing native code is loaded from the local file system 
 from a place where library files are conventionally obtained. The 
 details of this process are implementation-dependent. The 
 mapping from a library name to a specific filename is done in a 
 system-specific manner. 
 
 First, if there is a security manager, its checkLink 
 method is called with the libname as its argument. 
 This may result in a security exception. 
 
 The method System.loadLibrary(String) is the conventional 
 and convenient means of invoking this method. If native
 methods are to be used in the implementation of a class, a standard 
 strategy is to put the native code in a library file (call it 
 LibFile) and then to put a static initializer:
  static { System.loadLibrary("LibFile"); }
 
 within the class declaration. When the class is loaded and 
 initialized, the necessary native code implementation for the native 
 methods will then be loaded as well. 
 
 If this method is called more than once with the same library 
 name, the second and subsequent calls are ignored.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_loadLibrary_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_load_$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement method!*/
/*
Loads the specified filename as a dynamic library. The filename 
 argument must be a complete path name,
 (for example
 Runtime.getRuntime().load("/home/avh/lib/libX11.so");).
 
 First, if there is a security manager, its checkLink 
 method is called with the filename as its argument. 
 This may result in a security exception. 
 
 This is similar to the method loadLibrary(String), but it 
 accepts a general file name as an argument rather than just a library 
 name, allowing any file of native code to be loaded.
 
 The method System.load(String) is the conventional and 
 convenient means of invoking this method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_load_$_$Ljava_lang_String$$$_$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_maxMemory_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the maximum amount of memory that the Java virtual machine will
 attempt to use.  If there is no inherent limit then the value Long.MAX_VALUE will be returned.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_maxMemory_$_$$J"));
throw ex;
//must have a return of type long
};
var java_lang_Runtime_$_removeShutdownHook_$_$Ljava_lang_Thread$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
De-registers a previously-registered virtual-machine shutdown hook.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_removeShutdownHook_$_$Ljava_lang_Thread$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Runtime_$_runFinalization_$_$$V = function (arg0) {
/*implement method!*/
/*
Runs the finalization methods of any objects pending finalization.
 Calling this method suggests that the Java virtual machine expend 
 effort toward running the finalize methods of objects 
 that have been found to be discarded but whose finalize 
 methods have not yet been run. When control returns from the 
 method call, the virtual machine has made a best effort to 
 complete all outstanding finalizations. 
 
 The virtual machine performs the finalization process 
 automatically as needed, in a separate thread, if the 
 runFinalization method is not invoked explicitly. 
 
 The method System.runFinalization() is the conventional 
 and convenient means of invoking this method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_runFinalization_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_runFinalizersOnExit_$_$Z$V = function (arg0) {
/*implement method!*/
/*
Deprecated. This method is inherently unsafe.  It may result in
            finalizers being called on live objects while other threads are
      concurrently manipulating those objects, resulting in erratic
            behavior or deadlock.

Parameters:

value -> true to enable finalization on exit, false to disable

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_runFinalizersOnExit_$_$Z$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_totalMemory_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the total amount of memory in the Java virtual machine.
 The value returned by this method may vary over time, depending on 
 the host environment.
 
 Note that the amount of memory required to hold an object of any 
 given type may be implementation-dependent.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_totalMemory_$_$$J"));
throw ex;
//must have a return of type long
};
var java_lang_Runtime_$_traceInstructions_$_$Z$V = function (arg0, arg1) {
/*implement method!*/
/*
Enables/Disables tracing of instructions.
 If the boolean argument is true, this 
 method suggests that the Java virtual machine emit debugging 
 information for each instruction in the virtual machine as it 
 is executed. The format of this information, and the file or other 
 output stream to which it is emitted, depends on the host environment. 
 The virtual machine may ignore this request if it does not support 
 this feature. The destination of the trace output is system 
 dependent. 
 
 If the boolean argument is false, this 
 method causes the virtual machine to stop performing the 
 detailed instruction trace it is performing.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_traceInstructions_$_$Z$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_traceMethodCalls_$_$Z$V = function (arg0, arg1) {
/*implement method!*/
/*
Enables/Disables tracing of method calls.
 If the boolean argument is true, this 
 method suggests that the Java virtual machine emit debugging 
 information for each method in the virtual machine as it is 
 called. The format of this information, and the file or other output 
 stream to which it is emitted, depends on the host environment. The 
 virtual machine may ignore this request if it does not support 
 this feature.  
 
 Calling this method with argument false suggests that the
 virtual machine cease emitting per-call debugging information.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Runtime_$_traceMethodCalls_$_$Z$V"));
throw ex;
//must have a return of type void
};
var java_lang_Runtime_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$J$V(arg0, arg1);};
var java_lang_Runtime_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_Runtime_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_wait_$_$$V(arg0);};
var java_lang_Runtime_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Object_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_lang_Runtime_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_lang_Runtime_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_hashCode_$_$$I(arg0);};
var java_lang_Runtime_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_Runtime_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notify_$_$$V(arg0);};
var java_lang_Runtime_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Object_$_notifyAll_$_$$V(arg0);};
java_lang_Runtime.prototype.addShutdownHook_$_$Ljava_lang_Thread$$$_$V = java_lang_Runtime_$_addShutdownHook_$_$Ljava_lang_Thread$$$_$V;
java_lang_Runtime.prototype.availableProcessors_$_$$I = java_lang_Runtime_$_availableProcessors_$_$$I;
java_lang_Runtime.prototype.exec_$_$Ljava_lang_String$$$_$Ljava_lang_Process$$$_ = java_lang_Runtime_$_exec_$_$Ljava_lang_String$$$_$Ljava_lang_Process$$$_;
java_lang_Runtime.prototype.exec_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_ = java_lang_Runtime_$_exec_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_;
java_lang_Runtime.prototype.exec_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_Ljava_io_File$$$_$Ljava_lang_Process$$$_ = java_lang_Runtime_$_exec_$_$Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_Ljava_io_File$$$_$Ljava_lang_Process$$$_;
java_lang_Runtime.prototype.exec_$_$_$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_ = java_lang_Runtime_$_exec_$_$_$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_;
java_lang_Runtime.prototype.exec_$_$_$$$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_ = java_lang_Runtime_$_exec_$_$_$$$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_$Ljava_lang_Process$$$_;
java_lang_Runtime.prototype.exec_$_$_$$$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_Ljava_io_File$$$_$Ljava_lang_Process$$$_ = java_lang_Runtime_$_exec_$_$_$$$$$_Ljava_lang_String$$$__$$$$$_Ljava_lang_String$$$_Ljava_io_File$$$_$Ljava_lang_Process$$$_;
java_lang_Runtime.prototype.exit_$_$I$V = java_lang_Runtime_$_exit_$_$I$V;
java_lang_Runtime.prototype.freeMemory_$_$$J = java_lang_Runtime_$_freeMemory_$_$$J;
java_lang_Runtime.prototype.gc_$_$$V = java_lang_Runtime_$_gc_$_$$V;
java_lang_Runtime.prototype.getLocalizedInputStream_$_$Ljava_io_InputStream$$$_$Ljava_io_InputStream$$$_ = java_lang_Runtime_$_getLocalizedInputStream_$_$Ljava_io_InputStream$$$_$Ljava_io_InputStream$$$_;
java_lang_Runtime.prototype.getLocalizedOutputStream_$_$Ljava_io_OutputStream$$$_$Ljava_io_OutputStream$$$_ = java_lang_Runtime_$_getLocalizedOutputStream_$_$Ljava_io_OutputStream$$$_$Ljava_io_OutputStream$$$_;
java_lang_Runtime.getRuntime_$_$$Ljava_lang_Runtime$$$_ = java_lang_Runtime_$_getRuntime_$_$$Ljava_lang_Runtime$$$_;
java_lang_Runtime.prototype.halt_$_$I$V = java_lang_Runtime_$_halt_$_$I$V;
java_lang_Runtime.prototype.load0_$_$Ljava_lang_Class$$$_Ljava_lang_String$$$_$V = java_lang_Runtime_$_load0_$_$Ljava_lang_Class$$$_Ljava_lang_String$$$_$V;
java_lang_Runtime.prototype.loadLibrary0_$_$Ljava_lang_Class$$$_Ljava_lang_String$$$_$V = java_lang_Runtime_$_loadLibrary0_$_$Ljava_lang_Class$$$_Ljava_lang_String$$$_$V;
java_lang_Runtime.prototype.loadLibrary_$_$Ljava_lang_String$$$_$V = java_lang_Runtime_$_loadLibrary_$_$Ljava_lang_String$$$_$V;
java_lang_Runtime.prototype.load_$_$Ljava_lang_String$$$_$V = java_lang_Runtime_$_load_$_$Ljava_lang_String$$$_$V;
java_lang_Runtime.prototype.maxMemory_$_$$J = java_lang_Runtime_$_maxMemory_$_$$J;
java_lang_Runtime.prototype.removeShutdownHook_$_$Ljava_lang_Thread$$$_$Z = java_lang_Runtime_$_removeShutdownHook_$_$Ljava_lang_Thread$$$_$Z;
java_lang_Runtime.prototype.runFinalization_$_$$V = java_lang_Runtime_$_runFinalization_$_$$V;
java_lang_Runtime.runFinalizersOnExit_$_$Z$V = java_lang_Runtime_$_runFinalizersOnExit_$_$Z$V;
java_lang_Runtime.prototype.totalMemory_$_$$J = java_lang_Runtime_$_totalMemory_$_$$J;
java_lang_Runtime.prototype.traceInstructions_$_$Z$V = java_lang_Runtime_$_traceInstructions_$_$Z$V;
java_lang_Runtime.prototype.traceMethodCalls_$_$Z$V = java_lang_Runtime_$_traceMethodCalls_$_$Z$V;
java_lang_Runtime.prototype.wait_$_$J$V = java_lang_Runtime_$_wait_$_$J$V;
java_lang_Runtime.prototype.wait_$_$JI$V = java_lang_Runtime_$_wait_$_$JI$V;
java_lang_Runtime.prototype.wait_$_$$V = java_lang_Runtime_$_wait_$_$$V;
java_lang_Runtime.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_Runtime_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_Runtime.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_Runtime_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_Runtime.prototype.hashCode_$_$$I = java_lang_Runtime_$_hashCode_$_$$I;
java_lang_Runtime.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_Runtime_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_Runtime.prototype.notify_$_$$V = java_lang_Runtime_$_notify_$_$$V;
java_lang_Runtime.prototype.notifyAll_$_$$V = java_lang_Runtime_$_notifyAll_$_$$V;
