/** @constructor */
function java_io_ObjectInputStream() {}
var java_io_ObjectInputStream_$__$$$init$$$__$_$Ljava_io_InputStream$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Creates an ObjectInputStream that reads from the specified InputStream.
 A serialization stream header is read from the stream and verified.
 This constructor will block until the corresponding ObjectOutputStream
 has written and flushed the header.

 If a security manager is installed, this constructor will check for
 the "enableSubclassImplementation" SerializablePermission when invoked
 directly or indirectly by the constructor of a subclass which overrides
 the ObjectInputStream.readFields or ObjectInputStream.readUnshared
 methods.


Parameters:in - input stream to read from
Throws:
StreamCorruptedException - if the stream header is incorrect
IOException - if an I/O error occurs while reading stream header
SecurityException - if untrusted subclass illegally overrides
                security-sensitive methods
NullPointerException - if in is nullSee Also:ObjectInputStream(), 
readFields(), 
ObjectOutputStream.ObjectOutputStream(OutputStream)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$__$$$init$$$__$_$Ljava_io_InputStream$$$_$V"));
throw ex;
};
var java_io_ObjectInputStream_$_access$000_$_$Ljava_io_ObjectInputStream$$$_$I = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_access$000_$_$Ljava_io_ObjectInputStream$$$_$I"));
throw ex;
//must have a return of type int
};
var java_io_ObjectInputStream_$_access$002_$_$Ljava_io_ObjectInputStream$$$_I$I = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_access$002_$_$Ljava_io_ObjectInputStream$$$_I$I"));
throw ex;
//must have a return of type int
};
var java_io_ObjectInputStream_$_access$100_$_$Ljava_io_ObjectInputStream$$$_$Ljava_io_ObjectInputStream$HandleTable$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_access$100_$_$Ljava_io_ObjectInputStream$$$_$Ljava_io_ObjectInputStream$HandleTable$$$_"));
throw ex;
//must have a return of type HandleTable
};
var java_io_ObjectInputStream_$_access$200_$_$Ljava_io_ObjectInputStream$$$_$Ljava_io_ObjectInputStream$BlockDataInputStream$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_access$200_$_$Ljava_io_ObjectInputStream$$$_$Ljava_io_ObjectInputStream$BlockDataInputStream$$$_"));
throw ex;
//must have a return of type BlockDataInputStream
};
var java_io_ObjectInputStream_$_access$300_$_$Ljava_io_ObjectInputStream$$$_Z$Ljava_lang_Object$$$_ = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_access$300_$_$Ljava_io_ObjectInputStream$$$_Z$Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object
};
var java_io_ObjectInputStream_$_access$500_$_$Ljava_io_ObjectInputStream$$$_$Z = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_access$500_$_$Ljava_io_ObjectInputStream$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_io_ObjectInputStream_$_access$600_$_$Ljava_io_ObjectInputStream$$$_$V = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_access$600_$_$Ljava_io_ObjectInputStream$$$_$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectInputStream_$_access$700_$_$_$$$$$_BI_$$$$$_FII$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_access$700_$_$_$$$$$_BI_$$$$$_FII$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectInputStream_$_access$800_$_$_$$$$$_BI_$$$$$_DII$V = function (arg0, arg1, arg2, arg3, arg4) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_access$800_$_$_$$$$$_BI_$$$$$_DII$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectInputStream_$_available_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the number of bytes that can be read without blocking.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_available_$_$$I"));
throw ex;
//must have a return of type int
};
var java_io_ObjectInputStream_$_close_$_$$V = function (arg0) {
/*implement method!*/
/*
Closes the input stream. Must be called to release any resources
 associated with the stream.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_close_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectInputStream_$_defaultReadObject_$_$$V = function (arg0) {
/*implement method!*/
/*
Read the non-static and non-transient fields of the current class from
 this stream.  This may only be called from the readObject method of the
 class being deserialized. It will throw the NotActiveException if it is
 called otherwise.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_defaultReadObject_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectInputStream_$_enableResolveObject_$_$Z$Z = function (arg0, arg1) {
/*implement method!*/
/*
Enable the stream to allow objects read from the stream to be replaced.
 When enabled, the resolveObject method is called for every object being
 deserialized.

 If enable is true, and there is a security manager installed,
 this method first calls the security manager's
 checkPermission method with the
 SerializablePermission("enableSubstitution") permission to
 ensure it's ok to enable the stream to allow objects read from the
 stream to be replaced.

Parameters:

enable -> true for enabling use of resolveObject for
                every object being deserialized

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_enableResolveObject_$_$Z$Z"));
throw ex;
//must have a return of type boolean
};
var java_io_ObjectInputStream_$_readBoolean_$_$$Z = function (arg0) {
/*implement method!*/
/*
Reads in a boolean.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readBoolean_$_$$Z"));
throw ex;
//must have a return of type boolean
};
var java_io_ObjectInputStream_$_readByte_$_$$B = function (arg0) {
/*implement method!*/
/*
Reads an 8 bit byte.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readByte_$_$$B"));
throw ex;
//must have a return of type byte
};
var java_io_ObjectInputStream_$_readChar_$_$$C = function (arg0) {
/*implement method!*/
/*
Reads a 16 bit char.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readChar_$_$$C"));
throw ex;
//must have a return of type char
};
var java_io_ObjectInputStream_$_readClassDescriptor_$_$$Ljava_io_ObjectStreamClass$$$_ = function (arg0) {
/*implement method!*/
/*
Read a class descriptor from the serialization stream.  This method is
 called when the ObjectInputStream expects a class descriptor as the next
 item in the serialization stream.  Subclasses of ObjectInputStream may
 override this method to read in class descriptors that have been written
 in non-standard formats (by subclasses of ObjectOutputStream which have
 overridden the writeClassDescriptor method).  By default,
 this method reads class descriptors according to the format defined in
 the Object Serialization specification.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readClassDescriptor_$_$$Ljava_io_ObjectStreamClass$$$_"));
throw ex;
//must have a return of type ObjectStreamClass
};
var java_io_ObjectInputStream_$_readDouble_$_$$D = function (arg0) {
/*implement method!*/
/*
Reads a 64 bit double.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readDouble_$_$$D"));
throw ex;
//must have a return of type double
};
var java_io_ObjectInputStream_$_readFields_$_$$Ljava_io_ObjectInputStream$GetField$$$_ = function (arg0) {
/*implement method!*/
/*
Reads the persistent fields from the stream and makes them available by
 name.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readFields_$_$$Ljava_io_ObjectInputStream$GetField$$$_"));
throw ex;
//must have a return of type GetField
};
var java_io_ObjectInputStream_$_readFloat_$_$$F = function (arg0) {
/*implement method!*/
/*
Reads a 32 bit float.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readFloat_$_$$F"));
throw ex;
//must have a return of type float
};
var java_io_ObjectInputStream_$_readFully_$_$_$$$$$_B$V = function (arg0, arg1) {
/*implement method!*/
/*
Reads bytes, blocking until all bytes are read.

Parameters:

buf -> the buffer into which the data is read

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readFully_$_$_$$$$$_B$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectInputStream_$_readFully_$_$_$$$$$_BII$V = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Reads bytes, blocking until all bytes are read.

Parameters:

buf -> the buffer into which the data is read

off -> the start offset of the data

len -> the maximum number of bytes to read

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readFully_$_$_$$$$$_BII$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectInputStream_$_readInt_$_$$I = function (arg0) {
/*implement method!*/
/*
Reads a 32 bit int.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readInt_$_$$I"));
throw ex;
//must have a return of type int
};
var java_io_ObjectInputStream_$_readLine_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Deprecated. This method does not properly convert bytes to characters.
                see DataInputStream for the details and alternatives.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readLine_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_io_ObjectInputStream_$_readLong_$_$$J = function (arg0) {
/*implement method!*/
/*
Reads a 64 bit long.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readLong_$_$$J"));
throw ex;
//must have a return of type long
};
var java_io_ObjectInputStream_$_readObjectOverride_$_$$Ljava_lang_Object$$$_ = function (arg0) {
/*implement method!*/
/*
This method is called by trusted subclasses of ObjectOutputStream that
 constructed ObjectOutputStream using the protected no-arg constructor.
 The subclass is expected to provide an override method with the modifier
 "final".

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readObjectOverride_$_$$Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object
};
var java_io_ObjectInputStream_$_readObject_$_$$Ljava_lang_Object$$$_ = function (arg0) {
/*implement method!*/
/*
Read an object from the ObjectInputStream.  The class of the object, the
 signature of the class, and the values of the non-transient and
 non-static fields of the class and all of its supertypes are read.
 Default deserializing for a class can be overriden using the writeObject
 and readObject methods.  Objects referenced by this object are read
 transitively so that a complete equivalent graph of objects is
 reconstructed by readObject.

 The root object is completely restored when all of its fields and the
 objects it references are completely restored.  At this point the object
 validation callbacks are executed in order based on their registered
 priorities. The callbacks are registered by objects (in the readObject
 special methods) as they are individually restored.

 Exceptions are thrown for problems with the InputStream and for
 classes that should not be deserialized.  All exceptions are fatal to
 the InputStream and leave it in an indeterminate state; it is up to the
 caller to ignore or recover the stream state.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readObject_$_$$Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object
};
var java_io_ObjectInputStream_$_readShort_$_$$S = function (arg0) {
/*implement method!*/
/*
Reads a 16 bit short.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readShort_$_$$S"));
throw ex;
//must have a return of type short
};
var java_io_ObjectInputStream_$_readStreamHeader_$_$$V = function (arg0) {
/*implement method!*/
/*
The readStreamHeader method is provided to allow subclasses to read and
 verify their own stream headers. It reads and verifies the magic number
 and version number.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readStreamHeader_$_$$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectInputStream_$_readTypeString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readTypeString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_io_ObjectInputStream_$_readUTF_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Reads a String in
 modified UTF-8
 format.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readUTF_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_io_ObjectInputStream_$_readUnshared_$_$$Ljava_lang_Object$$$_ = function (arg0) {
/*implement method!*/
/*
Reads an "unshared" object from the ObjectInputStream.  This method is
 identical to readObject, except that it prevents subsequent calls to
 readObject and readUnshared from returning additional references to the
 deserialized instance obtained via this call.  Specifically:
 
   If readUnshared is called to deserialize a back-reference (the
       stream representation of an object which has been written
       previously to the stream), an ObjectStreamException will be
       thrown.

   If readUnshared returns successfully, then any subsequent attempts
       to deserialize back-references to the stream handle deserialized
       by readUnshared will cause an ObjectStreamException to be thrown.
 
 Deserializing an object via readUnshared invalidates the stream handle
 associated with the returned object.  Note that this in itself does not
 always guarantee that the reference returned by readUnshared is unique;
 the deserialized object may define a readResolve method which returns an
 object visible to other parties, or readUnshared may return a Class
 object or enum constant obtainable elsewhere in the stream or through
 external means. If the deserialized object defines a readResolve method
 and the invocation of that method returns an array, then readUnshared
 returns a shallow clone of that array; this guarantees that the returned
 array object is unique and cannot be obtained a second time from an
 invocation of readObject or readUnshared on the ObjectInputStream,
 even if the underlying data stream has been manipulated.

 ObjectInputStream subclasses which override this method can only be
 constructed in security contexts possessing the
 "enableSubclassImplementation" SerializablePermission; any attempt to
 instantiate such a subclass without this permission will cause a
 SecurityException to be thrown.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readUnshared_$_$$Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object
};
var java_io_ObjectInputStream_$_readUnsignedByte_$_$$I = function (arg0) {
/*implement method!*/
/*
Reads an unsigned 8 bit byte.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readUnsignedByte_$_$$I"));
throw ex;
//must have a return of type int
};
var java_io_ObjectInputStream_$_readUnsignedShort_$_$$I = function (arg0) {
/*implement method!*/
/*
Reads an unsigned 16 bit short.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_readUnsignedShort_$_$$I"));
throw ex;
//must have a return of type int
};
var java_io_ObjectInputStream_$_read_$_$$I = function (arg0) {
/*implement method!*/
/*
Reads a byte of data. This method will block if no input is available.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_read_$_$$I"));
throw ex;
//must have a return of type int
};
var java_io_ObjectInputStream_$_read_$_$_$$$$$_BII$I = function (arg0, arg1, arg2, arg3) {
/*implement method!*/
/*
Reads into an array of bytes.  This method will block until some input
 is available. Consider using java.io.DataInputStream.readFully to read
 exactly 'length' bytes.

Parameters:

buf -> the buffer into which the data is read

off -> the start offset of the data

len -> the maximum number of bytes read

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_read_$_$_$$$$$_BII$I"));
throw ex;
//must have a return of type int
};
var java_io_ObjectInputStream_$_registerValidation_$_$Ljava_io_ObjectInputValidation$$$_I$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*
Register an object to be validated before the graph is returned.  While
 similar to resolveObject these validations are called after the entire
 graph has been reconstituted.  Typically, a readObject method will
 register the object with the stream so that when all of the objects are
 restored a final set of validations can be performed.

Parameters:

obj -> the object to receive the validation callback.

prio -> controls the order of callbacks;zero is a good default.
                Use higher numbers to be called back earlier, lower numbers for
                later callbacks. Within a priority, callbacks are processed in
                no particular order.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_registerValidation_$_$Ljava_io_ObjectInputValidation$$$_I$V"));
throw ex;
//must have a return of type void
};
var java_io_ObjectInputStream_$_resolveClass_$_$Ljava_io_ObjectStreamClass$$$_$Ljava_lang_Class$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Load the local class equivalent of the specified stream class
 description.  Subclasses may implement this method to allow classes to
 be fetched from an alternate source. 

 The corresponding method in ObjectOutputStream is
 annotateClass.  This method will be invoked only once for
 each unique class in the stream.  This method can be implemented by
 subclasses to use an alternate loading mechanism but must return a
 Class object. Once returned, if the class is not an array
 class, its serialVersionUID is compared to the serialVersionUID of the
 serialized class, and if there is a mismatch, the deserialization fails
 and an InvalidClassException is thrown.

 The default implementation of this method in
 ObjectInputStream returns the result of calling
      Class.forName(desc.getName(), false, loader)
 
 where loader is determined as follows: if there is a
 method on the current thread's stack whose declaring class was
 defined by a user-defined class loader (and was not a generated to
 implement reflective invocations), then loader is class
 loader corresponding to the closest such method to the currently
 executing frame; otherwise, loader is
 null. If this call results in a
 ClassNotFoundException and the name of the passed
 ObjectStreamClass instance is the Java language keyword
 for a primitive type or void, then the Class object
 representing that primitive type or void will be returned
 (e.g., an ObjectStreamClass with the name
 "int" will be resolved to Integer.TYPE).
 Otherwise, the ClassNotFoundException will be thrown to
 the caller of this method.

Parameters:

desc -> an instance of class ObjectStreamClass

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_resolveClass_$_$Ljava_io_ObjectStreamClass$$$_$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_io_ObjectInputStream_$_resolveObject_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
This method will allow trusted subclasses of ObjectInputStream to
 substitute one object for another during deserialization. Replacing
 objects is disabled until enableResolveObject is called. The
 enableResolveObject method checks that the stream requesting to resolve
 object can be trusted. Every reference to serializable objects is passed
 to resolveObject.  To insure that the private state of objects is not
 unintentionally exposed only trusted streams may use resolveObject.

 This method is called after an object has been read but before it is
 returned from readObject.  The default resolveObject method just returns
 the same object.

 When a subclass is replacing objects it must insure that the
 substituted object is compatible with every field where the reference
 will be stored.  Objects whose type is not a subclass of the type of the
 field or array element abort the serialization by raising an exception
 and the object is not be stored.

 This method is called only once when each object is first
 encountered.  All subsequent references to the object will be redirected
 to the new object.

Parameters:

obj -> object to be substituted

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_resolveObject_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_"));
throw ex;
//must have a return of type Object
};
var java_io_ObjectInputStream_$_resolveProxyClass_$_$_$$$$$_Ljava_lang_String$$$_$Ljava_lang_Class$$$_ = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_resolveProxyClass_$_$_$$$$$_Ljava_lang_String$$$_$Ljava_lang_Class$$$_"));
throw ex;
//must have a return of type Class
};
var java_io_ObjectInputStream_$_skipBytes_$_$I$I = function (arg0, arg1) {
/*implement method!*/
/*
Skips bytes.

Parameters:

len -> the number of bytes to be skipped

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_io_ObjectInputStream_$_skipBytes_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_io_ObjectInputStream_$_mark_$_$I$V = function (arg0, arg1) {
/*invoke parent*/
return java_io_InputStream_$_mark_$_$I$V(arg0, arg1);};
var java_io_ObjectInputStream_$_reset_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_reset_$_$$V(arg0);};
var java_io_ObjectInputStream_$_read_$_$_$$$$$_B$I = function (arg0, arg1) {
/*invoke parent*/
return java_io_InputStream_$_read_$_$_$$$$$_B$I(arg0, arg1);};
var java_io_ObjectInputStream_$_skip_$_$J$J = function (arg0, arg1) {
/*invoke parent*/
return java_io_InputStream_$_skip_$_$J$J(arg0, arg1);};
var java_io_ObjectInputStream_$_markSupported_$_$$Z = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_markSupported_$_$$Z(arg0);};
var java_io_ObjectInputStream_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_io_InputStream_$_wait_$_$J$V(arg0, arg1);};
var java_io_ObjectInputStream_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_io_InputStream_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_io_ObjectInputStream_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_wait_$_$$V(arg0);};
var java_io_ObjectInputStream_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*invoke parent*/
return java_io_InputStream_$_equals_$_$Ljava_lang_Object$$$_$Z(arg0, arg1);};
var java_io_ObjectInputStream_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_toString_$_$$Ljava_lang_String$$$_(arg0);};
var java_io_ObjectInputStream_$_hashCode_$_$$I = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_hashCode_$_$$I(arg0);};
var java_io_ObjectInputStream_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_io_ObjectInputStream_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_notify_$_$$V(arg0);};
var java_io_ObjectInputStream_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_io_InputStream_$_notifyAll_$_$$V(arg0);};
java_io_ObjectInputStream.prototype._$$$init$$$__$_$Ljava_io_InputStream$$$_$V = java_io_ObjectInputStream_$__$$$init$$$__$_$Ljava_io_InputStream$$$_$V;
java_io_ObjectInputStream.access$000_$_$Ljava_io_ObjectInputStream$$$_$I = java_io_ObjectInputStream_$_access$000_$_$Ljava_io_ObjectInputStream$$$_$I;
java_io_ObjectInputStream.access$002_$_$Ljava_io_ObjectInputStream$$$_I$I = java_io_ObjectInputStream_$_access$002_$_$Ljava_io_ObjectInputStream$$$_I$I;
java_io_ObjectInputStream.access$100_$_$Ljava_io_ObjectInputStream$$$_$Ljava_io_ObjectInputStream$HandleTable$$$_ = java_io_ObjectInputStream_$_access$100_$_$Ljava_io_ObjectInputStream$$$_$Ljava_io_ObjectInputStream$HandleTable$$$_;
java_io_ObjectInputStream.access$200_$_$Ljava_io_ObjectInputStream$$$_$Ljava_io_ObjectInputStream$BlockDataInputStream$$$_ = java_io_ObjectInputStream_$_access$200_$_$Ljava_io_ObjectInputStream$$$_$Ljava_io_ObjectInputStream$BlockDataInputStream$$$_;
java_io_ObjectInputStream.access$300_$_$Ljava_io_ObjectInputStream$$$_Z$Ljava_lang_Object$$$_ = java_io_ObjectInputStream_$_access$300_$_$Ljava_io_ObjectInputStream$$$_Z$Ljava_lang_Object$$$_;
java_io_ObjectInputStream.access$500_$_$Ljava_io_ObjectInputStream$$$_$Z = java_io_ObjectInputStream_$_access$500_$_$Ljava_io_ObjectInputStream$$$_$Z;
java_io_ObjectInputStream.access$600_$_$Ljava_io_ObjectInputStream$$$_$V = java_io_ObjectInputStream_$_access$600_$_$Ljava_io_ObjectInputStream$$$_$V;
java_io_ObjectInputStream.access$700_$_$_$$$$$_BI_$$$$$_FII$V = java_io_ObjectInputStream_$_access$700_$_$_$$$$$_BI_$$$$$_FII$V;
java_io_ObjectInputStream.access$800_$_$_$$$$$_BI_$$$$$_DII$V = java_io_ObjectInputStream_$_access$800_$_$_$$$$$_BI_$$$$$_DII$V;
java_io_ObjectInputStream.prototype.available_$_$$I = java_io_ObjectInputStream_$_available_$_$$I;
java_io_ObjectInputStream.prototype.close_$_$$V = java_io_ObjectInputStream_$_close_$_$$V;
java_io_ObjectInputStream.prototype.defaultReadObject_$_$$V = java_io_ObjectInputStream_$_defaultReadObject_$_$$V;
java_io_ObjectInputStream.prototype.enableResolveObject_$_$Z$Z = java_io_ObjectInputStream_$_enableResolveObject_$_$Z$Z;
java_io_ObjectInputStream.prototype.readBoolean_$_$$Z = java_io_ObjectInputStream_$_readBoolean_$_$$Z;
java_io_ObjectInputStream.prototype.readByte_$_$$B = java_io_ObjectInputStream_$_readByte_$_$$B;
java_io_ObjectInputStream.prototype.readChar_$_$$C = java_io_ObjectInputStream_$_readChar_$_$$C;
java_io_ObjectInputStream.prototype.readClassDescriptor_$_$$Ljava_io_ObjectStreamClass$$$_ = java_io_ObjectInputStream_$_readClassDescriptor_$_$$Ljava_io_ObjectStreamClass$$$_;
java_io_ObjectInputStream.prototype.readDouble_$_$$D = java_io_ObjectInputStream_$_readDouble_$_$$D;
java_io_ObjectInputStream.prototype.readFields_$_$$Ljava_io_ObjectInputStream$GetField$$$_ = java_io_ObjectInputStream_$_readFields_$_$$Ljava_io_ObjectInputStream$GetField$$$_;
java_io_ObjectInputStream.prototype.readFloat_$_$$F = java_io_ObjectInputStream_$_readFloat_$_$$F;
java_io_ObjectInputStream.prototype.readFully_$_$_$$$$$_B$V = java_io_ObjectInputStream_$_readFully_$_$_$$$$$_B$V;
java_io_ObjectInputStream.prototype.readFully_$_$_$$$$$_BII$V = java_io_ObjectInputStream_$_readFully_$_$_$$$$$_BII$V;
java_io_ObjectInputStream.prototype.readInt_$_$$I = java_io_ObjectInputStream_$_readInt_$_$$I;
java_io_ObjectInputStream.prototype.readLine_$_$$Ljava_lang_String$$$_ = java_io_ObjectInputStream_$_readLine_$_$$Ljava_lang_String$$$_;
java_io_ObjectInputStream.prototype.readLong_$_$$J = java_io_ObjectInputStream_$_readLong_$_$$J;
java_io_ObjectInputStream.prototype.readObjectOverride_$_$$Ljava_lang_Object$$$_ = java_io_ObjectInputStream_$_readObjectOverride_$_$$Ljava_lang_Object$$$_;
java_io_ObjectInputStream.prototype.readObject_$_$$Ljava_lang_Object$$$_ = java_io_ObjectInputStream_$_readObject_$_$$Ljava_lang_Object$$$_;
java_io_ObjectInputStream.prototype.readShort_$_$$S = java_io_ObjectInputStream_$_readShort_$_$$S;
java_io_ObjectInputStream.prototype.readStreamHeader_$_$$V = java_io_ObjectInputStream_$_readStreamHeader_$_$$V;
java_io_ObjectInputStream.prototype.readTypeString_$_$$Ljava_lang_String$$$_ = java_io_ObjectInputStream_$_readTypeString_$_$$Ljava_lang_String$$$_;
java_io_ObjectInputStream.prototype.readUTF_$_$$Ljava_lang_String$$$_ = java_io_ObjectInputStream_$_readUTF_$_$$Ljava_lang_String$$$_;
java_io_ObjectInputStream.prototype.readUnshared_$_$$Ljava_lang_Object$$$_ = java_io_ObjectInputStream_$_readUnshared_$_$$Ljava_lang_Object$$$_;
java_io_ObjectInputStream.prototype.readUnsignedByte_$_$$I = java_io_ObjectInputStream_$_readUnsignedByte_$_$$I;
java_io_ObjectInputStream.prototype.readUnsignedShort_$_$$I = java_io_ObjectInputStream_$_readUnsignedShort_$_$$I;
java_io_ObjectInputStream.prototype.read_$_$$I = java_io_ObjectInputStream_$_read_$_$$I;
java_io_ObjectInputStream.prototype.read_$_$_$$$$$_BII$I = java_io_ObjectInputStream_$_read_$_$_$$$$$_BII$I;
java_io_ObjectInputStream.prototype.registerValidation_$_$Ljava_io_ObjectInputValidation$$$_I$V = java_io_ObjectInputStream_$_registerValidation_$_$Ljava_io_ObjectInputValidation$$$_I$V;
java_io_ObjectInputStream.prototype.resolveClass_$_$Ljava_io_ObjectStreamClass$$$_$Ljava_lang_Class$$$_ = java_io_ObjectInputStream_$_resolveClass_$_$Ljava_io_ObjectStreamClass$$$_$Ljava_lang_Class$$$_;
java_io_ObjectInputStream.prototype.resolveObject_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_ = java_io_ObjectInputStream_$_resolveObject_$_$Ljava_lang_Object$$$_$Ljava_lang_Object$$$_;
java_io_ObjectInputStream.prototype.resolveProxyClass_$_$_$$$$$_Ljava_lang_String$$$_$Ljava_lang_Class$$$_ = java_io_ObjectInputStream_$_resolveProxyClass_$_$_$$$$$_Ljava_lang_String$$$_$Ljava_lang_Class$$$_;
java_io_ObjectInputStream.prototype.skipBytes_$_$I$I = java_io_ObjectInputStream_$_skipBytes_$_$I$I;
java_io_ObjectInputStream.prototype.mark_$_$I$V = java_io_ObjectInputStream_$_mark_$_$I$V;
java_io_ObjectInputStream.prototype.reset_$_$$V = java_io_ObjectInputStream_$_reset_$_$$V;
java_io_ObjectInputStream.prototype.read_$_$_$$$$$_B$I = java_io_ObjectInputStream_$_read_$_$_$$$$$_B$I;
java_io_ObjectInputStream.prototype.skip_$_$J$J = java_io_ObjectInputStream_$_skip_$_$J$J;
java_io_ObjectInputStream.prototype.markSupported_$_$$Z = java_io_ObjectInputStream_$_markSupported_$_$$Z;
java_io_ObjectInputStream.prototype.wait_$_$J$V = java_io_ObjectInputStream_$_wait_$_$J$V;
java_io_ObjectInputStream.prototype.wait_$_$JI$V = java_io_ObjectInputStream_$_wait_$_$JI$V;
java_io_ObjectInputStream.prototype.wait_$_$$V = java_io_ObjectInputStream_$_wait_$_$$V;
java_io_ObjectInputStream.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_io_ObjectInputStream_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_io_ObjectInputStream.prototype.toString_$_$$Ljava_lang_String$$$_ = java_io_ObjectInputStream_$_toString_$_$$Ljava_lang_String$$$_;
java_io_ObjectInputStream.prototype.hashCode_$_$$I = java_io_ObjectInputStream_$_hashCode_$_$$I;
java_io_ObjectInputStream.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_io_ObjectInputStream_$_getClass_$_$$Ljava_lang_Class$$$_;
java_io_ObjectInputStream.prototype.notify_$_$$V = java_io_ObjectInputStream_$_notify_$_$$V;
java_io_ObjectInputStream.prototype.notifyAll_$_$$V = java_io_ObjectInputStream_$_notifyAll_$_$$V;
