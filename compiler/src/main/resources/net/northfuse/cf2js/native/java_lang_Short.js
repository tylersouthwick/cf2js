/** @constructor */
function java_lang_Short() {}
var java_lang_Short_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Short object that
 represents the short value indicated by the
 String parameter. The string is converted to a
 short value in exactly the manner used by the
 parseShort method for radix 10.


Parameters:s - the String to be converted to a 
                        Short
Throws:
NumberFormatException - If the String 
                        does not contain a parsable short.See Also:parseShort(java.lang.String, int)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$__$$$init$$$__$_$Ljava_lang_String$$$_$V"));
throw ex;
};
var java_lang_Short_$__$$$init$$$__$_$S$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Short object that
 represents the specified short value.


Parameters:value - the value to be represented by the 
                        Short.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$__$$$init$$$__$_$S$V"));
throw ex;
};
var java_lang_Short_$_byteValue_$_$$B = function (arg0) {
/*implement method!*/
/*
Returns the value of this Short as a
 byte.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_byteValue_$_$$B"));
throw ex;
//must have a return of type byte
};
var java_lang_Short_$_compareTo_$_$Ljava_lang_Object$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_compareTo_$_$Ljava_lang_Object$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Short_$_compareTo_$_$Ljava_lang_Short$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Compares two Short objects numerically.

Parameters:

anotherShort -> the Short to be compared.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_compareTo_$_$Ljava_lang_Short$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Short_$_decode_$_$Ljava_lang_String$$$_$Ljava_lang_Short$$$_ = function (arg0) {
/*implement method!*/
/*
Decodes a String into a Short.
 Accepts decimal, hexadecimal, and octal numbers given by
 the following grammar:

 
 
 DecodableString:
 Signopt DecimalNumeral
 Signopt 0x HexDigits
 Signopt 0X HexDigits
 Signopt # HexDigits
 Signopt 0 OctalDigits
 
 Sign:
 -
 
 

 DecimalNumeral, HexDigits, and OctalDigits
 are defined in §3.10.1 
 of the Java 
 Language Specification.
 
 The sequence of characters following an (optional) negative
 sign and/or radix specifier ("0x",
 "0X", "#", or
 leading zero) is parsed as by the Short.parseShort
 method with the indicated radix (10, 16, or 8).  This sequence
 of characters must represent a positive value or a NumberFormatException will be thrown.  The result is negated
 if first character of the specified String is the
 minus sign.  No whitespace characters are permitted in the
 String.

Parameters:

nm -> the String to decode.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_decode_$_$Ljava_lang_String$$$_$Ljava_lang_Short$$$_"));
throw ex;
//must have a return of type Short
};
var java_lang_Short_$_doubleValue_$_$$D = function (arg0) {
/*implement method!*/
/*
Returns the value of this Short as a
 double.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_doubleValue_$_$$D"));
throw ex;
//must have a return of type double
};
var java_lang_Short_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Compares this object to the specified object.  The result is
 true if and only if the argument is not
 null and is a Short object that
 contains the same short value as this object.

Parameters:

obj -> the object to compare with

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_equals_$_$Ljava_lang_Object$$$_$Z"));
throw ex;
//must have a return of type boolean
};
var java_lang_Short_$_floatValue_$_$$F = function (arg0) {
/*implement method!*/
/*
Returns the value of this Short as a
 float.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_floatValue_$_$$F"));
throw ex;
//must have a return of type float
};
var java_lang_Short_$_hashCode_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns a hash code for this Short.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_hashCode_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Short_$_intValue_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the value of this Short as an
 int.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_intValue_$_$$I"));
throw ex;
//must have a return of type int
};
var java_lang_Short_$_longValue_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the value of this Short as a
 long.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_longValue_$_$$J"));
throw ex;
//must have a return of type long
};
var java_lang_Short_$_parseShort_$_$Ljava_lang_String$$$_$S = function (arg0) {
/*implement method!*/
/*
Parses the string argument as a signed decimal
 short. The characters in the string must all be
 decimal digits, except that the first character may be an ASCII
 minus sign '-' ('\u002D') to
 indicate a negative value. The resulting short value is
 returned, exactly as if the argument and the radix 10 were
 given as arguments to the parseShort(java.lang.String,
 int) method.

Parameters:

s -> a String containing the short
                  representation to be parsed

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_parseShort_$_$Ljava_lang_String$$$_$S"));
throw ex;
//must have a return of type short
};
var java_lang_Short_$_parseShort_$_$Ljava_lang_String$$$_I$S = function (arg0, arg1) {
/*implement method!*/
/*
Parses the string argument as a signed short in
 the radix specified by the second argument. The characters in
 the string must all be digits, of the specified radix (as
 determined by whether Character.digit(char,
 int) returns a nonnegative value) except that the first
 character may be an ASCII minus sign '-'
 ('\u002D') to indicate a negative value.  The
 resulting byte value is returned.
 
 An exception of type NumberFormatException is
 thrown if any of the following situations occurs:
 
  The first argument is null or is a string of
 length zero.

  The radix is either smaller than Character.MIN_RADIX or larger than Character.MAX_RADIX.

  Any character of the string is not a digit of the specified
 radix, except that the first character may be a minus sign
 '-' ('\u002D') provided that the
 string is longer than length 1.

  The value represented by the string is not a value of type
 short.

Parameters:

s -> the String containing the 
                        short representation to be parsed

radix -> the radix to be used while parsing s

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_parseShort_$_$Ljava_lang_String$$$_I$S"));
throw ex;
//must have a return of type short
};
var java_lang_Short_$_reverseBytes_$_$S$S = function (arg0) {
/*implement method!*/
/*
Returns the value obtained by reversing the order of the bytes in the
 two's complement representation of the specified short value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_reverseBytes_$_$S$S"));
throw ex;
//must have a return of type short
};
var java_lang_Short_$_shortValue_$_$$S = function (arg0) {
/*implement method!*/
/*
Returns the value of this Short as a
 short.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_shortValue_$_$$S"));
throw ex;
//must have a return of type short
};
var java_lang_Short_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a String object representing this
 Short's value.  The value is converted to signed
 decimal representation and returned as a string, exactly as if
 the short value were given as an argument to the
 toString(short) method.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_toString_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Short_$_toString_$_$S$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a new String object representing the
 specified short. The radix is assumed to be 10.

Parameters:

s -> the short to be converted

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_toString_$_$S$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Short_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Short$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a Short object holding the
 value given by the specified String. The argument
 is interpreted as representing a signed decimal
 short, exactly as if the argument were given to
 the parseShort(java.lang.String) method. The result is
 a Short object that represents the
 short value specified by the string.   In other
 words, this method returns a Byte object equal to
 the value of:

 
 new Short(Short.parseShort(s))

Parameters:

s -> the string to be parsed

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Short$$$_"));
throw ex;
//must have a return of type Short
};
var java_lang_Short_$_valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Short$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a Short object holding the value
 extracted from the specified String when parsed
 with the radix given by the second argument. The first argument
 is interpreted as representing a signed short in
 the radix specified by the second argument, exactly as if the
 argument were given to the parseShort(java.lang.String,
 int) method. The result is a Short object that
 represents the short value specified by the string.
  In other words, this method returns a Short object
 equal to the value of:

 
 new Short(Short.parseShort(s, radix))

Parameters:

s -> the string to be parsed

radix -> the radix to be used in interpreting s

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Short$$$_"));
throw ex;
//must have a return of type Short
};
var java_lang_Short_$_valueOf_$_$S$Ljava_lang_Short$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a Short instance representing the specified
 short value.
 If a new Short instance is not required, this method
 should generally be used in preference to the constructor
 Short(short), as this method is likely to yield
 significantly better space and time performance by caching
 frequently requested values.

Parameters:

s -> a short value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Short_$_valueOf_$_$S$Ljava_lang_Short$$$_"));
throw ex;
//must have a return of type Short
};
var java_lang_Short_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$J$V(arg0, arg1);};
var java_lang_Short_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_Short_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$$V(arg0);};
var java_lang_Short_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_Short_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notify_$_$$V(arg0);};
var java_lang_Short_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notifyAll_$_$$V(arg0);};
java_lang_Short.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_lang_Short_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_lang_Short.prototype._$$$init$$$__$_$S$V = java_lang_Short_$__$$$init$$$__$_$S$V;
java_lang_Short.prototype.byteValue_$_$$B = java_lang_Short_$_byteValue_$_$$B;
java_lang_Short.prototype.compareTo_$_$Ljava_lang_Object$$$_$I = java_lang_Short_$_compareTo_$_$Ljava_lang_Object$$$_$I;
java_lang_Short.prototype.compareTo_$_$Ljava_lang_Short$$$_$I = java_lang_Short_$_compareTo_$_$Ljava_lang_Short$$$_$I;
java_lang_Short.decode_$_$Ljava_lang_String$$$_$Ljava_lang_Short$$$_ = java_lang_Short_$_decode_$_$Ljava_lang_String$$$_$Ljava_lang_Short$$$_;
java_lang_Short.prototype.doubleValue_$_$$D = java_lang_Short_$_doubleValue_$_$$D;
java_lang_Short.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_Short_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_Short.prototype.floatValue_$_$$F = java_lang_Short_$_floatValue_$_$$F;
java_lang_Short.prototype.hashCode_$_$$I = java_lang_Short_$_hashCode_$_$$I;
java_lang_Short.prototype.intValue_$_$$I = java_lang_Short_$_intValue_$_$$I;
java_lang_Short.prototype.longValue_$_$$J = java_lang_Short_$_longValue_$_$$J;
java_lang_Short.parseShort_$_$Ljava_lang_String$$$_$S = java_lang_Short_$_parseShort_$_$Ljava_lang_String$$$_$S;
java_lang_Short.parseShort_$_$Ljava_lang_String$$$_I$S = java_lang_Short_$_parseShort_$_$Ljava_lang_String$$$_I$S;
java_lang_Short.reverseBytes_$_$S$S = java_lang_Short_$_reverseBytes_$_$S$S;
java_lang_Short.prototype.shortValue_$_$$S = java_lang_Short_$_shortValue_$_$$S;
java_lang_Short.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_Short_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_Short.toString_$_$S$Ljava_lang_String$$$_ = java_lang_Short_$_toString_$_$S$Ljava_lang_String$$$_;
java_lang_Short.valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Short$$$_ = java_lang_Short_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Short$$$_;
java_lang_Short.valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Short$$$_ = java_lang_Short_$_valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Short$$$_;
java_lang_Short.valueOf_$_$S$Ljava_lang_Short$$$_ = java_lang_Short_$_valueOf_$_$S$Ljava_lang_Short$$$_;
java_lang_Short.prototype.wait_$_$J$V = java_lang_Short_$_wait_$_$J$V;
java_lang_Short.prototype.wait_$_$JI$V = java_lang_Short_$_wait_$_$JI$V;
java_lang_Short.prototype.wait_$_$$V = java_lang_Short_$_wait_$_$$V;
java_lang_Short.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_Short_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_Short.prototype.notify_$_$$V = java_lang_Short_$_notify_$_$$V;
java_lang_Short.prototype.notifyAll_$_$$V = java_lang_Short_$_notifyAll_$_$$V;
