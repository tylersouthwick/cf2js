/** @constructor */
function java_lang_Integer() {}
var java_lang_Integer_$__$$$init$$$__$_$I$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Integer object that
 represents the specified int value.


Parameters:value - the value to be represented by the 
                        Integer object.

*/
	arg0.num = arg1;
};
var java_lang_Integer_$__$$$init$$$__$_$Ljava_lang_String$$$_$V = function (arg0, arg1) {
/*implement constructor!*/
/*
Constructs a newly allocated Integer object that
 represents the int value indicated by the
 String parameter. The string is converted to an
 int value in exactly the manner used by the
 parseInt method for radix 10.


Parameters:s - the String to be converted to an
                 Integer.
Throws:
NumberFormatException - if the String does not
               contain a parsable integer.See Also:parseInt(java.lang.String, int)

*/
	arg0.num = java_lang_Integer_$_parseInt_$_$Ljava_lang_String$$$_$I(arg1);
};
var java_lang_Integer_$_access$000_$_$$Ljava_lang_String$$$_ = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_access$000_$_$$Ljava_lang_String$$$_"));
throw ex;
//must have a return of type String
};
var java_lang_Integer_$_bitCount_$_$I$I = function (arg0) {
/*implement method!*/
/*
Returns the number of one-bits in the two's complement binary
 representation of the specified int value.  This function is
 sometimes referred to as the population count.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_bitCount_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_byteValue_$_$$B = function (arg0) {
/*implement method!*/
/*
Returns the value of this Integer as a
 byte.

*/
//must have a return of type byte
	return arg0.num;
};
var java_lang_Integer_$_compareTo_$_$Ljava_lang_Integer$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*
Compares two Integer objects numerically.

Parameters:

anotherInteger -> the Integer to be compared.

*/
//must have a return of type int
	return java_lang_String_nullCheck(arg1, function () {
		var num1 = arg0.num;
		var num2 = arg1.num;
		if (num1 < num2) {
			return -1;
		} else if (num1 === num2) {
			return 0;
		} else {
			return 1;
		}
	});
};
var java_lang_Integer_$_compareTo_$_$Ljava_lang_Object$$$_$I = function (arg0, arg1) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_compareTo_$_$Ljava_lang_Object$$$_$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_decode_$_$Ljava_lang_String$$$_$Ljava_lang_Integer$$$_ = function (arg0) {
/*implement method!*/
/*
Decodes a String into an Integer.
 Accepts decimal, hexadecimal, and octal numbers given
 by the following grammar:

 
 
 DecodableString:
 Signopt DecimalNumeral
 Signopt 0x HexDigits
 Signopt 0X HexDigits
 Signopt # HexDigits
 Signopt 0 OctalDigits
 
 Sign:
 -
 
 

 DecimalNumeral, HexDigits, and OctalDigits
 are defined in §3.10.1 
 of the Java 
 Language Specification.
 
 The sequence of characters following an (optional) negative
 sign and/or radix specifier ("0x",
 "0X", "#", or
 leading zero) is parsed as by the Integer.parseInt
 method with the indicated radix (10, 16, or 8).  This sequence
 of characters must represent a positive value or a NumberFormatException will be thrown.  The result is negated
 if first character of the specified String is the
 minus sign.  No whitespace characters are permitted in the
 String.

Parameters:

nm -> the String to decode.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_decode_$_$Ljava_lang_String$$$_$Ljava_lang_Integer$$$_"));
throw ex;
//must have a return of type Integer
};
var java_lang_Integer_$_doubleValue_$_$$D = function (arg0) {
/*implement method!*/
/*
Returns the value of this Integer as a
 double.

*/
//must have a return of type double
	return arg0.num;
};
var java_lang_Integer_$_equals_$_$Ljava_lang_Object$$$_$Z = function (arg0, arg1) {
/*implement method!*/
/*
Compares this object to the specified object.  The result is
 true if and only if the argument is not
 null and is an Integer object that
 contains the same int value as this object.

Parameters:

obj -> the object to compare with.

*/
//must have a return of type boolean
	if (arg1 === null) {
		return false;
	} else {
		return arg0.num === arg1.num;
	}
};
var java_lang_Integer_$_floatValue_$_$$F = function (arg0) {
/*implement method!*/
/*
Returns the value of this Integer as a
 float.

*/
//must have a return of type float
	return arg0.num;
};
var java_lang_Integer_$_getAndRemoveCacheProperties_$_$$V = function () {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_getAndRemoveCacheProperties_$_$$V"));
throw ex;
//must have a return of type void
};
var java_lang_Integer_$_getChars_$_$II_$$$$$_C$V = function (arg0, arg1, arg2) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_getChars_$_$II_$$$$$_C$V"));
throw ex;
//must have a return of type void
};
var java_lang_Integer_$_getInteger_$_$Ljava_lang_String$$$_$Ljava_lang_Integer$$$_ = function (arg0) {
/*implement method!*/
/*
Determines the integer value of the system property with the
 specified name.
 
 The first argument is treated as the name of a system property. 
 System properties are accessible through the 
 System.getProperty(java.lang.String) method. The 
 string value of this property is then interpreted as an integer 
 value and an Integer object representing this value is 
 returned. Details of possible numeric formats can be found with 
 the definition of getProperty. 
 
 If there is no property with the specified name, if the specified name
 is empty or null, or if the property does not have 
 the correct numeric format, then null is returned.
 
 In other words, this method returns an Integer
 object equal to the value of:

 
 getInteger(nm, null)

Parameters:

nm -> property name.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_getInteger_$_$Ljava_lang_String$$$_$Ljava_lang_Integer$$$_"));
throw ex;
//must have a return of type Integer
};
var java_lang_Integer_$_getInteger_$_$Ljava_lang_String$$$_I$Ljava_lang_Integer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Determines the integer value of the system property with the
 specified name.
 
 The first argument is treated as the name of a system property.
 System properties are accessible through the System.getProperty(java.lang.String) method. The 
 string value of this property is then interpreted as an integer 
 value and an Integer object representing this value is 
 returned. Details of possible numeric formats can be found with 
 the definition of getProperty. 
 
 The second argument is the default value. An Integer object
 that represents the value of the second argument is returned if there
 is no property of the specified name, if the property does not have
 the correct numeric format, or if the specified name is empty or
  null.
 
 In other words, this method returns an Integer object 
 equal to the value of:
 
 getInteger(nm, new Integer(val))
 
 but in practice it may be implemented in a manner such as: 
  Integer result = getInteger(nm, null);
 return (result == null) ? new Integer(val) : result;
 
 to avoid the unnecessary allocation of an Integer 
 object when the default value is not needed.

Parameters:

nm -> property name.

val -> default value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_getInteger_$_$Ljava_lang_String$$$_I$Ljava_lang_Integer$$$_"));
throw ex;
//must have a return of type Integer
};
var java_lang_Integer_$_getInteger_$_$Ljava_lang_String$$$_Ljava_lang_Integer$$$_$Ljava_lang_Integer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns the integer value of the system property with the
 specified name.  The first argument is treated as the name of a
 system property.  System properties are accessible through the
 System.getProperty(java.lang.String) method.
 The string value of this property is then interpreted as an
 integer value, as per the Integer.decode method,
 and an Integer object representing this value is
 returned.
 
 If the property value begins with the two ASCII characters 
         0x or the ASCII character #, not 
      followed by a minus sign, then the rest of it is parsed as a 
      hexadecimal integer exactly as by the method 
      valueOf(java.lang.String, int) with radix 16. 
 If the property value begins with the ASCII character 
     0 followed by another character, it is parsed as an 
     octal integer exactly as by the method 
     valueOf(java.lang.String, int) with radix 8. 
 Otherwise, the property value is parsed as a decimal integer 
 exactly as by the method valueOf(java.lang.String, int) 
 with radix 10. 
 
 The second argument is the default value. The default value is
 returned if there is no property of the specified name, if the
 property does not have the correct numeric format, or if the
 specified name is empty or null.

Parameters:

nm -> property name.

val -> default value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_getInteger_$_$Ljava_lang_String$$$_Ljava_lang_Integer$$$_$Ljava_lang_Integer$$$_"));
throw ex;
//must have a return of type Integer
};
var java_lang_Integer_$_hashCode_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns a hash code for this Integer.

*/
//must have a return of type int
	return arg0.num;
};
var java_lang_Integer_$_highestOneBit_$_$I$I = function (arg0) {
/*implement method!*/
/*
Returns an int value with at most a single one-bit, in the
 position of the highest-order ("leftmost") one-bit in the specified
 int value.  Returns zero if the specified value has no
 one-bits in its two's complement binary representation, that is, if it
 is equal to zero.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_highestOneBit_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_intValue_$_$$I = function (arg0) {
/*implement method!*/
/*
Returns the value of this Integer as an
 int.

*/
//must have a return of type int
	return arg0.num;
};
var java_lang_Integer_$_longValue_$_$$J = function (arg0) {
/*implement method!*/
/*
Returns the value of this Integer as a
 long.

*/
//must have a return of type long
	return arg0.num;
};
var java_lang_Integer_$_lowestOneBit_$_$I$I = function (arg0) {
/*implement method!*/
/*
Returns an int value with at most a single one-bit, in the
 position of the lowest-order ("rightmost") one-bit in the specified
 int value.  Returns zero if the specified value has no
 one-bits in its two's complement binary representation, that is, if it
 is equal to zero.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_lowestOneBit_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_numberOfLeadingZeros_$_$I$I = function (arg0) {
/*implement method!*/
/*
Returns the number of zero bits preceding the highest-order
 ("leftmost") one-bit in the two's complement binary representation
 of the specified int value.  Returns 32 if the
 specified value has no one-bits in its two's complement representation,
 in other words if it is equal to zero.

 Note that this method is closely related to the logarithm base 2.
 For all positive int values x:
 
 floor(log2(x)) = 31 - numberOfLeadingZeros(x)
 ceil(log2(x)) = 32 - numberOfLeadingZeros(x - 1)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_numberOfLeadingZeros_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_numberOfTrailingZeros_$_$I$I = function (arg0) {
/*implement method!*/
/*
Returns the number of zero bits following the lowest-order ("rightmost")
 one-bit in the two's complement binary representation of the specified
 int value.  Returns 32 if the specified value has no
 one-bits in its two's complement representation, in other words if it is
 equal to zero.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_numberOfTrailingZeros_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_parseInt_$_$Ljava_lang_String$$$_$I = function (arg0) {
/*implement method!*/
/*
Parses the string argument as a signed decimal integer. The 
 characters in the string must all be decimal digits, except that 
 the first character may be an ASCII minus sign '-' 
 ('\u002D') to indicate a negative value. The resulting 
 integer value is returned, exactly as if the argument and the radix 
 10 were given as arguments to the 
 parseInt(java.lang.String, int) method.

Parameters:

s -> a String containing the int
             representation to be parsed

*/
//must have a return of type int
	return java_lang_Integer_$_parseInt_$_$Ljava_lang_String$$$_I$I(arg0, 10);
};
var java_lang_Integer_$_parseInt_$_$Ljava_lang_String$$$_I$I = function (arg0, arg1) {
/*implement method!*/
/*
Parses the string argument as a signed integer in the radix 
 specified by the second argument. The characters in the string 
 must all be digits of the specified radix (as determined by 
 whether Character.digit(char, int) returns a 
 nonnegative value), except that the first character may be an 
 ASCII minus sign '-' ('\u002D') to 
 indicate a negative value. The resulting integer value is returned. 
 
 An exception of type NumberFormatException is
 thrown if any of the following situations occurs:
 
 The first argument is null or is a string of
 length zero.
 The radix is either smaller than 
 Character.MIN_RADIX or
 larger than Character.MAX_RADIX. 
 Any character of the string is not a digit of the specified
 radix, except that the first character may be a minus sign
 '-' ('\u002D') provided that the
 string is longer than length 1.
 The value represented by the string is not a value of type
 int. 
 
 Examples:
  parseInt("0", 10) returns 0
 parseInt("473", 10) returns 473
 parseInt("-0", 10) returns 0
 parseInt("-FF", 16) returns -255
 parseInt("1100110", 2) returns 102
 parseInt("2147483647", 10) returns 2147483647
 parseInt("-2147483648", 10) returns -2147483648
 parseInt("2147483648", 10) throws a NumberFormatException
 parseInt("99", 8) throws a NumberFormatException
 parseInt("Kona", 10) throws a NumberFormatException
 parseInt("Kona", 27) returns 411787

Parameters:

s -> the String containing the integer 
                        representation to be parsed

radix -> the radix to be used while parsing s.

*/
//must have a return of type int
	var i;
	if (arg0 === null) {
		i = NaN;
	} else {
		i = parseInt(arg0.str, arg1);
	}

	if (isNaN(i)) {
		var ex = new java_lang_NumberFormatException();
		ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("invalid number " + arg0 + " for radix " + arg1));
		throw ex;
	} else {
		return i;
	}
};
var java_lang_Integer_$_reverseBytes_$_$I$I = function (arg0) {
/*implement method!*/
/*
Returns the value obtained by reversing the order of the bytes in the
 two's complement representation of the specified int value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_reverseBytes_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_reverse_$_$I$I = function (arg0) {
/*implement method!*/
/*
Returns the value obtained by reversing the order of the bits in the
 two's complement binary representation of the specified int
 value.

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_reverse_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_rotateLeft_$_$II$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the value obtained by rotating the two's complement binary
 representation of the specified int value left by the
 specified number of bits.  (Bits shifted out of the left hand, or
 high-order, side reenter on the right, or low-order.)

 Note that left rotation with a negative distance is equivalent to
 right rotation: rotateLeft(val, -distance) == rotateRight(val,
 distance).  Note also that rotation by any multiple of 32 is a
 no-op, so all but the last five bits of the rotation distance can be
 ignored, even if the distance is negative: rotateLeft(val,
 distance) == rotateLeft(val, distance & 0x1F).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_rotateLeft_$_$II$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_rotateRight_$_$II$I = function (arg0, arg1) {
/*implement method!*/
/*
Returns the value obtained by rotating the two's complement binary
 representation of the specified int value right by the
 specified number of bits.  (Bits shifted out of the right hand, or
 low-order, side reenter on the left, or high-order.)

 Note that right rotation with a negative distance is equivalent to
 left rotation: rotateRight(val, -distance) == rotateLeft(val,
 distance).  Note also that rotation by any multiple of 32 is a
 no-op, so all but the last five bits of the rotation distance can be
 ignored, even if the distance is negative: rotateRight(val,
 distance) == rotateRight(val, distance & 0x1F).

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_rotateRight_$_$II$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_shortValue_$_$$S = function (arg0) {
/*implement method!*/
/*
Returns the value of this Integer as a
 short.

*/
//must have a return of type short
	return arg0.num;
};
var java_lang_Integer_$_signum_$_$I$I = function (arg0) {
/*implement method!*/
/*
Returns the signum function of the specified int value.  (The
 return value is -1 if the specified value is negative; 0 if the
 specified value is zero; and 1 if the specified value is positive.)

*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_signum_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_stringSize_$_$I$I = function (arg0) {
/*implement method!*/
/*


*/
var ex = new java_lang_UnsupportedOperationException();
ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("java_lang_Integer_$_stringSize_$_$I$I"));
throw ex;
//must have a return of type int
};
var java_lang_Integer_$_toBinaryString_$_$I$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of the integer argument as an
 unsigned integer in base 2.
 
 The unsigned integer value is the argument plus 232
 if the argument is negative; otherwise it is equal to the
 argument.  This value is converted to a string of ASCII digits
 in binary (base 2) with no extra leading 0s.
 If the unsigned magnitude is zero, it is represented by a
 single zero character '0'
 ('\u0030'); otherwise, the first character of
 the representation of the unsigned magnitude will not be the
 zero character. The characters '0'
 ('\u0030') and '1'
 ('\u0031') are used as binary digits.

Parameters:

i -> an integer to be converted to a string.

*/
//must have a return of type String
	return java_lang_Integer_$_toString_$_$II$Ljava_lang_String$$$_(arg0, 2);
};
var java_lang_Integer_$_toHexString_$_$I$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of the integer argument as an
 unsigned integer in base 16.
 
 The unsigned integer value is the argument plus 232
 if the argument is negative; otherwise, it is equal to the
 argument.  This value is converted to a string of ASCII digits
 in hexadecimal (base 16) with no extra leading
 0s. If the unsigned magnitude is zero, it is
 represented by a single zero character '0'
 ('\u0030'); otherwise, the first character of
 the representation of the unsigned magnitude will not be the
 zero character. The following characters are used as
 hexadecimal digits:
  0123456789abcdef
 
 These are the characters '\u0030' through
 '\u0039' and '\u0061' through
 '\u0066'. If uppercase letters are
 desired, the String.toUpperCase() method may
 be called on the result:
  Integer.toHexString(n).toUpperCase()

Parameters:

i -> an integer to be converted to a string.

*/
//must have a return of type String
	return java_lang_Integer_$_toString_$_$II$Ljava_lang_String$$$_(arg0, 16);
};
var java_lang_Integer_$_toOctalString_$_$I$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a string representation of the integer argument as an
 unsigned integer in base 8.
 
 The unsigned integer value is the argument plus 232
 if the argument is negative; otherwise, it is equal to the
 argument.  This value is converted to a string of ASCII digits
 in octal (base 8) with no extra leading 0s.
 
 If the unsigned magnitude is zero, it is represented by a
 single zero character '0'
 ('\u0030'); otherwise, the first character of
 the representation of the unsigned magnitude will not be the
 zero character. The following characters are used as octal
 digits:
  01234567
 
 These are the characters '\u0030' through
 '\u0037'.

Parameters:

i -> an integer to be converted to a string.

*/
//must have a return of type String
	return java_lang_Integer_$_toString_$_$II$Ljava_lang_String$$$_(arg0, 8);
};
var java_lang_Integer_$_toString_$_$$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a String object representing this
 Integer's value. The value is converted to signed
 decimal representation and returned as a string, exactly as if
 the integer value were given as an argument to the toString(int) method.

*/
//must have a return of type String
	return java_lang_Integer_$_toString_$_$I$Ljava_lang_String$$$_(arg0.num);
};
var java_lang_Integer_$_toString_$_$I$Ljava_lang_String$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a String object representing the
 specified integer. The argument is converted to signed decimal
 representation and returned as a string, exactly as if the
 argument and radix 10 were given as arguments to the toString(int, int) method.

Parameters:

i -> an integer to be converted.

*/
//must have a return of type String
	return java_lang_Integer_$_toString_$_$II$Ljava_lang_String$$$_(arg0, 10);
};
var java_lang_Integer_$_toString_$_$II$Ljava_lang_String$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns a string representation of the first argument in the
 radix specified by the second argument.
 
 If the radix is smaller than Character.MIN_RADIX
 or larger than Character.MAX_RADIX, then the radix
 10 is used instead.
 
 If the first argument is negative, the first element of the
 result is the ASCII minus character '-'
 ('\u002D'). If the first argument is not
 negative, no sign character appears in the result.
 
 The remaining characters of the result represent the magnitude
 of the first argument. If the magnitude is zero, it is
 represented by a single zero character '0'
 ('\u0030'); otherwise, the first character of
 the representation of the magnitude will not be the zero
 character.  The following ASCII characters are used as digits: 
    0123456789abcdefghijklmnopqrstuvwxyz
 
 These are '\u0030' through
 '\u0039' and '\u0061' through
 '\u007A'. If radix is
 N, then the first N of these characters
 are used as radix-N digits in the order shown. Thus,
 the digits for hexadecimal (radix 16) are
 0123456789abcdef. If uppercase letters are
 desired, the String.toUpperCase() method may
 be called on the result:
  Integer.toString(n, 16).toUpperCase()

Parameters:

i -> an integer to be converted to a string.

radix -> the radix to use in the string representation.

*/
//must have a return of type String
	return new java_lang_String(arg0.toString(arg1));
};
var java_lang_Integer_$_valueOf_$_$I$Ljava_lang_Integer$$$_ = function (arg0) {
/*implement method!*/
/*
Returns a Integer instance representing the specified
 int value.
 If a new Integer instance is not required, this method
 should generally be used in preference to the constructor
 Integer(int), as this method is likely to yield
 significantly better space and time performance by caching
 frequently requested values.

Parameters:

i -> an int value.

*/
//must have a return of type Integer
	var i = new java_lang_Integer();
	java_lang_Integer_$__$$$init$$$__$_$I$V(i, arg0);
	return i;
};
var java_lang_Integer_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Integer$$$_ = function (arg0) {
/*implement method!*/
/*
Returns an Integer object holding the
 value of the specified String. The argument is
 interpreted as representing a signed decimal integer, exactly
 as if the argument were given to the parseInt(java.lang.String) method. The result is an
 Integer object that represents the integer value
 specified by the string.
 
 In other words, this method returns an Integer
 object equal to the value of:

 
 new Integer(Integer.parseInt(s))

Parameters:

s -> the string to be parsed.

*/
	if (arg0 === null) {
		var ex = new java_lang_NumberFormatException();
		ex._$$$init$$$__$_$Ljava_lang_String$$$_$V(ex, new java_lang_String("str cannot be null"));
		throw ex;
	} else {
		return java_lang_Integer_$_valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Integer$$$_(arg0, 10);
	}
//must have a return of type Integer
};
var java_lang_Integer_$_valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Integer$$$_ = function (arg0, arg1) {
/*implement method!*/
/*
Returns an Integer object holding the value
 extracted from the specified String when parsed
 with the radix given by the second argument. The first argument
 is interpreted as representing a signed integer in the radix
 specified by the second argument, exactly as if the arguments
 were given to the parseInt(java.lang.String, int)
 method. The result is an Integer object that
 represents the integer value specified by the string.
 
 In other words, this method returns an Integer
 object equal to the value of:

 
 new Integer(Integer.parseInt(s, radix))

Parameters:

s -> the string to be parsed.

radix -> the radix to be used in interpreting s

*/
//must have a return of type Integer
	var i = java_lang_Integer_$_parseInt_$_$Ljava_lang_String$$$_I$I(arg0, arg1);
	return java_lang_Integer_$_valueOf_$_$I$Ljava_lang_Integer$$$_(i);
};
var java_lang_Integer_$_wait_$_$J$V = function (arg0, arg1) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$J$V(arg0, arg1);};
var java_lang_Integer_$_wait_$_$JI$V = function (arg0, arg1, arg2) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$JI$V(arg0, arg1, arg2);};
var java_lang_Integer_$_wait_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_wait_$_$$V(arg0);};
var java_lang_Integer_$_getClass_$_$$Ljava_lang_Class$$$_ = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_getClass_$_$$Ljava_lang_Class$$$_(arg0);};
var java_lang_Integer_$_notify_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notify_$_$$V(arg0);};
var java_lang_Integer_$_notifyAll_$_$$V = function (arg0) {
/*invoke parent*/
return java_lang_Number_$_notifyAll_$_$$V(arg0);};
java_lang_Integer.prototype._$$$init$$$__$_$I$V = java_lang_Integer_$__$$$init$$$__$_$I$V;
java_lang_Integer.prototype._$$$init$$$__$_$Ljava_lang_String$$$_$V = java_lang_Integer_$__$$$init$$$__$_$Ljava_lang_String$$$_$V;
java_lang_Integer.access$000_$_$$Ljava_lang_String$$$_ = java_lang_Integer_$_access$000_$_$$Ljava_lang_String$$$_;
java_lang_Integer.bitCount_$_$I$I = java_lang_Integer_$_bitCount_$_$I$I;
java_lang_Integer.prototype.byteValue_$_$$B = java_lang_Integer_$_byteValue_$_$$B;
java_lang_Integer.prototype.compareTo_$_$Ljava_lang_Integer$$$_$I = java_lang_Integer_$_compareTo_$_$Ljava_lang_Integer$$$_$I;
java_lang_Integer.prototype.compareTo_$_$Ljava_lang_Object$$$_$I = java_lang_Integer_$_compareTo_$_$Ljava_lang_Object$$$_$I;
java_lang_Integer.decode_$_$Ljava_lang_String$$$_$Ljava_lang_Integer$$$_ = java_lang_Integer_$_decode_$_$Ljava_lang_String$$$_$Ljava_lang_Integer$$$_;
java_lang_Integer.prototype.doubleValue_$_$$D = java_lang_Integer_$_doubleValue_$_$$D;
java_lang_Integer.prototype.equals_$_$Ljava_lang_Object$$$_$Z = java_lang_Integer_$_equals_$_$Ljava_lang_Object$$$_$Z;
java_lang_Integer.prototype.floatValue_$_$$F = java_lang_Integer_$_floatValue_$_$$F;
java_lang_Integer.getAndRemoveCacheProperties_$_$$V = java_lang_Integer_$_getAndRemoveCacheProperties_$_$$V;
java_lang_Integer.getChars_$_$II_$$$$$_C$V = java_lang_Integer_$_getChars_$_$II_$$$$$_C$V;
java_lang_Integer.getInteger_$_$Ljava_lang_String$$$_$Ljava_lang_Integer$$$_ = java_lang_Integer_$_getInteger_$_$Ljava_lang_String$$$_$Ljava_lang_Integer$$$_;
java_lang_Integer.getInteger_$_$Ljava_lang_String$$$_I$Ljava_lang_Integer$$$_ = java_lang_Integer_$_getInteger_$_$Ljava_lang_String$$$_I$Ljava_lang_Integer$$$_;
java_lang_Integer.getInteger_$_$Ljava_lang_String$$$_Ljava_lang_Integer$$$_$Ljava_lang_Integer$$$_ = java_lang_Integer_$_getInteger_$_$Ljava_lang_String$$$_Ljava_lang_Integer$$$_$Ljava_lang_Integer$$$_;
java_lang_Integer.prototype.hashCode_$_$$I = java_lang_Integer_$_hashCode_$_$$I;
java_lang_Integer.highestOneBit_$_$I$I = java_lang_Integer_$_highestOneBit_$_$I$I;
java_lang_Integer.prototype.intValue_$_$$I = java_lang_Integer_$_intValue_$_$$I;
java_lang_Integer.prototype.longValue_$_$$J = java_lang_Integer_$_longValue_$_$$J;
java_lang_Integer.lowestOneBit_$_$I$I = java_lang_Integer_$_lowestOneBit_$_$I$I;
java_lang_Integer.numberOfLeadingZeros_$_$I$I = java_lang_Integer_$_numberOfLeadingZeros_$_$I$I;
java_lang_Integer.numberOfTrailingZeros_$_$I$I = java_lang_Integer_$_numberOfTrailingZeros_$_$I$I;
java_lang_Integer.parseInt_$_$Ljava_lang_String$$$_$I = java_lang_Integer_$_parseInt_$_$Ljava_lang_String$$$_$I;
java_lang_Integer.parseInt_$_$Ljava_lang_String$$$_I$I = java_lang_Integer_$_parseInt_$_$Ljava_lang_String$$$_I$I;
java_lang_Integer.reverseBytes_$_$I$I = java_lang_Integer_$_reverseBytes_$_$I$I;
java_lang_Integer.reverse_$_$I$I = java_lang_Integer_$_reverse_$_$I$I;
java_lang_Integer.rotateLeft_$_$II$I = java_lang_Integer_$_rotateLeft_$_$II$I;
java_lang_Integer.rotateRight_$_$II$I = java_lang_Integer_$_rotateRight_$_$II$I;
java_lang_Integer.prototype.shortValue_$_$$S = java_lang_Integer_$_shortValue_$_$$S;
java_lang_Integer.signum_$_$I$I = java_lang_Integer_$_signum_$_$I$I;
java_lang_Integer.stringSize_$_$I$I = java_lang_Integer_$_stringSize_$_$I$I;
java_lang_Integer.toBinaryString_$_$I$Ljava_lang_String$$$_ = java_lang_Integer_$_toBinaryString_$_$I$Ljava_lang_String$$$_;
java_lang_Integer.toHexString_$_$I$Ljava_lang_String$$$_ = java_lang_Integer_$_toHexString_$_$I$Ljava_lang_String$$$_;
java_lang_Integer.toOctalString_$_$I$Ljava_lang_String$$$_ = java_lang_Integer_$_toOctalString_$_$I$Ljava_lang_String$$$_;
java_lang_Integer.prototype.toString_$_$$Ljava_lang_String$$$_ = java_lang_Integer_$_toString_$_$$Ljava_lang_String$$$_;
java_lang_Integer.toString_$_$I$Ljava_lang_String$$$_ = java_lang_Integer_$_toString_$_$I$Ljava_lang_String$$$_;
java_lang_Integer.toString_$_$II$Ljava_lang_String$$$_ = java_lang_Integer_$_toString_$_$II$Ljava_lang_String$$$_;
java_lang_Integer.valueOf_$_$I$Ljava_lang_Integer$$$_ = java_lang_Integer_$_valueOf_$_$I$Ljava_lang_Integer$$$_;
java_lang_Integer.valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Integer$$$_ = java_lang_Integer_$_valueOf_$_$Ljava_lang_String$$$_$Ljava_lang_Integer$$$_;
java_lang_Integer.valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Integer$$$_ = java_lang_Integer_$_valueOf_$_$Ljava_lang_String$$$_I$Ljava_lang_Integer$$$_;
java_lang_Integer.prototype.wait_$_$J$V = java_lang_Integer_$_wait_$_$J$V;
java_lang_Integer.prototype.wait_$_$JI$V = java_lang_Integer_$_wait_$_$JI$V;
java_lang_Integer.prototype.wait_$_$$V = java_lang_Integer_$_wait_$_$$V;
java_lang_Integer.prototype.getClass_$_$$Ljava_lang_Class$$$_ = java_lang_Integer_$_getClass_$_$$Ljava_lang_Class$$$_;
java_lang_Integer.prototype.notify_$_$$V = java_lang_Integer_$_notify_$_$$V;
java_lang_Integer.prototype.notifyAll_$_$$V = java_lang_Integer_$_notifyAll_$_$$V;
