var stack;
var arg1 = 100;
var arg2 = 10;

function MyStaticClass() {}
MyStaticClass.test_$_$II$V = function (a1, a2) {
	if (a1 != arg1) {
		throw new java.lang.IllegalArgumentException("arg1 is wrong");
	}
	if (a2 != arg2) {
		throw new java.lang.IllegalArgumentException("arg2 is wrong");
	}
};
MyStaticClass.test_$_$$V = function () {
};
MyStaticClass.test_$_$II$I = function (a1, a2) {
	if (a1 != arg1) {
		throw new java.lang.IllegalArgumentException("arg1 is wrong");
	}
	if (a2 != arg2) {
		throw new java.lang.IllegalArgumentException("arg2 is wrong");
	}

	return a1 / a2;
};

function invokeStaticV(callback) {
	stack = [];
	stack.push(25);
	stack.push(35);
	stack.push(arg1);
	stack.push(arg2);

	callback();

	if (stack.length != 2) {
		throw new java.lang.IllegalArgumentException("stack.length is invalid: " + stack.length);
	}
}

function invokeStaticI(callback) {
	stack = [];
	stack.push(25);
	stack.push(35);
	stack.push(arg1);
	stack.push(arg2);

	callback();

	var result = stack.pop();
	var expected = MyStaticClass.test_$_$II$I(arg1, arg2);
	if (result != expected) {
		println("result should be " + expected + " -> not " + result);
		throw new java.lang.IllegalArgumentException("result should be " + expected + " -> not " + result);
	}
	if (stack.length != 2) {
		throw new java.lang.IllegalArgumentException("stack.length is invalid: " + stack.length);
	}
}

function invokeStaticNoArgs(callback) {
	stack = [];
	stack.push(25);
	stack.push(35);

	callback();
	if (stack.length != 2) {
		throw new java.lang.IllegalArgumentException("stack.length is invalid: " + stack.length);
	}
}