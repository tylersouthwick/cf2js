var stack;
var arg1 = 100;
var arg2 = 10;

function MyBaseClass() {}
var MyBaseClass_$_test_$_$II$V = function (i, a1, a2) {
	if (a1 != arg1) {
		println("arg1: " + a1 + " != " + arg1);
		throw new java.lang.IllegalArgumentException("arg1 is wrong");
	}
	if (a2 != arg2) {
		println("arg2: " + a2 + " != " + arg2);
		throw new java.lang.IllegalArgumentException("arg2 is wrong");
	}
};
var MyBaseClass_$_test_$_$$V = function () {
};
var MyBaseClass_$_test_$_$II$I = function (i, a1, a2) {
	if (a1 != arg1) {
		println("arg1: " + a1 + " != " + arg1);
		throw new java.lang.IllegalArgumentException("arg1 is wrong");
	}
	if (a2 != arg2) {
		println("arg2: " + a2 + " != " + arg2);
		throw new java.lang.IllegalArgumentException("arg2 is wrong");
	}

	return a1 / a2;
};
MyBaseClass.prototype.test_$_$II$I = MyBaseClass_$_test_$_$II$I;
MyBaseClass.prototype.test_$_$II$V = MyBaseClass_$_test_$_$II$V;
MyBaseClass.prototype.test_$_$$V = MyBaseClass_$_test_$_$$V;

function DerivedClass() {}
var DerivedClass_$_test_$_$II$V = MyBaseClass_$_test_$_$II$V;
var DerivedClass_$_test_$_$II$I = function (i, a1, a2) {
	return MyBaseClass_$_test_$_$II$I(i, a1, a2) * 1000;
};
DerivedClass.prototype.test_$_$II$I = DerivedClass_$_test_$_$II$I;
DerivedClass.prototype.test_$_$II$V = DerivedClass_$_test_$_$II$V;
DerivedClass.prototype.test_$_$$V = MyBaseClass_$_test_$_$$V;


function invokeV(callback, type) {
	stack = [];
	stack.push(25);
	stack.push(35);
	var obj = new DerivedClass();
	stack.push(obj);
	stack.push(arg1);
	stack.push(arg2);

	callback();

	if (stack.length != 2) {
		throw new java.lang.IllegalArgumentException("stack.length is invalid: " + stack.length);
	}
}

function invokeI(callback, type) {
	stack = [];
	stack.push(25);
	stack.push(35);
	var obj = new DerivedClass();
	stack.push(obj);
	stack.push(arg1);
	stack.push(arg2);

	callback();

	var result = stack.pop();

	var expected;
	if (type == "S") {
		//special
		expected = MyBaseClass_$_test_$_$II$I(obj, arg1, arg2);
	} else {
		expected = obj.test_$_$II$I(obj, arg1, arg2);
	}

	if (result != expected) {
		println("result should be " + expected + " -> not " + result);
		throw new java.lang.IllegalArgumentException("result should be " + expected + " -> not " + result);
	}
	if (stack.length != 2) {
		throw new java.lang.IllegalArgumentException("stack.length is invalid: " + stack.length);
	}
}

function invokeNoArgs(callback, type) {
	stack = [];
	stack.push(25);
	stack.push(35);
	var obj = new DerivedClass();
	stack.push(obj);

	callback();

	if (stack.length != 2) {
		throw new java.lang.IllegalArgumentException("stack.length is invalid: " + stack.length);
	}
}