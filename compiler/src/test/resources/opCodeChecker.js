var stack = [];
var args = [];

var result = undefined;
var exception = undefined;

var expectedResult = undefined;
var expectedException = undefined;

function debug(message) {
	//println(message)
}

function invokeOpCode(callback) {
	try {
		debug("invoking op code");
		result = callback();
		debug("got result: " + result);
	} catch (e) {
		debug("got exception: " + e);
		exception = e;
	}
}

var javaUtils = (function () {

	function unwrapIfNeeded(result) {
		//if it is an array in string form eval it
		//if (isString(result))
		if (isString(result) && (result.indexOf('[') > -1 || result.indexOf('{') > -1)) {
			var realResult = eval("(" + result + ")");
			debug("result: " + result);
			debug("realResult: " + realResult.constructor);
			return realResult;
		} else {
			return result;
		}
	}

	function doCheck(actual, expected) {
		if (actual === expected) {
			return null;
		} else {
			return "Expected [" + expected + "] got [" + actual + "]";
		}
	}

	function findLength(list) {
		if (list.length !== undefined) return list.length;
		if (list.size) return list.size();
		throw new java.lang.IllegalArgumentException("invalid list: " + list);
	}

	function assertTrue(message, condition) {
		if (!condition) throw new java.lang.AssertionError(message);
	}

	function isString(obj) {
		return obj && ((obj.constructor && obj.constructor.toString().indexOf("String") > -1) ||
						(obj.getClass && obj.getClass().getName().equals("java.lang.String")));
	}

	function isArray(obj) {
		return obj && ((obj.constructor && obj.constructor.toString().indexOf("Array") > -1)
			|| (obj instanceof java.lang.List));
	}

	function showTypeOf(object) {
		if (object && object.getClass) {
			return object.getClass();
		} else {
			return typeof object;
		}
	}

	function assertEquals(expected, actual) {
		function checkEquality() {
			assertTrue("Must be equal [" + expected + "] != [" + actual + "]", expected == actual);
		}

		function getKeys(obj) {
			var key, keys = [];
			for (key in obj) {
				if (obj.hasOwnProperty(key)) {
					keys.push(key);
				}
			}
			return keys;
		}

		function checkObjectEquals() {
			//check that they have the same keys
			debug("check object equals");
			var expectedKeys = getKeys(expected);
			var actualKeys = getKeys(actual);

			debug("expectedKeys: " + expectedKeys + " -> " + showTypeOf(expected));
			debug("actualKeys: " + actualKeys + " -> " + showTypeOf(actual));

			assertEquals(findLength(expectedKeys), findLength(actualKeys));

			for (var key in expectedKeys) {
				assertEquals(unwrapIfNeeded(expected[key]), unwrapIfNeeded(actual[key]));
			}
		}

		debug("assertEqual: " + expected + " -> " + actual);
		debug("assertEqual: " + showTypeOf(expected) + " -> " + showTypeOf(actual));

		if ((isString(expected) && isString(actual)) || (isArray(expected) && isArray(actual))) {
			checkEquality();
		} else if (typeof expected === 'object' && typeof actual === 'object') {
			checkObjectEquals();
		} else {
			checkEquality();
		}
	}

	function getListItem(list, i) {
		var result = (function () {
			if (isArray(list)) return list[i];
			return list.get(i);
		})();
		debug("getting list item: " + i + " -> " + result);
		return unwrapIfNeeded(result);
	}

	function checkList(name, actual, expected) {
		debug("checking list: " + name);
		debug("actual: " + actual);
		debug("expected: " + expected);

		var actualLength = findLength(actual);
		var expectedLength = findLength(expected);
		assertTrue("actual and expected sizes differ. Found " + actualLength + " need " + expectedLength, actualLength === expectedLength);

		for (var i = 0; i < actualLength; i += 1) {
			assertEquals(getListItem(expected, i), getListItem(actual, i));
		}
	}

	return {
		checkResult:function () {
			assertEquals(expectedResult, result);
		},
		checkException:function () {
			return doCheck(exception, expectedException);
		},
		exception: function () {
			if (exception) {
				return exception.javaClassName;
			} else {
				return null;
			}
		},
		getRawStack:function () {
			return stack;
		},
		popStack:function () {
			return stack.pop();
		},
		verifyArguments: function (expectedArgs) {
			checkList("args", args, unwrapIfNeeded(expectedArgs));
		},
		verifyStack: function (expectedStack) {
			debug("verifying stack");
			checkList("stack", stack, unwrapIfNeeded(expectedStack));
		}
	}
})();
