function convertObject(obj) {
	function convertArray(array) {
		var i, jArr = java.lang.reflect.Array.newInstance(java.lang.Object, array.length);
		for (i = 0; i < array.length; i++) {
			jArr[i] = convertObject(array[i]);
		}
		return jArr;
	}

	function isArray(obj) {
		if (obj && obj.constructor) {
			return obj.constructor.toString().indexOf("Array") > -1 || obj.constructor === Array;
		} else if (typeof obj === "object" && obj.length !== undefined && obj.length !== null) {
			return true;
		} else {
			return false;
		}
	}

	function convertJSObject(obj) {
		var key, map;
		map = new java.util.HashMap();
		for (key in obj) {
			if (obj.hasOwnProperty(key)) {
				//println(key + " -> " + obj[key] + " [typeof=" + (typeof obj[key]) + "]");
				map.put(key, convertObject(obj[key]));
			}
		}
		return map;
	}

	//println("obj: " + obj);
	if (obj === null || obj === undefined) {
		return null;
	} else if (isArray(obj)) {
		//println("found array: " + obj);
		return convertArray(obj);
	} else if (typeof obj === "number") {
		return obj;
	} else if (typeof obj === "string") {
		return obj;
	} else if (typeof obj === "boolean") {
		return obj;
	} else if (obj.getClass && obj.getClass().getName().equals("java.lang.Double")) {
		return obj;
	} else if (typeof obj === "object") {
		//println("found object: " + obj);
		return convertJSObject(obj);
	} else {
		(function () {
			var message = "Unknown type: ";
			message += (typeof obj);
			if (obj.getClass && obj.getClass().getName) {
				message += " [" + obj.getClass().getName() + "]";
			}
			throw new java.lang.RuntimeException(message);
		})();
	}
}
