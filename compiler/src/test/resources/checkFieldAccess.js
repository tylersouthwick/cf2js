var stack;
function FieldAccessTest() {}

function invokeReferenceGetField(callback) {
	stack = [];
	var expectedValue = 5;
	var ref = new FieldAccessTest();
	ref._testField = expectedValue;

	stack.push(ref);

	callback();
	
	var value = stack.pop();
	if (value != expectedValue) {
		var message = "expected " + expectedValue + " got " + value;
		//println(message);
		throw new java.lang.IllegalStateException(message);
	}
}

function invokeReferencePutField(callback) {
	stack = [];
	var expectedValue = 25;
	var reference = new FieldAccessTest();
	stack.push(reference);
	stack.push(expectedValue);

	callback();

	if (stack.length) {
		throw new java.lang.IllegalStateException("invalid stack size");
	}
	
	if (reference._testField != expectedValue) {
		throw new java.lang.IllegalStateException("expected " + expectedValue + " got " + reference._testField);
	}
}

function invokeStaticGetField(callback) {
	stack = [];
	var expectedValue = 5;
	FieldAccessTest._testField = expectedValue;

	callback();

	var value = stack.pop();
	if (value != expectedValue) {
		var message = "expected " + expectedValue + " got " + value;
		//println(message);
		throw new java.lang.IllegalStateException(message);
	}
}

function invokeStaticPutField(callback) {
	stack = [];
	var expectedValue = 25;
	stack.push(expectedValue);

	callback();

	if (stack.length) {
		throw new java.lang.IllegalStateException("invalid stack size");
	}

	if (FieldAccessTest._testField != expectedValue) {
		throw new java.lang.IllegalStateException("expected " + expectedValue + " got " + FieldAccessTest._testField);
	}
}