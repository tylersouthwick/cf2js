function java_lang_NegativeArraySizeException() {}
var stack = [];
var exception = null;
function checkArray(arrayCallback, count) {
	stack.push(count);
	try {
		arrayCallback();
		var array = stack.pop();
		if (array.length != count) {
			exception = "length not equal: " + array.length + " != " + count;
		}
	} catch (e) {
		exception = e.constructor.name;
	}
}