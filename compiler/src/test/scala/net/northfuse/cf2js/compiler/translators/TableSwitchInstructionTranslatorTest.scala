package net.northfuse.cf2js.compiler.translators

import org.junit.{Assert, Test}


/**
 * @author Tyler Southwick
 */
class TableSwitchInstructionTranslatorTest {

	@Test
	def checkArrayCreation() {
		val array = Array("one", "two", "three")
		Assert.assertEquals("[one, two, three]", TableSwitchInstructionTranslator.buildArray(array))
	}
}
