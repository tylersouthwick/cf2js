package net.northfuse.cf2js.compiler.translators

import org.junit.{Assert, Test}


/**
 * @author Tyler Southwick
 */
class VarVariableLoaderUnitTest {

	@Test
	def checkVarPrefixWithStore() {
		val stored1 = TestVarVariableLoader.store(1)
		Assert.assertTrue(stored1.startsWith("var "))
		val stored2 = TestVarVariableLoader.store(1)
		Assert.assertFalse(stored2.startsWith("var "))

		TestVarVariableLoader.clearVariables()

		val stored3 = TestVarVariableLoader.store(1)
		Assert.assertTrue(stored3.startsWith("var"))
	}

	@Test
	def checkVarPrefixWithStoreArgument() {
		val stored1 = TestVarVariableLoader.storeArgument(1, 1)
		Assert.assertTrue(stored1.startsWith("var "))
		val stored2 = TestVarVariableLoader.storeArgument(1, 1)
		Assert.assertFalse(stored2.startsWith("var "))

		TestVarVariableLoader.clearVariables()

		val stored3 = TestVarVariableLoader.storeArgument(1, 1)
		Assert.assertTrue(stored3.startsWith("var"))
	}

	object TestVarVariableLoader extends VarVariableLoader with TranslatorOperandStackArray
}