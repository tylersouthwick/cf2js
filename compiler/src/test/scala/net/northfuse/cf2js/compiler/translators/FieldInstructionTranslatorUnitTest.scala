package net.northfuse.cf2js.compiler.translators

import org.junit.Test
import org.objectweb.asm.Opcodes
import net.northfuse.cf2js.compiler.JavascriptUtils
import java.io.PrintWriter

/**
 * @author Tyler Southwick
 */
class FieldInstructionTranslatorUnitTest {

	import TestFieldInstructionTranslator._

	@Test
	def getfield() {
		invokeFieldTest(opcode = Opcodes.GETFIELD, functionName = "invokeReferenceGetField")
	}

	@Test
	def putfield() {
		invokeFieldTest(opcode = Opcodes.PUTFIELD, functionName = "invokeReferencePutField")
	}

	@Test
	def getstatic() {
		invokeFieldTest(opcode = Opcodes.GETSTATIC, functionName = "invokeStaticGetField")
	}

	@Test
	def putstatic() {
		invokeFieldTest(opcode = Opcodes.PUTSTATIC, functionName = "invokeStaticPutField")
	}

	def invokeFieldTest(opcode : Int, functionName : String) {
		val translatedOpcode = TestFieldInstructionTranslator(opcode, TEST_CLASS_NAME, TEST_FIELD_NAME)
		val baos = JavascriptUtils.loadFile("checkFieldAccess.js")
		val writer = new PrintWriter(baos)

		writer.append(functionName + "(function () {" + translatedOpcode + "})")
		writer.close()

		val script = new String(baos.toByteArray)
		val engine = JavascriptUtils.loadScript(script)
	}
}

object TestFieldInstructionTranslator extends FieldInstructionTranslator with TranslatorOperandStackArray {
	val TEST_CLASS_NAME = "FieldAccessTest"
	val TEST_FIELD_NAME = "testField"
}