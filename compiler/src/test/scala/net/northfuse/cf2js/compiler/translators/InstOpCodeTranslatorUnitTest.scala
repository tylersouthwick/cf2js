package net.northfuse.cf2js.compiler.translators

import net.northfuse.cf2js.compiler.NameMangler
import org.junit.Assert._
import org.objectweb.asm.Opcodes

import Opcodes._
import org.junit.{Assert, Test}

class InstOpCodeTranslatorUnitTest {

	val LOG = org.slf4j.LoggerFactory.getLogger(classOf[InstOpCodeTranslatorUnitTest])

	import OpCodeTester.{convertNativeArray, checkOpCode}
	import Reference.NaN

	@Test
	def nop() {
		checkOpCodeWithoutException(Array(5, 2), NOP, Array(5, 2))
	}

	@Test
	def pushNull() {
		checkOpCodeWithoutException(Array(5, 2), ACONST_NULL, Array(5, 2, null))
	}

	@Test
	def iconstM1() {
		checkOpCodeWithoutException(Array(5, 2), ICONST_M1, Array(5, 2, -1))
	}

	@Test
	def iconst0() {
		checkOpCodeWithoutException(Array(5, 2), ICONST_0, Array(5, 2, 0))
	}

	@Test
	def iconst1() {
		checkOpCodeWithoutException(Array(5, 2), ICONST_1, Array(5, 2, 1))
	}

	@Test
	def iconst2() {
		checkOpCodeWithoutException(Array(5, 2), ICONST_2, Array(5, 2, 2))
	}

	@Test
	def iconst3() {
		checkOpCodeWithoutException(Array(5, 2), ICONST_3, Array(5, 2, 3))
	}

	@Test
	def iconst4() {
		checkOpCodeWithoutException(Array(5, 2), ICONST_4, Array(5, 2, 4))
	}

	@Test
	def iconst5() {
		checkOpCodeWithoutException(Array(5, 2), ICONST_5, Array(5, 2, 5))
	}

	@Test
	def lconst0() {
		checkOpCodeWithoutException(Array(5, 2), LCONST_0, Array(5, 2, 0L))
	}

	@Test
	def lconst1() {
		checkOpCodeWithoutException(Array(5, 2), LCONST_1, Array(5, 2, 1L))
	}

	@Test
	def fconst0() {
		checkOpCodeWithoutException(Array(5, 2), FCONST_0, Array(5, 2, 0.0f))
	}

	@Test
	def fconst1() {
		checkOpCodeWithoutException(Array(5, 2), FCONST_1, Array(5, 2, 1.0f))
	}

	@Test
	def fconst2() {
		checkOpCodeWithoutException(Array(5, 2), FCONST_2, Array(5, 2, 2.0f))
	}

	@Test
	def dconst0() {
		checkOpCodeWithoutException(Array(5, 2), DCONST_0, Array(5, 2, 0.0))
	}

	@Test
	def dconst1() {
		checkOpCodeWithoutException(Array(5, 2), DCONST_1, Array(5, 2, 1.0))
	}

	@Test
	def iaload_valid() {
		checkOpCodeWithoutException(Array("[5, 2]", 0), IALOAD, Array(5))
	}

	@Test(expected = classOf[NullPointerException])
	def iaload_null_array() {
		checkOpCodeWithException(Array(null, 0), IALOAD, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def iaload_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3), IALOAD, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def laload_valid() {
		checkOpCodeWithoutException(Array("[5, 2]", 0), LALOAD, Array(5L))
	}

	@Test(expected = classOf[NullPointerException])
	def laload_null_array() {
		checkOpCodeWithException(Array(null, 0), LALOAD, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def laload_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3), LALOAD, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def faload_valid() {
		checkOpCodeWithoutException(Array("[5, 2]", 0), FALOAD, Array(5f))
	}

	@Test(expected = classOf[NullPointerException])
	def faload_null_array() {
		checkOpCodeWithException(Array(null, 0), FALOAD, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def faload_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3), FALOAD, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def daload_valid() {
		checkOpCodeWithoutException(Array("[5, 2]", 0), DALOAD, Array(5))
	}

	@Test(expected = classOf[NullPointerException])
	def daload_null_array() {
		checkOpCodeWithException(Array(null, 0), DALOAD, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def daload_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3), DALOAD, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def aaload_valid() {
		checkOpCodeWithoutException(Array("[5, 2]", 0), AALOAD, Array(5))
	}

	@Test(expected = classOf[NullPointerException])
	def aaload_null_array() {
		checkOpCodeWithException(Array(null, 0), AALOAD, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def aaload_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3), AALOAD, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def baload_valid() {
		checkOpCodeWithoutException(Array("[true, false]", 0), BALOAD, Array(true))
	}

	@Test(expected = classOf[NullPointerException])
	def baload_null_array() {
		checkOpCodeWithException(Array(null, 0), BALOAD, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def baload_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3), BALOAD, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def caload_valid() {
		checkOpCodeWithoutException(Array("['d', 'e']", 0), CALOAD, Array('d'))
	}

	@Test(expected = classOf[NullPointerException])
	def caload_null_array() {
		checkOpCodeWithException(Array(null, 0), CALOAD, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def caload_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("['d', '2', 'e']", 3), CALOAD, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def saload_valid() {
		checkOpCodeWithoutException(Array("[5, 2]", 0), SALOAD, Array(5.asInstanceOf[Short]))
	}

	@Test(expected = classOf[NullPointerException])
	def saload_null_array() {
		checkOpCodeWithException(Array(null, 0), SALOAD, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def saload_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3), SALOAD, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def iastore_valid() {
		val reference = Reference("[5, 2]")
		//should set index 0 to 25
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = IASTORE
			override def stack = Seq(reference, 0, 25)

			override def checkReference(referenceName : String, value : Array[AnyRef]) {
				referenceName match {
					case reference.reference => {
						LOG.debug("found reference " + referenceName + " -> " + value)
						assertNotNull(value)
						assertEquals(2, value.length)
						assertEquals(25, value(0).asInstanceOf[Double].intValue())
						assertEquals(2, value(1).asInstanceOf[Double].intValue())
					}
				}
			}
		})
	}

	@Test(expected = classOf[NullPointerException])
	def iastore_null_array() {
		checkOpCodeWithException(Array(null, 0, 2), IASTORE, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def iastore_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3, 7), IASTORE, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def lastore_valid() {
		val reference = Reference("[5, 2]")
		//should set index 0 to 25
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = LASTORE
			override def stack = Seq(reference, 0, 25L)

			override def checkReference(referenceName : String, value : Array[AnyRef]) {
				referenceName match {
					case reference.reference => {
						LOG.debug("found reference " + referenceName + " -> " + value)
						assertNotNull(value)
						assertEquals(2, value.length)
						assertEquals(25, value(0).asInstanceOf[Double].longValue())
						assertEquals(2, value(1).asInstanceOf[Double].longValue())
					}
				}
			}
		})
	}

	@Test(expected = classOf[NullPointerException])
	def lastore_null_array() {
		checkOpCodeWithException(Array(null, 0, 3L), LASTORE, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def lastore_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3, 4L), LASTORE, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def fastore_valid() {
		val reference = Reference("[5.5, 2.2]")
		//should set index 0 to 25
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = FASTORE
			override def stack = Seq(reference, 0, 25.5f)

			override def checkReference(referenceName : String, value : Array[AnyRef]) {
				referenceName match {
					case reference.reference => {
						LOG.debug("found reference " + referenceName + " -> " + value)
						assertNotNull(value)
						assertEquals(2, value.length)
						assertEquals(25.5f, value(0).asInstanceOf[Double].floatValue(), 0.1)
						assertEquals(2.2f, value(1).asInstanceOf[Double].floatValue(), 0.1)
					}
				}
			}
		})
	}

	@Test(expected = classOf[NullPointerException])
	def fastore_null_array() {
		checkOpCodeWithException(Array(null, 0, 2f), FASTORE, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def fastore_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3, 3f), FASTORE, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def dastore_valid() {
		val reference = Reference("[5, 2.5]")
		//should set index 0 to 25
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = DASTORE
			override def stack = Seq(reference, 0, 25.5)

			override def checkReference(referenceName : String, value : Array[AnyRef]) {
				referenceName match {
					case reference.reference => {
						LOG.debug("found reference " + referenceName + " -> " + value)
						assertNotNull(value)
						assertEquals(2, value.length)
						assertEquals(25.5, value(0))
						assertEquals(2.5, value(1))
					}
				}
			}
		})
	}

	@Test(expected = classOf[NullPointerException])
	def dastore_null_array() {
		checkOpCodeWithException(Array(null, 0, 3.3), DASTORE, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def dastore_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3, 2.3), DASTORE, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def aastore_valid() {
		val oldReference = Reference("[[2], [3]]")
		val newReference = Reference("[25, 35]")
		//should set index 0 to 25
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = AASTORE
			override def stack = Seq(oldReference, 0, newReference)

			override def checkReference(referenceName : String, value : Array[AnyRef]) {
				referenceName match {
					case oldReference.reference => {
						LOG.debug("found reference " + referenceName + " -> " + value)
						assertNotNull(value)
						assertEquals(2, value.length)

						val elem1 = convertNativeArray(value(0))
						val elem2 = convertNativeArray(value(1))

						LOG.debug("0 -> " + new java.util.LinkedList[AnyRef](java.util.Arrays.asList[AnyRef](elem1)))
						LOG.debug("1 -> " + new java.util.LinkedList[AnyRef](java.util.Arrays.asList[AnyRef](elem2)))

						assertEquals(2, elem1.length)
						assertEquals(25, elem1(0).asInstanceOf[Double].intValue())
						assertEquals(35, elem1(1).asInstanceOf[Double].intValue())

						assertEquals(1, elem2.length)
						assertEquals(3, elem2(0).asInstanceOf[Double].intValue())
					}
					case newReference.reference => {
						//don't need to do anything here
					}
				}
			}
		})
	}

	@Test(expected = classOf[NullPointerException])
	def aastore_null_array() {
		checkOpCodeWithException(Array(null, 0, "[25]"), AASTORE, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def aastore_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3, "[25]"), AASTORE, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def bastore_valid() {
		val reference = Reference("[true, false]")
		//should set index 0 to 25
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = BASTORE
			override def stack = Seq(reference, 0, "false")

			override def checkReference(referenceName : String, value : Array[AnyRef]) {
				referenceName match {
					case reference.reference => {
						LOG.debug("found reference " + referenceName + " -> " + value)
						assertNotNull(value)
						assertEquals(2, value.length)
						assertEquals("false", value(0).toString)
						assertEquals("false", value(1).toString)
					}
				}
			}
		})
	}

	@Test(expected = classOf[NullPointerException])
	def bastore_null_array() {
		checkOpCodeWithException(Array(null, 0, "false"), BASTORE, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def bastore_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3, "true"), BASTORE, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def castore_valid() {
		val reference = Reference("['d', 'e']")
		//should set index 0 to 25
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = CASTORE
			override def stack = Seq(reference, 0, 'f')

			override def checkReference(referenceName : String, value : Array[AnyRef]) {
				referenceName match {
					case reference.reference => {
						LOG.debug("found reference " + referenceName + " -> " + value)
						assertNotNull(value)
						assertEquals(2, value.length)
						assertEquals("f", value(0))
						assertEquals("e", value(1))
					}
				}
			}
		})
	}

	@Test(expected = classOf[NullPointerException])
	def castore_null_array() {
		checkOpCodeWithException(Array(null, 0, 'c'), CASTORE, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def castore_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("['d', '2', 'e']", 3, 'c'), CASTORE, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def sastore_valid() {
		val reference = Reference("[7, 3]")
		//should set index 0 to 25
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = SASTORE
			override def stack = Seq(reference, 0, 12)

			override def checkReference(referenceName : String, value : Array[AnyRef]) {
				referenceName match {
					case reference.reference => {
						LOG.debug("found reference " + referenceName + " -> " + value)
						assertNotNull(value)
						assertEquals(2, value.length)
						assertEquals(12, value(0).asInstanceOf[Double].shortValue())
						assertEquals(3, value(1).asInstanceOf[Double].shortValue())
					}
				}
			}
		})
	}

	@Test(expected = classOf[NullPointerException])
	def sastore_null_array() {
		checkOpCodeWithException(Array(null, 0, 5), SASTORE, Array(), classOf[NullPointerException])
	}

	@Test(expected = classOf[ArrayIndexOutOfBoundsException])
	def sastore_null_indexOutOfBounds() {
		checkOpCodeWithException(Array("[5, 3, 2]", 3, 2), SASTORE, Array(), classOf[ArrayIndexOutOfBoundsException])
	}

	@Test
	def pop() {
		checkOpCodeWithoutException(Array(25, 37, "[5]"), POP, Array(25, 37))
	}

	@Test
	def pop2() {
		checkOpCodeWithoutException(Array(25, 37, 92345), POP2, Array(25, 37))
	}

	@Test
	def dup() {
		checkOpCodeWithoutException(Array(25, 37), DUP, Array(25, 37, 37))
	}

	@Test
	def dup_x1() {
		checkOpCodeWithoutException(Array(25, 37), DUP_X1, Array(37, 25, 37))
	}

	@Test
	def dup_x2() {
		checkOpCodeWithoutException(Array(25, 37, 127), DUP_X2, Array(127, 25, 37, 127))
	}

	@Test
	def dup2() {
		checkOpCodeWithoutException(Array(25, 37), DUP2, Array(25, 37, 37))
	}

	@Test
	def dup2_x1() {
		checkOpCodeWithoutException(Array(25, 37), DUP2_X1, Array(37, 25, 37))
	}

	@Test
	def dup2_x2() {
		checkOpCodeWithoutException(Array(25, 37, 127), DUP2_X2, Array(127, 25, 37, 127))
	}

	@Test
	def swap() {
		checkOpCodeWithoutException(Array(25, 37), SWAP, Array(37, 25))
	}

	@Test
	def iadd() {
		checkOpCodeWithoutException(Array(5, 2), IADD, Array(7))
	}

	@Test
	def ladd() {
		checkOpCodeWithoutException(Array(5L, 2L), LADD, Array(7L))
	}

	@Test
	def fadd() {
		checkOpCodeWithoutException(Array(5.0f, 2.0f), FADD, Array(7.0f))
	}

	@Test
	def dadd() {
		checkOpCodeWithoutException(Array(5.0, 2.0), DADD, Array(7.0))
	}

	@Test
	def isub() {
		checkOpCodeWithoutException(Array(5, 2), ISUB, Array(3))
	}

	@Test
	def lsub() {
		checkOpCodeWithoutException(Array(5L, 2L), LSUB, Array(3L))
	}

	@Test
	def fsub() {
		checkOpCodeWithoutException(Array(5.0f, 2.0f), FSUB, Array(3.0f))
	}

	@Test
	def dsub() {
		checkOpCodeWithoutException(Array(5.0, 2.0), DSUB, Array(3.0))
	}

	@Test
	def imul() {
		checkOpCodeWithoutException(Array(5, 2), IMUL, Array(10))
	}

	@Test
	def lmul() {
		checkOpCodeWithoutException(Array(5L, 2L), LMUL, Array(10L))
	}

	@Test
	def fmul() {
		checkOpCodeWithoutException(Array(5.0f, 2.0f), FMUL, Array(10.0f))
	}

	@Test
	def dmul() {
		checkOpCodeWithoutException(Array(5.0, 2.0), DMUL, Array(10.0))
	}

	@Test
	def idiv() {
		checkOpCodeWithoutException(Array(10, 2), IDIV, Array(5))
	}

	@Test
	def idiv_round_down() {
		checkOpCodeWithoutException(Array(3, 2), IDIV, Array(1))
	}

	@Test(expected = classOf[ArithmeticException])
	def idiv_div_by_zero() {
		checkOpCodeWithException(Array(3, 0), IDIV, Array(), classOf[ArithmeticException])
	}

	@Test
	def ldiv() {
		checkOpCodeWithoutException(Array(10L, 2L), LDIV, Array(5L))
	}

	@Test
	def ldiv_round_down() {
		checkOpCodeWithoutException(Array(3L, 2L), LDIV, Array(1L))
	}

	@Test(expected = classOf[ArithmeticException])
	def ldiv_div_by_zero() {
		checkOpCodeWithException(Array(3L, 0L), LDIV, Array(), classOf[ArithmeticException])
	}

	@Test
	def fdiv() {
		checkOpCodeWithoutException(Array(10.0f, 2.0f), FDIV, Array(5.0f))
	}

	@Test(expected = classOf[ArithmeticException])
	def fdiv_div_by_zero() {
		checkOpCodeWithException(Array(3f, 0f), FDIV, Array(), classOf[ArithmeticException])
	}

	@Test
	def ddiv() {
		checkOpCodeWithoutException(Array(10.0, 2.0), DDIV, Array(5.0))
	}

	@Test(expected = classOf[ArithmeticException])
	def ddiv_div_by_zero() {
		checkOpCodeWithException(Array(3.0, 0.0), DDIV, Array(), classOf[ArithmeticException])
	}

	@Test
	def irem() {
		checkOpCodeWithoutException(Array(10, 3), IREM, Array(1))
	}

	@Test(expected = classOf[ArithmeticException])
	def irem_div_by_zero() {
		checkOpCodeWithException(Array(3, 0), IREM, Array(), classOf[ArithmeticException])
	}

	@Test
	def lrem() {
		checkOpCodeWithoutException(Array(10L, 3L), LREM, Array(1L))
	}

	@Test(expected = classOf[ArithmeticException])
	def lrem_div_by_zero() {
		checkOpCodeWithException(Array(3L, 0L), LREM, Array(), classOf[ArithmeticException])
	}

	@Test
	def frem() {
		checkOpCodeWithoutException(Array(10.0f, 3.0f), FREM, Array(1.0f))
	}

	@Test(expected = classOf[ArithmeticException])
	def frem_div_by_zero() {
		checkOpCodeWithException(Array(3f, 0f), FREM, Array(), classOf[ArithmeticException])
	}

	@Test
	def drem() {
		checkOpCodeWithoutException(Array(10.0, 3.0), DREM, Array(1.0))
	}

	@Test(expected = classOf[ArithmeticException])
	def drem_div_by_zero() {
		checkOpCodeWithException(Array(3.0, 0.0), DREM, Array(), classOf[ArithmeticException])
	}

	@Test
	def ineg() {
		checkOpCodeWithoutException(Array(23), INEG, Array(-23))
	}

	@Test
	def lneg() {
		checkOpCodeWithoutException(Array(8L), LNEG, Array(-8L))
	}

	@Test
	def fneg() {
		checkOpCodeWithoutException(Array(2.0f), FNEG, Array(-2.0f))
	}

	@Test
	def dneg() {
		checkOpCodeWithoutException(Array(5.0), DNEG, Array(-5.0))
	}

	@Test
	def ishl() {
		checkOpCodeWithoutException(Array(555, 2), ISHL, Array(555 << 2))
	}

	@Test
	def lshl() {
		checkOpCodeWithoutException(Array(555L, 2L), LSHL, Array(555L << 2L))
	}

	@Test
	def ishr() {
		checkOpCodeWithoutException(Array(555, 2), ISHR, Array(555 >> 2))
	}

	@Test
	def lshr() {
		checkOpCodeWithoutException(Array(555L, 2L), LSHR, Array(555L >> 2L))
	}

	@Test
	def iushr() {
        checkOpCodeWithoutException(Array(555, 2), IUSHR, Array(555 >>> 2))
	}

	@Test
	def lushr() {
        checkOpCodeWithoutException(Array(555L, 2L), LUSHR, Array(555L >>> 2L))
	}

    implicit def performInt(x : Int) = new {
        def and(y : Int) = x & y
        def or(y : Int) = x | y
        def xor(y : Int) = x ^ y
    }

    implicit def performInt(x : Long) = new {
        def and(y : Long) = x & y
        def or(y : Long) = x | y
        def xor(y : Long) = x ^ y
    }

	@Test
	def iand() {
        checkOpCodeWithoutException(Array(555, 235), IAND, Array(555.and(235)))
	}

	@Test
	def land() {
        checkOpCodeWithoutException(Array(555L, 958L), LAND, Array(555L.and(958L)))
	}

	@Test
	def ior() {
        checkOpCodeWithoutException(Array(555, 235), IOR, Array(555.or(235)))
	}

	@Test
	def lor() {
        checkOpCodeWithoutException(Array(555L, 958L), LOR, Array(555L.or(958L)))
	}

	@Test
	def ixor() {
        checkOpCodeWithoutException(Array(555, 235), IXOR, Array(555.xor(235)))
	}

	@Test
	def lxor() {
        checkOpCodeWithoutException(Array(555L, 958L), LXOR, Array(555L.xor(958L)))
	}

	@Test
	def i2l() {
        checkOpCodeWithoutException(Array(235), I2L, Array(235L))
	}

	@Test
	def i2f() {
        checkOpCodeWithoutException(Array(235), I2F, Array(235.0f))
	}

	@Test
	def i2d() {
        checkOpCodeWithoutException(Array(235), I2D, Array(235.0))
	}

	@Test
	def i2b() {
        checkOpCodeWithoutException(Array(553), I2B, Array(41))
	}

	@Test
	def i2s() {
        checkOpCodeWithoutException(Array(5523453), I2S, Array(18429))
	}

	@Test
	def i2c() {
        checkOpCodeWithoutException(Array(5523453), I2C, Array(18429))
	}

	@Test
	def l2i() {
        checkOpCodeWithoutException(Array(235L), L2I, Array(235))
	}

	@Test
	def l2f() {
        checkOpCodeWithoutException(Array(235L), L2F, Array(235.0f))
	}

	@Test
	def l2d() {
        checkOpCodeWithoutException(Array(235L), L2D, Array(235.0))
	}

	@Test
	def f2i() {
        checkOpCodeWithoutException(Array(235.5f), F2I, Array(235))
	}

	@Test
	def f2l() {
        checkOpCodeWithoutException(Array(235.5f), F2L, Array(235L))
	}

	@Test
	def f2d() {
        checkOpCodeWithoutException(Array(235.5f), F2D, Array(235.5))
	}

	@Test
	def d2i() {
        checkOpCodeWithoutException(Array(235.5), D2I, Array(235))
	}

	@Test
	def d2l() {
        checkOpCodeWithoutException(Array(235.5), D2L, Array(235L))
	}

	@Test
	def d2f() {
        checkOpCodeWithoutException(Array(235.5), D2F, Array(235.5f))
	}

	@Test
	def lcmp_lt() {
        checkOpCodeWithoutException(Array(16L, 17L), LCMP, Array(-1))
	}

	@Test
	def lcmp_equal() {
        checkOpCodeWithoutException(Array(17L, 17L), LCMP, Array(0))
	}

	@Test
	def lcmp_gt() {
        checkOpCodeWithoutException(Array(16L, 15L), LCMP, Array(1))
	}

	@Test
	def fcmpg_lt() {
        checkOpCodeWithoutException(Array(16.5f, 17.5f), FCMPG, Array(-1))
	}

	@Test
	def fcmpg_equal() {
        checkOpCodeWithoutException(Array(17.5f, 17.5f), FCMPG, Array(0))
	}

	@Test
	def fcmpg_gt() {
        checkOpCodeWithoutException(Array(16.5f, 15.5f), FCMPG, Array(1))
	}

	@Test
	def fcmpg_nan() {
        checkOpCodeWithoutException(Array(NaN, 15.5f), FCMPG, Array(1))
        checkOpCodeWithoutException(Array(15.5f, NaN), FCMPG, Array(1))
	}

	@Test
	def fcmpl_lt() {
        checkOpCodeWithoutException(Array(16.5f, 17.5f), FCMPL, Array(-1))
	}

	@Test
	def fcmpl_equal() {
        checkOpCodeWithoutException(Array(17.5f, 17.5f), FCMPL, Array(0))
	}

	@Test
	def fcmpl_gt() {
        checkOpCodeWithoutException(Array(16.5f, 15.5f), FCMPL, Array(1))
	}

	@Test
	def fcmpl_nan() {
        checkOpCodeWithoutException(Array(NaN, 15.5f), FCMPL, Array(-1))
        checkOpCodeWithoutException(Array(15.5f, NaN), FCMPL, Array(-1))
	}

	@Test
	def dcmpg_lt() {
        checkOpCodeWithoutException(Array(16.5, 17.5), DCMPG, Array(-1))
	}

	@Test
	def dcmpg_equal() {
        checkOpCodeWithoutException(Array(17.5, 17.5), DCMPG, Array(0))
	}

	@Test
	def dcmpg_gt() {
        checkOpCodeWithoutException(Array(16.5, 15.5), DCMPG, Array(1))
	}

	@Test
	def dcmpg_nan() {
        checkOpCodeWithoutException(Array(NaN, 15.5), DCMPG, Array(1))
        checkOpCodeWithoutException(Array(15.5, NaN), DCMPG, Array(1))
	}

	@Test
	def dcmpl_lt() {
        checkOpCodeWithoutException(Array(16.5, 17.5), DCMPL, Array(-1))
	}

	@Test
	def dcmpl_equal() {
        checkOpCodeWithoutException(Array(17.5, 17.5), DCMPL, Array(0))
	}

	@Test
	def dcmpl_gt() {
        checkOpCodeWithoutException(Array(16.5, 15.5), DCMPL, Array(1))
	}

	@Test
	def dcmpl_nan() {
        checkOpCodeWithoutException(Array(NaN, 15.5), DCMPL, Array(-1))
        checkOpCodeWithoutException(Array(15.5, NaN), DCMPL, Array(-1))
	}

	@Test
	def ireturn() {
		checkOpCodeReturn(Array(25, 1234), IRETURN, Reference(1234))
	}

	@Test
	def lreturn() {
		checkOpCodeReturn(Array(25, 1234), LRETURN, Reference(1234))
	}

	@Test
	def freturn() {
		checkOpCodeReturn(Array(25, 1234.5f), FRETURN, Reference(1234.5f))
	}

	@Test
	def dreturn() {
		checkOpCodeReturn(Array(25, 1234.5), DRETURN, Reference(1234.5))
	}

	@Test
	def areturn() {
		checkOpCodeReturn(Array(25, "Hello World"), ARETURN, Reference("Hello World"))
	}

	@Test
	def `return`() {
		checkOpCodeReturn(Array(25, "Hello World"), RETURN, Reference.undefined)
	}

	@Test
	def arraylength() {
		checkOpCodeWithoutException(Array(7, "[23, 25]"), ARRAYLENGTH, Array(7, 2))
	}

	@Test(expected = classOf[NullPointerException])
	def arraylength_null() {
		checkOpCodeWithException(Array(NaN, null), ARRAYLENGTH, Array(-1), classOf[NullPointerException])
	}

	@Test(expected = classOf[UnsupportedOperationException])
	def athrow() {
		val exceptionReference = Reference("new " + NameMangler.mangleClassName(classOf[UnsupportedOperationException]) + "()")
		checkOpCodeWithException(Array(25, exceptionReference), ATHROW, Array(exceptionReference), classOf[UnsupportedOperationException])
	}

	@Test(expected = classOf[NullPointerException])
	def athrow_null() {
		checkOpCodeWithException(Array(NaN, null), ATHROW, Array(2), classOf[NullPointerException])
	}

	def checkOpCodeReturn(oldStack : Array[_ <: Any], opcode : Int, returnValue : Reference) {
		val data = new CheckSingleOpcodeOpCodeData {
			def opCode = opcode
			override def stack = oldStack
			override def expectedResult = returnValue
		}
		checkOpCode(data)
		Assert.assertTrue(data.invokeOpCode.contains("/*cleared variables*/"))
	}

	def checkOpCodeWithException(mystack: Array[_ <: Any], opcode: Int, myresult: Array[_ <: Any], myexception : Class[_ <: Throwable]) {
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = opcode
			override def stack = mystack
			override def result = myresult
			override def expectedException = Some(myexception)
		})
	}

	def checkOpCodeWithoutException(mystack: Array[_ <: Any], opcode: Int, myresult: Array[_ <: Any]) {
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = opcode
			override def stack = mystack
			override def result = myresult
		})
	}
}

object TestInstOpCodeTranslator extends InstOpCodeTranslator with TranslatorOperandStackArray with ArrayVariableLoader

trait CheckSingleOpcodeOpCodeData extends CheckOpCodeData {
	def opCode : Int
	final def invokeOpCode = TestInstOpCodeTranslator(opCode)
}

// vim: set ts=4 sw=4 et:
