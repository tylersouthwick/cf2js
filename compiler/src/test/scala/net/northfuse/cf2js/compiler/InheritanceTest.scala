package net.northfuse.cf2js.compiler

import org.junit.Test
import tests.SimpleExtensionClass

/**
 * @author Tyler Southwick
 */
class InheritanceTest {

	import SimpleCompilerTest._

	@Test
	def simpleExtensionTest() {
		runStaticMethod(classOf[SimpleExtensionClass], "findInt", 10.0)
	}
}