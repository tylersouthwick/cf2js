package net.northfuse.cf2js.compiler.translators

import org.junit.Test
import net.northfuse.cf2js.compiler.JavascriptUtils
import java.io.PrintWriter
import org.objectweb.asm.Opcodes._

/**
 * @author Tyler Southwick
 */
class MethodInstructionTranslatorUnitTest {

	@Test
	def checkInvokeStaticVoid() {
		invokeStatic("V", functionSuffix = "V")
	}

	@Test
	def checkInvokeStaticWithReturn() {
		invokeStatic("I", functionSuffix = "I")
	}
	@Test
	def checkInvokeStaticNoArgs() {
		invokeStatic("V", args = "", functionSuffix = "NoArgs")
	}

	@Test
	def checkInvokeSpecialVoid() {
		invokeSpecial("V", functionSuffix = "V")
	}
	
	@Test
	def checkInvokeSpecialNoArgs() {
		invokeSpecial("V", args = "", functionSuffix = "NoArgs")
	}

	@Test
	def checkInvokeSpecialWithReturn() {
		invokeSpecial("I", functionSuffix = "I")
	}

	@Test
	def checkInvokeVirtualVoid() {
		invokeVirtual("V", functionSuffix = "V")
	}

	@Test
	def checkInvokeVirtualWithReturn() {
		invokeVirtual("I", functionSuffix = "I")
	}

	@Test
	def checkInvokeVirtualNoArgs() {
		invokeVirtual("V", args = "", functionSuffix = "NoArgs")
	}

	@Test
	def checkInvokeInterfaceVoid() {
		invokeInterface("V", functionSuffix = "V")
	}

	@Test
	def checkInvokeInterfaceWithReturn() {
		invokeInterface("I", functionSuffix = "I")
	}

	@Test
	def checkInvokeInterfaceNoArgs() {
		invokeInterface(returnDesc = "V", args = "", functionSuffix = "NoArgs")
	}

	def invokeStatic(returnDesc: String, args : String = "II", functionSuffix : String) {
		invokeJavascript(INVOKESTATIC, "checkInvokeStatic.js", "test", "(" + args + ")" + returnDesc, "invokeStatic" + functionSuffix, "undefined")
	}

	def invokeSpecial(returnDesc: String, args : String = "II", functionSuffix : String) {
		invokeJavascript(INVOKESPECIAL, "checkMethodInvocationOnInstance.js", "test", "(" + args + ")" + returnDesc, "invoke" + functionSuffix, "\"S\"", owner = "MyBaseClass")
	}

	def invokeVirtual(returnDesc: String, args : String = "II", functionSuffix : String) {
		invokeJavascript(INVOKEVIRTUAL, "checkMethodInvocationOnInstance.js", "test", "(" + args + ")" + returnDesc, "invoke" + functionSuffix, "\"V\"")
	}

	def invokeInterface(returnDesc: String, args : String = "II", functionSuffix : String) {
		invokeJavascript(INVOKEINTERFACE, "checkMethodInvocationOnInstance.js", "test", "(" + args + ")" + returnDesc, "invoke" + functionSuffix, "\"I\"")
	}

	def invokeJavascript(opcode: Int, fileName: String, name: String, desc: String, functionName: String, `type`: String, owner: String = "MyStaticClass") {
		val translatedOpcode = TestMethodInstructionTranslator(opcode = opcode, name = name, owner = owner, desc = desc)
		val baos = JavascriptUtils.loadFile(fileName)
		val writer = new PrintWriter(baos)

		writer.append(functionName + "(function () {return " + translatedOpcode + "}, " + `type` + ")")
		writer.close()

		val script = new String(baos.toByteArray)
		val engine = JavascriptUtils.loadScript(script)
	}
}

object TestMethodInstructionTranslator extends MethodInstructionTranslator with TranslatorOperandStackArray with ArrayVariableLoader