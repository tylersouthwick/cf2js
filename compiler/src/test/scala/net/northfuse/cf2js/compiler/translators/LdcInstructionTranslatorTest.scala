package net.northfuse.cf2js.compiler.translators

import org.junit.{Assert, Test}


/**
 * @author Tyler Southwick
 */
class LdcInstructionTranslatorTest {

	@Test
	def validateStringWithNewLine() {
		val original = "\nException"
		val processed = LdcInstructionTranslator.processString(original)
		Assert.assertEquals("\"\\\nException\"", processed)
	}

	@Test
	def validateStringWithDoubleQuote() {
		val original = "test\""
		val processed = LdcInstructionTranslator.processString(original)
		Assert.assertEquals("\"test\\\"\"", processed)
	}
	
	@Test
	def convertEscapedDoubleQuotes() {
		val original = """\""""
		val processed = LdcInstructionTranslator.processString(original)
		Assert.assertEquals(""""\\\""""", processed)
	}
}
