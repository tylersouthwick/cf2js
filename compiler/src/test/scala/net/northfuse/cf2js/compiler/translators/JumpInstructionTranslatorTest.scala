package net.northfuse.cf2js.compiler.translators

import org.junit.Test
import net.northfuse.cf2js.compiler.JavascriptUtils
import java.io.PrintWriter
import org.objectweb.asm.Label

/**
 * @author tylers2
 */
class JumpInstructionTranslatorTest {

	@Test
	def ifeq_true() {
	}

	@Test
	def ifeq_fals() {
	}

	def invokeJumpTest(opcode : Int, label : Label) {
		val translatedOpcode = TestJumpInstructionTranslator(opcode, label)
		val baos = JavascriptUtils.loadFile("checkJumpInstructions.js")
		val writer = new PrintWriter(baos)

		val functionName = "invokeJumpTest"
		writer.append(functionName + "(function () {" + translatedOpcode + "})")
		writer.close()

		val script = new String(baos.toByteArray)
		val engine = JavascriptUtils.loadScript(script)
	}
}

object TestJumpInstructionTranslator extends JumpInstructionTranslator with TranslatorOperandStackArray
