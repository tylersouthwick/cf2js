package net.northfuse.cf2js.compiler.translators

import org.junit.Test
import org.objectweb.asm.Opcodes._

/**
 * @author Tyler Soutwhick
 */
class VarInstructionTranslatorUnitTest {

	@Test
	def iload() {
		checkVarLoadInstruction(ILOAD, 0, 25)
	}

	@Test
	def lload() {
		checkVarLoadInstruction(LLOAD, 0, 25)
	}

	@Test
	def fload() {
		checkVarLoadInstruction(FLOAD, 0, 25)
	}

	@Test
	def dload() {
		checkVarLoadInstruction(DLOAD, 0, 25)
	}

	@Test
	def aload() {
		checkVarLoadInstruction(ALOAD, 0, "{}")
	}


	@Test
	def istore() {
		checkVarStoreInstruction(ISTORE, 0, 25)
	}

	@Test
	def lstore() {
		checkVarStoreInstruction(LSTORE, 0, 25)
	}

	@Test
	def fstore() {
		checkVarStoreInstruction(FSTORE, 0, 25)
	}

	@Test
	def dstore() {
		checkVarStoreInstruction(DSTORE, 0, 25)
	}

	@Test
	def astore() {
		checkVarStoreInstruction(ASTORE, 0, "{}")
	}

	def checkVarLoadInstruction(opcode : Int,  index : Int, value : Any) {
		OpCodeTester.checkOpCode(new VarInstructionOpcodeData(opcode, index) {
			override def variables = Seq(value)
			override def result = Seq(value)
		})
	}

	def checkVarStoreInstruction(opcode : Int,  index : Int, value : Any) {
		OpCodeTester.checkOpCode(new VarInstructionOpcodeData(opcode, index) {
			override def stack  = Seq(value)
			override def endArguments = Seq(value)
		})
	}
}

object TestVarInstructionTranslator extends VarInstructionTranslator with TranslatorOperandStackArray with ArrayVariableLoader
abstract class VarInstructionOpcodeData(opcode : Int, index : Int) extends CheckOpCodeData {
	final def invokeOpCode = TestVarInstructionTranslator(opcode, index)
}
