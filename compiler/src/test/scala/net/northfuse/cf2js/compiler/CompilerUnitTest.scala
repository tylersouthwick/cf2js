package net.northfuse.cf2js.compiler

import org.junit.{Assert, Test, Ignore}

/**
 * @author Tyler Southwick
 */
@Ignore
class CompilerUnitTest {

	@Test
	def checkWhiteListIncludeJavaLangObject() {
		Assert.assertTrue(Compiler.validClass("java.lang.Object"))
	}
	
	@Test
	def checkWhiteListExcludeJavaSomething() {
		Assert.assertFalse(Compiler.validClass("java.Something"))
	}

	@Test
	def checkWhiteListExcludeSunTest() {
		Assert.assertFalse(Compiler.validClass("sun.Test"))
	}
	
}
