package net.northfuse.cf2js.compiler

import org.junit._
import java.io.ByteArrayOutputStream
import org.slf4j.LoggerFactory

/**
 * @author Tyler Southwick
 */
class SimpleCompilerTest {

	import SimpleCompilerTest._

	@Test
	def staticMethodReturnConstant10() {
		runStaticMethod(classOf[tests.SimpleTestReturn], "returnConstant10", 10.0)
	}

	@Test
	def staticMethodReturnAdd11() {
		runStaticMethod(classOf[tests.SimpleTestReturn], "returnAdd11", 11.0)
	}

	@Test
	def instanceMethodReturnConstant15() {
		runInstanceMethod(classOf[tests.SimpleTestReturn], "returnConstant15", 15.0)
	}

	@Test
	def staticMethodReturnStaticField5() {
		runStaticMethod(classOf[tests.SimpleFieldAccessTest], "staticMethodReturnStaticField5", 5.0)
	}

	@Test
	def instanceMethodReturnStaticField7() {
		runInstanceMethod(classOf[tests.SimpleFieldAccessTest], "instanceMethodReturnStaticField7", 7.0)
	}

	@Test
	def instanceMethodReturnInstanceField3() {
		runInstanceMethod(classOf[tests.SimpleFieldAccessTest], "instanceMethodReturnInstanceField3", 3.0)
	}

	@Test
	def instanceInvokeStaticMethod25() {
		runInstanceMethod(classOf[tests.SimpleMethodInvocationTest], "instanceInvokeStaticMethod25", 25.0)
	}

	@Test
	def staticCreateInstanceInvokeStaticMethod25() {
		runStaticMethod(classOf[tests.SimpleMethodInvocationTest], "createInstanceAndInvoke", 25.0)
	}

}

object SimpleCompilerTest {

	private val LOG = LoggerFactory.getLogger(classOf[SimpleCompilerTest])

	def runStaticMethod(klass: Class[_], methodName: String, expectedValue: Any) {
		val baos = new ByteArrayOutputStream
		Compiler.compile(klass.getName, methodName, new java.io.PrintWriter(baos))
		val script = new StringBuilder(new String(baos.toByteArray))
		script.append("function invokeStaticMethod() { return ")
		script.append(NameMangler.mangleClassName(klass))
		script.append(".")
		script.append(NameMangler.mangleMethodName(name = methodName, desc = "()I"))
		script.append("(); }")
		val engine = JavascriptUtils.loadScript(script.toString())
		val result = engine.asInstanceOf[javax.script.Invocable].invokeFunction("invokeStaticMethod")
		LOG.debug("result : " + result)
		Assert.assertEquals(expectedValue, result)
	}

	def runInstanceMethod(klass: Class[_], methodName: String, expectedValue: Any) {
		val baos = new ByteArrayOutputStream
		Compiler.compile(klass.getName, methodName, new java.io.PrintWriter(baos))
		val script = new StringBuilder(new String(baos.toByteArray))

		script.append("function invokeInstanceMethod() {var i = new ").append(NameMangler.mangleClassName(klass)).append("(); return i.").append(NameMangler.mangleMethodName(name = methodName, desc = "()I")).append("(i);}")
		val engine = JavascriptUtils.loadScript(script.toString())
		val result = engine.asInstanceOf[javax.script.Invocable].invokeFunction("invokeInstanceMethod")
		LOG.debug("result : " + result)
		Assert.assertEquals(expectedValue, result)
	}
}
