package net.northfuse.cf2js.compiler.translators

import net.northfuse.cf2js.compiler.{JavascriptUtils, NameMangler}

object OpCodeTester {
	val LOG = org.slf4j.LoggerFactory.getLogger("net.northfuse.cf2js.compiler.translators.OpCodeTester")

	def convertNativeArray(o: AnyRef) = convertNative(o).asInstanceOf[Array[AnyRef]]
	def convertNative(o: AnyRef) = JavascriptUtils.convertJavascriptType(o)

	def checkOpCodeReturn(oldStack: Array[_ <: Any], opcode: Int, returnValue: Reference) {
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = opcode

			override def stack = oldStack

			override def expectedResult = returnValue
		})
	}

	def checkOpCodeWithException(mystack: Array[_ <: Any], opcode: Int, myresult: Array[_ <: Any], myexception: Class[_ <: Throwable]) {
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = opcode

			override def stack = mystack

			override def result = myresult

			override def expectedException = Some(myexception)
		})
	}

	def checkOpCodeWithoutException(mystack: Array[_ <: Any], opcode: Int, myresult: Array[_ <: Any]) {
		checkOpCode(new CheckSingleOpcodeOpCodeData {
			def opCode = opcode

			override def stack = mystack

			override def result = myresult
		})
	}

	def checkOpCode(data: CheckOpCodeData) {
		import java.io._
		val baos = {
			val out = new ByteArrayOutputStream
			val in = classOf[Reference].getResourceAsStream("/opCodeChecker.js")
			org.apache.commons.io.IOUtils.copy(in, out)
			out
		}
		val writer = new PrintWriter(baos)

		def writeException(exception: Class[_ <: Throwable]) {
			val name = NameMangler.mangleClassName(exception)
			writer.println("function " + name + "() {}")
			writer.println(name + ".prototype.javaClassName = \"" + exception.getName + "\";")
		}

		for (exception <- data.exceptions) {
			writeException(exception)
		}

		def writeReference(r: Reference) = {
			writer.println("var " + r.reference + " = " + r + ";")
			r.reference
		}

		writeReference(data.expectedResult)

		writer.println("expectedResult = " + data.expectedResult.reference + ";")

		val (stack, references) = data.stack.foldLeft((Seq[Any](), Seq[Reference]())) {
			case ((newStack, newReferences), stackElement) => {
				stackElement match {
					case r: Reference => r
					case s: String => Reference(s)
					case i: Int => stackElement
					case l: Long => stackElement
					case d: Double => stackElement
					case f: Float => stackElement
					case c: Char => "'" + stackElement + "'"
					case b: Byte => stackElement
					case b: Boolean => stackElement
					case null => Reference(null)
					case _ => throw new RuntimeException("Invalid stack element: " + stackElement)
				}
			} match {
				case r: Reference => (newStack :+ writeReference(r), r +: newReferences)
				case a: Any => (newStack :+ a, newReferences)
			}
		}

		for (stackValue <- stack) {
			writer.println("stack.push(" + stackValue + ");")
		}

		writer.println("var " + VariableLoader.LOCAL_VARIABLE_ARRAY_NAME + " = args;")
		for (zippedVariable <- data.variables.zipWithIndex) {
			val (variable, index) = zippedVariable
			writer.println(VariableLoader.LOCAL_VARIABLE_ARRAY_NAME + "[" + index + "] = " + variable)
		}

		writer.println("invokeOpCode(function () {")
		writer.println(data.invokeOpCode)
		writer.println("})")

		writer.close()
		baos.close()
		val script = new String(baos.toByteArray)

		//LOG.debug("invoking")
		val engine = JavascriptUtils.loadScript(script)

		import org.junit.Assert._

		trait JavaUtils {
			def popStack(): Any

			def exception: String

			def getRawStack: AnyRef

			def checkResult()

			def checkException(): String

			import java.util.{List => JList}
			def verifyStack(stack : JList[Any])

			def verifyArguments(args : JList[Any])
		}

		val javaUtils = engine.asInstanceOf[javax.script.Invocable].getInterface(engine.get("javaUtils"), classOf[JavaUtils])

		if (javaUtils.exception != null) {
			throw Class.forName(javaUtils.exception).newInstance().asInstanceOf[Throwable];
		}

		import scala.collection.JavaConversions._
		javaUtils.checkResult()
		javaUtils.verifyStack(data.result)
		javaUtils.verifyArguments(data.endArguments)

		//validate references
		for (reference <- references) {
			val result = engine.get(reference.reference)
			if (result == null) {
				data.checkNullReference(reference.reference)
			} else if (result.isInstanceOf[String]) {
				data.checkReference(reference.reference, result.asInstanceOf[String])
			} else if (result.isInstanceOf[Number]) {
				data.checkReference(reference.reference, result.asInstanceOf[Number])
			} else {
				convertNative(result) match {
					case map : java.util.Map[_, _] => data.checkReference(reference.reference, map)
					case list : Array[AnyRef] => data.checkReference(reference.reference, list)
					case _ => throw new RuntimeException("Unable to verfiy reference [" + result + "] of type [" + result.getClass + "]")
				}
			}
		}

	}
}

trait CheckOpCodeData {
	def invokeOpCode: String

	def variables = Seq[Any]()

	def stack = Seq[Any]()

	def result = Seq[Any]()

	def endArguments = variables

	def expectedResult: Reference = Reference.undefined

	def expectedException: Option[Class[_ <: Throwable]] = None

	def exceptions: Seq[Class[_ <: Throwable]] = expectedException match {
		case Some(exception) => Seq(exception)
		case None => Seq()
	}

	import OpCodeTester.LOG

	def checkReference(reference: String, value: Array[AnyRef]) {
		LOG.debug("checkReference: " + reference)
	}

	def checkReference(reference: String, value: String) {
		LOG.debug("checkReference: " + reference)
	}

	def checkReference(reference: String, value: Number) {
		LOG.debug("checkReference: " + reference)
	}

	def checkNullReference(reference: String) {
		LOG.debug("checkNullReference: " + reference)
	}

	def checkReference(reference: String, map : java.util.Map[_, _]) {
		LOG.debug("checkMapReference: " + reference)
	}
}

class Reference(val value: String) {
	override def toString = value

	val reference = "reference_" + NameMangler.randomString
}

object Reference {
	val undefined = new Reference("undefined")
	val NaN = new Reference("Number.NaN")

	def apply(s: String) = {
		if (s == null) {
			new Reference("null")
		} else if (s.startsWith("[") || s.startsWith("{") || s.startsWith("new ")) {
			new Reference(s)
		} else {
			new Reference("\"" + s.replaceAllLiterally("\"", "\\\"") + "\"")
		}
	}

	def apply(i: Int) = new Reference(Integer.toString(i))

	def apply(f: Float) = new Reference(f.toString)

	def apply(d: Double) = new Reference(d.toString)
}

// vim: set ts=4 sw=4 et:
