package net.northfuse.cf2js.compiler

import org.junit.runners.BlockJUnit4ClassRunner
import org.junit.runners.model._
import collection.JavaConversions._
import java.io.ByteArrayOutputStream

object JavascriptUtils {

	import javax.script._
	import java.io._

	private val mgr = new ScriptEngineManager
	private val LOG = org.slf4j.LoggerFactory.getLogger("net.northfuse.cf2js.compiler.JavascriptUtils")

	/**
	 * Copy a String on the clipboard.
	 */
	def copyToClipboard(str: String) {
		LOG.info("Copying " + str + " to clipboard")
		import java.awt._

		import java.awt.datatransfer.StringSelection
		try {
			val stringSelection = new StringSelection(str)
			val clipboard = Toolkit.getDefaultToolkit.getSystemClipboard
			clipboard.setContents(stringSelection, null)
		} catch {
			case t: Throwable => {
				LOG.error("Unable to copy to clipboard: " + t.getMessage)
			}
		}
	}

	def loadFile(fileName : String) = {
		val out = new ByteArrayOutputStream
		val in = classOf[JavascriptTestRunner].getResourceAsStream("/" + fileName)
		org.apache.commons.io.IOUtils.copy(in, out)
		out
	}

	def convertJavascriptType(obj : AnyRef) = {
		val engine = mgr.getEngineByName("JavaScript")
		engine.eval(new InputStreamReader(classOf[JavascriptTestRunner].getResourceAsStream("/convertJavascriptType.js")))
		LOG.debug("Trying to convert: " + obj)
		engine.asInstanceOf[Invocable].invokeFunction("convertObject", obj)
	}

	def loadScript(script : String) : ScriptEngine = {
		val engine = mgr.getEngineByName("JavaScript")

		val file = if (LOG.isDebugEnabled) {
			val file = File.createTempFile("cf2js", ".js")
			val writer = new PrintWriter(new FileOutputStream(file))
			writer.write(script)
			writer.close()
			Some(file.getAbsolutePath)
		} else None

		def error(t : Throwable) {
			file match {
				case Some(fileName) => copyToClipboard(fileName)
				case None =>
			}
			//LOG.error(script)
			LOG.error("Error evaluating script", t)
			throw t
		}

		try {
			file match {
				case Some(fileName) => LOG.debug("Evaluation JS " + fileName)
				case None =>
			}
			engine.eval(script)
		} catch {
			case t: Throwable => error(t)
		}

		engine
	}

	def hasText(s : String) = s != null && !s.trim.isEmpty
}

// vim: set ts=4 sw=4 et:
