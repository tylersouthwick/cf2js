package net.northfuse.cf2js.compiler

import org.junit.runners.BlockJUnit4ClassRunner
import org.junit.runners.model._
import collection.JavaConversions._
import org.junit.ComparisonFailure

class JavascriptTestRunner(klass: Class[_]) extends BlockJUnit4ClassRunner(klass) {

	lazy val script = {
		import java.io._
		val baos = new ByteArrayOutputStream
		val writer = new PrintWriter(baos)
		Compiler.compile(klass.getName, "test", writer)
		writer.close()
		baos.close()
		new String(baos.toByteArray)
	}

	override lazy val computeTestMethods: java.util.List[FrameworkMethod] = {
		val defaultMethods = super.computeTestMethods()
		val scriptMethods = defaultMethods.map(method => new JavascriptFrameworkMethod(method.getMethod))

		defaultMethods ++ scriptMethods
	}

	override def methodInvoker(method: FrameworkMethod, test: Object) = method match {
		case jfm: JavascriptFrameworkMethod => jfm.buildInvoker(script)
		case fm: FrameworkMethod => super.methodInvoker(method, test)
	}

	override def testName(method: FrameworkMethod) = method match {
		case jfm: JavascriptFrameworkMethod => "JS-" + super.testName(method)
		case fm: FrameworkMethod => super.testName(method)
	}

}

import java.lang.reflect.Method

class JavascriptFrameworkMethod(method: Method) extends FrameworkMethod(method) {

	import JavascriptFrameworkMethod.LOG
	def buildInvoker(script: String) = new Statement {
		def evaluate() {
			val runner = buildRunner()
			runner.run()
			val e = runner.e;
			if (e != null) {
				//println("JS exception: " + e)
				val name = e.replaceAllLiterally("_", ".")
				val ex = {
					if (e == "Error") {
						classOf[Error]
					} else {
						Class.forName(name).asInstanceOf[Class[Throwable]]
					}
				}
				throw name match {
					case "org.junit.ComparisonFailure" => {
						val message = ""//runner.message TODO fix this
						val expected = runner.expected
						val actual = runner.actual
						new ComparisonFailure(message, expected, actual)
					}
					case _ => {
						val message = runner.message
						val constructor = try {
							val cons = ex.getConstructor(classOf[String])
							new {
								def create = cons.newInstance(message)
							}
						} catch {
							case _ => {
								try {
									val cons = ex.getConstructor(classOf[Object])
									new {
										def create = cons.newInstance(message)
									}
								} catch {
									case _ => {
										val cons = ex.getConstructor()
										new {
											def create = cons.newInstance()
										}
									}
								}
							}
						}
						constructor.create
					}
				}
			}
		}

		def buildRunner() = {
			LOG.trace("invoke JS method: " + method.getName)
			val testName = "mytest"
			val testClass = NameMangler.mangleClassName(method.getDeclaringClass.getName)
			val testMethod = NameMangler.mangleMethodName(new MethodDetails(method.getName, method))
			def buildStringFunction(functionName : String, methodName : String) = {
				"this." + functionName + " = function () {\n" +
					"if (typeof e." + functionName + " === 'string') return e." + functionName + " + \" -> line #\" + e.lineNumber;\n" +
					"var msg = e." + NameMangler.mangleMethodName (name = methodName, desc = "()Ljava/lang/String;") + "(e);\n" +
					"if (msg) return msg.str; else return null;\n" +
					"};\n"
			}
			val testInvoker = "var " + testName + " = {run : function () {" +
				"var test = new " + testClass + "();\n" +
				"try {\n" +
				"test." + testMethod + "(test);\n" +
				"this.e = function () {return null;};\n" +
				"} catch (e) {\n" +
				"this.e = function () {return e.constructor.name;};\n" +
				buildStringFunction ("message", "getMessage") +
				buildStringFunction ("expected", "getExpected") +
				buildStringFunction ("actual", "getActual") +
				"}\n" +
				"}}";
			val engine = JavascriptUtils.loadScript(script + "\n" + testInvoker)
			val test = engine.get(testName);
			engine.asInstanceOf[javax.script.Invocable].getInterface(test, classOf[JavascriptRunnable])
		}

		trait JavascriptRunnable extends Runnable {
			def e : String
			def message : String
			def expected : String
			def actual: String
		}
	}
}

object JavascriptFrameworkMethod {
	val LOG = org.slf4j.LoggerFactory.getLogger(classOf[JavascriptFrameworkMethod])
}

// vim: set ts=4 sw=4 et:
