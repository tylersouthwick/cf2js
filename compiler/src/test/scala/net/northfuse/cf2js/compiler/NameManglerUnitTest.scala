package net.northfuse.cf2js.compiler

import org.junit.{Assert, Test}

/**
 * @author Tyler Southwick
 */
class NameManglerUnitTest {

	@Test
	def checkNoArgs() {
		checkName(name = "toString", desc = "()V", owner = "java/lang/String", expected = "java_lang_String_$_toString_$_$$V")
	}

	@Test
	def checkObjectGetClass() {
		checkName(name = "getClass", desc = "()Ljava/lang/Class;", owner = "java/lang/Object", expected = "java_lang_Object_$_getClass_$_$$Ljava_lang_Class$$$_")
	}

	@Test
	def checkJavaLangClassGetEnumClass() {
		checkName(name = "getEnumConstants", desc = "()[Ljava/lang/Class;", owner = "java/lang/Object", expected = "java_lang_Object_$_getEnumConstants_$_$$_$$$$$_Ljava_lang_Class$$$_")
	}

	@Test
	def checkObjectHashCode() {
		checkName(name = "hashCode", desc = "()I", owner = "java/lang/Object", expected = "java_lang_Object_$_hashCode_$_$$I")
	}

	@Test
	def checkObjectWait() {
		checkName(name = "wait", desc = "(JI)I", owner = "java/lang/Object", expected = "java_lang_Object_$_wait_$_$JI$I")
	}


	@Test
	def checkNoArgsWithNoOwner() {
		checkName(name = "toString", desc = "()V", expected = "toString_$_$$V")
	}

	@Test
	def checkObjectGetClassWithNoOwner() {
		checkName(name = "getClass", desc = "()Ljava/lang/Class", expected = "getClass_$_$$Ljava_lang_Class")
	}

	@Test
	def checkObjectHashCodeWithNoOwner() {
		checkName(name = "hashCode", desc = "()I", expected = "hashCode_$_$$I")
	}

	@Test
	def checkObjectWaitWithNoOwner() {
		checkName(name = "wait", desc = "(JI)I", expected = "wait_$_$JI$I")
	}

	@Test
	def checkNameOfStaticConstructor() {
		checkName(name = "<clinit>", desc = "()V", expected = "_$$$clinit$$$__$_$$V")
	}

	@Test
	def checkDefaultMethodName() {
		Assert.assertEquals("test_$_$$V", NameMangler.mangleMethodName(name="test"))
	}

	@Test
	def checkConstructor() {
		Assert.assertEquals("_$$$init$$$__$_$$V", NameMangler.mangleMethodName(name = "<init>"))
	}

	@Test
	def checkStaticConstructor() {
		Assert.assertEquals("_$$$clinit$$$__$_$$V", NameMangler.mangleMethodName(name = "<clinit>"))
	}

	def checkName(owner : String = "", name : String, desc : String, expected : String) {
		Assert.assertEquals(expected, NameMangler.mangleMethodName(owner = owner, name = name, desc = desc))
	}
}