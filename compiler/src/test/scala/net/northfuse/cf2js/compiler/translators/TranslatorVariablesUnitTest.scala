package net.northfuse.cf2js.compiler.translators

import org.junit.{Before, Assert, Test}


/**
 * @author Tyler Southwick
 */
class TranslatorVariablesUnitTest {

	@Before
	def initializeStack() {
		TranslatorOperandStackVariables.clear()
	}

	@Test
	def checkPopAndPush() {
		val push1 = TranslatorOperandStackVariables.push()
		val pop1 = TranslatorOperandStackVariables.pop()

		Assert.assertEquals(push1, pop1);

		val push2 = TranslatorOperandStackVariables.push()
		val pop2 = TranslatorOperandStackVariables.pop()

		Assert.assertEquals(push2, pop2)

		TranslatorOperandStackVariables.clear()

		val push3 = TranslatorOperandStackVariables.push()

		Assert.assertEquals(push1, push3)
	}
}