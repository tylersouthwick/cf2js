package net.northfuse.cf2js.compiler.translators

import org.junit.Test
import org.objectweb.asm.Opcodes._
import OpCodeTester._
import net.northfuse.cf2js.compiler.JavascriptUtils
import java.io.PrintWriter

/**
 * @author Tyler Southwick
 */
class IntInstructionOpCodeTranslatorUnitTest {

	@Test
	def bipush() {
		checkOpCode(new CheckIntOpcodeData(BIPUSH, 25) {
			override def result = Array(35, 23, 25)

			override def stack = Array(35, 23)
		})
	}

	@Test
	def sipush() {
		checkOpCode(new CheckIntOpcodeData(SIPUSH, 25) {
			override def result = Array(35, 23, 25)

			override def stack = Array(35, 23)
		})
	}

	@Test
	def newarray_boolean() {
		checkNewArray(T_BOOLEAN, 5)
	}

	@Test(expected = classOf[NegativeArraySizeException])
	def newarray_boolean_invalid_size() {
		checkNewArray(T_BOOLEAN, -1)
	}

	@Test
	def newarray_char() {
		checkNewArray(T_CHAR, 5)
	}

	@Test(expected = classOf[NegativeArraySizeException])
	def newarray_char_invalid_size() {
		checkNewArray(T_CHAR, -1)
	}

	@Test
	def newarray_float() {
		checkNewArray(T_FLOAT, 5)
	}

	@Test(expected = classOf[NegativeArraySizeException])
	def newarray_float_invalid_size() {
		checkNewArray(T_FLOAT, -1)
	}

	@Test
	def newarray_double() {
		checkNewArray(T_DOUBLE, 5)
	}

	@Test(expected = classOf[NegativeArraySizeException])
	def newarray_double_invalid_size() {
		checkNewArray(T_DOUBLE, -1)
	}

	@Test
	def newarray_byte() {
		checkNewArray(T_BYTE, 5)
	}

	@Test(expected = classOf[NegativeArraySizeException])
	def newarray_byte_invalid_size() {
		checkNewArray(T_BYTE, -1)
	}

	@Test
	def newarray_short() {
		checkNewArray(T_SHORT, 5)
	}

	@Test(expected = classOf[NegativeArraySizeException])
	def newarray_short_invalid_size() {
		checkNewArray(T_SHORT, -1)
	}

	@Test
	def newarray_int() {
		checkNewArray(T_INT, 5)
	}

	@Test(expected = classOf[NegativeArraySizeException])
	def newarray_int_invalid_size() {
		checkNewArray(T_INT, -1)
	}

	@Test
	def newarray_long() {
		checkNewArray(T_LONG, 5)
	}

	@Test(expected = classOf[NegativeArraySizeException])
	def newarray_long_invalid_size() {
		checkNewArray(T_LONG, -1)
	}

	def checkNewArray(arrayType : Int, count : Int) {
		val translatedOpcode = TestIntInstOpCodeTranslator(opcode = NEWARRAY, operand = arrayType)
		ArrayChecker(translatedOpcode, count)
	}
}

object TestIntInstOpCodeTranslator extends IntInstOpCodeTranslator with TranslatorOperandStackArray
abstract class CheckIntOpcodeData(opcode: Int, operand: Int) extends CheckOpCodeData {
	final def invokeOpCode = TestIntInstOpCodeTranslator(opcode, operand)
}

object ArrayChecker {
	def apply(translatedOpcode : String, count : Int) {
		val baos = JavascriptUtils.loadFile("checkArray.js")
		val writer = new PrintWriter(baos)

		writer.append("checkArray(function () {return " + translatedOpcode + "}, " + count + ");")
		writer.close()

		val script = new String(baos.toByteArray)
		val engine = JavascriptUtils.loadScript(script)
		engine.get("exception") match {
			case s : String => {
				if (s.replaceAllLiterally("_", ".") == classOf[NegativeArraySizeException].getName) {
					throw new NegativeArraySizeException
				} else {
					throw new IllegalStateException("caught invalid exception: " + s)
				}
			}
			case null =>
		}
	}
}
