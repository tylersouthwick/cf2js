package net.northfuse.cf2js.compiler.translators

import org.junit.Test
import org.objectweb.asm.Opcodes._

/**
 * @author Tyler Southwick
 */
class TypeInstructionTranslatorUnitTest {

	@Test
	def createNewArray() {
		checkNewArray(NEWARRAY, 25)
	}

	@Test
	def createNewObjectArray() {
		checkNewArray(ANEWARRAY, 25)
	}

	def checkNewArray(opcode: Int, count : Int) {
		val translatedOpcode = TestTypeInstructionTranslator(opcode, "java/lang/String")
		ArrayChecker(translatedOpcode, count)
	}
}

object TestTypeInstructionTranslator extends TypeInstructionTranslator with TranslatorOperandStackArray

abstract class CheckTypeOpcodeData(opcode: Int, `type`: String) extends CheckOpCodeData {
	final def invokeOpCode = TestTypeInstructionTranslator(opcode, `type`)
}
