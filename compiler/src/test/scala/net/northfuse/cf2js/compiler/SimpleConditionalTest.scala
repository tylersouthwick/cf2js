package net.northfuse.cf2js.compiler

import org.junit.Test
import tests.SimpleConditionalTests

/**
 * @author tylers2
 */

class SimpleConditionalTest {

	@Test
	def checkIf() {
		doSimpleTest("checkIf", 20)
	}

	@Test
	def checkElse() {
		doSimpleTest("checkElse", 10)
	}

	@Test
	def checkComplicatedIf() {
		doSimpleTest("checkComplicatedIf", 25.0)
	}

	@Test
	def checkComplicatedElse() {
		doSimpleTest("checkComplicatedIf", 25.0)
	}

	def doSimpleTest(name : String, expected : Double) {
		SimpleCompilerTest.runStaticMethod(classOf[SimpleConditionalTests], name, expected)
	}
}
