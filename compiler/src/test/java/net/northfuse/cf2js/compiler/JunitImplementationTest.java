package net.northfuse.cf2js.compiler;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Tests to make sure the JS compilation of JUnit is successful.
 * This is important since lots of other tests rely on this.
 *
 * @author Tyler Southwick
 */
@RunWith(JavascriptTestRunner.class)
public class JunitImplementationTest {

	@Test(expected = AssertionError.class)
	public void assertTrue() {
		Assert.assertTrue(false);
	}
}
