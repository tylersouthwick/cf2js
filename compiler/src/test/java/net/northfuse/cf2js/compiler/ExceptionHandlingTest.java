package net.northfuse.cf2js.compiler;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Tyler Southwick
 */
@RunWith(JavascriptTestRunner.class)
public class ExceptionHandlingTest {

	@Test(expected = NullPointerException.class)
	public void checkExceptionThrowing() {
		Object o = null;
		o.toString();
	}

	@Test
	public void catchNullPointer() {
		String test = null;
		try {
			test.substring(0);
			throw new IllegalStateException("should be a npe");
		} catch (NullPointerException npe) {
			//yay!
		}
	}
	
	@Test
	public void implementsFinally() {
		String test = null;
		try {
			test.substring(0);
			throw new IllegalStateException("should be a npe");
		} catch (NullPointerException npe) {
			//yay!
		} finally {
			test = "hello";
		}
		
		if (!test.equals("hello")) {
			throw new IllegalStateException();
		}
	}
}
