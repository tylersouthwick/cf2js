package net.northfuse.cf2js.compiler;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Tyler Southwick
 */
@RunWith(JavascriptTestRunner.class)
public class ConditionalStatementTest {
	
	@Test(expected = RuntimeException.class)
	public void ifCheck() {
		if (true) {
			throw new RuntimeException();
		}
	}

	@Test
	public void ifCheckAddition() {
		int x = 8;
		if (x != 8) {
			throw new IllegalStateException();
		}
	}

	@Test(expected = IllegalStateException.class)
	public void checkIfBlock() {
		int x = 33;
		if (x != 35) {
			throw new IllegalStateException();
		}
	}
}
