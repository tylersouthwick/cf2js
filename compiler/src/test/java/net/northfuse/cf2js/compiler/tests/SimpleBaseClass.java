package net.northfuse.cf2js.compiler.tests;

/**
 * @author Tyler Southwick
 */
public class SimpleBaseClass {
	private int test;

	public SimpleBaseClass() {
		setTest(5);
	}

	public int getTest() {
		return test;
	}

	public void setTest(int test) {
		this.test = test;
	}
}
