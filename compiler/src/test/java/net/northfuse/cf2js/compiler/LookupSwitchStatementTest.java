package net.northfuse.cf2js.compiler;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Tyler Southwick
 */
@RunWith(JavascriptTestRunner.class)
public class LookupSwitchStatementTest {

	@Test(expected = NullPointerException.class)
	public void simpleLookupSwitch() {
		int x = 5;

		switch (x) {
			case 0: throw new IllegalArgumentException();
			case 3: throw new IllegalArgumentException();
			case 5: throw new NullPointerException();
			case 25: throw new IllegalArgumentException();
			default: throw new IllegalArgumentException();
		}
	}
}
