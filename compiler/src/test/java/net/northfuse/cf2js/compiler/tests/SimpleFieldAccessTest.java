package net.northfuse.cf2js.compiler.tests;

/**
 * @author Tyler Southwick
 */
public class SimpleFieldAccessTest {
	private static int myfield;
	private int instanceField;

	public static int staticMethodReturnStaticField5() {
		myfield = 5;
		return myfield;
	}

	public int instanceMethodReturnStaticField7() {
		myfield = 7;
		return myfield;
	}

	public int instanceMethodReturnInstanceField3() {
		instanceField = 3;
		return instanceField;
	}
}
