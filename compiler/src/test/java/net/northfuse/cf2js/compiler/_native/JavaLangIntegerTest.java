package net.northfuse.cf2js.compiler._native;

import net.northfuse.cf2js.compiler.JavascriptTestRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author tylers2
 */
@RunWith(JavascriptTestRunner.class)
public class JavaLangIntegerTest {

	@Test
	public void value() {
		byte b = 1;
		Assert.assertTrue("test1", b == Integer.valueOf(1).byteValue());
		Assert.assertTrue("test2", 7L == Integer.valueOf(7).longValue());
		Assert.assertTrue("test3", 13 == Integer.valueOf(13).intValue());
	}
	
	@Test
	public void equals() {
		Integer i1 = new Integer(5);
		Integer i2 = new Integer(15);
		Integer i3 = new Integer(5);

		Assert.assertEquals(i1, i3);
		Assert.assertFalse(i1.equals(i2));
		Assert.assertFalse(i1.equals(null));
	}
	
	@Test
	public void checkHashCode() {
		Integer i = new Integer(72);
		
		Assert.assertTrue(i.hashCode() == 72);
	}
	
	@Test
	public void checkToString() {
		Assert.assertEquals("72", Integer.toString(72));
		Assert.assertEquals("2", Integer.toString(2));
		Assert.assertEquals("10", Integer.toString(16, 16));
		Assert.assertEquals("a", Integer.toString(10, 16));
		Assert.assertEquals("10", Integer.toString(8, 8));
		Assert.assertEquals("10", Integer.toString(2, 2));
		Assert.assertEquals("11", Integer.toString(3, 2));

		Assert.assertEquals("a", Integer.toHexString(10));
		Assert.assertEquals("10", Integer.toOctalString(8));
		Assert.assertEquals("10", Integer.toBinaryString(2));
		Assert.assertEquals("11", Integer.toBinaryString(3));
		
		Integer i = new Integer(25);
		Assert.assertEquals("25", i.toString());
	}
	
	@Test
	public void valueOf() {
		Assert.assertEquals("test1", new Integer(7), Integer.valueOf(7));
		Assert.assertEquals("test2", new Integer(89), Integer.valueOf(89));
		Assert.assertEquals("test3", new Integer(7), Integer.valueOf("7"));
		Assert.assertEquals("test4", new Integer(89), Integer.valueOf("89"));
		Assert.assertEquals("test5", new Integer(16), Integer.valueOf("10", 16));

		try {
			Integer.valueOf(null);
			throw new AssertionError("test6");
		} catch (NumberFormatException nfe) {
			//good
		}

		try {
			Integer.valueOf(null, 17);
			throw new AssertionError("test7");
		} catch (NumberFormatException nfe) {
			//good
		}

		try {
			Integer.valueOf("lk;");
			throw new AssertionError("test8");
		} catch (NumberFormatException nfe) {
			//good
		}

		try {
			Integer.valueOf("';asdf", 17);
			throw new AssertionError("test9");
		} catch (NumberFormatException nfe) {
			//good
		}
	}

	@Test
	public void parse() {
		Assert.assertTrue("test1", 7 == Integer.parseInt("7"));
		Assert.assertTrue("test2", 89 == Integer.parseInt("89"));
		Assert.assertTrue("test3", 10 == Integer.parseInt("A", 16));

		try {
			Integer.parseInt(null);
			throw new AssertionError("test4");
		} catch (NumberFormatException nfe) {
			//good
		}

		try {
			Integer.parseInt(null, 17);
			throw new AssertionError("test5");
		} catch (NumberFormatException nfe) {
			//good
		}

		try {
			Integer.parseInt("lk;");
			throw new AssertionError("test6");
		} catch (NumberFormatException nfe) {
			//good
		}

		try {
			Integer.parseInt("';asdf", 17);
			throw new AssertionError("test7");
		} catch (NumberFormatException nfe) {
			//good
		}
		
		Assert.assertTrue(0 == Integer.parseInt("0", 10));
		Assert.assertTrue(473 == Integer.parseInt("473", 10));
		Assert.assertTrue(0 == Integer.parseInt("-0", 10));
		Assert.assertTrue(-255 == Integer.parseInt("-FF", 16));
		Assert.assertTrue(102  == Integer.parseInt("1100110", 2));
		Assert.assertTrue(2147483647  == Integer.parseInt("2147483647", 10));
		Assert.assertTrue(-2147483648  == Integer.parseInt("-2147483648", 10));

		/*
		try {
			Integer.parseInt("2147483648", 10);
			throw new AssertionError("test7");
		} catch (NumberFormatException nfe) {
			//good
		}
		*/
		try {
			Integer.parseInt("99", 8);
			throw new AssertionError("test7");
		} catch (NumberFormatException nfe) {
			//good
		}
		try {
			Integer.parseInt("Kona", 10);
			throw new AssertionError("test7");
		} catch (NumberFormatException nfe) {
			//good
		}
		Assert.assertTrue(411787 == Integer.parseInt("Kona", 27));
	}

	@Test
	public void compareTo() {
		compareTo(2, 2, 0);
		compareTo(1, 2, -1);
		compareTo(2, 1, 1);
		
		try {
			Integer i = new Integer(5);
			i.compareTo(null);
			throw new AssertionError("should be npe");
		} catch (NullPointerException npe) {
			//good
		}
	}
	
	private void compareTo(int x, int y, int r) {
		Integer i1 = Integer.valueOf(x);
		Integer i2 = Integer.valueOf(y);
		Assert.assertTrue(r == i1.compareTo(i2));
	}

}
