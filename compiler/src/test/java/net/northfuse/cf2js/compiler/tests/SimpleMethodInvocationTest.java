package net.northfuse.cf2js.compiler.tests;

/**
 * @author Tyler Southwick
 */
public class SimpleMethodInvocationTest {
	
	public int instanceInvokeStaticMethod25() {
		return staticMethod25();
	}
	
	private static int staticMethod25() {
		return 25;
	}

	public static int createInstanceAndInvoke() {
		SimpleMethodInvocationTest instance = new SimpleMethodInvocationTest();
		return instance.instanceInvokeStaticMethod25();
	}
}
