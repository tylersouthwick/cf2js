package net.northfuse.cf2js.compiler;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author tylers2
 */
@RunWith(JavascriptTestRunner.class)
public class MultiANewArrayTest {
	
	@Test
	public void createArray() {
		String[][] test = new String[3][5];

		Assert.assertNotNull("check not null", test);
		Assert.assertTrue("check length [length = " + test.length + "]", test.length == 3);
	}

	@Test
	public void useArray() {
		String[][] test = new String[3][5];

		setValue(test, 0, 0, "test00");
		setValue(test, 0, 1, "test01");
		setValue(test, 0, 2, "test02");
		setValue(test, 0, 3, "test03");
		setValue(test, 0, 4, "test04");

		setValue(test, 1, 0, "test10");
		setValue(test, 1, 1, "test11");
		setValue(test, 1, 2, "test12");
		setValue(test, 1, 3, "test13");
		setValue(test, 1, 4, "test14");

		setValue(test, 2, 0, "test20");
		setValue(test, 2, 1, "test21");
		setValue(test, 2, 2, "test22");
		setValue(test, 2, 3, "test23");
		setValue(test, 2, 4, "test24");
		
		//validate
		for (int x = 0; x < test.length; x++) {
			String[] temp = test[x];
			Assert.assertNotNull("notNull: " + x, temp);
			Assert.assertTrue("length: " + x, 5 == temp.length);
			for (int y = 0; y < temp.length; y++) {
				Assert.assertEquals("test" + x + y, temp[y]);
			}
		}
	}
	
	private void setValue(String[][] test, int x, int y, String value) {
		String coords = "(" + x + ", " + y + ")";
		Assert.assertNull("null test " + coords, test[x][y]);
		test[x][y] = value;
		Assert.assertEquals("equals test " + coords, test[x][y], value);
	}
}
