package net.northfuse.cf2js.compiler._native;

import net.northfuse.cf2js.compiler.JavascriptTestRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Tyler Southwick
 */
@RunWith(JavascriptTestRunner.class)
public class JavaLangObjectTest {
	
	@Test
	public void oneObjectIsEqualToItself() {
		Object o = new Object();
		Assert.assertEquals(o, o);
	}

	@Test(expected = AssertionError.class)
	public void twoObjectsAreNotEquals() {
		Object o1 = new Object();
		Object o2 = new Object();
		Assert.assertEquals(o1, o2);
	}
	
	@Test
	public void oneObjectHasSameHashCode() {
		Object o = new Object();
		int i1 = o.hashCode();
		int i2 = o.hashCode();
		Assert.assertTrue(i1 == i2);
	}
	
	@Test
	public void twoObjectsHaveDifferentHashCode() {
		Object o1 = new Object();
		Object o2 = new Object();
		Assert.assertFalse(o1.hashCode() == o2.hashCode());
	}
	
	@Test(expected = CloneNotSupportedException.class)
	public void cloneObject() throws CloneNotSupportedException {
		new Object() {
			@Override
			public Object clone() throws CloneNotSupportedException {
				return super.clone();
			}
		}.clone();
	}
	
	@Test
	public void objectToStringNotNull() {
		Assert.assertNotNull(new Object().toString());
	}
}
