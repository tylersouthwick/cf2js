package net.northfuse.cf2js.compiler;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author tylers2
 */
@RunWith(JavascriptTestRunner.class)
public class StringTest {
	
	@Test
	public void length() {
		Assert.assertTrue("hello world".length() == 11);
	}

	@Test
	public void isEmpty() {
		Assert.assertTrue("".isEmpty());
		Assert.assertFalse("hello world".isEmpty());
	}

	@Test
	public void charAt() {
		Assert.assertTrue('d' == "date".charAt(0));
		Assert.assertTrue('e' == "date".charAt(3));
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void charAtOutOfBounds_Lower() {
		"hello".charAt(-1);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void charAtOutOfBounds_Upper() {
		"hello".charAt(5);
	}

	//todo implement code points
	
	
	@Test
	public void checkEquals() {
		Assert.assertEquals("test1", "test1");
		Assert.assertFalse("test1".equals("test2"));
		Assert.assertFalse("test1".equals(null));
	}

	@Test
	public void checkEqualsIgnoreCase() {
		Assert.assertTrue("test1".equalsIgnoreCase("TeSt1"));
		Assert.assertFalse("test1".equalsIgnoreCase("Test2"));
		Assert.assertFalse("test1".equalsIgnoreCase(null));
	}
	
	@Test
	public void checkCompareTo() {
		Assert.assertTrue("test1", "test1".compareTo("test1") == 0);
		Assert.assertTrue("test2", "b".compareTo("a") > 0);
		Assert.assertTrue("test3", "a".compareTo("b") < 0);
		try {
			"a".compareTo(null);
			throw new AssertionError("test4");
		} catch (NullPointerException npe) {
			//good
		}
		Assert.assertTrue("test5", "test1".compareTo("Test1") > 0);
	}

	@Test
	public void checkCompareToIgnoreCase() {
		Assert.assertTrue("test1", "teSt1".compareToIgnoreCase("Test1") == 0);
		Assert.assertTrue("test2", "B".compareToIgnoreCase("a") > 0);
		Assert.assertTrue("test3", "A".compareToIgnoreCase("b") < 0);
		try {
			"a".compareToIgnoreCase(null);
			throw new AssertionError("test4");
		} catch (NullPointerException npe) {
			//good
		}
	}
	
	@Test
	public void startsWith() {
		Assert.assertTrue("hello".startsWith("he"));
		Assert.assertFalse("Hello".startsWith("he"));
		Assert.assertTrue("Hello".startsWith("el", 1));
		Assert.assertFalse("HHello".startsWith("el", 1));
		try {
			"hello".startsWith(null);
			throw new AssertionError("Expects NPE");
		} catch (NullPointerException npe) {
			//good
		}
		try {
			"hello".startsWith(null, 1);
			throw new AssertionError("Expects NPE");
		} catch (NullPointerException npe) {
			//good
		}
	}
	
	@Test
	public void endsWith() {
		Assert.assertTrue("hello".endsWith("lo"));
		Assert.assertFalse("hello".endsWith("lot"));
		try {
			"hello".endsWith(null);
			throw new AssertionError("Expects NPE");
		} catch (NullPointerException npe) {
			//good
		}
	}
	
	@Test
	public void checkHashCode() {
		Assert.assertTrue("test1", "".hashCode() == 0);
		String s = "hello";
		int hashCode = ('h' * 31 * 31 * 31 * 31) + ('e' * 31 * 31 * 31) + ('l' * 31 * 31) + ('l' * 31) + 'o';
		Assert.assertTrue("test2", s.hashCode() == hashCode);
	}
	
	@Test
	public void indexOf() {
		Assert.assertTrue("test0", "hello".indexOf('h') == 0);
		Assert.assertTrue("test1", "hello".indexOf('e') == 1);
		Assert.assertTrue("test2", "hello".indexOf('l') == 2);
		Assert.assertTrue("test3", "hello".indexOf('o') == 4);
		Assert.assertTrue("test4", "hello".indexOf('x') == -1);
		Assert.assertTrue("test5", "".indexOf('x') == -1);

		Assert.assertTrue("test6", "hello".indexOf('e', 1) == 1);
		Assert.assertTrue("test7", "hello".indexOf('l', 3) == 3);
		Assert.assertTrue("test8", "hello".indexOf('x', 1) == -1);
		Assert.assertTrue("test9", "".indexOf('x', 1) == -1);
		
		Assert.assertTrue("test10", "hello".indexOf("ello") == 1);
		Assert.assertTrue("test11", "hello".indexOf("x") == -1);

		Assert.assertTrue("test12", "hello".indexOf("l", 3) == 3);
		Assert.assertTrue("test13", "hello".indexOf("x", 5) == -1);
		Assert.assertTrue("test14", "hello".indexOf("x", 9) == -1);
		
		try {
			"hello".indexOf(null);
			throw new AssertionError("test15");
		} catch (NullPointerException npe) {
			//good
		}
		try {
			"hello".indexOf(null, 5);
			throw new AssertionError("test16");
		} catch (NullPointerException npe) {
			//good
		}
	}

	@Test
	public void lastIndexOf() {
		Assert.assertTrue("test1", "hello".lastIndexOf('h') == 0);
		Assert.assertTrue("test2", "hello".lastIndexOf('e') == 1);
		Assert.assertTrue("test3", "hello".lastIndexOf('l') == 3);
		Assert.assertTrue("test4", "hello".lastIndexOf('o') == 4);

		Assert.assertTrue("test5", "alalala".lastIndexOf('l', 5) == 5);
		Assert.assertTrue("test6", "alalala".lastIndexOf('l', 4) == 3);
		Assert.assertTrue("test7", "alalala".lastIndexOf('l', 3) == 3);

		Assert.assertTrue("test8", "hello".lastIndexOf("h") == 0);
		Assert.assertTrue("test9", "hello".lastIndexOf("e") == 1);
		Assert.assertTrue("test10", "hello".lastIndexOf("l") == 3);
		Assert.assertTrue("test11", "hello".lastIndexOf("o") == 4);
		try {
			"hello".lastIndexOf(null);
			throw new AssertionError("should be npe: test12");
		} catch (NullPointerException npe) {
			//good
		}

		Assert.assertTrue("test13", "alalala".lastIndexOf("l", 5) == 5);
		Assert.assertTrue("test14", "alalala".lastIndexOf("l", 4) == 3);
		Assert.assertTrue("test15", "alalala".lastIndexOf("l", 3) == 3);
		try {
			"hello".lastIndexOf(null, 5);
			throw new AssertionError("should be npe: test16");
		} catch (NullPointerException npe) {
			//good
		}
	}
	
	@Test
	public void substring() {
		Assert.assertEquals("happy", "unhappy".substring(2));
		Assert.assertEquals("bison", "Harbison".substring(3));
		Assert.assertEquals("", "emptiness".substring(9));
		Assert.assertEquals("", "test".substring(4));

		checkSubstring("test", -1);
		checkSubstring("test", 5);

		Assert.assertEquals("urge", "hamburger".substring(4, 8));
		Assert.assertEquals("mile", "smiles".substring(1, 5));
		Assert.assertEquals("est", "test".substring(1, 4));

		checkSubstring("test", -1, 2);
		checkSubstring("test", -1, 4);
		checkSubstring("test", 1, 5);
	}
	
	private void checkSubstring(String s, int beginIndex) {
		try {
			s.substring(beginIndex);
			throw new AssertionError("index " + beginIndex + " should be invalid for [" + s + "]");
		} catch (IndexOutOfBoundsException ioobe) {
			//good
		}
	}

	private void checkSubstring(String s, int beginIndex, int endIndex) {
		try {
			s.substring(beginIndex, endIndex);
			throw new AssertionError("index range (" + beginIndex + ", " + endIndex + ") should be invalid for [" + s + "]");
		} catch (IndexOutOfBoundsException ioobe) {
			//good
		}
	}
	
	@Test
	public void concat() {
		Assert.assertEquals("caress", "cares".concat("s"));
		Assert.assertEquals("together", "to".concat("get").concat("her"));
		try {
			"cares".concat(null);
			throw new AssertionError("test3");
		} catch (NullPointerException npe) {
			//good;
		}
	}
	
	@Test
	public void replace() {
		Assert.assertEquals("mosquito in your collar", "mesquite in your cellar".replace('e', 'o'));
		Assert.assertEquals("the way of bayonets", "the war of baronets".replace('r', 'y'));
		Assert.assertEquals("starring with a turtle tortoise", "sparring with a purple porpoise".replace('p', 't'));
		Assert.assertEquals("JonL", "JonL".replace('q', 'x'));
		Assert.assertEquals("JonL", "JonL".replace('j', 'x'));
	}
	
	@Test
	public void contains() {
		Assert.assertTrue("hello world".contains("ello"));
		Assert.assertFalse("hello world".contains("elLo"));
		try {
			"hello".contains(null);
			throw new AssertionError("should throw NPE");
		} catch (NullPointerException npe) {
			//good
		}
	}
	
	@Test
	public void toLowerCase() {
		Assert.assertEquals("test", "tESt".toLowerCase());
	}

	@Test
	public void toUpperCase() {
		Assert.assertEquals("TEST", "tESt".toUpperCase());
	}
	
	@Test
	public void trim() {
		Assert.assertEquals("t", "  t  ".trim());
		String s1 = "test";
		String s2 = s1.trim();
		Assert.assertSame(s1, s2);
	}

	@Test
	public void checkToString() {
		String s1 = "test";
		String s2 = s1.toString();
		Assert.assertSame(s1, s2);
	}
	
	@Test
	public void valueOf() {
		Object o = new Object();
		Assert.assertEquals(o.toString(), String.valueOf(o));
		Assert.assertEquals("true", String.valueOf(true));
		Assert.assertEquals("false", String.valueOf(false));
		Assert.assertEquals("c", String.valueOf('c'));
		Assert.assertEquals("25", String.valueOf(25));
		Assert.assertEquals("25", String.valueOf(25L));
	}
}
