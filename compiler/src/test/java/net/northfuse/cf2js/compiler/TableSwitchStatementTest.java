package net.northfuse.cf2js.compiler;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Tyler Southwick
 */
@RunWith(JavascriptTestRunner.class)
public class TableSwitchStatementTest {

	@Test(expected = NullPointerException.class)
	public void tableSwitch() {
		int x = 1;
		switch (x) {
			case 0: throw new IllegalStateException();
			case 1: throw new NullPointerException();
			case 2: throw new IllegalStateException();
		}
		throw new IllegalStateException();
	}

	@Test
	public void tableSwitchValue() {
		int x = 1;
		switch (x) {
			case 0: {
				x+= 25;
				break;
			}
			case 1: {
				x += 7;
				break;
			}
			case 2: {
				x += 25;
				break;
			}
		}
		if (x != 8) {
			throw new IllegalStateException();
		}
	}

	@Test
	public void tableSwitchValueNoBreaks() {
		int x = 1;
		switch (x) {
			case 0: {
				x+= 25;
			}
			case 1: {
				x += 7;
			}
			case 2: {
				x += 25;
			}
		}
		if (x != 33) {
			throw new IllegalStateException();
		}
	}

	@Test(expected = IllegalStateException.class)
	public void tableSwitchValueNoBreaksFailure() {
		int x = 1;
		switch (x) {
			case 0: {
				x+= 25;
			}
			case 1: {
				x += 7;
			}
			case 2: {
				x += 25;
			}
		}
		if (x != 35) {
			throw new IllegalStateException();
		}
	}
}
