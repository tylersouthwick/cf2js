package net.northfuse.cf2js.compiler.tests;

/**
 * @author Tyler Southwick
 */
public class SimpleExtensionClass extends SimpleBaseClass {
	public SimpleExtensionClass() {
		setTest(getTest() + 5);
	}

	public static int findInt() {
		return new SimpleExtensionClass().getTest();
	}
}
