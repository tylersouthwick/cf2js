package net.northfuse.cf2js.compiler.tests;

/**
 * @author tylers2
 */
public class SimpleConditionalTests {
	
	private static int doIfCheck(boolean actual) {
		if (actual) {
			return 20;
		} else {
			return 10;
		}
	}

	private static int doComplicatedIfCheck(boolean actual) {
		int value = 5;
		if (actual) {
			value += 20;
			return value;
		} else {
			value += 10;
		}
		value += 5;
		return value;
	}

	/**
	 * should return 20
	 * @return
	 */
	public static int checkIf() {
		return doIfCheck(true);
	}

	/**
	 * should return 10
	 * @return
	 */
	public static int checkElse() {
		return doIfCheck(false);
	}

	/**
	 * should return 25
	 * @return
	 */
	public static int checkComplicatedIf() {
		return doComplicatedIfCheck(true);
	}

	/**
	 * should return 20
	 * @return
	 */
	public static int checkComplicatedElse() {
		return doComplicatedIfCheck(false);
	}
}
