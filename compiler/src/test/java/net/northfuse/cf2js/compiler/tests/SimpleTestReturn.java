package net.northfuse.cf2js.compiler.tests;

/**
 * Has Simple Methods for Test Cases.
 *
 * @author Tyler Southwick
 */
public class SimpleTestReturn {

	public static int returnConstant10() {
		return 10;
	}

	public static int returnAdd11() {
		int x = 25;
		int y = 4;

		x = 7;
		return x + y;
	}

    public int returnConstant15() {
        return 15;
    }
}
