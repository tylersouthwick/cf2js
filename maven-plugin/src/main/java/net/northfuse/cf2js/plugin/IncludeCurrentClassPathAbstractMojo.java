package net.northfuse.cf2js.plugin;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.slf4j.impl.StaticLoggerBinder;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * @author tylers2
 */
abstract class IncludeCurrentClassPathAbstractMojo extends AbstractMojo {
	/**
	 * The maven project.
	 *
	 * @parameter expression="${project}"
	 * @readonly
	 * @required
	 */
	private MavenProject project;

	/**
	 * The plugin dependencies.
	 *
	 * @parameter expression="${plugin.artifacts}"
	 * @readonly
	 * @required
	 */
	private List<Artifact> pluginDependencies;

	private void initializeSlf4j(ClassLoader classLoader) throws MojoFailureException {
		getLog().debug("Initializing SLF4J");
		try {
			Class binder = classLoader.loadClass(StaticLoggerBinder.class.getName());
			Object instance = binder.getField("SINGLETON").get(null);
			Method method = binder.getMethod("setLog", Log.class);
			method.invoke(instance, getLog());
		} catch (Throwable t) {
			throw new MojoFailureException("Unable to initialize slf4j", t);
		}
		//StaticLoggerBinder.SINGLETON.setLog(getLog());
		getLog().debug("Initialized SLF4J");
	}

	protected final Class<?> loadClass(String className) throws MojoFailureException, MojoExecutionException {
		try {
			return getClassLoader().loadClass(className);
		} catch (ClassNotFoundException e) {
			throw new MojoExecutionException("Unable to load class: " + className, e);
		}
	}

	private ClassLoader getClassLoader() throws MojoFailureException {
		try {
			ClassLoader classLoader = createClassLoader();
			initializeSlf4j(classLoader);
			return classLoader;
		} catch (DependencyResolutionRequiredException e) {
			throw new MojoFailureException("Need dependencies", e);
		} catch (MalformedURLException e) {
			throw new MojoFailureException("Unable to create URL", e);
		}
	}

	private ClassLoader createClassLoader() throws DependencyResolutionRequiredException, MalformedURLException {
		List<URL> urls = new LinkedList<URL>();
		List<String> classpathElements = project.getCompileClasspathElements();
		if (classpathElements == null) {
			classpathElements = Collections.emptyList();
		}
		for (Object object : classpathElements) {
			String path = (String) object;
			getLog().info("Adding " + path + " to classpath");
			urls.add(new File(path).toURI().toURL());
		}

		/*
		Artifact executableArtifact = findExecutableArtifact();
		Artifact executablePomArtifact = getExecutablePomArtifact(executableArtifact);
		List<Artifact> pluginDependencies = resolveExecutableDependencies(executablePomArtifact);
		*/
		getLog().info("Adding " + pluginDependencies.size() + " plugin dependencies");
		for (Artifact artifact : pluginDependencies) {
			getLog().info("Adding plugin dependency artifact: " + artifact.getArtifactId() + " to classpath");
			urls.add(artifact.getFile().toURI().toURL());
		}
		return new ParentLastURLClassLoader(urls);
		//return new URLClassLoader(urls.toArray(new URL[urls.size()]), this.getClass().getClassLoader());
	}
}
