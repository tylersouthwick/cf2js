package net.northfuse.cf2js.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Tyler Southwick
 * @goal generate-stub
 * @requiresDependencyResolution compile
 */
public class NativeImplementationMojo extends IncludeCurrentClassPathAbstractMojo {

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		Class<?> klass = loadClass("net.northfuse.cf2js.compiler.NativeImplementation");
		try {
			Method method = klass.getMethod("generateStubInteraction");
			method.invoke(null);
		} catch (NoSuchMethodException e) {
			throw new MojoExecutionException("Unable to find interaction", e);
		} catch (InvocationTargetException e) {
			throw new MojoExecutionException("Unable to invoke interaction", e);
		} catch (IllegalAccessException e) {
			throw new MojoExecutionException("Unable to invoke interaction", e);
		}
	}
}
