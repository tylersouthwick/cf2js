package net.northfuse.cf2js.plugin;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tyler Southwick
 * @goal compile
 * @requiresDependencyResolution compile
 */
public class CompilerMojo extends IncludeCurrentClassPathAbstractMojo {

	/**
	 * @parameter expression="${outputLocation}"
	 */
	private String outputLocation;

	/**
	 * @parameter expression="${outputFile}"
	 */
	private String outputFile;

	/**
	 * The class with the main method
	 * @parameter expression="${className}"
	 */
	private String className;

	@Override
	@SuppressWarnings("unchecked")
	public void execute() throws MojoExecutionException, MojoFailureException {
		StringBuilder fileName = new StringBuilder();
		if (outputLocation == null || outputLocation.trim().isEmpty()) {
		} else {
			File dir = new File(outputLocation);
			dir.mkdirs();
			fileName.append(outputLocation).append(File.separator);
		}
		fileName.append(outputFile);
		File file = new File(fileName.toString());
		getLog().info("Compiling " + className + " -> " + fileName);
		Class<?> klass = loadClass("net.northfuse.cf2js.compiler.Compiler");
		try {
			Method method = klass.getMethod("compile", String.class, String.class, PrintWriter.class);
			PrintWriter printWriter = new PrintWriter(file);
			method.invoke(null, className, "main", printWriter);
		} catch (NoSuchMethodException e) {
			throw new MojoExecutionException("Unable to find compile method", e);
		} catch (InvocationTargetException e) {
			throw new MojoExecutionException("Unable to compile", e);
		} catch (FileNotFoundException e) {
			throw new MojoExecutionException("file not found", e);
		} catch (IllegalAccessException e) {
			throw new MojoExecutionException("Unable to compile", e);
		}
	}
}
