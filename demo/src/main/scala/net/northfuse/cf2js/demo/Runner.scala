package net.northfuse.cf2js.demo

import net.northfuse.cf2js.dom._

/**
 * @author tylers2
 */

object Runner {

	def main(args : Array[String]) {
		Console.debug("prompting for name")
		promptForName()
	}
	
	def promptForName() {
		Window.prompt("What is your name", "bob") match {
			case Some(name) => {
				Window.alert("You said that your name was: " + name)
			}
			case None => {
				val result = Window.confirm("You did not give a name. Do you want to give a name?")
				if (result == true) {
					promptForName()
				} else {
					Window.alert("You don't have a name!")
				}
			}
		}
	}
}
